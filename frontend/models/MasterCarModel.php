<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_car_model".
 *
 * @property string $make_cd
 * @property string $model_cd
 * @property string $model_name
 * @property float|null $order_seq 12.12.11 added
 * @property string|null $upd_user
 * @property string|null $upd_date
 * @property string|null $disp_flg 1:Show to system, 0:Disabled
 */
class MasterCarModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_car_model';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['make_cd', 'model_cd', 'model_name'], 'required'],
            [['order_seq'], 'number'],
            [['upd_date'], 'safe'],
            [['make_cd', 'model_cd', 'upd_user'], 'string', 'max' => 20],
            [['model_name'], 'string', 'max' => 255],
            [['disp_flg'], 'string', 'max' => 1],
            [['make_cd', 'model_cd'], 'unique', 'targetAttribute' => ['make_cd', 'model_cd']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'make_cd' => 'Make Cd',
            'model_cd' => 'Model Cd',
            'model_name' => 'Model Name',
            'order_seq' => 'Order Seq',
            'upd_user' => 'Upd User',
            'upd_date' => 'Upd Date',
            'disp_flg' => 'Disp Flg',
        ];
    }

    public function getmodelname($model_cd) {
        $response = false;

        $master_car_model_memcache_key = "/master_car_model/cd/" . $model_cd;
        $master_car_model_memcache = Yii::$app->cache->get($master_car_model_memcache_key);
        if ($master_car_model_memcache !== false) {
            $response = $master_car_model_memcache;
        } else {
            $master_car_model = $this->find()->where(['model_cd' => (string)$model_cd])->one();
            if ($master_car_model) {
                $response = $master_car_model->model_name;

                Yii::$app->cache->set($master_car_model_memcache_key, $response, 3600);
            }
        }
        
        return $response;
    }

    public function getmodelcd($model_name) {
        $response = false;

        $master_car_model_memcache_key = "/master_car_model/name/" . $model_name;
        $master_car_model_memcache = Yii::$app->cache->get($master_car_model_memcache_key);
        if ($master_car_model_memcache !== false) {
            $response = $master_car_model_memcache;
        } else {
            $master_car_model = $this->find()->where(['ilike', 'model_name', (string)$model_name])->orderBy('model_name')->one();
            if ($master_car_model) {
                $response = $master_car_model->model_cd;
                Yii::$app->cache->set($master_car_model_memcache_key, $response, 3600);
            }
        }

        return $response;
    }

    public function getmodelbymake($make_name) {
        $model_array = [];
        
        $master_car_model_memcache_key = "/master_car_model/bymakename/" . $make_name;
        $master_car_model_memcache = Yii::$app->cache->get($master_car_model_memcache_key);
        if ($master_car_model_memcache !== false) {
            $model_array = $master_car_model_memcache;
        } else {
            $master_car_model = $this->find()->where(['make_cd' => (string)$make_name, 'disp_flg' => '1'])->orderBy('model_name')->all();
            if (count($master_car_model)) {
                foreach ($master_car_model as $master_car_data_loop) {
                    $model_array[$master_car_data_loop->model_cd] = $master_car_data_loop->model_name;
                }

                Yii::$app->cache->set($master_car_model_memcache_key, $model_array, 3600);
            }
        }
        return $model_array;
    }
}
