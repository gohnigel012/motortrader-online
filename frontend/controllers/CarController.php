<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use app\models\MasterGeneral;
use app\models\MasterCarMake;
use app\models\MasterCarModel;
use app\models\MasterCarModelGroup;
use app\models\MasterCarVariant;
use app\models\MasterCarDescription;
use app\models\MasterDealer;
use app\models\MasterDealerReview;
use app\models\MasterDealerReviewInfo;
use app\models\SuperDeals;
use app\models\TransOrder;
use app\models\TransOrderExtraInfo;
use app\models\TransOrderPicture;
use app\models\TransStatisticCount;

/**
 * Car controller
 */
class CarController extends Controller
{
	/**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
		$master_general = new MasterGeneral();
		$master_car_make = new MasterCarMake();
		$master_car_model = new MasterCarModel();
		$master_car_model_group = new MasterCarModelGroup();
		$master_car_variant = new MasterCarVariant();
		$master_car_description = new MasterCarDescription();
		$master_dealer = new MasterDealer();
		$super_deals = new SuperDeals();
		$trans_order = new TransOrder();
		$trans_order_extra_info = new TransOrderExtraInfo();
		
		$location_array = $master_general->getLocation();

		$meta_keyword_array = [];
		$meta_keyword_array[] = "Motor Trader";
		$meta_keyword_array[] = "Used Car";
		$meta_keyword_array[] = "Used Cars";
		$meta_keyword_array[] = "Second hand car";
		$meta_keyword_array[] = "Second hand cars";

		$pdata = [];

		$pdata['limit'] = 9;
		$pdata['offset'] = 0;
		$pdata['page'] = 1;

		$pdata['autoscroll'] = false;

		if (isset($_REQUEST['limit']) && $_REQUEST['limit']) {
			$limit = $_REQUEST['limit'];
			$pdata['offset'] = 0;
		}

		if (isset($_REQUEST['autoscroll']) && in_array($_REQUEST['autoscroll'], ['result', 'model'])) {
			$pdata['autoscroll'] = $_REQUEST['autoscroll'];
		}

		if (isset($_REQUEST['page']) && (int)$_REQUEST['page'] == $_REQUEST['page']) {

			$pdata['autoscroll'] = 'result';
			
			$pdata['page'] = (int)$_REQUEST['page'];

			if ($pdata['page'] > 0) {
				$pdata['offset'] = $pdata['limit'] * ($pdata['page'] - 1);
			} else {
				$pdata['offset'] = 0;
			}
		}
		
		$trans_advert_criteria = $trans_order->find()->where("t.status = '40' AND t.classification_cd = '1141' AND (t.make_cd <> '' OR t.bodytype_code = 'bt9')");

		if (isset($_REQUEST['make'])) {
			$pdata['filter']['make'] = $_REQUEST['make'];
		}
		
		$pdata['filter']['make_cd'] = '';
		if (isset($_REQUEST['make_cd'])) {
			$pdata['filter']['make_cd'] = $_REQUEST['make_cd'];
		}
		
		$pdata['filter']['model_cd'] = '';
		if (isset($_REQUEST['model_cd'])) {
			$pdata['filter']['model_cd'] = $_REQUEST['model_cd'];
		}
		
		$pdata['filter']['location'] = '';
		$pdata['filter']['locationname'] = '';
		
		if (isset($_REQUEST['location']) && isset($location_array[$_REQUEST['location']])) {
			$pdata['filter']['location'] = $_REQUEST['location'];
			$pdata['filter']['locationname'] = strtolower(preg_replace('~[^\\pL\d]+~u', '_', $location_array[$pdata['filter']['location']]));
			
		} else if (isset($_REQUEST['locationname']) && trim($_REQUEST['locationname'])) {
			$pdata['filter']['locationname'] = ltrim($_REQUEST['locationname'], "/");
		}

		$pdata['filter']['keyword'] = '';
		if (isset($_REQUEST['keyword'])) {
			$pdata['filter']['keyword'] = trim($_REQUEST['keyword']);
		}
		
		$pdata['filter']['color'] = '';
		if (isset($_REQUEST['color'])) {
			$pdata['filter']['color'] = trim($_REQUEST['color']);
		}
		
		$pdata['filter']['transmission'] = '';
		if (isset($_REQUEST['transmission'])) {
			$pdata['filter']['transmission'] = trim($_REQUEST['transmission']);
		}
		
		$pdata['filter']['mileage'] = '';
		if (isset($_REQUEST['mileage'])) {
			$pdata['filter']['mileage'] = trim($_REQUEST['mileage']);
		}
		
		$pdata['filter']['price'] = '';
		if (isset($_REQUEST['price'])) {
			$pdata['filter']['price'] = $_REQUEST['price'];
		}
		
		$pdata['filter']['bodytype_code'] = '';
		if (isset($_REQUEST['bodytype_code'])) {
			$pdata['filter']['bodytype_code'] = $_REQUEST['bodytype_code'];
		}
		
		$pdata['filter']['year_make'] = '';
		if (isset($_REQUEST['year_make'])) {
			$pdata['filter']['year_make'] = $_REQUEST['year_make'];
		}
		
		$pdata['filter']['car_info'] = '';
		if (isset($_REQUEST['car_info'])) {
			$pdata['filter']['car_info'] = $_REQUEST['car_info'];
		}
		
		$pdata['filter']['dealer_id'] = '';
		if (isset($_REQUEST['dealer_id'])) {
			$pdata['filter']['dealer_id'] = $_REQUEST['dealer_id'];
		}

		$pdata['filter']['superdeals'] = '0';
		if (isset($_REQUEST['superdeals'])) {
			$pdata['filter']['superdeals'] = $_REQUEST['superdeals'];
		}

		$pdata['filter']['type'] = 'all';
		if (isset($_REQUEST['type']) && in_array($_REQUEST['type'], ['all', 'used', 'new', 'recond'])) {
			$pdata['filter']['type'] = $_REQUEST['type'];
		}
		
		$pdata['filter']['dealers_type'] = 'all';
		if (isset($_REQUEST['dealers_type']) && in_array($_REQUEST['dealers_type'], ['1', '2'])) {
			$pdata['filter']['dealers_type'] = $_REQUEST['dealers_type'];
		}
		
		$pdata['sort'] = 'updatehigh';
		if (isset($_REQUEST['sort']) && in_array($_REQUEST['sort'], ['pricehigh', 'pricelow', 'yearhigh', 'yearlow', 'updatelow', 'updatehigh', 'lth', 'htl'])) {
			if (in_array($_REQUEST['sort'], ['pricehigh', 'htl'])) {
				$pdata['sort'] = 'pricehigh';
			} else if (in_array($_REQUEST['sort'], ['pricelow', 'lth'])) {
				$pdata['sort'] = 'pricelow';
			} else {
				$pdata['sort'] = $_REQUEST['sort'];
			}
	    }
		
		$trans_advert_params_array = [];
		
		$pdata['advert_category'] = 1;

		$make_filter_cd = false;
		$make_filter_name = false;

		if (isset($_REQUEST['make']) && trim($_REQUEST['make'])){
			$pdata['filter']['make'] = $_REQUEST['make'];
			$trans_advert_criteria->andWhere("make_cd = :make");
			$trans_advert_params_array[':make'] = $pdata['filter']['make'];
			
			$make_filter_cd = $pdata['filter']['make'];
			$make_filter_name = $master_car_make->getmakename(strtolower($make_filter_cd));
			$meta_keyword_array[] = $make_filter_name;
			$meta_keyword_array[] = 'buy ' . $make_filter_name;
			$meta_keyword_array[] = 'beli ' . $make_filter_name;

			$pdata['filter']['make_name'] = $make_filter_name;
			
			$pdata['autoscroll'] = 'model';


		} else if (isset($_REQUEST['makename']) && trim($_REQUEST['makename'])) {
			$pdata['filter']['make_name'] = $_REQUEST['makename'];
			
			$trans_advert_criteria->andWhere("make_name ILIKE :make_name");
			$trans_advert_params_array[':make_name'] = $pdata['filter']['make_name'];
			
			$make_filter_cd = $master_car_make->getmakecd($pdata['filter']['make_name']);
			$make_filter_name = $pdata['filter']['make_name'];
			$meta_keyword_array[] = $make_filter_name;
			$meta_keyword_array[] = 'buy ' . $make_filter_name;
			$meta_keyword_array[] = 'beli ' . $make_filter_name;
			
			$_REQUEST['make'] = $make_filter_cd;
			
			$pdata['autoscroll'] = 'model';
		}
		
		$model_filter_cd = false;
		$model_filter_name = false;
		
		if (isset($_REQUEST['model']) && trim($_REQUEST['model'])){
			if(strpos($_REQUEST['model'], ',') !== false) {
				$pdata['filter']['model'] = $_REQUEST['model'];
				
				$model_group_array = explode(",", $_REQUEST['model']);
				$trans_advert_criteria->andWhere(['in', 'model_cd', implode(',', $model_group_array)]);
				
			} else {
				$pdata['filter']['model'] = $_REQUEST['model'];
				$trans_advert_criteria->andWhere("model_cd = :model");
				$trans_advert_params_array[':model'] = $pdata['filter']['model'];

				$model_filter_cd = $pdata['filter']['model'];

				$model_filter_name = $master_car_model->getmodelname($model_filter_cd);
				$meta_keyword_array[] = $model_filter_name;
				$meta_keyword_array[] = 'buy ' . $model_filter_name;
				$meta_keyword_array[] = 'beli ' . $model_filter_name;
			}
		} else if (isset($_REQUEST['modelname']) && trim($_REQUEST['modelname'])) {
			
			$pdata['filter']['model_name'] = $_REQUEST['modelname'];

			if (strtolower($pdata['filter']['model_name']) == 'other' && trim($pdata['filter']['make_name'])) {

				$model_filter_name = 'OTHER';

				$get_other_cd = false;
				$get_other_model = $master_car_model->find()->where(['and', ['=', 'make_cd', $make_filter_cd], ['like', 'model_name', 'other']])->one();
				if ($get_other_model) {

					$model_filter_cd = $pdata['filter']['model_cd'] = $get_other_model->model_cd;

					$trans_advert_criteria->andWhere("model_cd = :model_cd");
					$trans_advert_params_array[':model_cd'] = $pdata['filter']['model_cd'];

				} else {
					$trans_advert_criteria->andWhere(['ilike', 'model_name', ':model_name']);
					$trans_advert_params_array[':model_name'] = $pdata['filter']['model_name'];
				}

			} else {
				$group_model_filter_cd = $master_car_model_group->getmodelgroupcd($make_filter_cd, $pdata['filter']['model_name']);
				if ($group_model_filter_cd) {
					$model_filter_cd = $group_model_filter_cd;
					
					$model_group_array = explode(",", $group_model_filter_cd);
					$trans_advert_criteria->andWhere(['in', 'model_cd', implode(',', $model_group_array)]);
					$model_filter_name = $pdata['filter']['model_name'];
				} else {
					$model_filter_cd = $master_car_model->getmodelcd($pdata['filter']['model_name']);

					$trans_advert_criteria->andWhere(['model_name ILIKE :model_name']);
					$trans_advert_params_array[':model_name'] = $pdata['filter']['model_name'];

					$model_filter_name = $pdata['filter']['model_name'];
				}
			}
			
			$meta_keyword_array[] = $model_filter_name;
			$meta_keyword_array[] = 'buy ' . $model_filter_name;
			$meta_keyword_array[] = 'beli ' . $model_filter_name;
			
			$_REQUEST['model'] = $model_filter_cd;
		}

		$variant_filter_name = false;

		if (isset($_REQUEST['variant']) && trim($_REQUEST['variant'])){
			$pdata['filter']['variant'] = $_REQUEST['variant'];
			$trans_advert_criteria->andWhere("variant_cd = :variant");
			$trans_advert_params_array[':variant'] = $pdata['filter']['variant'];

			$variant_filter_cd = $pdata['filter']['variant'];

			$variant_filter_name = $master_car_variant->getvariantname($variant_filter_cd);
			$meta_keyword_array[] = $variant_filter_name;
		
		} else if (isset($_REQUEST['variantname']) && trim($_REQUEST['variantname'])) {
			$pdata['filter']['variant_name'] = $_REQUEST['variantname'];
			
			$trans_advert_criteria->andWhere(['variant_name ILIKE :variant_name']);
			$trans_advert_params_array[':variant_name'] = '%' . $pdata['filter']['variant_name'] . '%';
			
			$variant_filter_cd = $master_car_variant->getvariantcd($pdata['filter']['variant_name']);
			$variant_filter_name = $pdata['filter']['variant_name'];
			$meta_keyword_array[] = $variant_filter_name;
			
			$_REQUEST['variant'] = $variant_filter_cd;
		}

		if (isset($_REQUEST['superdeals']) && $_REQUEST['superdeals'] == '1') {
			$pdata['page_label'] = '';
		} else {
			$pdata['page_label'] = 'Cars';
		}
		
		$pdata['page_year'] = '';
		$pdata['page_city'] = '';
		
		if (isset($_REQUEST['chscars']) && $_REQUEST['chscars'] == '1') {
			$pdata['page_location'] = '马来西亚';
		} else {
			$pdata['page_location'] = 'Malaysia';
		}
		
		$pdata['action_route'] = 'car';

		switch ($pdata['filter']['type']) {
			case 'car':

					$trans_advert_criteria->andWhere("is_used_flg = '1'");
					$pdata['page_label'] = 'All cars';
					$meta_keyword_array[] = 'All Cars';

					if ($make_filter_name !== false) {
						$pdata['page_label'] = 'All ' . ucfirst(strtolower($make_filter_name)) .  'cars';
						$meta_keyword_array[] = 'All ' . $make_filter_name;
					}

				break;			
			case 'used':

					$trans_advert_criteria->andWhere("is_used_flg = '1'");
					$pdata['page_label'] = 'Used cars';
					$meta_keyword_array[] = 'Used Cars';

					if ($make_filter_name !== false) {
						$pdata['page_label'] = 'Used ' . ucfirst(strtolower($make_filter_name)) . ' cars';
						$meta_keyword_array[] = 'Used ' . $make_filter_name;
					}

				break;
			case 'new':

					$trans_advert_criteria->andWhere("is_used_flg = '2'");
					$pdata['page_label'] = 'New cars';
					$meta_keyword_array[] = 'New Cars';

					if ($make_filter_name !== false) {
						$pdata['page_label'] = 'New ' . ucfirst(strtolower($make_filter_name)) . ' cars';
						$meta_keyword_array[] = 'New ' . $make_filter_name;
					}
					
				break;
			case 'recond':
					
					$trans_advert_criteria->andWhere("is_used_flg = '3'");
					$pdata['page_label'] = 'Recond cars';
					$meta_keyword_array[] = 'Recond Cars';

					if ($make_filter_name !== false) {
						$pdata['page_label'] = 'Recond ' . ucfirst(strtolower($make_filter_name)) . ' cars';
						$meta_keyword_array[] = $make_filter_name;
					}
					
				break;
			case 'commercial':

					$trans_advert_criteria->andWhere("bodytype_code = 'bt9'");
					$pdata['page_label'] = 'Commercial cars';
					$meta_keyword_array[] = 'Commercial Cars';
				break;
			default:
					if ($make_filter_name !== false) {
						$pdata['page_label'] = ucfirst(strtolower($make_filter_name)) . ' cars';
					}
				break;
		}

		if ($model_filter_name !== false) {
			$pdata['page_label'] .= ' ' . str_replace("_", " ", ucfirst(strtolower($model_filter_name)));
		}

		if ($variant_filter_name !== false) {
			$pdata['page_label'] .= ' ' . $variant_filter_name;
		}
		
		$pdata['filter']['location'] = false;
		if (isset($_REQUEST['location']) && trim($_REQUEST['location'])){
			$pdata['filter']['location'] = $_REQUEST['location'];
			$trans_advert_criteria->andWhere("location_cd = :location");
			$trans_advert_params_array[':location'] = $pdata['filter']['location'];

			$tmp_location_array = Yii::$app->general->location();
			if (isset($tmp_location_array[$pdata['filter']['location']])) {
				$pdata['page_location'] = $tmp_location_array[$pdata['filter']['location']];
				$meta_keyword_array[] = $tmp_location_array[$pdata['filter']['location']];
			}
		}
		
		$pdata['filter']['color'] = false;
		if (isset($_REQUEST['color']) && trim($_REQUEST['color'])){
			$pdata['filter']['color'] = $_REQUEST['color'];
			$trans_advert_criteria->andWhere("color_cd = :color");
			$trans_advert_params_array[':color'] = $pdata['filter']['color'];
		}
		
		$pdata['filter']['transmission'] = false;
		if (isset($_REQUEST['transmission']) && trim($_REQUEST['transmission'])){
			$pdata['filter']['transmission'] = $_REQUEST['transmission'];
			$trans_advert_criteria->andWhere("transmission_code = :transmission");
			$trans_advert_params_array[':transmission'] = $pdata['filter']['transmission'];
		}
		
		$pdata['filter']['mileage'] = false;
		if (isset($_REQUEST['mileage']) && trim($_REQUEST['mileage'])){
			$pdata['filter']['mileage'] = $_REQUEST['mileage'];
			$trans_advert_criteria->andWhere("mileage_code = :mileage");
			$trans_advert_params_array[':mileage'] = $pdata['filter']['mileage'];
		}

		$meta_keyword_array[] = 'Malaysia';
		
		if (isset($_REQUEST['keyword']) && trim($_REQUEST['keyword'])){
			$pdata['filter']['keyword'] = $_REQUEST['keyword'];
			
			$keyword_temp_array = [];
			$count_keyword = 0;
			foreach (explode(" ", $pdata['filter']['keyword']) as $keyword_loop) {
				// $count_keyword++;
				
				$keyword_temp_array[] = ['and', ['ilike', 'make_name', $keyword_loop], ['ilike', 'model_name', $keyword_loop]];
				// $trans_advert_params_array[':keyword_' . $count_keyword] = "%" . $keyword_loop . "%";
			}
			
			$trans_advert_criteria->andWhere('upd_data_date >= :less_than_one_year');
			$trans_advert_params_array[':less_than_one_year'] = date("Y-m-d H:i:s", strtotime("- 1 year"));
			$trans_advert_criteria->orderBy("upd_data_date DESC"); 
		}
		
		if ((isset($_REQUEST['minPrice']) && trim($_REQUEST['minPrice'])) || (isset($_REQUEST['maxPrice']) && trim($_REQUEST['maxPrice']))){
			$trans_advert_criteria->andWhere(" t.poa_flg <> '1' ");
		}
		
		if (isset($_REQUEST['minPrice']) && trim($_REQUEST['minPrice'])) {
			$min_price = str_replace(",", "", $_REQUEST['minPrice']);
			if (is_numeric($min_price)) {
				$pdata['filter']['minPrice'] = $min_price;
				$trans_advert_criteria->andWhere("(CASE WHEN price = 'POA' OR poa_flg = '1' THEN -1 ELSE CAST(t.price AS DECIMAL) END) >= :minPrice");
				$trans_advert_params_array[':minPrice'] = $pdata['filter']['minPrice'];
			}
		}

		if (isset($_REQUEST['maxPrice']) && trim($_REQUEST['maxPrice'])) {
			$max_price = str_replace(",", "", $_REQUEST['maxPrice']);
			if (is_numeric($max_price)) {
				$pdata['filter']['maxPrice'] = $max_price;
				$trans_advert_criteria->andWhere("(CASE WHEN price = 'POA' OR poa_flg = '1' THEN -1 ELSE CAST(t.price AS DECIMAL) END) <= :maxPrice");

				$trans_advert_params_array[':maxPrice'] = $pdata['filter']['maxPrice'];
			}
		}	
		
		switch ($pdata['filter']['dealers_type']) {
			case '1':
				$trans_advert_criteria->innerJoin("master_dealer md", "md.dealer_id = t.dealer_id");
				$trans_advert_criteria->andWhere("md.dealer_type = '1'");
				break;
			case '2':
				$trans_advert_criteria->innerJoin("master_dealer md", "md.dealer_id = t.dealer_id");
				$trans_advert_criteria->andWhere("md.dealer_type = '2'");
				break;
		}
				
		if (isset($_REQUEST['bodyType']) && trim($_REQUEST['bodyType'])){
			$pdata['filter']['bodyType'] = $_REQUEST['bodyType'];
			$trans_advert_criteria->andWhere("bodytype_code = :bodyType");
			$trans_advert_params_array[':bodyType'] = $pdata['filter']['bodyType'];
			
			if (strtolower($pdata['filter']['bodyType']) == 'bt9') {
				$pdata['page_label'] = 'Commercial Cars';
			}
		}
		
		if (isset($_REQUEST['cityname']) && trim($_REQUEST['cityname'])){
			$pdata['filter']['cityname'] = $_REQUEST['cityname'];
			$trans_advert_criteria->andWhere("city_name ILIKE :cityname");
			$trans_advert_params_array[':cityname'] = $pdata['filter']['cityname'];
			$pdata['page_city'] = ucwords(strtolower($pdata['filter']['cityname']));
		}
		
		if (isset($_REQUEST['minYear']) && trim($_REQUEST['minYear']) ) {
			$pdata['filter']['minYear'] = $_REQUEST['minYear'];
			$trans_advert_criteria->andWhere("CAST(CASE WHEN t.year_make ~ '^[0-9]+$' THEN t.year_make ELSE '1979' END AS INTEGER) >= :minYear");
			$trans_advert_params_array[':minYear'] = $pdata['filter']['minYear'];
		
		}
		
		if (isset($_REQUEST['maxYear']) && trim($_REQUEST['maxYear']) ){
			$pdata['filter']['maxYear'] = $_REQUEST['maxYear'];
			$trans_advert_criteria->andWhere("CAST(CASE WHEN t.year_make ~ '^[0-9]+$' THEN t.year_make ELSE '1979' END AS INTEGER) <= :maxYear");
			$trans_advert_params_array[':maxYear'] = $pdata['filter']['maxYear'];
		
		}
		
		if (isset($pdata['filter']['minYear']) && isset($pdata['filter']['maxYear']) && $pdata['filter']['minYear'] == $pdata['filter']['maxYear']) {
			$pdata['page_year'] = $pdata['filter']['minYear'];
		}
		
		$pdata['filter']['from_date'] = false;
		if (isset($_REQUEST['days']) && (int)$_REQUEST['days']) {
			if (in_array($_REQUEST['days'], [1, 7, 30, 90])) {
				$pdata['filter']['from_date'] = date('Y-m-d H:i:s', strtotime('- ' . $_REQUEST['days'] . ' days'));
			}
			
			$trans_advert_criteria->andWhere("upd_data_date >= :from");
			$trans_advert_params_array[':from'] = $pdata['filter']['from_date'];
		}
		
		$pdata['filter']['video_file'] = false;
		if (isset($_REQUEST['video']) && (int)$_REQUEST['video']) {
			$pdata['filter']['video_file'] = $_REQUEST['video'];
			$trans_advert_criteria->innerJoin("trans_order_youtube tay", "tay.urn_no = t.urn_no");
		}
			
		if ($pdata['filter']['superdeals'] == '1') {
			$has_join_table = true;
			if ($has_join_table) {
				$trans_advert_criteria->innerJoin("super_deals sd", "sd.urn_no = t.urn_no");
				$trans_advert_criteria->andWhere("sd.super_deals_schedule_start_date <= :date");
				$trans_advert_criteria->andWhere("sd.super_deals_schedule_end_date >= :date");
				$trans_advert_params_array[':date'] = date("Y-m-d H:i:s");
			} else {
				$trans_advert_criteria->innerJoin("super_deals sd", "sd.urn_no = t.urn_no");
				$trans_advert_criteria->andWhere("sd.super_deals_schedule_start_date <= :date");
				$trans_advert_criteria->andWhere("sd.super_deals_schedule_end_date >= :date");
				$trans_advert_params_array[':date'] = date("Y-m-d H:i:s");
			}
		}
		
		$pdata['filter']['voc_file'] = false;
		if (isset($_REQUEST['voc']) && (int)$_REQUEST['voc']) {
			$has_join_table = true;
			$pdata['filter']['voc_file'] = $_REQUEST['voc'];
			$trans_advert_criteria->innerJoin("trans_order_extra_info td", "td.urn_no = t.urn_no");
			$trans_advert_criteria->andWhere("td.trans_order_extra_key = 'vehicle_cert'");
		}
		
		// $pdata['filter']['video_file'] = false;
		// if (isset($_REQUEST['video']) && (int)$_REQUEST['video']) {
			// $has_join_table = true;
			// $pdata['filter']['video_file'] = $_REQUEST['video'];
			// $trans_advert_criteria->join = "INNER JOIN {{trans_order_youtube}} toy ON toy.urn_no = t.urn_no ";
			// $trans_advert_criteria->addCondition(" toy.youtube_status = 'public' AND toy.trans_order_youtube_status = 'approved' ");
		// }
		
		$pdata['seller_type'] = '';
		if (isset($_REQUEST['seller_type']) && in_array($_REQUEST['seller_type'], array('dealer', 'private'))) {
			$pdata['seller_type'] = $_REQUEST['seller_type'];

			switch ($pdata['seller_type']) {
				case 'dealer':
					array_push($trans_advert_criteria, ['and', ['in', 'dealer_flg', ['1', '2']]]);
					break;
				case 'private':
				array_push($trans_advert_criteria, ['and', ['in', 'dealer_flg', ['3']]]);
					break;
			}
		}
		
		$trans_advert_select_array = [];
		$trans_advert_select_array[] = '*';

		if (in_array($pdata['sort'], ['pricelow', 'pricehigh', 'lth', 'htl'])) {
			$trans_advert_select_array[] = "(CASE WHEN price = 'POA' OR poa_flg = '1' THEN 9999999999 ELSE price::numeric END) as plprice";
			$trans_advert_select_array[] = "(CASE WHEN price = 'POA' OR poa_flg = '1' THEN 0 ELSE price::numeric END) as phprice";
		}

		$trans_advert_criteria->select($trans_advert_select_array)->from('trans_order t');

		switch ($pdata['sort']) {
			case 'pricehigh':
			case 'htl':
				$trans_advert_criteria->andWhere("upd_data_date >= :price_date");
				$trans_advert_params_array[':price_date'] = date("Y-m-d H:i:s", strtotime("- 1 year"));
				$trans_advert_criteria->addOrderBy("price DESC, upd_data_date DESC");
	        break;
	        case 'pricelow':
	        case 'lth':
				$trans_advert_criteria->andWhere("upd_data_date >= :price_date");
				$trans_advert_params_array[':price_date'] = date("Y-m-d H:i:s", strtotime("- 1 year"));
				$trans_advert_criteria->addOrderBy("price, upd_data_date DESC");
	        break;
	        case 'yearhigh':
	        	$trans_advert_criteria->andWhere("upd_data_date >= :year_date");
				$trans_advert_params_array[':year_date'] = date("Y-m-d H:i:s", strtotime("- 1 year"));
	        	$trans_advert_criteria->addOrderBy("year_make DESC, upd_data_date DESC");
	        break;
	        case 'yearlow':
				$trans_advert_criteria->andWhere("upd_data_date >= :year_date");
				$trans_advert_params_array[':year_date'] = date("Y-m-d H:i:s", strtotime("- 1 year"));
	        	$trans_advert_criteria->addOrderBy("year_make, upd_data_date DESC");
	        break;
	        case 'updatelow':
	        	$trans_advert_criteria->addOrderBy("upd_data_date");
	        break;
	        default:
	        	$trans_advert_criteria->addOrderBy("upd_data_date DESC");
        	break;
        }

        $pdata['trans_advert_next_key'] = Yii::$app->general->nextadvert($trans_advert_criteria);
		
		$pdata['total'] = $trans_advert_criteria->limit($pdata['limit'])->offset($pdata['offset'])->params($trans_advert_params_array)->count();
		
		$trans_advert_model = $trans_advert_criteria->limit($pdata['limit'])->offset($pdata['offset'])->params($trans_advert_params_array)->all();
		
		// if (trim($pdata['filter']['make_cd']) && trim($pdata['filter']['make_cd'])) {
		// 	$trans_advert_criteria->addInCondition('make_cd', $pdata['filter']['make_cd'], 'AND');
		// }
		
		// if (trim($pdata['filter']['model_cd']) && trim($pdata['filter']['model_cd'])) {
		// 	$trans_advert_criteria->addInCondition('model_cd', $pdata['filter']['model_cd'], 'AND');
		// }

		$pdata['featured'] = $trans_order->featured('1165', $make_filter_cd, $model_filter_cd, $pdata['filter']['keyword'], $pdata['filter']['type']);
		//$pdata['similar'] = TransAdvert::model()->similar('1', 6, $make_filter_cd, $model_filter_cd, $pdata['filter']['keyword'], $pdata['filter']['type']);

		$pdata['right_car_description'] = false;
		if ($make_filter_cd !== false) {
			$master_car_description_model = $master_car_description->find()->where(['make_cd' => $make_filter_cd])->one();
			if($master_car_description_model) {
				$pdata['right_car_description'] = $master_car_description_model->description;
			} else {
				$pdata['right_car_description'] = false;
			}
		}

		$recent_view_array = Yii::$app->general->advfromrecent('car');
		$pdata['recentview'] = [];
		if (count($recent_view_array)) {
			foreach ($recent_view_array as $recent_urn_loop) {
				$recent_view_info = TransAdvert::model()->loadinfo($recent_urn_loop);
				if ($recent_view_info !== false) {
					$pdata['recentview'][] = $recent_view_info;
				}
			}
		}

		$pdata['bookmark_array'] = Yii::$app->general->advfrombookmark();
		
		$pdata['compare_array'] = Yii::$app->general->advfromcompare();
		
		if (isset($_REQUEST['location']) && trim($_REQUEST['location'])) {
			$breadcrumb_location = strtoupper(str_replace("_", " ",$pdata['filter']['locationname']));
			// $this->breadcrumb[$breadcrumb_location] = '';
		} else {
			// $this->breadcrumb['Car Details'] = '';
		}
		
		// explode('/', trim($url['path'], '/'));

		$url_parse = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
		parse_str($url_parse, $url_array);
		$url_array = Yii::$app->general->filterurlparams($url_array);
		/*
			Remember to put default at the last
		*/
		$url_filter_params = [];
		if ($pdata['page'] > 1) {
			$url_filter_params['page'] = $pdata['page'];
		}

		if ($make_filter_name !== false) {
			$url_filter_params['makename'] = $make_filter_name;

			if ($model_filter_name !== false) {
				$url_filter_params['modelname'] = $model_filter_name;
			}
		}
		
		if (!isset($url_array['bodyType']) && isset($_REQUEST['bodyType'])) {
			$url_array['bodyType'] = $_REQUEST['bodyType'];
		}
		
		if (isset($url_array['page_city'])) {
			$url_array['cityname'] = ucwords(strtolower($pdata['page_city']));
		}
		
		if (trim($pdata['filter']['locationname'])) {
			//
		} else if ($pdata['filter']['location'] !== false) {
			$url_array['location'] = $pdata['filter']['location'];
		}

		// $url_array['autoscroll'] = 'result';
		
		if (isset($_REQUEST['keretamurah']) && $_REQUEST['keretamurah'] == '1') {
			$pdata['action_route'] = 'kereta-murah';
		} 
		
		if (isset($_REQUEST['chscars']) && $_REQUEST['chscars'] == '1') {
			$pdata['action_route'] = 'cars-chinese';
		}
		
		$pdata['url_superdeals_on'] = Yii::$app->general->generatecarsseo($pdata['action_route'], 0, 0, $pdata['filter']['locationname'], 0, $make_filter_name, $model_filter_name, $variant_filter_name, 1, $url_array);
		$pdata['url_superdeals_off'] = Yii::$app->general->generatecarsseo($pdata['action_route'], 0, 0, $pdata['filter']['locationname'], 0, $make_filter_name, $model_filter_name, $variant_filter_name, 0, $url_array);
		
		if (isset($_REQUEST['keretamurah']) && $_REQUEST['keretamurah'] == '1') {
			$pdata['url_type_all'] = Yii::$app->general->generatecarsseo('kereta-murah', 0, 0, $pdata['filter']['locationname'], 0, $make_filter_name, $model_filter_name, $variant_filter_name, $pdata['filter']['superdeals'], $url_array);
			$pdata['url_type_used'] = Yii::$app->general->generatecarsseo('kereta-murah-terpakai', 0, 0, $pdata['filter']['locationname'], 0, $make_filter_name, $model_filter_name, $variant_filter_name, $pdata['filter']['superdeals'], $url_array);
			$pdata['url_type_new'] = Yii::$app->general->generatecarsseo('kereta-murah-baru', 0, 0, $pdata['filter']['locationname'], 0, $make_filter_name, $model_filter_name, $variant_filter_name, $pdata['filter']['superdeals'], $url_array);
			$pdata['url_type_recond'] = Yii::$app->general->generatecarsseo('kereta-murah-recond', 0, 0, $pdata['filter']['locationname'], 0, $make_filter_name, $model_filter_name, $variant_filter_name, $pdata['filter']['superdeals'], $url_array);
		
			$pdata['page_label'] = 'Kereta murah bawah RM30K';

			switch ($pdata['filter']['type']) {
				case 'car':
						$this->breadcrumb['Kereta Murah'] = Yii::$app->general->generatecarsseo('kereta-murah');

						$trans_advert_criteria->addCondition(" is_used_flg = '1' ");
						$pdata['page_label'] = 'Kereta murah bawah RM30K';
						$meta_keyword_array[] = 'Kereta murah bawah RM30K';

						if ($make_filter_name !== false) {
							$pdata['page_label'] = 'Kereta murah bawah RM30K ' . $make_filter_name;
							$meta_keyword_array[] = 'Kereta murah bawah RM30K ' . $make_filter_name;
						}

					break;			
				case 'used':
						$pdata['action_route'] = 'kereta-murah-terpakai';

						$this->breadcrumb['Kereta Murah Terpakai'] = Yii::$app->general->generatecarsseo('kereta-murah-terpakai');

						array_push($trans_advert_criteria, " is_used_flg = '1' ");
						$pdata['page_label'] = 'Kereta Murah Terpakai bawah RM30K';
						$meta_keyword_array[] = 'Kereta Murah Terpakai bawah RM30K';

						if ($make_filter_name !== false) {
							$pdata['page_label'] = 'Kereta Murah Terpakai bawah RM30K ' . $make_filter_name;
							$meta_keyword_array[] = 'Kereta Murah Terpakai bawah RM30K ' . $make_filter_name;
						}

					break;
				case 'new':
						$pdata['action_route'] = 'kereta-murah-baru';

						$this->breadcrumb['Kereta Murah Baru'] = Yii::$app->general->generatecarsseo('kereta-murah-baru');

						array_push($trans_advert_criteria, " is_used_flg = '2' ");
						$pdata['page_label'] = 'Kereta Murah Baru bawah RM30K';
						$meta_keyword_array[] = 'Kereta Murah Baru bawah RM30K';

						if ($make_filter_name !== false) {
							$pdata['page_label'] = 'Kereta Murah Baru bawah RM30K ' . $make_filter_name;
							$meta_keyword_array[] = 'Kereta Murah Baru bawah RM30K ' . $make_filter_name;
						}
						
					break;
				case 'recond':
						$pdata['action_route'] = 'kereta-murah-recond';

						// $this->breadcrumb['Kereta Murah Recond'] = Yii::$app->general->generatecarsseo('kereta-murah-recond');
						
						array_push($trans_advert_criteria, " is_used_flg = '3' ");
						$pdata['page_label'] = 'Kereta Murah Recond bawah RM30K';
						$meta_keyword_array[] = 'Kereta Murah Recond bawah RM30K';

						if ($make_filter_name !== false) {
							$pdata['page_label'] = 'Kereta Murah Recond bawah RM30K ' . $make_filter_name;
							$meta_keyword_array[] = $make_filter_name;
						}
						
					break;
				default:
						$this->breadcrumb['Kereta Murah'] = Yii::$app->general->generatecarsseo('kereta-murah');
						
						if ($make_filter_name !== false) {
							$pdata['page_label'] = 'Kereta murah bawah RM30K ' . $make_filter_name;
						}
					break;
			}
		} else {
			if (isset($_REQUEST['chscars']) && $_REQUEST['chscars'] == '1') {
				$pdata['url_type_all'] = Yii::$app->general->generatecarsseo('cars-chinese', 0, 0, $pdata['filter']['locationname'], 0, $make_filter_name, $model_filter_name, $variant_filter_name, $pdata['filter']['superdeals'], $url_array);
				$pdata['url_type_used'] = Yii::$app->general->generatecarsseo('cars-used-chinese', 0, 0, $pdata['filter']['locationname'], 0, $make_filter_name, $model_filter_name, $variant_filter_name, $pdata['filter']['superdeals'], $url_array);
				$pdata['url_type_new'] = Yii::$app->general->generatecarsseo('cars-new-chinese', 0, 0, $pdata['filter']['locationname'], 0, $make_filter_name, $model_filter_name, $variant_filter_name, $pdata['filter']['superdeals'], $url_array);
				$pdata['url_type_recond'] = Yii::$app->general->generatecarsseo('cars-recond-chinese', 0, 0, $pdata['filter']['locationname'], 0, $make_filter_name, $model_filter_name, $variant_filter_name, $pdata['filter']['superdeals'], $url_array);
			
				$pdata['page_label'] = '汽车';

				switch ($pdata['filter']['type']) {
					case 'car':
							// $this->breadcrumb['汽车'] = Yii::$app->general->generatecarsseo('cars-chinese');

							array_push($trans_advert_criteria, " is_used_flg = '1' ");
							$pdata['page_label'] = '汽车';
							$meta_keyword_array[] = '汽车';

							if ($make_filter_name !== false) {
								$pdata['page_label'] = '汽车 ' . $make_filter_name;
								$meta_keyword_array[] = '汽车 ' . $make_filter_name;
							}

						break;			
					case 'used':
							$pdata['action_route'] = 'cars-used-chinese';

							// $this->breadcrumb['二手车'] = Yii::$app->general->generatecarsseo('cars-used-chinese');

							array_push($trans_advert_criteria, " is_used_flg = '1' ");
							$pdata['page_label'] = '二手车';
							$meta_keyword_array[] = '二手车';

							if ($make_filter_name !== false) {
								$pdata['page_label'] = '二手车 ' . $make_filter_name;
								$meta_keyword_array[] = '二手车 ' . $make_filter_name;
							}

						break;
					case 'new':
							$pdata['action_route'] = 'cars-new-chinese';

							// $this->breadcrumb['新车'] = Yii::$app->general->generatecarsseo('cars-new-chinese');

							array_push($trans_advert_criteria, " is_used_flg = '2' ");
							$pdata['page_label'] = '新车';
							$meta_keyword_array[] = '新车';

							if ($make_filter_name !== false) {
								$pdata['page_label'] = '新车 ' . $make_filter_name;
								$meta_keyword_array[] = '新车 ' . $make_filter_name;
							}
							
						break;
					case 'recond':
							$pdata['action_route'] = 'cars-recond-chinese';

							// $this->breadcrumb['复新车'] = Yii::$app->general->generatecarsseo('cars-recond-chinese');
							
							array_push($trans_advert_criteria, " is_used_flg = '3' ");
							$pdata['page_label'] = '复新车';
							$meta_keyword_array[] = '复新车';

							if ($make_filter_name !== false) {
								$pdata['page_label'] = '复新车 ' . $make_filter_name;
								$meta_keyword_array[] = '复新车 ' . $make_filter_name;
							}
							
						break;
					default:
							// $this->breadcrumb['汽车'] = Yii::$app->general->generatecarsseo('cars-chinese');
							
							if ($make_filter_name !== false) {
								$pdata['page_label'] = '汽车 ' . $make_filter_name;
							}
						break;
				}
			} else {
				$url_array['type'] = 'all';
				$pdata['url_type_all'] = Yii::$app->general->generatecarsseo('car', 0, 0, $pdata['filter']['locationname'], 0, $make_filter_cd, $model_filter_cd, $variant_filter_name, $pdata['filter']['superdeals'], $url_array);

				$url_array['type'] = 'used';
				$pdata['url_type_used'] = Yii::$app->general->generatecarsseo('car', 0, 0, $pdata['filter']['locationname'], 0, $make_filter_cd, $model_filter_cd, $variant_filter_name, $pdata['filter']['superdeals'], $url_array);

				$url_array['type'] = 'new';
				$pdata['url_type_new'] = Yii::$app->general->generatecarsseo('car', 0, 0, $pdata['filter']['locationname'], 0, $make_filter_cd, $model_filter_cd, $variant_filter_name, $pdata['filter']['superdeals'], $url_array);

				$url_array['type'] = 'recond';
				$pdata['url_type_recond'] = Yii::$app->general->generatecarsseo('car', 0, 0, $pdata['filter']['locationname'], 0, $make_filter_cd, $model_filter_cd, $variant_filter_name, $pdata['filter']['superdeals'], $url_array);

				$url_array['type'] = $pdata['filter']['type'];
			}
		}
		
		$url_array['sort'] = 'pricehigh';
		$pdata['url_sort_price_high'] = Yii::$app->general->generatecarsseo($pdata['action_route'], 0, 0, $pdata['filter']['locationname'], 0, $make_filter_cd, $model_filter_cd, $variant_filter_name, $pdata['filter']['superdeals'], $url_array);

		$url_array['sort'] = 'pricelow';
		$pdata['url_sort_price_low'] = Yii::$app->general->generatecarsseo($pdata['action_route'], 0, 0, $pdata['filter']['locationname'], 0, $make_filter_cd, $model_filter_cd, $variant_filter_name, $pdata['filter']['superdeals'], $url_array);

		$url_array['sort'] = 'yearhigh';
		$pdata['url_sort_year_high'] = Yii::$app->general->generatecarsseo($pdata['action_route'], 0, 0, $pdata['filter']['locationname'], 0, $make_filter_cd, $model_filter_cd, $variant_filter_name, $pdata['filter']['superdeals'], $url_array);

		$url_array['sort'] = 'yearlow';
		$pdata['url_sort_year_low'] = Yii::$app->general->generatecarsseo($pdata['action_route'], 0, 0, $pdata['filter']['locationname'], 0, $make_filter_cd, $model_filter_name, $variant_filter_name, $pdata['filter']['superdeals'], $url_array);

		$url_array['sort'] = 'updatehigh';
		$pdata['url_sort_update_low'] = Yii::$app->general->generatecarsseo($pdata['action_route'], 0, 0, $pdata['filter']['locationname'], 0, $make_filter_cd, $model_filter_cd, $variant_filter_name, $pdata['filter']['superdeals'], $url_array);

		$url_array['sort'] = 'updatelow';
		$pdata['url_sort_update_high'] = Yii::$app->general->generatecarsseo($pdata['action_route'], 0, 0, $pdata['filter']['locationname'], 0, $make_filter_cd, $model_filter_cd, $variant_filter_name, $pdata['filter']['superdeals'], $url_array);

		$url_array['sort'] = $pdata['sort'];

		if (isset($_REQUEST['superdeals']) && $_REQUEST['superdeals'] != '') {
			$meta_temp = 'Superdeals' . preg_replace('/(^All)/i', '', $pdata['page_label']);
		} else {
			$meta_temp = preg_replace('/(^All)/i', '', $pdata['page_label']);
		}
		
		if (isset($pdata['filter']['dealers_type']) && $pdata['filter']['dealers_type'] == '3') {
			$meta_title_str = (isset($pdata['page_year']) ? $pdata['page_year'] : '') . ' ' . trim($meta_temp) . ' for sale by direct owner in' . (isset($pdata['page_city']) ? $pdata['page_city'] : '') . ' ' . $pdata['page_location'];
		} else {
			if (isset($pdata['page_year']) && $pdata['page_year'] != '') {
				$meta_title_str = (isset($pdata['page_year']) ? $pdata['page_year'] : '') . ' ' . trim($meta_temp) . ' for sale in ' . (isset($pdata['page_city']) ? $pdata['page_city'] : '') . ' ' . $pdata['page_location'];
			} else {
				if (isset($_REQUEST['chscars']) && $_REQUEST['chscars'] == '1') {
					$meta_title_str = '马来西亚二手车买卖网站';
				} else {
					$meta_title_str = trim($meta_temp) . ' for sale in ' . (isset($pdata['page_city']) ? $pdata['page_city'] : '') . ' ' . $pdata['page_location'];
				}
			}
		}
		
		
		if ($pdata['page'] > 1) {
			$meta_title_str .= ' - Page ' . $pdata['page'];
		}
		
		$user_id = Yii::$app->general->getuserid();
		
		if (!empty($_REQUEST['make']) || !empty($_REQUEST['model']) || !empty($_REQUEST['keyword'])) {
			if ($pdata['page'] < 2) {
				if ($user_id != "") {
					if ($searchuserstatus = Yii::$app->api->advertsustatus($user_id)) {
						Yii::$app->general->setasus($searchuserstatus);
						$advert_sus = Yii::$app->general->getasus();
						if ($user_id && $advert_sus == 1) {
							if (!empty($_REQUEST['make']) || !empty($_REQUEST['model']) || !empty($_REQUEST['keyword'])) {	
								if (isset($_REQUEST['make'])) {
									$car_maker = $master_car_make->getmakename($_REQUEST['make']);
									$car_make_cd = $_REQUEST['make'];
								}
								
								if (isset($_REQUEST['model'])) {
									$car_model = $master_car_model->getmodelname($_REQUEST['model']);
									$car_model_cd = $_REQUEST['model'];
								}
								
								if (isset($_REQUEST['keyword'])) {
									$car_search_keyword = $_REQUEST['keyword'];
								}	
								
								$user_name = "";
								$user_contact = "";
								$user_email = "";
								
								$srfollow = Yii::$app->api->searchresult($user_id, $user_name, $user_contact, $user_email, (isset($car_maker)&&$car_maker!=''?$car_maker:''), (isset($car_model)&&$car_model!=''?$car_model:''), (isset($car_make_cd)&&$car_make_cd!=''?$car_make_cd:''), (isset($car_model_cd)&&$car_model_cd!=''?$car_model_cd:''), (isset($car_search_keyword)&&$car_search_keyword!=''?$car_search_keyword:''));
								
							}
						}
					} else {
						$setsus = Yii::$app->api->searchuserstatus($user_id);
						if ($setsus == 1) {
							if ($searchuserstatus = Yii::$app->api->advertsustatus($user_id)) {
								Yii::$app->general->setasus($searchuserstatus);
								$advert_sus = Yii::$app->general->getasus();
								if ($user_id && $advert_sus == 1) {
									if (!empty($_REQUEST['make']) || !empty($_REQUEST['model']) || !empty($_REQUEST['keyword'])) {	
										if (isset($_REQUEST['make'])) {
											$car_maker = $master_car_make->getmakename($_REQUEST['make']);
											$car_make_cd = $_REQUEST['make'];
										}
										
										if (isset($_REQUEST['model'])) {
											$car_model = $master_car_model->getmodelname($_REQUEST['model']);
											$car_model_cd = $_REQUEST['model'];
										}	
										
										if (isset($_REQUEST['keyword'])) {
											$car_search_keyword = $_REQUEST['keyword'];
										}
										
										$user_name = "";
										$user_contact = "";
										$user_email = "";
										
										$srfollow = Yii::$app->api->searchresult($user_id, $user_name, $user_contact, $user_email, (isset($car_maker)&&$car_maker!=''?$car_maker:''), (isset($car_model)&&$car_model!=''?$car_model:''), (isset($car_make_cd)&&$car_make_cd!=''?$car_make_cd:''), (isset($car_model_cd)&&$car_model_cd!=''?$car_model_cd:''), (isset($car_search_keyword)&&$car_search_keyword!=''?$car_search_keyword:''));
										
									}
								}
							}
						}
					}
				} else {
					
					if (isset($_SESSION['car_search_created']) && is_numeric($_SESSION['car_search_created']) &&
						$_SESSION['car_search_created'] < ((time() - (3 * 24 * 60 * 60)))
						) {
							
						if (isset($_SESSION['car_search_customer_name'])) unset($_SESSION['car_search_customer_name']);
						if (isset($_SESSION['car_search_customer_email'])) unset($_SESSION['car_search_customer_email']);
						if (isset($_SESSION['car_search_customer_phone'])) unset($_SESSION['car_search_customer_phone']);
					}
					
					if (isset($_SESSION['car_search_customer_name']) != "" && 
						isset($_SESSION['car_search_customer_email']) != "" && 
						isset($_SESSION['car_search_customer_phone']) != "") {
						
						if (!empty($_REQUEST['make']) || !empty($_REQUEST['model']) || !empty($_REQUEST['keyword'])) {	
							if (isset($_REQUEST['make'])) {
								$car_maker = $master_car_make->getmakename($_REQUEST['make']);
								$car_make_cd = $_REQUEST['make'];
							}
							
							if (isset($_REQUEST['model'])) {
								$car_model = $master_car_model->getmodelname($_REQUEST['model']);
								$car_model_cd = $_REQUEST['model'];
							}
							
							if (isset($_REQUEST['keyword'])) {
								$car_search_keyword = $_REQUEST['keyword'];
							}
							
							$user_id = "";
						
							$user_name_session = $_SESSION['car_search_customer_name'];
							$user_contact_session = $_SESSION['car_search_customer_email'];
							$user_email_session = $_SESSION['car_search_customer_phone'];
							
							$srfollow = Yii::$app->api->searchresult($user_id, $user_name_session, $user_contact_session, $user_email_session, (isset($car_maker)&&$car_maker!=''?$car_maker:''), (isset($car_model)&&$car_model!=''?$car_model:''), (isset($car_make_cd)&&$car_make_cd!=''?$car_make_cd:''), (isset($car_model_cd)&&$car_model_cd!=''?$car_model_cd:''), (isset($car_search_keyword)&&$car_search_keyword!=''?$car_search_keyword:''));					
							
						}	
					}	
				}	
			}	
		}
		
		$pdata['url_pagination'] = Yii::$app->general->generatecarsseo($pdata['action_route'], 0, 0, $pdata['filter']['locationname'], '__PAGE__', $make_filter_name, $model_filter_name, $variant_filter_name, $pdata['filter']['superdeals'], $url_array);

		$pdata['make_filter_name'] = $make_filter_name;
		$pdata['make_filter_cd'] = $make_filter_cd;
		$pdata['model_filter_name'] = $model_filter_name;
		$pdata['model_filter_cd'] = $model_filter_cd;

    	 return $this->render('index', [
    	 	'trans_advert_model' => $trans_advert_model, 
    	 	'pdata' => $pdata,
    	 	'trans_order_extra_info' => $trans_order_extra_info,
    	 	'master_dealer' => $master_dealer,
    	 	'super_deals' => $super_deals,
    	 	'master_general' => $master_general,
    	 	'master_car_make' => $master_car_make,
    	 	'master_car_model' => $master_car_model,
    	 	'master_car_model_group' => $master_car_model_group,
    	 	'master_car_variant' => $master_car_variant,
    	 	'trans_order' => $trans_order,
    	]);
    }

    public function actionView()
    {
    	$master_general = new MasterGeneral();
    	$master_dealer = new MasterDealer();
    	$master_dealer_review = new MasterDealerReview();
    	$master_dealer_review_info = new MasterDealerReviewInfo();
    	$super_deals = new SuperDeals();
    	$trans_order = new TransOrder();
    	$trans_order_extra_info = new TransOrderExtraInfo();
    	$trans_order_picture = new TransOrderPicture();
    	$trans_statistic_count = new TransStatisticCount();

    	if (isset($_REQUEST['urn_no'])) {

    		if($trans_order_picture->findOne($_REQUEST['urn_no'])) {
    			$trans_advert_model =  $trans_order->find()->from("trans_order")->where(['trans_order.urn_no' => $_REQUEST['urn_no']])->innerJoinWith("transOrderPictures")->one();
    		} else {
    			$trans_advert_model = $trans_order->findOne($_REQUEST['urn_no']);
    		}

    		if($trans_advert_model) {
    			$pdata['master_dealer_model'] = $master_dealer->findOne($trans_advert_model->dealer_id);

	    		if($pdata['master_dealer_model']) {
					$pdata['dealer_seourl'] = Yii::$app->general->generateseo('dealer', $trans_advert_model->dealer_id, $pdata['master_dealer_model']->dealer_id, $pdata['master_dealer_model']->dealer_name);
				} else {
					$pdata['master_dealer_model'] = false;
					$pdata['dealer_seourl'] = Yii::$app->general->generateseo('dealer', $trans_advert_model->dealer_id, 'private');
				}

				$pdata['car_video'] = Yii::$app->api->getvideo($trans_advert_model->urn_no);

				$pdata['upd_data_date'] = strtotime($trans_advert_model->upd_data_date);

				$pdata['voc_file'] = '';
				$pdata['voc_approved_status'] = false;
				$voc_file = $trans_order_extra_info->getvalue($trans_advert_model->urn_no, 'vehicle_cert_approved');
				if ($voc_file !== false) {
					$pdata['voc_file'] = $voc_file;
					$pdata['voc_approved_status'] = true;
				} else {
					$voc_file = $trans_order_extra_info->getvalue($trans_advert_model->urn_no, 'vehicle_cert');
					if ($voc_file !== false) {
						$pdata['voc_file'] = $voc_file;
					}
				}

				$pdata['picture_array'] = [];

				$image_path = rtrim(Yii::$app->fpath->mountimages['path'], "/") . '/advert/wmfull/';

				foreach ($trans_advert_model->transOrderPictures as $trans_order_picture) {
					if (trim($trans_order_picture->picture_no)) {
						$pdata['picture_array'][$trans_order_picture->picture_no] = ['url' => Yii::$app->general->advertimageurl($trans_order_picture->picture_file, 'wmfull', $pdata['upd_data_date'],$trans_order_picture->is_new), 'thumb' => Yii::$app->general->advertimageurl($trans_order_picture->picture_file, 'webad', $pdata['upd_data_date'],$trans_order_picture->is_new), 'filename' => $trans_order_picture->picture_file];
					}

					if (!count($pdata['picture_array'])) {
					$pdata['picture_array'][1] = [
						'url' => Yii::$app->general->advertimageurl($ttrans_order_picture->picture_file, 'wmfull', $pdata['upd_data_date'], $trans_order_picture->is_new), 
						'thumb' => Yii::$app->general->advertimageurl($trans_order_picture->picture_file, 'webad', $pdata['upd_data_date'], $trans_order_picture->is_new), 
						'filename' => ''];
					}
				}

				$pdata['voc_picture_array'] = [];
				if (trim($pdata['voc_file'])) {
					$pdata['voc_picture_array'] = ['url' => Yii::$app->general->advertimagecerturl($pdata['voc_file'], $trans_advert_model->urn_no, 'carcert', $pdata['upd_data_date']), 'thumb' => Yii::$app->general->advertimagecerturl($pdata['voc_file'], $trans_advert_model->urn_no, 'carcert', $pdata['upd_data_date']), 'filename' => $pdata['voc_file']];
				}

	    		switch($trans_advert_model->is_used_flg){
					case '1':
						$pdata['condition'] = 'Used Car';
						break;
					case '2':
						$pdata['condition'] = 'New Car';
						break;
					case '3':
						$pdata['condition'] = 'Recon Car';
						break;
					default:
						$pdata['condition'] = 'Used Car';
						break;
				}

				$pdata['similar'] = $trans_order->similar('1141', 6, $trans_advert_model->make_cd, $trans_advert_model->model_cd, '', '');

				$pdata['bookmark_array'] = Yii::$app->general->advfrombookmark();

				if ($trans_advert_model->bodytype_code == 'bt9') {
					$pdata['condition'] = 'Commercial Car';
				}

	    		$pdata['roadtax_region'] =  '';
					$pdata['roadtax_type'] =  '';
					$roadtax_cc = str_replace("cc", "", $trans_advert_model->engine_name);
					$pdata['roadtax_cc'] = (int)$roadtax_cc;
					$roadtax_amount =  '0';
					
					switch ($trans_advert_model->location_cd) {
						case 'L00012':
							$pdata['roadtax_region']  = '2';
						break;
						case 'L00013':
							$pdata['roadtax_region']  = '2';
						break;
						default:
							$pdata['roadtax_region']  = '1';
						break;
					}
					
					if (in_array($trans_advert_model->bodytype_code, ['bt1', 'bt2', 'bt3', 'bt4', 'bt5'])) {
						$pdata['roadtax_type'] = '1';
					} else {
						$pdata['roadtax_type'] = '3';
					}
			
					if ($pdata['roadtax_region'] == '1') {
						if ($pdata['roadtax_type'] == '1') {
							if ($pdata['roadtax_cc'] <= 1000) {
								$roadtax_amount = 20;
							} else if ($pdata['roadtax_cc'] >= 1001 && $pdata['roadtax_cc'] <= 1200) {
								$roadtax_amount = 55;
							} else if ($pdata['roadtax_cc'] >= 1201 && $pdata['roadtax_cc'] <= 1400) {
								$roadtax_amount = 70;
							} else if ($pdata['roadtax_cc'] >= 1401 && $pdata['roadtax_cc'] <= 1600) {
								$roadtax_amount  = 90;
							} else if ($pdata['roadtax_cc'] >= 1601 && $pdata['roadtax_cc'] <= 1800) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 1601) + 1) * 0.4) + 200); 
							} else if ($pdata['roadtax_cc'] >= 1801 && $pdata['roadtax_cc'] <= 2000) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 1801) + 1) * 0.5) + 280); 
							} else if ($pdata['roadtax_cc'] >= 2001 && $pdata['roadtax_cc'] <= 2500) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 2001) + 1) * 1) + 380);
							} else if ($pdata['roadtax_cc'] >= 2501 && $pdata['roadtax_cc'] <= 3000) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 2501) + 1) * 2.5) + 880);
							} else if ($pdata['roadtax_cc'] >= 3001) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 3001) + 1) * 4.5) + 2130);
							}
						} else if ($pdata['roadtax_type'] == '2') {
							if ($pdata['roadtax_cc'] <= 1000) {
								$roadtax_amount = 20;
							} else if ($pdata['roadtax_cc'] >= 1001 && $pdata['roadtax_cc'] <= 1200) {
								$roadtax_amount = 110;
							} else if ($pdata['roadtax_cc'] >= 1201 && $pdata['roadtax_cc'] <= 1400) {
								$roadtax_amount = 140;
							} else if ($pdata['roadtax_cc'] >= 1401 && $pdata['roadtax_cc'] <= 1600) {
								$roadtax_amount = 180;
							} else if ($pdata['roadtax_cc'] >= 1601 && $pdata['roadtax_cc'] <= 1800) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 1601) + 1) * 0.8) + 400); 
							} else if ($pdata['roadtax_cc'] >= 1801 && $pdata['roadtax_cc'] <= 2000) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 1801) + 1) * 1) + 560); 				
							} else if ($pdata['roadtax_cc'] >= 2001 && $pdata['roadtax_cc'] <= 2500) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 2001) + 1) * 3) + 760); 
							} else if ($pdata['roadtax_cc'] >= 2501 && $pdata['roadtax_cc'] <= 3000) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 2501) + 1) * 7.5) + 2260); 
							} else if ($pdata['roadtax_cc'] >= 3001) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 3001) + 1) * 13.5) + 6010); 
							}
						} else {
							if ($pdata['roadtax_cc'] <= 1000) {
								$roadtax_amount = 20;
							} else if ($pdata['roadtax_cc'] >= 1001 && $pdata['roadtax_cc'] <= 1200) {
								$roadtax_amount = 85;
							} else if ($pdata['roadtax_cc'] >= 1201 && $pdata['roadtax_cc'] <= 1400) {
								$roadtax_amount = 100;
							} else if ($pdata['roadtax_cc'] >= 1401 && $pdata['roadtax_cc'] <= 1600) {
								$roadtax_amount = 120;
							} else if ($pdata['roadtax_cc'] >= 1601 && $pdata['roadtax_cc'] <= 1800) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 1601) + 1) * 0.3) + 300); 
							} else if ($pdata['roadtax_cc'] >= 1801 && $pdata['roadtax_cc'] <= 2000) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 1801) + 1) * 0.4) + 360); 
							} else if ($pdata['roadtax_cc'] >= 2001 && $pdata['roadtax_cc'] <= 2500) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 2001) + 1) * 0.8) + 440); 
							} else if ($pdata['roadtax_cc'] >= 2501 && $pdata['roadtax_cc'] <= 3000) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 2501) + 1) * 1.6) + 840); 
							} else if ($pdata['roadtax_cc'] >= 3001) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 3001) + 1) * 1.6) + 1640); 
							}
						}
					} else if ($pdata['roadtax_region'] == '2') {
						if ($pdata['roadtax_type'] == '1' || $pdata['roadtax_type'] == '2') {
							if ($pdata['roadtax_cc'] <= 1000) {
								$roadtax_amount = 20;
							} else if ($pdata['roadtax_cc'] >= 1001 && $pdata['roadtax_cc'] <= 1200) {
								$roadtax_amount = 44;
							} else if ($pdata['roadtax_cc'] >= 1201 && $pdata['roadtax_cc'] <= 1400) {
								$roadtax_amount = 56;
							} else if ($pdata['roadtax_cc'] >= 1401 && $pdata['roadtax_cc'] <= 1600) {
								$roadtax_amount = 72;
							} else if ($pdata['roadtax_cc'] >= 1601 && $pdata['roadtax_cc'] <= 1800) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 1601) + 1) * 0.32) + 160); 
							} else if ($pdata['roadtax_cc'] >= 1801 && $pdata['roadtax_cc'] <= 2000) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 1801) + 1) * 0.25) + 224); 
							} else if ($pdata['roadtax_cc'] >= 2001 && $pdata['roadtax_cc'] <= 2500) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 2001) + 1) * 0.5) + 274); 
							} else if ($pdata['roadtax_cc'] >= 2501 && $pdata['roadtax_cc'] <= 3000) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 2501) + 1) * 1) + 524); 
							} else if ($pdata['roadtax_cc'] >= 3001) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 3001) + 1) * 1.35) + 1024); 
							}
						} else {
							if ($pdata['roadtax_cc'] <= 1000) {
								$roadtax_amount = 20;
							} else if ($pdata['roadtax_cc'] >= 1001 && $pdata['roadtax_cc'] <= 1200) {
								$roadtax_amount = 42.5;
							} else if ($pdata['roadtax_cc'] >= 1201 && $pdata['roadtax_cc'] <= 1400) {
								$roadtax_amount = 50;
							} else if ($pdata['roadtax_cc'] >= 1401 && $pdata['roadtax_cc'] <= 1600) {
								$roadtax_amount = 60;
							} else if ($pdata['roadtax_cc'] >= 1601 && $pdata['roadtax_cc'] <= 1800) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 1601) + 1) * 0.17) + 165); 
							} else if ($pdata['roadtax_cc'] >= 1801 && $pdata['roadtax_cc'] <= 2000) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 1801) + 1) * 0.22) + 199); 
							} else if ($pdata['roadtax_cc'] >= 2001 && $pdata['roadtax_cc'] <= 2500) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 2001) + 1) * 0.44) + 243); 
							} else if ($pdata['roadtax_cc'] >= 2501 && $pdata['roadtax_cc'] <= 3000) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 2501) + 1) * 0.88) + 463); 
							} else if ($pdata['roadtax_cc'] >= 3001) {
								$roadtax_amount = (((($pdata['roadtax_cc'] - 3001) + 1) * 1.2) + 903); 
							}
						}
					}
				
				$getsimilar_superdeals_array = $super_deals->getsimilarsuperdeals($trans_advert_model->make_cd, $trans_advert_model->model_cd);
				$pdata['getsimilarsuperdeals'] = $getsimilar_superdeals_array;
				
				$pdata['roadtax_amount'] = number_format($roadtax_amount);

				$pdata['mileage_array'] = $master_general->getMileage();

    			return $this->render('view', ['trans_advert_model' => $trans_advert_model, 'pdata' => $pdata, 'master_dealer_review' => $master_dealer_review, 'master_dealer_review_info' => $master_dealer_review_info, 'super_deals' => $super_deals, 'trans_statistic_count' => $trans_statistic_count]);
    		} else {
    			$this->redirect(Yii::$app->urlManager->createAbsoluteUrl('site/error', array('urn_no' => $_REQUEST['urn_no'])));
    		}

    		
    	} else {
    		return $this->redirect('/car');
    	}
    }

    public function actionTips() {
    	return $this->render('tips');
    }
}

