<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Motor Trader Car';
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-home">
	<div class="jumbotron">
		<p class="text-left">
<?php
	if (isset($pdata['page_year']) && $pdata['page_year'] != '') {
		$total_advert = '';
	} else {
		$total_advert = (isset($pdata['total']) && $pdata['total'] != '0' ? number_format($pdata['total'], 0) : '');
	}
	
	if (isset($pdata['filter']['dealers_type']) && $pdata['filter']['dealers_type'] == '2') {
?>								
								<?php echo $total_advert; ?> <?php echo (isset($pdata['page_year']) ? $pdata['page_year'] : '') ?> <?php echo $pdata['page_label'] ?> for sale by direct owner in <?php echo (isset($pdata['page_city']) ? $pdata['page_city'] : '') ?> <?php echo $pdata['page_location'] ?> - Page <?php echo $pdata['page'] ?>
<?php		
	} else {	
		if (isset($_REQUEST['keretamurah']) && $_REQUEST['keretamurah'] == '1') {
?>							
								
								<?php echo $total_advert; ?> <?php echo (isset($pdata['page_year']) ? $pdata['page_year'] : '') ?> <?php echo $pdata['page_label'] ?> untuk dijual di <?php echo (isset($pdata['page_city']) ? $pdata['page_city'] : '') ?> <?php echo $pdata['page_location'] ?> - Page <?php echo $pdata['page'] ?> 
<?php
		} else if (isset($_REQUEST['chscars']) && $_REQUEST['chscars'] == '1') {
?>
								<?php echo ($total_advert != '' ? $total_advert.'辆' : ''); ?><?php echo (isset($pdata['page_year']) ? $pdata['page_year'] : '') ?><?php echo $pdata['page_label'] ?>出售在<?php echo (isset($pdata['page_city']) ? $pdata['page_city'] : '') ?><?php echo $pdata['page_location'] ?> - 第<?php echo $pdata['page'] ?>页
<?php
		} else if (isset($_REQUEST['superdeals']) && $_REQUEST['superdeals'] == '1') {
?>								
								<?php echo $total_advert; ?> <?php echo (isset($pdata['page_year']) ? $pdata['page_year'] : '') ?> SuperDeals <?php echo $pdata['page_label'] ?> for sale in <?php echo (isset($pdata['page_city']) ? $pdata['page_city'] : '') ?> <?php echo $pdata['page_location'] ?> - Page <?php echo $pdata['page'] ?>
<?php
		} else {
?>	
								<?php echo $total_advert; ?> <?php echo (isset($pdata['page_year']) ? $pdata['page_year'] : '') ?> <?php echo $pdata['page_label'] ?> for sale in <?php echo (isset($pdata['page_city']) ? $pdata['page_city'] : '') ?> <?php echo $pdata['page_location'] ?> - Page <?php echo $pdata['page'] ?>
<?php
		} 
	}
?>
        </p>
    </div>

    <div class="proto-slide" id="link-slider">
    	<div class="proto-slide-box">
    		<ul>
		    	<li><a href="/car" class="btn <?php if(strpos(Url::current(), '/car/index') !== false && Url::current() != '/car/index?bodyType=bt9') echo "active"; ?> choose__icon"><img src="/images/caricon.png" alt="Car icon"> Cars</a></li>
		    	<li><a href="/autopart" class="btn choose__icon"><img src="/images/autoparticon.png" alt="Autoparts icon"> Autoparts</a></li>
			    <li><a href="/numberplate" class="btn choose__icon"><img src="/images/numberplateicon.png" alt="Number plate icon"> Number plates</a></li>
			    <li><a href="/specialnumber" class="btn choose__icon"><img src="/images/phoneicon.png" alt="Phone number icon"> Phone number</a></li>
			    <li><a href="/car?bodyType=bt9" class="btn <?php if(Url::current() == '/car/index?bodyType=bt9') echo "active"; else echo ""; ?> choose__icon"><img src="/images/commercialicon.png" alt="Commercial cars icon"> Commercial cars</a></li>
			   	<li><a href="/bike" class="btn choose__icon"><img src="/images/motorbikeicon.png" alt="Motorbike icon"> Motorbike</a></li>
			   	<li><a href="/homeAndLI" class="btn choose__icon"><img src="/images/itineryicon.png" alt="Home and itinery icon"> Home and Living</a></li>
		   </ul>
	   </div>
	   <div class="proto-slide-arrow proto-slide-arrow-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>
		<div class="proto-slide-arrow proto-slide-arrow-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
	</div>

	<div class="list-collapse">
		<ul class="text-center">
	    	<li data-toggle="collapse" data-target="#buy"><a href="#" class="btn choose__icon-responsive"><img src="<?php if(strpos(Url::current(), '/car/index') !== false && Url::current() != '/car/index?bodyType=bt9') echo "/images/caricon.png"; else echo "/images/commercialicon.png" ?>" alt="<?php if(strpos(Url::current(), '/car/index') !== false && Url::current() != '/car/index?bodyType=bt9') echo "Car icon"; else echo "Commercial car icon" ?>"> <?php if(strpos(Url::current(), '/car/index') !== false && Url::current() != '/car/index?bodyType=bt9') echo "Cars"; else echo "Commercial cars" ?></a></li>
    	 	<div id="buy" class="collapse">
    	 		<li><a href="/car" class="btn <?php if(strpos(Url::current(), '/car/index') !== false && Url::current() != '/car/index?bodyType=bt9') echo "active"; ?> choose__icon-responsive"><img src="/images/caricon.png" alt="Car icon"> Cars</a></li>
		    	<li><a href="/autopart" class="btn choose__icon-responsive"><img src="/images/autoparticon.png" alt="Autoparts icon"> Autoparts</a></li>
			    <li><a href="/numberplate" class="btn choose__icon-responsive"><img src="/images/numberplateicon.png" alt="Number plate icon"> Number plates</a></li>
			    <li><a href="/contactNumber" class="btn choose__icon-responsive"><img src="/images/phoneicon.png" alt="Phone number icon"> Phone number</a></li>
			    <li><a href="/car?bodyType=bt9" class="btn <?php if(Url::current() == '/car/index?bodyType=bt9') echo "active"; else echo ""; ?> choose__icon-responsive"><img src="/images/commercialicon.png" alt="Commercial cars icon"> Commercial cars</a></li>
			   	<li><a href="/bike" class="btn choose__icon-responsive"><img src="/images/motorbikeicon.png" alt="Motorbike icon"> Motorbike</a></li>
			   	<li><a href="/homeAndLI" class="btn choose__icon-responsive"><img src="/images/itineryicon.png" alt="Home and itinery icon"> Home and Living</a></li>
		   </div>
	   </ul>
	</div>

	<div class="car-search__row">
		<div class="row no-gutters">
			<div class="col-lg-3">
				<div class="car-search__column">
					<div class="row no-gutters">
						<div class="col-lg-12 col-7">
							<div class="car-search__border">
								<img src="/images/search.png" class="car-search-feedback" alt="Search icon">
								<input type="text" name="keyword" id="" class="form-control car-search-text" placeholder="Search" value="<?php echo (isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '') ?>" autocomplete="off" onkeyup="if (event.keyCode === 13) { window.location = 'car?keyword=' + this.value; }"/>
							</div>
						</div>
						<div class="col-lg-12 col-5">
							<div class="car-search__column-responsive">
								<div class="row">
									<div class="col-6">
										<p>
											<span class="header__burger"></span>
									        <span class="header__burger header__burger--adjust"></span>
									        <span class="header__burger"></span>
										</p>
									</div>
									<div class="col-6">
										<p class="car-search__filter-text">Filter</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="car-search__desktop">
<?php
	if ($pdata['right_car_description'] !== false) {
?>
						<div class="car-search__border">
							<p class="car-search__clear car-search__clear--margin">Learn more about 
								<img id="car-description-open" <?php echo ( (isset($_REQUEST['superdeals']) && $_REQUEST['superdeals'] != '' ) || ( isset($_REQUEST['video']) && $_REQUEST['video'] != '' ) ) ? 'src="/images/close.png"' : 'src="/images/plus.png"'; ?> >
							</p>
							<div class="d-none" id="car-description-show"><?php echo $pdata['right_car_description']; ?></div>
						</div>
<?php
	}
?>						
						<div class="car-search__border">
							<p class="car-search__clear">
								<span style="font-size: 25px;">Filter</span> <span><a href="/car" class="car-search__link">Clear all</a></span>
							</p>
						</div>
						<div class="car-search__border">
							<p class="car-search__clear car-search__clear--margin">Show with 
								<img id="car-filter-open" <?php echo ( (isset($_REQUEST['superdeals']) && $_REQUEST['superdeals'] != '' ) || ( isset($_REQUEST['video']) && $_REQUEST['video'] != '' ) ) ? 'src="/images/close.png"' : 'src="/images/plus.png"'; ?> >
							</p>
							<p class="<?php echo ( (isset($_REQUEST['superdeals']) && $_REQUEST['superdeals'] != '' ) || ( isset($_REQUEST['video']) && $_REQUEST['video'] != '' ) ) ? '' : 'd-none'; ?>" id="car-filter-show">
								<a class="btn car-search__button <?php echo (isset($_REQUEST['superdeals']) && $_REQUEST['superdeals'] != '') ? 'active' : ''; ?>" href="<?php echo Yii::$app->general->generatecarsseo('car', 0, 0, '', 0, '', '', '', 1, []); ?>">Super Deals</a> 
								<a class="btn car-search__button <?php echo (isset($_REQUEST['video']) && $_REQUEST['video'] != '') ? 'active' : ''; ?>" href="<?php echo Yii::$app->general->generatecarsseo('car', 0, 0, '', 0, '', '', '', 0, ['video' => 1]); ?>">Video</a> 
								<a class="btn car-search__button " href="<?php echo Yii::$app->general->generatecarsseo('car', 0, 0, '', 0, '', '', '', 0, ['voc' => 1]); ?>">Registration cards</a> 
								<a class="btn car-search__button <?php echo (isset($_REQUEST['dealers_type']) && $_REQUEST['dealers_type'] != '') ? 'active' : ''; ?>" href="<?php echo Yii::$app->general->generatecarsseo('car', 0, 0, '', 0, '', '', '', 0, ['dealers_type' => 1]); ?>">Verified dealers</a>
							</p>
						</div>
						<div class="car-search__border">
							<p class="car-search__clear car-search__clear--margin">Condition <img id="car-condition-open" <?php echo (isset($_REQUEST['type']) && $_REQUEST['type'] != '') ? 'src="/images/close.png"' : 'src="/images/plus.png"'; ?>></p>
							<p class="<?php echo (isset($_REQUEST['type']) && $_REQUEST['type'] != '') ? '' : 'd-none'; ?>" id="car-condition-show">
								<a class="btn car-search__button <?php echo (isset($_REQUEST['type']) && $_REQUEST['type'] == 'used') ? 'active' : ''; ?>" href="<?php echo $pdata['url_type_used']; ?>">Used car</a> 
								<a class="btn car-search__button <?php echo (isset($_REQUEST['type']) && $_REQUEST['type'] == 'new') ? 'active' : ''; ?>" href="<?php echo $pdata['url_type_new']; ?>">New car</a> 
								<a class="btn car-search__button <?php echo (isset($_REQUEST['type']) && $_REQUEST['type'] == 'recond') ? 'active' : ''; ?>" href="<?php echo $pdata['url_type_recond']; ?>">Recon car</a></p>
						</div>
						<div class="car-search__border">
							<p class="car-search__clear car-search__clear--margin">Car make <img id="car-make-open" <?php echo ((isset($_REQUEST['make']) && $_REQUEST['make'] != '' )) ? 'src="/images/close.png"' : 'src="/images/plus.png"'; ?>></p>
<?php
	$make_array = $master_car_make->getmake();

	if (count($make_array)) {
?>		
							<p class="<?php echo ( (isset($_REQUEST['make']) && $_REQUEST['make'] != '' ) ) ? '' : 'd-none'; ?>" id="car-make-show">
<?php
		foreach ($make_array as $make_id_loop => $make_data_loop) {
?>				
								<a class="btn car-search__button <?php echo ( (isset($_REQUEST['make']) && $_REQUEST['make'] == $make_id_loop ) ) ? 'active' : ''; ?>" href="/car?make=<?php echo $make_id_loop ?>" id="car-search__load-<?php echo $make_id_loop ?>"><?php echo $make_data_loop; ?></a>
<?php
		}
?>
							</p>
<?php
	}
?>
						</div>
<?php
	if ($pdata['make_filter_name']) {
		$model_group_array = $master_car_model_group->getmodelgroup($pdata['make_filter_cd']);
		$display_car_array = $trans_order->getpopularmodel($pdata['make_filter_cd']);

		$model_group_list = [];
		$model_group_total = 0;
		if (count($model_group_array)) {
			foreach ($model_group_array as $model_group_array_loop => $model_group_array_data_loop) {
				$group_name = $model_group_array_data_loop['group_name'];
				$model_group_total = 0;
				foreach ($model_group_array_data_loop['model_group'] as $model_group_key_loop => $model_group_data_loop) {
					if (count($display_car_array)) {
						foreach ($display_car_array as $display_car_model_data_loop => $total) {
							if ($display_car_model_data_loop == $model_group_data_loop) {
								$model_group_total += $total;
								$model_group_list[$group_name] = $display_car_model_data_loop;
								$display_car_array = $model_group_list + $display_car_array;
								unset($display_car_array[$display_car_model_data_loop]);
							}
						}
					}
				}
			}
		}

		if (count($display_car_array)) {
?>					
						<div class="car-search__border">
							<p class="car-search__clear car-search__clear--margin">Car models <img id="car-model-open" <?php echo ( (isset($_REQUEST['model']) && $_REQUEST['model'] != '' ) ) ? 'src="/images/close.png"' : 'src="/images/plus.png"'?>></p>
							<p class="<?php echo ( (isset($_REQUEST['model']) && $_REQUEST['model'] != '' ) ) ? '' : 'd-none'?>" id="car-model-show">
<?php
				foreach ($display_car_array as $display_car_model_data_loop => $model_cd) {
					
					$car_model_name = $master_car_model->getmodelname($display_car_model_data_loop);
					if (isset($car_model_name) && trim($car_model_name)) {
						$model_name = $car_model_name;
						$model_link = Yii::$app->general->generateseo('brand', $pdata['filter']['make'] . '&model=' . $model_cd);
					} else {
						$model_name = $display_car_model_data_loop;
						$model_link = Yii::$app->general->generateseo('brand', $pdata['filter']['make'] . '&model=' . $model_cd);
					}
?>						
								<a class="btn car-search__button <?php echo ( (isset($_REQUEST['model']) && $_REQUEST['model'] == $model_cd ) ) ? 'active' : ''?>" href="<?php echo $model_link; ?>"><?php echo $model_name; ?></a>
<?php
				}
?>						
							</p>
						</div>	
<?php
		}
	}
	if ($pdata['model_filter_name']) {
		$car_model = $master_car_variant->find()->where(['model_cd' => $pdata['model_filter_cd']])->all();

		if(count($car_model)) {
?>
						<div class="car-search__border">
							<p class="car-search__clear car-search__clear--margin" >Car variant <img id="car-variant-open" <?php echo ( (isset($_REQUEST['variant']) && $_REQUEST['variant'] != '' ) ) ? 'src="/images/close.png"' : 'src="/images/plus.png"'?>></p>
							<p class="<?php echo ( (isset($_REQUEST['variant']) && $_REQUEST['variant'] != '' ) ) ? '' : 'd-none'?>" id="car-variant-show">
<?php
			foreach ($car_model as $car_model_loop => $car_variant) {
				$variant_link = Yii::$app->general->generateseo('brand', $pdata['filter']['make'] . '&model=' . strtoupper($model_cd) . '&variant='. $car_variant->variant_cd);
?>
								<a class="btn car-search__button <?php echo ( (isset($_REQUEST['variant']) && $_REQUEST['variant'] == $car_variant->variant_cd ) ) ? 'active' : ''?>" href="<?php echo $variant_link; ?>"><?php echo $car_variant->variant_name; ?></a>				
					
<?php
			}
?>
							</p>
						</div>
<?php
		}
	}
?>					
						<div class="car-search__border">
							<p class="car-search__clear">Car type <img id="car-type-open" <?php echo (isset($_REQUEST['bodyType']) && $_REQUEST['bodyType'] != '' ) ? 'src="/images/close.png"' : 'src="/images/plus.png"'; ?>></p>
				
<?php
	$body_array = $master_general->getBodyType();
	if (count($body_array)) {
?>			
							<p class="<?php echo ( (isset($_REQUEST['bodyType']) && $_REQUEST['bodyType'] != '' ) ) ? '' : 'd-none'?>" id="car-type-show">
<?php
		foreach ($body_array as $body_array_id_loop => $body_array_data_loop) {
?>				
								<a class="btn car-search__button <?php echo ( (isset($_REQUEST['bodyType']) && $_REQUEST['bodyType'] == $body_array_id_loop ) ) ? 'active' : ''?>" href="<?php echo Yii::$app->general->generatecarsseo('car', 0, 0, '', 0, '', '', '', 0, ['bodyType' => $body_array_id_loop]); ?>"><?php echo $body_array_data_loop ?></a>
<?php
		}
?>				
							</p>
<?php
	}
?>
						</div>
						<div class="car-search__border">
							<p class="car-search__clear car-search__clear--margin">Price</p>
							<div id="slider-range">
								<div id="custom-handle" class="ui-slider-handle"></div>
								<div id="custom-handle-2" class="ui-slider-handle"></div>
							</div>
							<p class="car-search__margin">
								<a class="btn car-search__slide" href="#">
									<span class="car-search__slide-rm">RM</span><br>
									<span id="minPrice"></span>
								</a>
								<img src="/images/blueline.png" class="car-search__slide-to" alt="To">
								<a class="btn car-search__slide" href="#">
									<span class="car-search__slide-rm">RM</span><br>
									<span id="maxPrice"></span>
								</a>
							</p>
						</div>
						<div class="car-search__border">
							<p class="car-search__clear car-search__clear--margin">Year</p>
							<div id="slider-range-2">
								<div id="custom-handle-3" class="ui-slider-handle"></div>
								<div id="custom-handle-4" class="ui-slider-handle"></div>
							</div>
							<p class="car-search__margin">
								<a class="btn car-search__slide car-search__slide--position" href="#">
									<span id="minYear"></span>
								</a>
								<img src="/images/blueline.png" class="car-search__slide-to" alt="To">
								<a class="btn car-search__slide car-search__slide--position" href="#">
									<span id="maxYear"></span>
								</a>
							</p>
						</div>												
						<div class="car-search__border">
							<p class="car-search__clear car-search__clear--margin">Mileage</p>
							<div id="slider-range-3">
								<div id="custom-handle-5" class="ui-slider-handle"></div>
								<div id="custom-handle-6" class="ui-slider-handle"></div>
							</div>
							<p class="car-search__margin">
								<a class="btn car-search__slide" href="#">
									<span class="car-search__slide-rm">KM</span><br>
									<span id="minMileage"></span>
								</a>
								<img src="/images/blueline.png" class="car-search__slide-to" alt="To">
								<a class="btn car-search__slide" href="#">
									<span class="car-search__slide-rm">KM</span><br>
									<span id="maxMileage"></span>
								</a>
							</p>
						</div>
						<!-- <p>Seller type</p>
						<p><a class="btn" href="<?php echo Yii::$app->general->generatecarsseo('car', 0, 0, '', 0, '', '', '', 0, ['dealers_type' => 2]); ?>">Dealer</a> <a class="btn" href="<?php echo Yii::$app->general->generatecarsseo('car', 0, 0, '', 0, '', '', '', 0, ['dealers_type' => 3]); ?>">Private Owner</a></p>
						<hr/> -->
						<div class="car-search__border">
							<p class="car-search__clear car-search__clear--margin">State <img id="car-state-open" <?php echo (isset($_REQUEST['location']) && $_REQUEST['location'] != '' ) ? 'src="/images/close.png"' : 'src="/images/plus.png"'; ?>></p>
<?php
	$location_array = $master_general->getLocation();
	if (count($location_array)) {
?>			
							<p class="<?php echo (isset($_REQUEST['location']) && $_REQUEST['location'] != '' ) ? '' : 'd-none'; ?>" id="car-state-show">
<?php
		foreach ($location_array as $location_array_id_loop => $location_array_data_loop) {
?>
								<a class="btn car-search__button <?php echo (isset($_REQUEST['location']) && $_REQUEST['location'] == $location_array_id_loop ) ? 'active' : ''; ?>" href="<?php echo Yii::$app->general->generatecarsseo('car', '', '', $location_array_id_loop, 0); ?>"><?php echo $location_array_data_loop; ?></a>
<?php
		}
?>
							</p>
<?php
	}
?>					
						</div>
						<div class="car-search__border">
							<p class="car-search__clear car-search__clear--margin">Transmission <img id="car-transmission-open" <?php echo (isset($_REQUEST['transmission']) && $_REQUEST['transmission'] != '' ) ? 'src="/images/close.png"' : 'src="/images/plus.png"'; ?>></p>
							<p class="<?php echo (isset($_REQUEST['transmission']) && $_REQUEST['transmission'] != '' ) ? '' : 'd-none'; ?>" id="car-transmission-show"><a class="btn car-search__button <?php echo ( (isset($_REQUEST['transmission']) && $_REQUEST['transmission'] == 'at' ) ) ? 'active' : ''?>" href="<?php echo Yii::$app->general->generatecarsseo('car', 0, 0, '', 0, '', '', '', 0, ['transmission' => 'at']); ?>">Automatic</a> <a class="btn car-search__button <?php echo ( (isset($_REQUEST['transmission']) && $_REQUEST['transmission'] == 'mn' ) ) ? 'active' : ''?>" href="<?php echo Yii::$app->general->generatecarsseo('car', 0, 0, '', 0, '', '', '', 0, ['transmission' => 'mn']); ?>">Manual</a></p>
						</div>
						<!-- <div class="car-search__border">
							<p class="car-search__clear car-search__clear--margin">Fuel <img id="car-fuel-open" src="/images/plus.png"></p>
							<p class="d-none" id="car-fuel-show"><a class="btn car-search__button" href="<?php echo Yii::$app->general->generatecarsseo('car', 0, 0, '', 0, '', '', '', 0, ['fuel' => 'petrol']); ?>">Petrol</a></p>
						</div>
						<div class="car-search__border">
							<p class="car-search__clear car-search__clear--margin">Driven wheel <img id="car-wheel-open" src="/images/plus.png"></p>
							<p class="d-none" id="car-wheel-show"><a class="btn car-search__button" href="<?php echo Yii::$app->general->generatecarsseo('car', 0, 0, '', 0, '', '', '', 0, ['wheel' => '1']); ?>">Wheel</a></p>
						</div> -->
					</div>						
				</div>
			</div>
			<div class="col-lg-9">
				<div class="car-list">
					<div class="car-list__head">				
						<div class="row">
							<div class="col-lg-6">
								<p class="car-list__show">Showing <?php echo $total_advert; ?> results</p>
							</div>
							<div class="col-lg-6">
								<p class="car-list__sort">
									Sort by 
									<select class="form-control" onchange="location = this.value;">
										<option <?php if ($pdata['sort'] == '') echo "selected"; ?> disabled>Select one</option>
										<option <?php if($pdata['sort'] == 'updatehigh') echo "selected"; ?> value="<?php echo $pdata['url_sort_update_low']; ?>">Recent (latest first)</option>
										<option <?php if($pdata['sort'] == 'updatelow') echo "selected"; ?> value="<?php echo $pdata['url_sort_update_high']; ?>">Recent (earliest first)</option>
										<option <?php if (in_array($pdata['sort'], ['pricehigh', 'htl'])) echo "selected"; ?> value="<?php echo $pdata['url_sort_price_high']; ?>">Price (highest first)</option>
										<option <?php if (in_array($pdata['sort'], ['pricelow', 'lth'])) echo "selected"; ?> value="<?php echo $pdata['url_sort_price_low']; ?>">Price (lowest first)</option>
										<option <?php if($pdata['sort'] == 'yearhigh') echo "selected"; ?> value="<?php echo $pdata['url_sort_year_high']; ?>">Year (latest first)</option>
										<option <?php if($pdata['sort'] == 'yearlow') echo "selected"; ?> value="<?php echo $pdata['url_sort_year_low']; ?>">Year (earliest first)</option>
									</select>
								</p>
							</div>
						</div>
					</div>
<?php
	if (isset($pdata['featured']) && count($pdata['featured'])) {
?>
					<div class="row">
<?php
		$count = 0;
		foreach ($pdata['featured'] as $featured_data_loop) {
			$count++;

			if ($featured_data_loop['poa'] == 1) {
				$price = 'POA';
			} else {
				$price = '<small class="featured-ads-section__price-currency">RM</small> ' . number_format($featured_data_loop['price']);

				$deposit = 10;
				$interest = 3.69;
				if ($featured_data_loop['is_used_flg'] == '2') {		
					$term = 9;
				} else {
					$term = 7;
				}
				$interestRate = $interest / 100;    
				$oriPrice = str_replace(",","", number_format($featured_data_loop['price']));
				$depositAmount = $oriPrice * ($deposit / 100);
				$loanPaidAmount = $oriPrice - $depositAmount; 
				$interestPaidAmount = ($term * $interestRate) * $loanPaidAmount;                            
				$month = ($interestPaidAmount + $loanPaidAmount) / ($term * 12);
				$month = number_format(round($month));
			}
?>
						<div class="col-lg-4">
							<div class="superdeals-featured-cnabadv__featured-ads-section-listing superdeals-featured-cnabadv__featured-ads-section-listing--adjust">
								<p class="car-list__position">
				                	<a href="<?php echo $featured_data_loop['seourl'] ?>&utm_medium=FeaturedAds_Listing">
				                		<img class="featured-ads-section__img lazyload" src="<?php echo Yii::$app->fpath->images("ajax-loader.gif"); ?>" data-src="<?php echo (@getimagesize($featured_data_loop['image'])) ? $featured_data_loop['image'] : ($featured_data_loop['bodytype_code'] == 'bt9') ? '/images/bus.png' : '/images/mercedes.png'; ?>" alt="<?php echo $featured_data_loop['make_name'] . ' ' . $featured_data_loop['model_name']; ?>">
				                	</a>
				                	<button class="btn superdeals-featured-cnabadv__insights-btn superdeals-featured-cnabadv__insights-btn--position">Featured</button>
									<span class="car-list__bookmark <?php echo (in_array($featured_data_loop['urn_no'], $pdata['bookmark_array']) ? 'car-list__show-bookmark' : '') ?>" data-urn="<?php echo $featured_data_loop['urn_no'] ?>"><img class="car-list__none" src="/images/bookmark.png"><img class="car-list__have" src="/images/bookmarked.png"></span>
								</p>
<?php
			$car_video = Yii::$app->api->getvideo($featured_data_loop['urn_no']);
			if ($car_video !== false)	{
?>
								<div class="adv-video-icon">
									<div class="icons-video float-right" onclick="popupvideo('<?php echo $car_video['video_url'] ?>', '<?php echo addslashes($car_video['video_title']) ?>')">
										<img src="/images/vid_ico.png" alt="Video icon">
									</div>
								</div>
<?php
			}
?>								
								<p class="text-center featured-ads-section__title"><a href="<?php echo $featured_data_loop['seourl'] ?>&utm_medium=FeaturedAds_Listing"><?php echo $featured_data_loop['make_name'] . ' ' . $featured_data_loop['model_name']; ?></a></p>
								<div class="row">
									<div class="col-4">
										<p class="featured-ads-section__month">RM <?php echo $month; ?> / month</p>
									</div>
									<div class="col-8">
										<p class="featured-ads-section__price text-right"><?php echo $price; ?></p>
									</div>
								</div>
								<hr>
								<div class="row">
			                        <div class="col-6">
			                            <div class="row">
			                                <div class="col-5">
			                                    <p><img class="featured-ads-section__icon" src="images/calendar.png" alt="calendar" /></p>
			                                </div>
			                                <div class="col-7">
			                                    <span class="featured-ads-section__text">Year</span>
			                                    <p><?php echo $featured_data_loop['year_make']; ?></p>
			                                </div>
			                            </div>
			                        </div>
			                        <div class="col-6">
			                            <div class="row">
			                                <div class="col-5">
			                                    <p><img class="featured-ads-section__icon" src="images/location.png" alt="location" /></p>
			                                </div>
			                                <div class="col-7">
			                                    <span class="featured-ads-section__text">Location</span>
			                                    <p><?php echo $featured_data_loop['location_name']; ?></p>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                    <div class="row">
			                        <div class="col-6">
			                            <div class="row">
			                                <div class="col-5">
			                                    <p><img class="featured-ads-section__icon" src="images/mileage.png" alt="mileage" /></p>
			                                </div>
			                                <div class="col-7">
			                                    <span class="featured-ads-section__text">Mileage</span>
			                                    <p><?php echo $featured_data_loop['mileage_name']; ?></p>
			                                </div>
			                            </div>
			                        </div>
			                        <div class="col-6">
			                            <div class="row">
			                                <div class="col-5">
			                                    <p><img class="featured-ads-section__icon" src="images/transmission.png" alt="transmission" /></p>
			                                </div>
			                                <div class="col-7">
			                                    <span class="featured-ads-section__text">Transmission</span>
			                                    <p><?php echo $featured_data_loop['transmission_name']; ?></p>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
							</div>
						</div>
<?php
		}
?>			
					</div>
<?php
		if (count($pdata['featured']) > 3) {
?>			
					<p class="text-center featured-ads-section__page"><img src="/images/previous.png" alt="Previous"> <span>1/2</span> <img src="/images/next.png" alt="Next"></p>
<?php
		}
	}
?>			
					<div class="row">
<?php
	$gk_frame_array = ['1'=>'-',
							'2'=>'Needs Rectification',
							'3'=>'Acceptable',
							'4'=>'Good',
							'5'=>'Excellent'
					];

	$gk_engine_array = [	'1'=>'-',
								'2'=>'-',
								'3'=>'-',
								'4'=>'-',
								'5'=>'Good'
								];

	if (count($trans_advert_model)) {
		$count_advert = 0;
		foreach ($trans_advert_model as $trans_advert_data_loop) {
			$count_advert++;
			$next_offset = ((($pdata['page']-1) * $pdata['limit']) + $count_advert);

			$seo_url = Yii::$app->general->generateseo('car', $trans_advert_data_loop->urn_no, $trans_advert_data_loop->make_name . '-' . $trans_advert_data_loop->model_name);
			
			$advert_image = Yii::$app->general->advertimageurl($trans_advert_data_loop->urn_no . '.jpg', 'webad');
			
			$voc = '';
			$extra_info_array = [];

			$extra_info_array = $trans_order_extra_info->getadvertextrainfo($trans_advert_data_loop->urn_no);

			if (isset($extra_info_array['vehicle_cert']) && $extra_info_array['vehicle_cert']!= '') {
				$voc = $extra_info_array['vehicle_cert'];
			}
			
			$year_make = ((int)date("Y") - (int)$trans_advert_data_loop->year_make);
			
			$master_dealer_model = $master_dealer->getdealer($trans_advert_data_loop->dealer_id);
			
			$trans_advert_super_deals_model = $super_deals->getsuperdeals($trans_advert_data_loop->urn_no);
			
			$has_super_deals = $trans_advert_super_deals_model['got_super_deals'];
			
?>				
						<div class="col-lg-4">
							<div class="superdeals-featured-cnabadv__featured-ads-section-listing superdeals-featured-cnabadv__featured-ads-section-listing--adjust <?php echo $has_super_deals === '1' ? 'superdeals-featured-cnabadv__featured-ads-section-listing--border' : ''; ?>">
								<p class="car-list__position">
			                    	<a href="<?php echo $seo_url; ?>">
			                    		<img class="featured-ads-section__img lazyload" src="<?php echo Yii::$app->fpath->images("ajax-loader.gif"); ?>" data-src="<?php echo (@getimagesize($advert_image)) ? $advert_image : ($trans_advert_data_loop->bodytype_code == 'bt9') ? '/images/bus.png' : '/images/mercedes.png'; ?>" alt="<?php echo $trans_advert_data_loop->make_name . ' ' . $trans_advert_data_loop->model_name; ?>">
			                    	</a>
<?php 		
				if($has_super_deals === '1') { 
?>
									<button class="btn superdeals-featured-cnabadv__insights-btn superdeals-featured-cnabadv__insights-btn--position">Super Deals</button>
<?php 
				} 
?>
									<span class="car-list__bookmark <?php echo (in_array($trans_advert_data_loop->urn_no, $pdata['bookmark_array']) ? 'car-list__show-bookmark' : '') ?>" data-urn="<?php echo $trans_advert_data_loop->urn_no ?>"><img class="car-list__none" src="/images/bookmark.png"><img class="car-list__all" src="/images/bookmarked.png"></span>
								</p>
<?php
				$car_video = Yii::$app->api->getvideo($trans_advert_data_loop->urn_no);
				if ($car_video !== false)	{
?>
								<div class="adv-video-icon">
									<div onclick="popupvideo('<?php echo $car_video['video_url'] ?>', '<?php echo addslashes($car_video['video_title']) ?>')">
										<img src="/images/vid_ico.png" alt="Video icon">
									</div>
								</div>
<?php
				}
?>								
	                    		<p class="text-center featured-ads-section__title"><a href="<?php echo $seo_url; ?>"><?php echo $trans_advert_data_loop->make_name . ' ' . $trans_advert_data_loop->model_name; ?></a></p>
<?php
				if (isset($trans_advert_super_deals_model) && date("Y-m-d H:i:s") >= $trans_advert_super_deals_model['super_deals_schedule_start_date'] && date("Y-m-d H:i:s") <= $trans_advert_super_deals_model['super_deals_schedule_end_date']) {
?>                    
	                    		<div class="row no-gutters">

 <?php
					if ($trans_advert_super_deals_model['sale_price'] == 0) {
						$car_price =  'POA';
					} else {
						if (trim($voc)) {
							$car_price =  number_format($trans_advert_super_deals_model['sale_price']); 
						} else {
							$car_price =  number_format($trans_advert_super_deals_model['sale_price']); 
						}
						
						$deposit = 10;
						$interest = 3.69;
					if ($trans_advert_data_loop->is_used_flg == '2') {		
						$term = 9;
					}else{
						$term = 7;
					}
						$interestRate = $interest / 100;    
						$oriPrice = str_replace(",","", $trans_advert_super_deals_model['sale_price']);
						$depositAmount = $oriPrice * ($deposit / 100);
						$loanPaidAmount = $oriPrice - $depositAmount; 
						$interestPaidAmount = ($term * $interestRate) * $loanPaidAmount;                            
						$month = ($interestPaidAmount + $loanPaidAmount) / ($term * 12);
						$month = number_format(round($month));
					}
					
					if ($trans_advert_super_deals_model['sale_price'] != $trans_advert_super_deals_model['regular_price']) {
						$car_price = number_format($trans_advert_super_deals_model['regular_price']);
					} 			
?>
								</div>						
<?php
				} else {

?>
		                		<div class="row">

<?php
					if ($trans_advert_data_loop->price == 0) {
						$car_price =  'POA';
					} else {
						if (trim($voc)) {
							$car_price = '<small class="featured-ads-section__price-currency">RM</small> ' . number_format($trans_advert_data_loop->price); 
						} else {
							$car_price = '<small class="featured-ads-section__price-currency">RM</small> ' . number_format($trans_advert_data_loop->price); 
						}
						
						$deposit = 10;
						$interest = 3.69;
					if ($trans_advert_data_loop->is_used_flg == '2') {		
						$term = 9;
					}else{
						$term = 7;
					}
						$interestRate = $interest / 100;    
						$oriPrice = str_replace(",","", $trans_advert_data_loop->price);
						$depositAmount = $oriPrice * ($deposit / 100);
						$loanPaidAmount = $oriPrice - $depositAmount; 
						$interestPaidAmount = ($term * $interestRate) * $loanPaidAmount;                            
						$month = ($interestPaidAmount + $loanPaidAmount) / ($term * 12);
						$month = number_format(round($month));
					}
				}

		if ($year_make < 13 || $year_make == date("Y")) {				
?>                    	
	                        		<div class="col-4">
<?php
			if (isset($trans_advert_super_deals_model) && date("Y-m-d H:i:s") >= $trans_advert_super_deals_model['super_deals_schedule_start_date'] && date("Y-m-d H:i:s") <= $trans_advert_super_deals_model['super_deals_schedule_end_date']) {
				if ($trans_advert_super_deals_model['sale_price'] == 0) {
?>				
										<p class="featured-ads-section__month">-</p>
<?php				
				} else {
?>
			                    		<p class="featured-ads-section__month">RM <?php echo $month; ?> / month</p>
<?php
				} 
			} else {											
				if ($trans_advert_data_loop->price == 0) {
?>				
										<p class="featured-ads-section__month">-</p>
<?php	
				} else {
?>				
								 		<p class="featured-ads-section__month">RM <?php echo $month; ?> / month</p>
<?php
				}										
			}
?>
									</div>
<?php							
		} else {
?>
									<div class="col-4">
										<p class="featured-ads-section__month">-</p>
									</div>
<?php		
		}
?>	                           
			                        <div class="col-8 text-right">
			                            <p class="featured-ads-section__price"><?php echo $car_price; ?></p>
			                        </div>
			                    </div>
			                    <hr/>
			                    <div class="row">
			                        <div class="col-6">
			                            <div class="row no-gutters">
			                                <div class="col-6">
			                                    <p><img class="featured-ads-section__icon" src="images/calendar.png" alt="calendar" /></p>
			                                </div>
			                                <div class="col-6">
			                                    <span class="featured-ads-section__text">Year</span>
			                                    <p><?php echo $trans_advert_data_loop->year_make; ?></p>
			                                </div>
			                            </div>
			                        </div>
			                        <div class="col-6">
			                            <div class="row no-gutters">
			                                <div class="col-6">
			                                    <p><img class="featured-ads-section__icon" src="images/location.png" alt="location" /></p>
			                                </div>
			                                <div class="col-6">
			                                    <span class="featured-ads-section__text">Location</span>
			                                    <p><?php echo $trans_advert_data_loop->location_name; ?></p>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                    <div class="row">
			                        <div class="col-6">
			                            <div class="row no-gutters">
			                                <div class="col-6">
			                                    <p><img class="featured-ads-section__icon" src="images/mileage.png" alt="mileage" /></p>
			                                </div>
			                                <div class="col-6">
			                                    <span class="featured-ads-section__text">Mileage</span>
			                                    <p><?php echo $trans_advert_data_loop->mileage_name; ?></p>
			                                </div>
			                            </div>
			                        </div>
			                        <div class="col-6">
			                            <div class="row no-gutters">
			                                <div class="col-6">
			                                    <p><img class="featured-ads-section__icon" src="images/transmission.png" alt="transmission" /></p>
			                                </div>
			                                <div class="col-6">
			                                    <span class="featured-ads-section__text">Transmission</span>
			                                    <p><?php echo $trans_advert_data_loop->transmission_name; ?></p>
			                                </div>
			                            </div>
			                        </div>
			                    </div>	                    
			                </div>
						</div> 
<?php
		}
	}
?>                
					</div>
<?php
	if ($pdata['total'] > 0) {
		$pagination_range = 5;

        $start_page = $pdata['page'] - floor($pagination_range / 2);
        if ($start_page < 1) {
            $start_page = 1;
        }

        $total_end_page = ceil($pdata['total'] / $pdata['limit']);

        $end_page = $pdata['page'] + floor($pagination_range / 2);
        if ($end_page > $total_end_page) {
            $end_page = $total_end_page;
        }

        if (($end_page - $pdata['page']) < floor($pagination_range / 2) && $pagination_range < floor($pdata['total'] / $pdata['limit'])) {
            $start_page = ceil($pdata['total'] / $pdata['limit']) - $pagination_range;
        }

        if ($end_page < $pagination_range && floor($pdata['total'] / $pdata['limit']) >= $pagination_range) {
            $end_page = $pagination_range;
        }
?>
					<div>
						<p class="text-center featured-ads-section__page">
<?php
		if ($pdata['page'] > 1) {
			$processed_link = str_replace('__PAGE__', ($pdata['page'] - 1), $pdata['url_pagination']);
?>					
							<span class="<?php echo ($start_page > 1 ? '' : 'd-none-lg-up'); ?>"><a href="<?php echo $processed_link; ?>"><img src="/images/previous.png" alt="Previous"></a>
<?php
		}
?>					
					<span><?php echo $pdata['page'] . ' / ' . $end_page; ?></span>
<?php
		if ($pdata['page'] < ceil($pdata['total'] / $pdata['limit']) ) {
			$processed_link = str_replace('__PAGE__', ($pdata['page'] + 1), $pdata['url_pagination']);
?>					
							<a href="<?php echo $processed_link; ?>"><img src="/images/next.png" alt="Next"></a></span>
<?php
		}
?>				
						</p>
						</div>
<?php
	}
?>
						<div class="jumbotron--field">
							<p><img src="/images/usedcar.png" alt="Used car" class="car-list__used"></p>
						    <div class="jumbotron jumbotron--layer">
						    	<p>Used cars buying tips</p>
						        <p class="text-center jumbotron__button"><a class="btn banner-search__search-btn" href="/car/tips"><span class="banner-search__search-btn-text">Read more</span></a></p>
						    </div>
					    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="popupFrame">
	<div id="videoForm">
		<div id="title">
			<span id="video-title">Video</span>
			<div id="closeBtn" onClick="closeVideo()">X</div>
		</div>
		<div id="video-popup-frame-container">		
			<object id="popup-video-object" controls autoplay muted width="100%" height="100%" class="video img img-fluid video-popup-frame"></object>
			<video id="popup-video-video" controls autoplay muted width="100%" height="100%" class="video img img-fluid video-popup-frame"></video>
		</div>
	</div>
</div>
