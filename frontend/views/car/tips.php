<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = 'Used car buying tips';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page_home">
	<h1 class="banner-search__title text-center">Important Tips for Buying a Used Car</h1>
	<p class="text-center used-car__subtitle">Last updated at 23 Nov 2020</p>

	<iframe class="responsive-iframe" width="100%" height="1078" src="https://www.youtube.com/embed/hCUa7EKfLJU">
	</iframe>

	<div class="used-car__content">
		 <div class="row">
		 	<div class="col-2 col-lg-1">
		 		<p><img class="used-car__content-social used-car__content-social--margin" src="/images/facebook.png" alt="Facebook icon"/></p>
		 		<p><img class="used-car__content-social" class="used-car__content-social" src="/images/twitter.png" alt="Twitter icon"/></p>
		 		<p><img class="used-car__content-social" src="/images/linkedin.png" alt="LinkedIn icon"/></p>
		 		<p><img class="used-car__content-social" src="/images/whatsapp.png" alt="Whatsapp icon"/></p>
		 	</div>
		 	<div class="col-10 col-lg-11">
		 		<p class="used-car__content-initial">Buying a used car can be a scary process, especially if you aren’t sure of what you are doing. Here are 7 simple guides to help you out.</p>
		 		<h1 class="used-car__content-tip">#1, Never pay deposit without seeing the car.</h1>
		 		<p class="used-car__content-explain">We can’t ensure all dealerships and salesmen are honest. To protect yourself from getting conned, never pay deposit first, and only work with only reputable dealers. A legit dealer or salesman would provide you advices on the car you’re interested in, which suits your needs the most and if it’s good value.</p>
		 		<p><img class="used-car__content-image" src="/images/tip1.png" alt="Tip 1"></p>
		 		<h1 class="used-car__content-tip">#2, Visit seller to test drive the car.</h1>
		 		<p class="used-car__content-explain">Bring a friend along with you and take turns test drive, so that you’ll have an immediate second opinion from him/her. Here’s a checklist for you while test drive:</p>
				<ul class="used-car__content-explain">
					<li>Check on the ground if there’s stains when you leave the parking area</li>
					<li>Electronic windows, wipers, indicators, headlights, speedometer, fuel meter and so on</li>
					<li>Check if there’s any unusual sounds by switching off the radio and aircond</li>
					<li>Alignment of steering and power steering</li>
					<li>Absorbers</li>
				</ul>
				<p class="used-car__content-explain">If your salesman refuses to let you test drive, you may just walk away.</p>
				<p><img class="used-car__content-image" src="/images/tip2.png" alt="Tip 2"></p>
				<h1 class="used-car__content-tip">#3, Check The Car’s Historical Details</h1>
				<p class="used-car__content-explain">It’s always important to check on the car’s historical details. For eg. Service history. This is to ensure the car is serviced on time and verify the car’s exact mileage. Check if there’s any outstanding loan, ensure it’s fully settled before the change of ownership takes place. Last but not least, arrange for a car inspection. Bring your most trustworthy mechanic along to check the condition of the car. </p>
				<p><img class="used-car__content-image" src="/images/tip3.png" alt="Tip 3"></p>
				<h1 class="used-car__content-tip">#4, Thoroughly check all documents to avoid getting scammed</h1>
				<p class="used-car__content-explain">There are total of two forms to check through - B7 and B5. The B7 form is a hire purchase inspection that is given out by Puspakom after an 18-point check that includes both physical and identity checks. The B5 form is also from Puspakom for Transfer of Ownership.</p>
				<p class="used-car__content-explain">*The form cost RM90 to obtain.</p>
				<p class="used-car__content-explain">Request for the car registration card as well, to verify with the Jabatan Pengangkutan Jalan (JPJ) if the seller is registered as the owner of the car, and to check it’s not reported as stolen car. You may insist on a proper Sales and Purchase Agreement.</p>
				<p><img class="used-car__content-image" src="/images/tip4.png" alt="Tip 4"></p>
		 	</div>
		 </div>
	</div>

</div>