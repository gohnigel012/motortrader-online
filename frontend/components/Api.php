<?php
namespace frontend\components;
use Yii;
use yii\base\Component;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\web\UploadedFile;
use yii\helpers\Url;
use linslin\yii2\curl;
use app\models\TransOrderYoutube;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Api extends Component {
    public $wpurl;
    public $url;
    public $key;

    public function wpnews() {
        $wp_news_memcache_key = '/api/wp/home';
        $wp_news_memcache = Yii::$app->cache->get($wp_news_memcache_key);
        if ($wp_news_memcache) {
            $wp_content = $wp_news_memcache;
        } else {
            //Init curl
            $curl = new curl\Curl();

            $wp_content = $curl->get(rtrim($this->wpurl, '/') . '/mt-home-json.php');
            Yii::$app->cache->set($wp_news_memcache_key, $wp_content, 600);
        }
        return $wp_content;
    }

    public function login($username, $password, &$message='', &$user_name='') {
        
        $curl_data = [
            'username' => $username,
            'password' => $password,
        ];

        $request_array = [];

        ksort($curl_data);
        foreach ($curl_data as $request_key_loop => $request_data_loop) {
            if (in_array($request_key_loop, ['remark'])) continue;
            $request_array[] = $request_key_loop . '=' . $request_data_loop;
        }
        
        $curl_data['signature'] = md5( implode("&", $request_array) . $this->key);

        $curl = new curl\Curl();
        
        //die(rtrim($this->url, '/') . '/user/login?' . http_build_query($curl_data));
        $curl_json =  $curl->get(rtrim($this->url, '/') . '/user/login', false, $curl_data);
        $curl_array = json_decode($curl_json, 1);
        
        if (isset($curl_array['result']) && $curl_array['result'] == '1') {
            $user_name = $curl_array['user_name'];
            return $curl_array['user_id'];
        } else {
            $message = $curl_array['message'];
        }

        return false;
    }

    public function advertsustatus($user_id) {
        
        $curl_data = ['user_id' => $user_id,
                            ];

        $request_array = [];

        ksort($curl_data);
        foreach ($curl_data as $request_key_loop => $request_data_loop) {
            if (in_array($request_key_loop, ['remark'])) continue;
            $request_array[] = $request_key_loop . '=' . $request_data_loop;
        }
        
        $curl_data['signature'] = md5( implode("&", $request_array) . $this->key);

        $curl = new curl\Curl();
        
        // die(rtrim($this->url, '/') . '/user/login?' . http_build_query($curl_data));
        $curl_json = $curl->get(rtrim($this->url, '/') . '/user/advertsustatus', false, $curl_data);
        $curl_array = json_decode($curl_json, 1);
        
        if (isset($curl_array['result']) && $curl_array['result'] == '1') {
            return $curl_array['advertsus'];
        } else {
            $message = $curl_array['message'];
        }

        return false;
    }

    public function register($username, $password, $email, $phone, $type = 'private', $social='', $uuid='', &$message='') {
        
        $curl_data = [
            'username' => $username,
            'password' => $password,
            'email' => $email,
            'phone' => $phone,
            'seller_type' => $type,
        ];

        if (in_array($social, ['facebook', 'google'])) {
            $curl_data['social'] = $social;
            $curl_data['uuid'] = $uuid;
        }

        $request_array = [];

        ksort($curl_data);
        foreach ($curl_data as $request_key_loop => $request_data_loop) {
            if (in_array($request_key_loop, ['remark'])) continue;
            $request_array[] = $request_key_loop . '=' . $request_data_loop;
        }
        
        $curl_data['signature'] = md5( implode("&", $request_array) . $this->key);

        $curl = new curl\Curl();
        
        // die(rtrim($this->url, '/') . '/user/register?' . http_build_query($curl_data));
        $curl_json = $curl->get(rtrim($this->url, '/') . '/user/register', false, $curl_data);
        $curl_array = json_decode($curl_json, 1);
        
        if (isset($curl_array['result']) && $curl_array['result'] == '1') {
            return $curl_array['user_id'];
        } else {
            $message = $curl_array['message'];
        }

        return false;
    }

    public function searchresult($user_id, $name, $email, $phone, $make, $model, $make_cd, $model_cd, $keyword) {
    
        $curl_data = [ 
                            'user_id' => $user_id,
                            'name' => $name,
                            'email' => $email,
                            'phone' => $phone,
                            'make' => $make,
                            'model' => $model,
                            'make_cd' => $make_cd,
                            'model_cd' => $model_cd,
                            'keyword' => $keyword,
                            ];

        $request_array = [];

        ksort($curl_data);
        foreach ($curl_data as $request_key_loop => $request_data_loop) {
             if (in_array($request_key_loop, ['remark'])) continue;
            $request_array[] = $request_key_loop . '=' . $request_data_loop;
        }
        
        $curl_data['signature'] = md5( implode("&", $request_array) . $this->key);
        
        // die(rtrim($this->url, '/') . '/user/submitseachresult?' . http_build_query($curl_data));
        $curl = new curl\Curl();
        $curl_json = $curl->get(rtrim($this->url, '/') . '/user/submitseachresult', false, $curl_data);

        $curl_array = json_decode($curl_json, 1);
        
        if (isset($curl_array['result']) && $curl_array['result'] == '1') {
            return $curl_array['data'];
        }
        
        return false;
    }

    public function searchuserstatus($user_id) {
    
        $curl_data = [
                            'user_id' => $user_id,
                            ];

        $request_array = [];

        ksort($curl_data);
        foreach ($curl_data as $request_key_loop => $request_data_loop) {
             if (in_array($request_key_loop, array('remark'))) continue;
            $request_array[] = $request_key_loop . '=' . $request_data_loop;
        }
        
        $curl_data['signature'] = md5( implode("&", $request_array) . $this->key);
        
        // die(rtrim($this->url, '/') . '/user/submitsearchuserstatus?' . http_build_query($curl_data));
        $curl = new curl\Curl();
        $curl_json = $curl->get(rtrim($this->url, '/') . '/user/submitsearchuserstatus', false, $curl_data);

        $curl_array = json_decode($curl_json, 1);
        
        if (isset($curl_array['result']) && $curl_array['result'] == '1') {
            return $curl_array['advertsus'];
        } else {
            $message = $curl_array['message'];
        }
        
        return false;
    }


    public function cardescription($make_cd) {
        $response = false;
		
        $cardescription_memcache_key = '/api/cardescription/make_cd/' . $make_cd;
        $cardescription_memcache = Yii::$app->cache->get($cardescription_memcache_key);
        // if ($cardescription_memcache !== false) {
        //     $reponse = $cardescription_memcache;
        // } else {
		
			$curl_data = ['make_cd' => $make_cd];
			$request_array = [];
			ksort($curl_data);
			foreach ($curl_data as $request_key_loop => $request_data_loop) {
				if (in_array($request_key_loop, ['remark'])) continue;
				$request_array[] = $request_key_loop . '=' . $request_data_loop;
			}

			$curl = new curl\Curl();
			
			$curl_data['signature'] = md5( implode("&", $request_array) . $this->key);
			// die(rtrim($this->url, '/') . '/configuration/cardescription?' . http_build_query($curl_data));
			$curl_json = $curl->get(rtrim($this->url, '/') . '/configuration/cardescription', false, $curl_data);
			$curl_array = json_decode($curl_json, 1);
			
			if (isset($curl_array['result']) && $curl_array['result'] == '1') {
				$response = $curl_array['description'];
				Yii::$app->cache->set($cardescription_memcache_key, $response, 600);
			}
		// }
		
        return $response;
    }

    public function getvideo($urn_no) {
        $response_array = false;
        $trans_order_youtube = new TransOrderYoutube();

        $curl_data = ['urn_no' => $urn_no];
        
        $getvideo_memcache_key = '/api/getvideo/urn/' . $urn_no;
        $getvideo_memcache = Yii::$app->cache->get($getvideo_memcache_key);
        if ($getvideo_memcache !== false) {
            $response_array = $getvideo_memcache;
        } else {
            /*
            ksort($curl_data);
            foreach ($curl_data as $request_key_loop => $request_data_loop) {
                if (in_array($request_key_loop, array('remark'))) continue;
                $request_array[] = $request_key_loop . '=' . $request_data_loop;
            }
            
            $curl_data['signature'] = md5( implode("&", $request_array) . $this->key);
            
            // die(rtrim($this->url, '/') . '/video/index?' . http_build_query($curl_data));
            
            $curl_json = Yii::app()->curl->run(rtrim($this->url, '/') . '/video/index', false, $curl_data);
            $curl_array = json_decode($curl_json, 1);
            
            if (isset($curl_array['result']) && $curl_array['result'] == '1') {
                $response_array =  array('video_url' => $curl_array['youtube_url'],
                                        'video_title' => $curl_array['youtube_title'],
                                        'video_description' => $curl_array['youtube_description'],
                                        );
            }
            */

            $trans_advert_youtube_model = $trans_order_youtube->find()->where(['urn_no' => $urn_no])->one();
            if ($trans_advert_youtube_model) {

                $video_url = Yii::$app->general->advertvideourl($trans_advert_youtube_model->trans_order_youtube_filename, 'video');
                
                if (trim($trans_advert_youtube_model->youtube_id)) {
                    $video_url = 'https://www.youtube.com/embed/' . $trans_advert_youtube_model->youtube_id . '?autoplay=1&mute=1'; // . '&html5=True';
                }

                $response_array =  [
                                        'youtube_id' => $trans_advert_youtube_model->youtube_id,
                                        'video_url' => $video_url,
                                        'video_title' => $trans_advert_youtube_model->trans_order_youtube_title,
                                        'video_description' => $trans_advert_youtube_model->trans_order_youtube_description,
                                    ];

                Yii::$app->cache->set($getvideo_memcache_key, $response_array, 240);
            }
        }

        return $response_array;
    }
}