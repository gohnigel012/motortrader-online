<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = 'Motor Trader FAQ';
?>
<div class="page-home">
	<h1 class="banner-search__title text-center">How to sell your car with us</h1>
	<div class="faq">
		<p class="faq__question" id="faq-question" data-toggle="collapse" data-target="#question1">How can I sign up as a Dealer? <img id="faq-plus" src="/images/plus.png" alt="Plus"></p>
		<div id="question1" class="collapse">
			<p class="faq_content">You can click on the <a class="superdeals-featured-cnabadv__link" href="/site/login">Register link here</a> in Dealer Login page, fill in the form and our friendly staff will contact you for registration. All you need to provide us is just the Company name and registration number and minimum credit top-up of RM50.</p>
	    </div>
	    <hr>
	    <p class="faq__question" id="faq-question-2" data-toggle="collapse" data-target="#question2">What is the benefit of having a dealer account? <img id="faq-plus-2" src="/images/plus.png" alt="Plus"></p>
		<div id="question2" class="collapse">
			<p class="faq_content">First, your advertisement seller type will be display as “Dealer”. There is an inventory system for you to manage your car advertisement. You can use the premium ads service such as boost, featured ads, video upload.</p>
	    </div>
	    <hr>
	    <p class="faq__question" id="faq-question-3" data-toggle="collapse" data-target="#question3">How much is the cost of premium ads services? <img id="faq-plus-3" src="/images/plus.png" alt="Plus"></p>
		<div id="question3" class="collapse table-responsive">
			<table class="table table-bordered">
				<tr class="">
					<th colspan="4" class="text-center">Credits</th>
				</tr>
				<tr class="">
					<td class="header-text">Premium ads Service</td>
					<td>Car</td>
					<td>Number Plate</td>
					<td>Auto parts</td>
				</tr>
				<tr class="">
					<td class="header-text">Publish</td>
					<td>7</td>
					<td>5</td>
					<td>3</td>
				</tr>
				<tr class=""> 
					<td class="header-text">Edit</td>
					<td>6</td>
					<td>4</td>
					<td>2</td>
				</tr>
				<tr class="">
					<td class="header-text">Boost</td>
					<td>6</td>
					<td>4</td>
					<td>2</td>
				</tr>
				<tr class="">
					<td class="header-text">Featured</td>
					<td>10</td>
					<td>10</td>
					<td>10</td>
				</tr>
				<tr class="">
					<td class="header-text">Upload Video</td>
					<td>3</td>
					<td>3</td>
					<td>3</td>
				</tr>
			</table>
	    </div>
	    <hr>
	    <p class="faq__question" id="faq-question-4" data-toggle="collapse" data-target="#question4">What is "Publish"? <img id="faq-plus-4" src="/images/plus.png" alt="Plus"></p>
	    <div id="question4" class="collapse">
	    	<p class="faq_content">Publish is to post 1 advert with 10 images into Motor Trader website</p>
	    </div>
	    <hr>
	    <p class="faq__question" id="faq-question-5" data-toggle="collapse" data-target="#question5">What is "Boost"? <img id="faq-plus-5" src="/images/plus.png" alt="Plus"></p>
	    <div id="question5" class="collapse">
	    	<p class="faq_content">As our listing is sorted by last updated date, older ads will be pushed to the latter page over the time.  By boosting your ads, we will bring your ads to the Top position again.</p>
	    </div>
	    <hr>
	    <p class="faq__question" id="faq-question-6" data-toggle="collapse" data-target="#question6">What is "Featured"? <img id="faq-plus-6" src="/images/plus.png" alt="Plus"></p>
	    <div id="question6" class="collapse">
	    	<p class="faq_content">Your ads will be placed in prime position in our website. Your ads will be shown the users who search the model of the car you advertised (for 1 week).</p>
	    </div>
	    <hr>
	    <p class="faq__question" id="faq-question-7" data-toggle="collapse" data-target="#question7">What is upload video? <img id="faq-plus-7" src="/images/plus.png" alt="Plus"></p>
	    <div id="question7" class="collapse">
	    	<p class="faq_content">We provide an option for dealer who wish to showcase their item using video. You can upload 1 single video of 50MB.</p>
	    </div>
	    <hr>
	    <p class="faq__question" id="faq-question-8" data-toggle="collapse" data-target="#question8">Is there an expiry date for the credits? <img id="faq-plus-8" src="/images/plus.png" alt="Plus"></p>
	    <div id="question8" class="collapse">
	    	<p class="faq_content">Yes, the expiry date of your credits is normally 60 days. However, we will provide longer expiry period when you topup more.</p>
	    </div>
	    <hr>
	    <p class="faq__question" id="faq-question-9" data-toggle="collapse" data-target="#question9">How much is 1 credit worth in RM? <img id="faq-plus-9" src="/images/plus.png" alt="Plus"></p>
	    <div id="question9" class="collapse">
	    	<p class="faq_content">The Value of 1 credit is normally = RM1. You will get more credit when you topup more. Example: By topping up RM1000, you will get 1250 credits. Value of 1 credit = RM0.80</p>
	    </div>
	    <hr>
	    <p class="faq__question" id="faq-question-10" data-toggle="collapse" data-target="#question10">What will happen to my expired credits? <img id="faq-plus-10" src="/images/plus.png" alt="Plus"></p>
	    <div id="question10" class="collapse">
	    	<p class="faq_content">You will then have a 30-day grace period to top up your credits and extend their validity. If you choose to top up your credits at this point, the total credits you have remaining will be carried forward. However, if no action is taken, the expired credits will be forfeited after the 30-day grace period.</p>
	    </div>
	    <hr>
	    <p class="faq__question" id="faq-question-11" data-toggle="collapse" data-target="#question11">How do I Buy/Topup credits? <img id="faq-plus-11" src="/images/plus.png" alt="Plus"></p>
	    <div id="question11" class="collapse">
	    	<p class="faq_content">To buy Credits, first log into your dealer account. Then, click on 'Credits' at the top of the page. We have various payment methods available for you :</p>
	    	<ul class="faq_list">
	    		<li>Credit card</li>
	    		<li>Bank transfer</li>
	    		<li>Cheque</li>
	    		<li>Cash deposit</li>
	    		<li>PayPal</li>
	    	</ul>
	    </div>
	    <hr>
	    <p class="faq__question" id="faq-question-12" data-toggle="collapse" data-target="#question12">How long does my published ads be shown in Motortrader.com.my? <img id="faq-plus-12" src="/images/plus.png" alt="Plus"></p>
	    <div id="question12" class="collapse">
	    	<p class="faq_content">1 year. However, you can choose to remove the ads anytime you like.</p>
	    </div>
	    <hr>
	    <p class="faq__question" id="faq-question-13" data-toggle="collapse" data-target="#question13">How can I monitor my advertisement performance? <img id="faq-plus-13" src="/images/plus.png" alt="Plus"></p>
	    <div id="question13" class="collapse">
	    	<p class="faq_content">We will provide you a three page google analytics report for you to understand your advert performance.</p>
	    	<p>Page 1 – Ads Performance</p>
	    	<p class="faq_list">Provide information about Pageviews, Users by months, Most viewed Stocks.</p>
	    	<p>Page 2 – Audience Demography</p>
	    	<p class="faq_list">Provide information about your audience demography such as Gender, Age, Location, and their browsing behavior.</p>
	    	<p>Page 3 – Ads Response</p>
	    	<p class="faq_list">Provide information about the response generated from your advertisement. 1. Click to Call, Click to view, 3. WhatsApp</p>
	    </div>
	    <hr>
	    <p class="faq__question" id="faq-question-14" data-toggle="collapse" data-target="#question14">What if I have multiple sales person? <img id="faq-plus-14" src="/images/plus.png" alt="Plus"></p>
	    <div id="question14" class="collapse">
	    	<p class="faq_content">In dealer login, there is a child account management system. You can apply child account for each of your sales person. Allocate credit to them and monitoring their spending and advert performance.</p>
	    </div>
	</div>
	<p class="text-center"><button class="btn btn-default faq__search-btn"><span class="faq__search-btn-text">Contact us</span></button> <button class="btn btn-default faq__search-btn"><span class="faq__search-btn-text">Register now</span></button></p>
</div>