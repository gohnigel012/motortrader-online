<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'enableCookieValidation' => true,
            'cookieValidationKey' => 'aaaaaa',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',                                
                '<controller:\w+>/<action:\w+>/' => '<controller>/<action>'
            ],
        ],

        'fcookie' => [
            'class' => 'frontend\components\Fcookie',
        ],
        'fpath' => [
            'class' => 'frontend\components\Fpath',
            'mtsa' => ['url' => 'https://staging.mtsa.com.my'],
            'cdn' => [
                        'stockList' => [
                            'flag' => false,
                            'url' => 'https://d2qwk5aroe5gj7.cloudfront.net/',
                            'thumbnail_folder' => '/mtsa/images/advert/webad/',
                        ],
                     ],
            'css' => [ 
                'path' => 'C:/xampp/htdocs/motortrader-project/frontend/web/css',
                'url' => 'http://motortrader-project.localhost/css',
            ],
            'assets' => [  
                'path' => 'C:/xampp/htdocs/motortrader-project/frontend/web/js',
                'url' => 'http://motortrader-project.localhost/js',
            ],              
                            
            'images' => [  
                'path' => 'C:/xampp/htdocs/motortrader-project/frontend/web/images',
                'url' => 'http://motortrader-project.localhost/images',
            ],
            'mountimages' => [
                'domain' => 'http://www.motortrader.com.my',
                'path' => 'C:/xampp/htdocs/motortrader-project/frontend/web/images',
                'url' => 'http://motortrader-project.localhost/advert-images',
            ],
        ],
        'general' => [
            'class' => 'frontend\components\General',
            'advert'=> [
                "temporary_folder" => 'C:/xampp/htdocs/motortrader-project/advert-images/',
            ],
            'compile_css' => '0',               
            's3_flag' => '1',
            's3_domain' => 'https://cloudkia-testing.s3.ap-southeast-1.amazonaws.com',
            'domain' => 'http://motortrader-project.localhost',
        ],
        'api'=> [
            'class' => 'frontend\components\Api',
            'wpurl' => 'https://news.motortrader.com.my',
            'url' => 'http://mtsa.localhost/mtcs.php/api/',
            'key' => 'aaaaaaaaaaaa',
        ],
        'cache' => [
            'class' => 'yii\caching\MemCache',
            'servers' => [
                [
                    'host' => 'motortrader-project.localhost',
                    'port' => 11211,
                    'weight' => 100,
                ]
            ],
        ]
    ],
    
    'params' => $params,
    
];
