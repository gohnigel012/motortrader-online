$.fn.protoSlider = function(options) {
	var _proto_direction = '';

	var _proto_slider = this;
	var _proto_slider_offset = _proto_slider.offset();
	var _proto_slider_li_margin_right = parseInt(jQuery("ul li", _proto_slider).css('margin-right'));
	var _column_width = (jQuery("ul li", _proto_slider).width() + _proto_slider_li_margin_right + 2);
	var ul_width = jQuery("ul li", _proto_slider).length * _column_width;

	jQuery('.proto-slide-box', _proto_slider).css('display', 'block');
	jQuery("ul", _proto_slider).width(ul_width);
	jQuery("ul", _proto_slider).fadeIn(options.fadeintime);

	var _new_left;
	jQuery(".proto-slide-arrow-prev", _proto_slider).click(function() {
		if (_proto_direction != 'prev') {
			jQuery('.proto-slide-box', _proto_slider).stop();
		}
		_proto_direction = 'prev';

		jQuery('.proto-slide-box', _proto_slider).animate({
		    scrollLeft: '-=' + _column_width
		}, 800);
	});

	jQuery(".proto-slide-arrow-next", _proto_slider).click(function() {
		if (_proto_direction != 'next') {
			jQuery('.proto-slide-box', _proto_slider).stop();
		}
		_proto_direction = 'next';


		jQuery('.proto-slide-box', _proto_slider).animate({
		    scrollLeft: '+=' + _column_width
		}, 800);
	});

    return this;
}