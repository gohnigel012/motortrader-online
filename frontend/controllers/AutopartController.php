<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use app\models\MasterGeneral;
use app\models\MasterCarMake;
use app\models\MasterCarModel;
use app\models\MasterCarModelGroup;
use app\models\MasterCarPart;
use app\models\MasterCarSubPart;
use app\models\MasterDealer;
use app\models\MasterDealerReview;
use app\models\MasterDealerReviewInfo;
use app\models\SuperDeals;
use app\models\TransOrder;
use app\models\TransOrderExtraInfo;
use app\models\TransOrderPicture;
use app\models\TransStatisticCount;

/**
 * Autopart controller
 */
class AutopartController extends Controller
{
	/**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
    	$master_general = new MasterGeneral();
    	$master_car_make = new MasterCarMake();
    	$master_car_model = new MasterCarModel();
    	$master_car_model_group = new MasterCarModelGroup();
    	$master_car_part = new MasterCarPart();
    	$master_car_sub_part = new MasterCarSubPart();
    	$master_dealer = new MasterDealer();
    	$super_deals = new SuperDeals();
    	$trans_order = new TransOrder();
    	$trans_statistic_count = new TransStatisticCount();

    	$location_array = $master_general->getLocation();

    	$url_filter_params = [];

    	$pdata = [];

		$pdata['limit'] = 9;
		$pdata['offset'] = 0;
		$pdata['page'] = 1;

		$pdata['page_label'] = 'Auto Parts';
		
		$pdata['autoscroll'] = false;

		if (isset($_REQUEST['limit']) && $_REQUEST['limit']) {
			$limit = $_REQUEST['limit'];
			$pdata['offset'] = 0;
		}
		
		if (isset($_REQUEST['autoscroll']) && in_array($_REQUEST['autoscroll'], ['result', 'category'])) {
			$pdata['autoscroll'] = $_REQUEST['autoscroll'];
		}


		if (isset($_REQUEST['page']) && (int)$_REQUEST['page'] == $_REQUEST['page']) {
			
			$pdata['autoscroll'] = 'result';
			
			$pdata['page'] = (int)$_REQUEST['page'];

			if ($pdata['page'] > 0) {
				$pdata['offset'] = $pdata['limit'] * ($pdata['page'] - 1);
			} else {
				$pdata['offset'] = 0;
			}
		}

		if ($pdata['page'] > 1) {
			$url_filter_params['page'] = $pdata['page'];
		}

		$trans_advert_criteria = [];
		$trans_advert_criteria2 = [];
		$trans_advert_criteria_join = [];
		$trans_advert_array = [];
		$trans_advert_criteria[] =  " t.status = '40' AND t.classification_cd = '1166' ";

		if (isset($_REQUEST['make'])) {
			$pdata['filter']['make'] = $_REQUEST['make'];
		}
		
		$pdata['filter']['make_cd'] = '';
		if (isset($_REQUEST['make_cd'])) {
			$pdata['filter']['make_cd'] = $_REQUEST['make_cd'];
		}
		
		$pdata['filter']['model_cd'] = '';
		if (isset($_REQUEST['model_cd'])) {
			$pdata['filter']['model_cd'] = $_REQUEST['model_cd'];
		}
		
		$pdata['filter']['location'] = '';
		$pdata['filter']['locationname'] = '';
		
		$pdata['filter']['superdeals'] = '0';
		if (isset($_REQUEST['superdeals'])) {
			$pdata['filter']['superdeals'] = $_REQUEST['superdeals'];
		}

		if (isset($_REQUEST['location']) && isset($location_array[$_REQUEST['location']])) {
			$pdata['filter']['location'] = $_REQUEST['location'];
			$pdata['filter']['locationname'] = strtolower(preg_replace('~[^\\pL\d]+~u', '_', $location_array[$pdata['filter']['location']]));

		} else if (isset($_REQUEST['locationname']) && trim($_REQUEST['locationname'])) {
			$pdata['filter']['locationname'] = ltrim($_REQUEST['locationname'], "/");
		}

		$pdata['filter']['keyword'] = '';
		if (isset($_REQUEST['keyword'])) {
			$pdata['filter']['keyword'] = trim($_REQUEST['keyword']);
		}
		
		$pdata['filter']['dealers_type'] = 'all';
		if (isset($_REQUEST['dealers_type']) && in_array($_REQUEST['dealers_type'], array('1', '2', '3'))) {
			$pdata['filter']['dealers_type'] = $_REQUEST['dealers_type'];
		}
		
		$pdata['filter']['type'] = 'all';
		if (isset($_REQUEST['type']) && in_array($_REQUEST['type'], ['all', 'used', 'new', 'recond'])) {
			$pdata['filter']['type'] = $_REQUEST['type'];
		}

		$pdata['autopart'] = [];
		$trans_advert_params_array = [];
		$trans_advert_params_array2 = [];

		$car_model_cd = false;
		$car_model_name = false;

		$car_make_cd = false;
		$car_make_name = false;

		if (isset($_REQUEST['model']) && trim($_REQUEST['model'])){
			$pdata['filter']['model'] = $_REQUEST['model'];

			$trans_advert_criteria_join[] = " INNER JOIN {{trans_order_extra_info}} taei ON taei.urn_no = t.urn_no ";
			$trans_advert_criteria[] = " taei.trans_order_extra_key = 'any_car_make' AND (taei.trans_order_extra_key = 'only_selected_car_make' AND taei.trans_order_extra_value ILIKE :trans_order_extra_value) AND (taei.trans_order_extra_key = 'only_selected_car_make_model' AND taei.trans_order_extra_value ILIKE :trans_order_extra_value2) ";
			$trans_advert_params_array[':trans_order_extra_value'] = "%" . $_REQUEST['make'] . "%";
			$trans_advert_params_array[':trans_order_extra_value2'] = "%" . $_REQUEST['model'] . "%";

			$car_make_cd = $_REQUEST['make'];
			$car_make_name = $master_car_make->getmakename($car_make_cd);
			$pdata['car_make_name'] = $car_make_name;
			$car_model_cd = $_REQUEST['model'];
			$car_model_name = $master_car_model->getmodelname($car_model_cd);
			$pdata['car_model_name'] = $car_model_name;
		} else {
			if (isset($_REQUEST['make']) && trim($_REQUEST['make'])){
				$pdata['filter']['make'] = $_REQUEST['make'];

				$trans_advert_criteria_join[] = " INNER JOIN {{trans_order_extra_info}} taei ON taei.urn_no = t.urn_no ";
				$trans_advert_criteria[] = " taei.trans_order_extra_key = 'any_car_make' AND (taei.trans_order_extra_key = 'only_selected_car_make' AND taei.trans_order_extra_value ILIKE :trans_order_extra_value) ";
				$trans_advert_params_array[':trans_order_extra_value'] = "%" . $_REQUEST['make'] . "%";
				
				$car_make_cd = $_REQUEST['make'];
				$car_make_name = $master_car_make->getmakename($car_make_cd);
				$pdata['car_make_name'] = $car_make_name;
			}
		}

		$url_array['type'] = 'all';
		$pdata['url_type_all'] = Yii::$app->general->generatecarsseo('autopart', 0, 0, $pdata['filter']['locationname'], 0, $car_make_cd, $car_model_cd, '', $pdata['filter']['superdeals'], $url_array);

		$url_array['type'] = 'used';
		$pdata['url_type_used'] = Yii::$app->general->generatecarsseo('autopart', 0, 0, $pdata['filter']['locationname'], 0, $car_make_cd, $car_model_cd, '', $pdata['filter']['superdeals'], $url_array);

		$url_array['type'] = 'new';
		$pdata['url_type_new'] = Yii::$app->general->generatecarsseo('autopart', 0, 0, $pdata['filter']['locationname'], 0, $car_make_cd, $car_model_cd, '', $pdata['filter']['superdeals'], $url_array);

		$url_array['type'] = 'recond';
		$pdata['url_type_recond'] = Yii::$app->general->generatecarsseo('autopart', 0, 0, $pdata['filter']['locationname'], 0, $car_make_cd, $car_model_cd, '', $pdata['filter']['superdeals'], $url_array);

		$url_array['type'] = $pdata['filter']['type'];

		$pdata['filter']['category'] = [];

		if (isset($_REQUEST['category']) && trim($_REQUEST['category'])) {
			$master_car_part_model = $master_car_part->find()->where(['part_cd' => $_REQUEST['category']])->one();

			if ($master_car_part_model) {
				$pdata['filter']['category'] = $_REQUEST['category'];
				$trans_advert_params_array[':category'] = $master_car_part_model->part_name;

				$pdata['page_label'] .= " ( " . $master_car_part_model->part_name;

				if (isset($_REQUEST['subcategory']) && trim($_REQUEST['subcategory'])){
					$master_car_sub_part_model = $master_car_sub_part->find()->where(['sub_part_cd' => $_REQUEST['subcategory']])->one();
					if ($master_car_sub_part_model) {
						$pdata['filter']['subcategory'] = $_REQUEST['subcategory'];

						$trans_advert_params_array[':category'] = $trans_advert_params_array[':category'] . ' > ' . $master_car_sub_part_model->sub_part_name;

						$pdata['page_label'] .= " > " . $master_car_sub_part_model->sub_part_name;
					}
				}

				$pdata['page_label'] .= " ) ";


				$trans_advert_params_array[':category'] = '%' . $trans_advert_params_array[':category'] . '%';

				$trans_advert_criteria[] = " model_name ILIKE :category ";
			} 
		} else if (isset($_REQUEST['categoryname']) && trim($_REQUEST['categoryname'])) {
			$pdata['filter']['categoryname'] = $_REQUEST['categoryname'];

			$trans_advert_params_array[':category'] = $pdata['filter']['categoryname'];

			$pdata['page_label'] .= " ( " . $pdata['filter']['categoryname'] . " ) ";
			
			$pdata['autoscroll'] = 'category';

			$trans_advert_params_array[':category'] = '%' . $trans_advert_params_array[':category'] . '%';

			$trans_advert_criteria[] = " model_name ILIKE :category ";
			// $trans_advert_criteria->addCondition(" model_name ILIKE :category ");
		}

		$pdata['page_location'] = 'Malaysia';

		if (isset($pdata['filter']['location']) && trim($pdata['filter']['location'])) {
			$pdata['filter']['location'] = $_REQUEST['location'];
			
			$trans_advert_criteria[] = " location_cd = :location ";
			$trans_advert_params_array[':location'] = $pdata['filter']['location'];

			$tmp_location_array = Yii::$app->general->location();
			if (isset($tmp_location_array[$pdata['filter']['location']])) {
				$pdata['page_location'] = $tmp_location_array[$pdata['filter']['location']] . ', Malaysia';

				$meta_keyword_array[] = $tmp_location_array[$pdata['filter']['location']];
			}
		}

		if (isset($_REQUEST['keyword']) && trim($_REQUEST['keyword'])){
			$pdata['filter']['keyword'] = trim($_REQUEST['keyword']);
			
			$keyword_temp_array = [];
			$count_keyword = 0;
			foreach (explode(" ", $pdata['filter']['keyword']) as $keyword_loop) {
				$count_keyword++;
				
				$keyword_temp_array[] = "(make_name ILIKE :keyword_" . $count_keyword . ")";
				$trans_advert_params_array[':keyword_' . $count_keyword] = "%" . trim($keyword_loop) . "%";
			}
			
			// $trans_advert_criteria->addCondition(implode(" OR ", $keyword_temp_array));
			$trans_advert_criteria[] = implode(" OR ", $keyword_temp_array);
		}

		if ((isset($_REQUEST['minPrice']) && trim($_REQUEST['minPrice'])) || (isset($_REQUEST['maxPrice']) && trim($_REQUEST['maxPrice']))){
			$trans_advert_criteria[] = "t.poa_flg <> '1'";
		}
		
		if (isset($_REQUEST['minPrice']) && trim($_REQUEST['minPrice'])) {
			$min_price = str_replace(",", "", $_REQUEST['minPrice']);
			if (is_numeric($min_price)) {
				$pdata['filter']['minPrice'] = $min_price;
				
				$trans_advert_criteria[] = "(CASE WHEN price = 'POA' OR poa_flg = '1' THEN -1 ELSE CAST(t.price AS DECIMAL) END) >= " . $pdata['filter']['minPrice'];
				// $trans_advert_params_array[':minPrice'] = $pdata['filter']['minPrice'];
			}
		}

		if (isset($_REQUEST['maxPrice']) && trim($_REQUEST['maxPrice'])) {
			$max_price = str_replace(",", "", $_REQUEST['maxPrice']);
			if (is_numeric($max_price)) {
				$pdata['filter']['maxPrice'] = $max_price;
				$trans_advert_criteria[] = "(CASE WHEN price = 'POA' OR poa_flg = '1' THEN -1 ELSE CAST(t.price AS DECIMAL) END) <= " . $pdata['filter']['maxPrice'];
				// $trans_advert_params_array[':maxPrice'] = $pdata['filter']['maxPrice'];
			}
		}

		$pdata['filter']['from_date'] = false;
		if (isset($_REQUEST['days']) && (int)$_REQUEST['days']) {
			if (in_array($_REQUEST['days'], array(1, 7, 30, 90))) {
				$pdata['filter']['from_date'] = date('Y-m-d H:i:s', strtotime('- ' . $_REQUEST['days'] . ' days'));
			}
			
			$trans_advert_criteria[] = " upd_data_date >= :from ";
			$trans_advert_params_array[':from'] = $pdata['filter']['from_date'];
		}

		$pdata['filter']['video_file'] = false;
		if (isset($_REQUEST['video']) && (int)$_REQUEST['video']) {
			$pdata['filter']['video_file'] = $_REQUEST['video'];
			
			$trans_advert_criteria_join[] = "INNER JOIN {{trans_order_youtube}} tay ON tay.urn_no = t.urn_no ";
		}

		if ($pdata['filter']['superdeals'] == '1') {
			$has_join_table = true;
			if ($has_join_table) {
				$trans_advert_criteria_join[] = " INNER JOIN {{super_deals}} sd ON sd.urn_no = t.urn_no ";
				$trans_advert_criteria[] = " sd.super_deals_schedule_start_date <= :date ";
				$trans_advert_criteria[] = " sd.super_deals_schedule_end_date >= :date ";
				$trans_advert_params_array[':date'] = date("Y-m-d H:i:s");
			} else {
				$trans_advert_criteria_join[] = " INNER JOIN {{super_deals}} sd ON sd.urn_no = t.urn_no ";
				$trans_advert_criteria[] =  " sd.super_deals_schedule_start_date <= :date ";
				$trans_advert_criteria[] = " sd.super_deals_schedule_end_date >= :date ";
				$trans_advert_params_array[':date'] = date("Y-m-d H:i:s");
			}
		}

		switch ($pdata['filter']['type']) {	
			case 'used':
					$trans_advert_criteria[] = " is_used_flg = '1' ";
				break;
			case 'new':
					$trans_advert_criteria[] = " is_used_flg = '2' ";
				break;
			case 'recond':
					$trans_advert_criteria[] = " is_used_flg = '3' ";
			break;
			default:
				break;
		}

		$pdata['sort'] = 'updatehigh';
		if (isset($_REQUEST['sort']) && in_array($_REQUEST['sort'], ['pricehigh', 'pricelow', 'updatelow', 'updatehigh'])) {
			$pdata['sort'] = $_REQUEST['sort'];
		}

	    switch($pdata['sort']) {
			case 'pricehigh':
	        	$trans_advert_criteria_order = "phprice DESC, t.upd_data_date DESC";
	        break;
	        case 'pricelow':
	        	$trans_advert_criteria_order = "plprice, t.upd_data_date DESC";
	        break;
	        case 'updatelow':
	        	$trans_advert_criteria_order = "t.upd_data_date ASC";
	        break;
	        default:
	        	$trans_advert_criteria_order = "t.upd_data_date DESC ";
        	break;
        }

        if (in_array($pdata['sort'], ['pricelow', 'pricehigh'])) {
			$trans_advert_array[] = "(SELECT 1 as priority, t.*, (CASE WHEN price = 'POA' OR poa_flg = '1' THEN 9999999999 ELSE price::numeric END) as plprice, (CASE WHEN price = 'POA' OR poa_flg = '1' THEN 0 ELSE price::numeric END) as phprice FROM trans_order t " . implode(" ", $trans_advert_criteria_join) . "WHERE " . implode(" AND ", $trans_advert_criteria) . " ORDER BY " . $trans_advert_criteria_order . " )";
		} else {
			$trans_advert_array[] = "(SELECT 1 as priority, t.* FROM trans_order t " . implode(" ", $trans_advert_criteria_join) . "WHERE " . implode(" AND ", $trans_advert_criteria) . " ORDER BY " . $trans_advert_criteria_order . ")";
	
		}

		if (isset($_REQUEST['keyword']) && trim($_REQUEST['keyword'])){
			$pdata['filter']['keyword'] = trim($_REQUEST['keyword']);

			$keyword_temp_array = [];
			$count_keyword = 0;
			foreach (explode(" ", $pdata['filter']['keyword']) as $keyword_loop) {
				$count_keyword++;
				
				$car_make_cd = $master_car_make->getmakecd($keyword_loop);
				$car_model_cd = $master_car_model->getmodelcd($keyword_loop);
				
				if (isset($car_make_cd) && trim($car_make_cd)) {	
					$trans_advert_criteria2[] = " taei.trans_order_extra_key = 'any_car_make' ";
					if (isset($car_model_cd) && trim($car_model_cd)) {
						$trans_advert_criteria2[] = " taei.trans_order_extra_key = 'only_selected_car_make_model' AND taei.trans_order_extra_value ILIKE :trans_order_extra_value_model_" . $count_keyword;
						$trans_advert_params_array2['trans_order_extra_value_model_' . $count_keyword] = "%" . $car_model_cd . "%";
					} else {	
						if (isset($car_make_cd) && trim($car_make_cd)) {
							$trans_advert_criteria2[] = " taei.trans_order_extra_key = 'only_selected_car_make' AND taei.trans_order_extra_value ILIKE :trans_order_extra_value_make_" . $count_keyword;
							$trans_advert_params_array2[':trans_order_extra_value_make_' . $count_keyword] = "%" . $car_make_cd . "%";
						}
					}
				}
			}

			if (in_array($pdata['sort'], ['pricelow', 'pricehigh'])) {
				$trans_advert_array[] = "(SELECT 2 as priority, t.*, (CASE WHEN price = 'POA' OR poa_flg = '1' THEN 9999999999 ELSE price::numeric END) as plprice, (CASE WHEN price = 'POA' OR poa_flg = '1' THEN 0 ELSE price::numeric END) as phprice FROM trans_order t INNER JOIN {{trans_order_extra_info}} taei ON taei.urn_no = t.urn_no WHERE advert_category = '2' AND classification_cd = '1166' AND NOT EXISTS (SELECT 1 FROM trans_advert s1 WHERE s1.urn_no = t.urn_no)" . implode(" OR ", $trans_advert_criteria2) . " ORDER BY " . $trans_advert_criteria_order . " ";						
			} else {
				$trans_advert_array[] = "(SELECT 2 as priority, t.* FROM trans_order t INNER JOIN {{trans_order_extra_info}} taei ON taei.urn_no = t.urn_no WHERE classification_cd = '1166' AND NOT EXISTS (SELECT 1 FROM trans_order s1 WHERE s1.urn_no = t.urn_no)" . implode(" OR ", $trans_advert_criteria2) . " ORDER BY " . $trans_advert_criteria_order . ")";
			}
		}

		$trans_advert_model_command = Yii::$app->db->createCommand("SELECT * FROM (" . implode(" UNION ", $trans_advert_array) . ") as class ORDER BY priority LIMIT " . $pdata['limit'] . " OFFSET " . $pdata['offset']);

		$trans_advert_model_command_total = Yii::$app->db->createCommand("SELECT * FROM (" . implode(" UNION ", $trans_advert_array) . ") as class ORDER BY priority");

		if (count($trans_advert_params_array)) {
			foreach ($trans_advert_params_array as $trans_advert_params_key_loop => $trans_advert_params_value_loop) {
				$trans_advert_model_command->bindParam($trans_advert_params_key_loop, $trans_advert_params_value_loop);
				$trans_advert_model_command_total->bindParam($trans_advert_params_key_loop, $trans_advert_params_value_loop);
			}

		}
		
		if (count($trans_advert_params_array2)) {
			foreach ($trans_advert_params_array2 as $trans_advert_params2_key_loop => $trans_advert_params2_value_loop) {
				$trans_advert_model_command->bindParam($trans_advert_params2_key_loop, $trans_advert_params2_value_loop);
				$trans_advert_model_command_total->bindParam($trans_advert_params_key_loop, $trans_advert_params_value_loop);
			}
		}

		$trans_advert_model = $trans_advert_model_command->queryAll();

		$trans_advert_model_count = $trans_advert_model_command_total->queryAll();

		$pdata['total'] = count($trans_advert_model_count);

		$pdata['featured'] = $trans_order->featured('2', 6);
		
		$recent_view_array = Yii::$app->general->advfromrecent('autopart');
		$pdata['recentview'] = [];
		if (count($recent_view_array)) {
			foreach ($recent_view_array as $recent_urn_loop) {
				$recent_view_info = $trans_order->loadinfo($recent_urn_loop);
				if ($recent_view_info !== false) {
					$pdata['recentview'][] = $recent_view_info;
				}
			}
		}

		$pdata['bookmark_array'] = Yii::$app->general->advfrombookmark();
		
		/*
			Define URL
			Remember to put default at the last
		*/

		$url_parse = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
		parse_str($url_parse, $url_array);
		$url_array = Yii::$app->general->filterurlparams($url_array);
		unset($url_array['location']);
		unset($url_array['sort']);
		
		$pdata['url_superdeals_on'] = Yii::$app->general->generatecarsseo('autopart', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', 1, $url_array);
		$pdata['url_superdeals_off'] = Yii::$app->general->generatecarsseo('autopart', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', 0, $url_array);
		
		$url_array['sort'] = 'pricehigh';
		$pdata['url_sort_price_high'] = Yii::$app->general->generatecarsseo('autopart', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', 0, $url_array + ['sort' => 'pricehigh']);

		$url_array['sort'] = 'pricelow';
		$pdata['url_sort_price_low'] = Yii::$app->general->generatecarsseo('autopart', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', 0, $url_array + ['sort' => 'pricelow']);

		$url_array['sort'] = 'updatehigh';
		$pdata['url_sort_update_high'] = Yii::$app->general->generatecarsseo('autopart', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', 0, $url_array + ['sort' => 'updatehigh']);

		$url_array['sort'] = 'updatelow';
		$pdata['url_sort_update_low'] = Yii::$app->general->generatecarsseo('autopart', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', 0, $url_array + ['sort' => 'updatelow']);

		$url_array['sort'] = $pdata['sort'];

		// if (count($url_array)) {
		// 	unset($url_array['sort']);
		// }
		
		$pdata['url_pagination'] = Yii::$app->general->generatecarsseo('autopart', 0, 0, $pdata['filter']['locationname'], '__PAGE__', '', '', '', $pdata['filter']['superdeals'], $url_array);

		if (isset($_REQUEST['make']) && trim($_REQUEST['make'])){
			$pdata['filter']['make'] = $_REQUEST['make'];

			if (isset($_REQUEST['model']) && trim($_REQUEST['model'])){
				$pdata['filter']['model'] = $_REQUEST['model'];
			}
		}


		$url_array['sort'] = $pdata['sort'];

		$pdata['car_make_name'] = $car_make_name;
		$pdata['car_make_cd'] = $car_make_cd;
		$pdata['car_model_name'] = $car_model_name;
		$pdata['car_model_cd'] = $car_model_cd;

		$pdata['filter']['category'] = $pdata['filter']['category'];

    	 return $this->render('index', ['trans_advert_model' => $trans_advert_model, 'pdata' => $pdata, 'trans_statistic_count' => $trans_statistic_count, 'master_car_make' => $master_car_make, 'master_car_model' => $master_car_model, 'master_car_model_group' => $master_car_model_group, 'master_general' => $master_general, 'master_dealer' => $master_dealer, 'master_car_sub_part' => $master_car_sub_part, 'super_deals' => $super_deals, 'trans_order' => $trans_order]);
    }

    public function actionView()
    {
    	$master_car_make = new MasterCarMake();
    	$master_car_model = new MasterCarModel();
    	$master_car_sub_part = new MasterCarSubPart();
    	$master_dealer = new MasterDealer();
    	$master_dealer_review = new MasterDealerReview();
    	$master_dealer_review_info = new MasterDealerReviewInfo();
    	$super_deals = new SuperDeals();
    	$trans_order = new TransOrder();
    	$trans_order_extra_info = new TransOrderExtraInfo();
    	$trans_order_picture = new TransOrderPicture();

    	$pdata = [];

    	if (isset($_REQUEST['urn_no'])) {

    		if($trans_order_picture->findOne($_REQUEST['urn_no'])) {
    			$trans_advert_model = $trans_order->find()->where(['trans_order.urn_no' => $_REQUEST['urn_no'],  'classification_cd' => '1166'])->innerJoinWith("transOrderPictures")->one();
    		} else {
    			$trans_advert_model = $trans_order->find()->where(['urn_no' => $_REQUEST['urn_no'],  'classification_cd' => '1166'])->one();
    		}
    		

    		if ($trans_advert_model) {
    			$pdata['advert_seourl'] = Yii::$app->general->generateseo('autopart', $trans_advert_model->urn_no, $trans_advert_model->make_name . ' ' . $trans_advert_model->model_name);

	    		$pdata['master_dealer_model'] = $master_dealer->findOne($trans_advert_model->dealer_id);

	    		if ($pdata['master_dealer_model']) {
					$pdata['dealer_seourl'] = Yii::$app->general->generateseo('dealer', $pdata['master_dealer_model']->dealer_id, $pdata['master_dealer_model']->dealer_name);
				} else {
					$pdata['master_dealer_model'] = false;
					$pdata['dealer_seourl'] = Yii::$app->general->generateseo('dealer', $trans_advert_model->dealer_id, 'private');
				}

				$pdata['autopart_video'] = Yii::$app->api->getvideo($trans_advert_model->urn_no);

				$pdata['upd_data_date'] = strtotime($trans_advert_model->upd_data_date);

				$pdata['picture_array'] = [];

				$image_path = rtrim(Yii::$app->fpath->mountimages['path'], "/") . '/advert/wmfull/';

				foreach ($trans_advert_model->transOrderPictures as $trans_order_picture) {
					if (trim($trans_order_picture->picture_no)) {
						$pdata['picture_array'][$trans_order_picture->picture_no] = ['url' => Yii::$app->general->advertimageurl($trans_order_picture->picture_file, 'wmfull', $pdata['upd_data_date'],$trans_order_picture->is_new), 'thumb' => Yii::$app->general->advertimageurl($trans_order_picture->picture_file, 'webad', $pdata['upd_data_date'],$trans_order_picture->is_new), 'filename' => $trans_order_picture->picture_file];
					}

					if (!count($pdata['picture_array'])) {
					$pdata['picture_array'][1] = [
						'url' => Yii::$app->general->advertimageurl($ttrans_order_picture->picture_file, 'wmfull', $pdata['upd_data_date'], $trans_order_picture->is_new), 
						'thumb' => Yii::$app->general->advertimageurl($trans_order_picture->picture_file, 'webad', $pdata['upd_data_date'], $trans_order_picture->is_new), 
						'filename' => ''];
					}
				}

				$pdata['dealer_type'] = 'Dealer';

				if ($pdata['master_dealer_model']->dealer_type != 2) {
					if ($trans_advert_model->bodytype_name == 'carPost'){
						$pdata['dealer_type'] = ' Free Post User';
					} else {
						$pdata['dealer_type'] = ' Private User';
					}
				}

				switch($trans_advert_model->is_used_flg){
					case '1':
						$pdata['condition'] = 'Used';
						break;
					case '2':
						$pdata['condition'] = 'New';
						break;
					case '3':
						$pdata['condition'] = 'Recond';
						break;	
					default:
						$pdata['condition'] = 'Used';
						break;
				}

				$pdata['similar'] = $trans_order->similar('1166', 6, $trans_advert_model->make_cd, $trans_advert_model->model_cd, '', '');

				$pdata['bookmark_array'] = Yii::$app->general->advfrombookmark();

    			return $this->render('view', ['super_deals' => $super_deals,'trans_advert_model' => $trans_advert_model, 'pdata' => $pdata, 'trans_order_extra_info' => $trans_order_extra_info, 'master_car_make' => $master_car_make,  'master_car_model' => $master_car_model, 'master_car_sub_part' => $master_car_sub_part, 'master_dealer_review' => $master_dealer_review, 'master_dealer_review_info' => $master_dealer_review_info,]);
    		} else {
				return $this->redirect('/autopart');
    		}

    	} else {
    		return $this->redirect('/autopart');
    	}
    }
}