<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use app\models\TransOrder;
use app\models\TransOrderExtraInfo;
use app\models\TransStatisticCount;
use app\models\SuperDeals;

/**
 * Bookmark controller
 */
class BookmarkController extends Controller
{
	/**
     * Displays homepage.
     *
     * @return_no mixed
     */
    public function actionIndex()
    {
    	$trans_order = new TransOrder;
    	$trans_order_extra_info = new TransOrderExtraInfo;
    	$trans_statistic_count = new TransStatisticCount;
    	$super_deals = new SuperDeals;

    	$pdata = [];
		$pdata['bookmark_array'] = Yii::$app->general->advfrombookmark();
		$pdata['compare_array'] = Yii::$app->general->advfromcompare();

		$pdata['limit'] = 20;
		if (isset($_REQUEST['limit']) && $_REQUEST['limit']) {
			$pdata['limit'] = $_REQUEST['limit'];
			$offset = 0;
		}

		$pdata['page'] = 1;
		if (isset($_REQUEST['page']) && (int)$_REQUEST['page'] == $_REQUEST['page']) {
			$pdata['page'] = (int)$_REQUEST['page'];
			$offset = $pdata['limit'] * $pdata['page'];
		}

		$pdata['total'] = 0;

		$trans_advert_model = [];
		
		if (count($pdata['compare_array'])) {
			$trans_advert_criteria = $trans_order->find()->where(['in', 'urn_no', $pdata['compare_array']]);
			
			$pdata['compare_total'] = $trans_advert_criteria->count();

			$trans_advert_criteria->orderBy("upd_data_date DESC");
			$trans_advert_criteria->limit = 4;	

		}
		
		if (count($pdata['bookmark_array'])) {
			$trans_advert_criteria = $trans_order->find()->where(['in', 'trans_order.urn_no', $pdata['bookmark_array']]);
			
			$pdata['total'] = $trans_advert_criteria->count();

			$trans_advert_criteria->orderBy("upd_data_date DESC");
			$trans_advert_criteria->limit = 10;	

			$trans_advert_model = $trans_advert_criteria->all();
		}

    	return $this->render('index', ['trans_advert_model' => $trans_advert_model, 'trans_order_extra_info' => $trans_order_extra_info, 'trans_statistic_count' => $trans_statistic_count, 'super_deals' => $super_deals, 'pdata' => $pdata]);
    }

    public function actionAddbookmark() {
		$response_array = ['result' => 0, 'message' => 'Unknown error occurred, please try again.'];		

		if (isset($_REQUEST['urn_no']) && trim($_REQUEST['urn_no'])) {
			if (Yii::$app->general->advtobookmark($_REQUEST['urn_no'])) {
				$response_array['message'] = 'Success';
				$response_array['result'] = 1;
			}
		}
		
		echo json_encode($response_array);
	}

	public function actionRemovebookmark() {
		$response_array = ['result' => 0, 'message' => 'Unknown error occurred, please try again.'];		

		if (isset($_REQUEST['urn_no']) && trim($_REQUEST['urn_no'])) {
			if (Yii::$app->general->advbookmarkremove($_REQUEST['urn_no'])) {
				$response_array['message'] = 'Success';
				$response_array['result'] = 1;
			}
		}
		
		echo json_encode($response_array);
	}
}
?>