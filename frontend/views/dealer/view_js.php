<script type="text/javascript">
	var url_params = '<?php echo basename($_SERVER['REQUEST_URI']) ?>';
	if (url_params.includes("nav-dealer-review")) {
		jQuery('#icon-box-sale').removeClass('active');
		jQuery('#icon-box-review').addClass('active');

		jQuery("#dealer-sale").addClass('d-none');
    	jQuery("#dealer-review").removeClass('d-none');

        var aTop = (jQuery('#dealer-review').offset().top - 250);
        jQuery('html, body').animate({
            scrollTop: aTop 
        }, 1000);
	};

	 window.addEventListener("load", function(event) {
        lazyload();
    });

    jQuery("#icon-box-sale").click(function() {
    	jQuery("#icon-box-sale").addClass('active');
    	jQuery("#icon-box-review").removeClass('active');
    	jQuery("#dealer-sale").removeClass('d-none');
    	jQuery("#dealer-review").addClass('d-none');
    });

    jQuery("#icon-box-review").click(function() {
    	jQuery("#icon-box-sale").removeClass('active');
    	jQuery("#icon-box-review").addClass('active');
    	jQuery("#dealer-sale").addClass('d-none');
    	jQuery("#dealer-review").removeClass('d-none');
    });

    function popupreviewform() {
		var _title = _title || 'Write a Review';

		jQuery("#reivewForm #review-title").text(_title);

		jQuery("#popupReviewform").css("display","block");
	}
	
	function closeReviewForm(){
		jQuery("#popupReviewform").css("display","none");
	}
</script>