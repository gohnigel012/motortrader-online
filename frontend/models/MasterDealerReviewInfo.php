<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_dealer_review_info".
 *
 * @property int $master_dealer_review_info_id
 * @property string|null $dealer_id
 * @property string|null $dealer_name
 * @property string|null $review_status
 * @property string|null $admin_review_status
 * @property string $review_created
 * @property string|null $review_title
 * @property int|null $review_overall_rating
 * @property int|null $review_customer_service
 * @property int|null $review_pricing
 * @property int|null $review_showroom_environment
 * @property int|null $review_buying_process
 * @property string|null $review_make_a_purchases
 * @property string|null $review_recommend_dealer
 * @property string|null $review_user_name
 * @property string|null $review_user_city_state
 * @property string|null $review_user_phone_no
 * @property string|null $review_user_email
 * @property string|null $review_picture
 * @property string|null $review_description
 * @property string|null $dealer_comment
 */
class MasterDealerReviewInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_dealer_review_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['review_created'], 'required'],
            [['review_created'], 'safe'],
            [['review_overall_rating', 'review_customer_service', 'review_pricing', 'review_showroom_environment', 'review_buying_process'], 'default', 'value' => null],
            [['review_overall_rating', 'review_customer_service', 'review_pricing', 'review_showroom_environment', 'review_buying_process'], 'integer'],
            [['review_description', 'dealer_comment'], 'string'],
            [['dealer_id', 'review_user_name'], 'string', 'max' => 20],
            [['dealer_name', 'review_user_city_state'], 'string', 'max' => 80],
            [['review_status', 'admin_review_status', 'review_make_a_purchases', 'review_recommend_dealer'], 'string', 'max' => 1],
            [['review_title', 'review_user_email'], 'string', 'max' => 255],
            [['review_user_phone_no'], 'string', 'max' => 30],
            [['review_picture'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'master_dealer_review_info_id' => 'Master Dealer Review Info ID',
            'dealer_id' => 'Dealer ID',
            'dealer_name' => 'Dealer Name',
            'review_status' => 'Review Status',
            'admin_review_status' => 'Admin Review Status',
            'review_created' => 'Review Created',
            'review_title' => 'Review Title',
            'review_overall_rating' => 'Review Overall Rating',
            'review_customer_service' => 'Review Customer Service',
            'review_pricing' => 'Review Pricing',
            'review_showroom_environment' => 'Review Showroom Environment',
            'review_buying_process' => 'Review Buying Process',
            'review_make_a_purchases' => 'Review Make A Purchases',
            'review_recommend_dealer' => 'Review Recommend Dealer',
            'review_user_name' => 'Review User Name',
            'review_user_city_state' => 'Review User City State',
            'review_user_phone_no' => 'Review User Phone No',
            'review_user_email' => 'Review User Email',
            'review_picture' => 'Review Picture',
            'review_description' => 'Review Description',
            'dealer_comment' => 'Dealer Comment',
        ];
    }

    public function countdealerreviewinfo($id)
    {
        $from_date = date("Y-m-d", strtotime("- 24 months"));
        $to_date = date("Y-m-d");
        
        $master_dealer_review_info_model = $this->find()->where(['and', ['>=', 'review_created', date("Y-m-d 00:00:00", strtotime($from_date))], ['<=', 'review_created', date("Y-m-d 23:59:59", strtotime($to_date))], ['dealer_id' => $id]])->orderBy('master_dealer_review_info_id DESC')->all();
        if ($master_dealer_review_info_model) {
            return count($master_dealer_review_info_model);
        }
        return false;                                                                   
    }

    public function getdealerreviewinfo($id)
    {
        $master_dealer_review_model = $this->find()->where(['dealer_id' => $id])->one();
        if ($master_dealer_review_model) {
            return $master_dealer_review_model;
        }
        return false;                                                                   
    }

    public static function getaveragerating($id)
    {
        $total_overall = 0;
        $average_total_overall = 0;
        
        $from_date = date("Y-m-d", strtotime("- 24 months"));
        $to_date = date("Y-m-d");
        
        $master_dealer_review_info_model_average_overall = MasterDealerReviewInfo::find()->where(['and', ['>=', 'review_created',  date("Y-m-d 00:00:00", strtotime($from_date))], ['<=', 'review_created', date("Y-m-d 23:59:59", strtotime($to_date))], ['dealer_id' => $id]])->all();
        if ($master_dealer_review_info_model_average_overall) { 
            $count = count($master_dealer_review_info_model_average_overall);
            if (count($master_dealer_review_info_model_average_overall) > 0) {                   
                foreach($master_dealer_review_info_model_average_overall as $master_dealer_review_info_model_average_overall_data_loop) {  
                    $total_overall += $master_dealer_review_info_model_average_overall_data_loop->review_overall_rating;
                }
            }
            
            $average_total_overall = round(($total_overall / $count));

            return $average_total_overall;
        }
        return false;                                                                   
    }
}
