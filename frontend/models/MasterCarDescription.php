<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_car_description".
 *
 * @property int $master_car_description_id
 * @property string $make_cd
 * @property string $model_cd
 * @property string|null $upd_user
 * @property string|null $upd_date
 * @property string|null $disp_flg
 * @property string|null $description
 */
class MasterCarDescription extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_car_description';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['make_cd', 'model_cd'], 'required'],
            [['upd_date'], 'safe'],
            [['description'], 'string'],
            [['make_cd', 'model_cd', 'upd_user'], 'string', 'max' => 20],
            [['disp_flg'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'master_car_description_id' => 'Master Car Description ID',
            'make_cd' => 'Make Cd',
            'model_cd' => 'Model Cd',
            'upd_user' => 'Upd User',
            'upd_date' => 'Upd Date',
            'disp_flg' => 'Disp Flg',
            'description' => 'Description',
        ];
    }
}
