<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = 'Motor Trader Dealer';
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-home">
	<div class="jumbotron">
		<p class="text-left">
			<?php echo $pdata['total']; ?> car Dealers in <?php echo $pdata['page_location']; ?>
		</p>
    </div>

    <div class="car-search__row">
    	<div class="row">
    		<div class="col-lg-3">
    			<div class="car-search__column">
    				<div class="row">
						<div class="col-lg-12 col-8">
							<div class="car-search__border">
								<img src="/images/search.png" class="car-search-feedback" alt="Search icon">
								<input type="text" name="keyword" id="" class="form-control car-search-text" placeholder="Search" value="<?php echo (isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '') ?>" autocomplete="off"/>
							</div>
						</div>
						<div class="col-lg-12 col-4">
							<div class="car-search__column-responsive">
								<div class="row">
									<div class="col-6">
										<p>
											<span class="header__burger"></span>
									        <span class="header__burger header__burger--adjust"></span>
									        <span class="header__burger"></span>
										</p>
									</div>
									<div class="col-6">
										<p class="car-search__filter-text">Filter</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="car-search__desktop">
						<div class="car-search__clear car-search__border"><span style="font-size: 25px;">Filter</span> <span><a href="/dealer" class="car-search__link">Clear all</a></span></div>
						<div class="car-search__border">
							<p class="car-search__clear car-search__clear--margin">Location <img id="dealer-state-open" <?php echo (isset($_REQUEST['location']) && $_REQUEST['location'] != '' ) ? 'src="/images/close.png"' : 'src="/images/plus.png"'; ?>></p>
<?php
	$location_array = $master_general->getLocation();
	if (count($location_array)) {
?>			
						<p class="<?php echo (isset($_REQUEST['location']) && $_REQUEST['location'] != '' ) ? '' : 'd-none'; ?>" id="dealer-state-show">
<?php
		foreach ($location_array as $location_array_id_loop => $location_array_data_loop) {
?>
							<a class="btn btn-default car-search__button <?php echo (isset($_REQUEST['location']) && $_REQUEST['location'] == $location_array_id_loop ) ? 'active' : ''; ?>" href="<?php echo Yii::$app->general->generatecarsseo('dealer', '', '', $location_array_id_loop, 0); ?>"><?php echo $location_array_data_loop; ?></a>	
<?php
		}
?>							
						</p>
<?php
	}
?>		
						</div>
						<div class="car-search__border">
							<p class="car-search__clear car-search__clear--margin">Dealer <img id="dealer-dealer-open" <?php echo (isset($_REQUEST['dealers_type']) && $_REQUEST['dealers_type'] != '' ) ? 'src="/images/close.png"' : 'src="/images/plus.png"'; ?>></p>
							<p class="<?php echo (isset($_REQUEST['dealers_type']) && $_REQUEST['dealers_type'] != '' ) ? '' : 'd-none'; ?>" id="dealer-dealer-show"><a class="btn btn-default car-search__button <?php echo ( (isset($_REQUEST['transmission']) && $_REQUEST['dealers_type'] == '1' ) ) ? 'active' : ''?>" href="<?php echo Yii::$app->general->generatecarsseo('dealer', 0, 0, '', 0, '', '', '', 0, ['dealers_type' => '1']); ?>">Dealers</a> <a class="btn btn-default car-search__button <?php echo ( (isset($_REQUEST['transmission']) && $_REQUEST['dealers_type'] == '2' ) ) ? 'active' : ''?>" href="<?php echo Yii::$app->general->generatecarsseo('dealer', 0, 0, '', 0, '', '', '', 0, ['dealers_type' => '2']); ?>">Private dealers</a></p>
						</div>
					</div>	
    			</div>
			</div>
			<div class="col-lg-9">
				<div class="car-list">
					<div class="car-list__head">
<?php
	if (isset($pdata['page_year']) && $pdata['page_year'] != '') {
		$total_advert = '';
	} else {
		$total_advert = (isset($pdata['total']) && $pdata['total'] != '0' ? number_format($pdata['total'], 0) : '');
	}
?>				
						<div class="row">
							<div class="col-lg-6">
								<p class="car-list__show">Showing <?php echo $total_advert; ?> results</p>
							</div>
							<div class="col-lg-6">
								<p class="car-list__sort">
									Sort by 
									<select class="form-control" onchange="location = this.value;">
										<option <?php if ($pdata['sort'] == '') echo "selected"; ?> disabled>Select one</option>
										<option <?php if($pdata['sort'] == 'totalstockasc') echo "selected"; ?> value="<?php echo $pdata['url_sort_totalstock_low']; ?>">Total stock (ascending)</option>
										<option <?php if($pdata['sort'] == 'totalstockdesc') echo "selected"; ?> value="<?php echo $pdata['url_sort_totalstock_high']; ?>">Total stock (descending)</option>
										<option <?php if ($pdata['sort'] == 'nameasc') echo "selected"; ?> value="<?php echo $pdata['url_sort_name_low']; ?>">Dealer name (A_Z)</option>
										<option <?php if ($pdata['sort'] == 'namedesc') echo "selected"; ?> value="<?php echo $pdata['url_sort_name_high']; ?>">Dealer name (Z_A)</option>
									</select>
								</p>
							</div>
						</div>
					</div>
					<div class="row">
<?php
	if (count($dealer_array)) {
		$count_dealer = 0;
		foreach ($dealer_array as $dealer_id_loop => $dealer_data_loop) {
            $dealer_seo_url = Yii::$app->general->generateseo('dealer', $dealer_id_loop, $dealer_data_loop['dealer_name']);
            $count_dealer++;
?>			
						<div class="col-lg-4">
							<div class="superdeals-featured-cnabadv__featured-ads-section-listing superdeals-featured-cnabadv__featured-ads-section-listing--adjust">
								<p class="car-list__position">
<?php 
	if ($dealer_data_loop['master_dealer_logo'] != '') {
		$dealer_logo_image = Yii::$app->general->dealer_cover_logo_image($dealer_data_loop['master_dealer_logo'], strtotime("now"), $dealer_data_loop['dealer_id']);
		if ($dealer_logo_image) {
?>									
								<a href="<?php echo $dealer_seo_url; ?>">
									<img class="featured-ads-section__img featured-ads-section__img--adjust lazyload" src="<?php echo Yii::$app->fpath->images("ajax-loader.gif"); ?>" data-src="<?php echo (@getimagesize($dealer_logo_image)) ? $dealer_logo_image : '/images/dealerexample.png'; ?>" alt="Dealer">
								</a>
<?php 
		} 
	} else {
?>
								<a href="<?php echo $dealer_seo_url; ?>">
									<img class="featured-ads-section__img featured-ads-section__img--adjust lazyload" src="<?php echo Yii::$app->fpath->images("ajax-loader.gif"); ?>" data-src="/images/dealerexample.png" alt="Dealer">
								</a>
<?php			
	}	
?>								
								</p>
								<p class="text-center featured-ads-section__title"><a href="<?php echo $dealer_seo_url; ?>"><?php echo $dealer_data_loop['dealer_name']; ?></a></p>
								<p>
<?php 
	$master_dealer_review_model = $master_dealer_review->getdealerreviewinfo($dealer_id_loop);
	if ($master_dealer_review_model) {
		if ($master_dealer_review_model->master_dealer_review_status == '1') {
			
			$count_master_dealer_review_info = $master_dealer_review_info->countdealerreviewinfo($master_dealer_review_model->dealer_id);

			$average_total_overall = $master_dealer_review_info->getaveragerating($master_dealer_review_model->dealer_id);
	
			switch ($average_total_overall) {
				case '1':
					echo '	<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star-empty.png">
							<img class="dealer-list__star" src="/images/star-empty.png">
							<img class="dealer-list__star" src="/images/star-empty.png">
							<img class="dealer-list__star" src="/images/star-empty.png">
							<span class="dealer-list__rating"> 1.0 </span>
							<a class="dealer-list__review" href="' . $dealer_seo_url . '&type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '1.5':
					echo '	<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star-empty.png">
							<img class="dealer-list__star" src="/images/star-empty.png">
							<img class="dealer-list__star" src="/images/star-empty.png">
							<span class="dealer-list__rating"> 1.5 </span>
							<a class="dealer-list__review" href="' . $dealer_seo_url . '&type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '2':
					echo '	<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star-empty.png">
							<img class="dealer-list__star" src="/images/star-empty.png">
							<img class="dealer-list__star" src="/images/star-empty.png">
							<span class="dealer-list__rating"> 2.0 </span>
							<a class="dealer-list__review" href="' . $dealer_seo_url . '&type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '2.5':
					echo '	<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star-empty.png">
							<img class="dealer-list__star" src="/images/star-empty.png">
							<span class="dealer-list__rating"> 2.5 </span>
							<a class="dealer-list__review" href="' . $dealer_seo_url . '&type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '3':
					echo '	<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star-empty.png">
							<img class="dealer-list__star" src="/images/star-empty.png">
							<span class="dealer-list__rating"> 3.0 </span>
							<a class="dealer-list__review" href="' . $dealer_seo_url . '&type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '3.5':
					echo '	<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star-empty.png">
							<span class="dealer-list__rating"> 3.5</span>
							<a class="dealer-list__review" href="' . $dealer_seo_url . '&type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '4':
					echo '	<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star-empty.png">
							<span class="dealer-list__rating"> 4.0</span>
							<a class="dealer-list__review" href="' . $dealer_seo_url . '&type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '4.5':
					echo '	<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star.png">
							<span class="dealer-list__rating"> 4.5</span>
							<a class="dealer-list__review" href="' . $dealer_seo_url . '&type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '5':
					echo '	<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star.png">
							<img class="dealer-list__star" src="/images/star.png">
							<span class="dealer-list__rating"> 5.0</span>
							<a class="dealer-list__review" href="' . $dealer_seo_url . '&type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '0':
					echo '	<img class="dealer-list__star" src="/images/star-empty.png">
							<img class="dealer-list__star" src="/images/star-empty.png">
							<img class="dealer-list__star" src="/images/star-empty.png">
							<img class="dealer-list__star" src="/images/star-empty.png">
							<img class="dealer-list__star" src="/images/star-empty.png">
							<span class="dealer-list__rating"> 0.0</span>
							<a class="dealer-list__review" href="' . $dealer_seo_url . '&type=nav-dealer-review">
								<span class="rml-5">No review yet</span>
							</a>';
				break;
			}
		}
	} else {
		echo '	<img class="dealer-list__star" src="/images/star-empty.png">
				<img class="dealer-list__star" src="/images/star-empty.png">
				<img class="dealer-list__star" src="/images/star-empty.png">
				<img class="dealer-list__star" src="/images/star-empty.png">
				<img class="dealer-list__star" src="/images/star-empty.png">
				<span class="dealer-list__rating"> 0.0</span>
				<a class="dealer-list__review" href="' . $dealer_seo_url . '?type=nav-dealer-review">
					<span>No review yet</span>
				</a>';
	}
?>
								</p>
								<p><a class="dealer-list__view" href="<?php echo $dealer_seo_url; ?>">View all <?php echo $dealer_data_loop['total_stock']; ?> cars / items</a></p>
								<hr>
								<div class="row">
									<div class="col-6">
										<div class="row">
					                        <div class="col-4">
					                            <p><img class="featured-ads-section__icon" src="images/dealer.png" alt="dealer type" /></p>
					                        </div>
					                        <div class="col-8">
					                            <span class="featured-ads-section__text">Dealer type</span>
					                            <p>
<?php
	switch ($dealer_data_loop['dealer_type']) {
		case '1':
			echo '<span>Dealer</span>';
		break;
		case '2':
			echo '<span>Private</span>';
		break;
	}
?>		                            	
					                            </p>
					                        </div>
					                    </div>
									</div>
									<div class="col-6">
										<div class="row">
					                        <div class="col-4">
					                            <p><img class="featured-ads-section__icon" src="images/location.png" alt="location" /></p>
					                        </div>
					                        <div class="col-8">
					                            <span class="featured-ads-section__text">Location</span>
					                            <p><?php echo $dealer_data_loop['location_name']; ?></p>
					                        </div>
					                    </div>
									</div>
								</div>
							</div>					
						</div>
			
<?php
		}
	}
?>
					</div>
<?php
	if ($pdata['total'] > 0) {
		$pagination_range = 5;

        $start_page = $pdata['page'] - floor($pagination_range / 2);
        if ($start_page < 1) {
            $start_page = 1;
        }

        $total_end_page = ceil($pdata['total'] / $pdata['limit']);

        $end_page = $pdata['page'] + floor($pagination_range / 2);
        if ($end_page > $total_end_page) {
            $end_page = $total_end_page;
        }

        if (($end_page - $pdata['page']) < floor($pagination_range / 2) && $pagination_range < floor($pdata['total'] / $pdata['limit'])) {
            $start_page = ceil($pdata['total'] / $pdata['limit']) - $pagination_range;
        }

        if ($end_page < $pagination_range && floor($pdata['total'] / $pdata['limit']) >= $pagination_range) {
            $end_page = $pagination_range;
        }
?>
					<div>
						<p class="text-center featured-ads-section__page">
<?php
		if ($pdata['page'] > 1) {
			$processed_link = str_replace('__PAGE__', ($pdata['page'] - 1), $pdata['url_pagination']);
?>					
								<span class="<?php echo ($start_page > 1 ? '' : 'hidden-md-up'); ?>"><a href="<?php echo $processed_link; ?>"><img src="/images/previous.png" alt="Previous"></a>
<?php
		}
?>					
					<span><?php echo $pdata['page'] . ' / ' . $end_page; ?></span>
<?php
		if ($pdata['page'] < ceil($pdata['total'] / $pdata['limit']) ) {
			$processed_link = str_replace('__PAGE__', ($pdata['page'] + 1), $pdata['url_pagination']);
?>					
								<a href="<?php echo $processed_link; ?>"> <img src="/images/next.png" alt="Next"></a></span></a></span>
<?php
		}
?>				
						</p>
					</div>
<?php
	}
?>
					
				</div>
		    </div>
    	</div>
	</div>
</div>