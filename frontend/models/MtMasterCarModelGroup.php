<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mt_master_car_model_group".
 *
 * @property string $make_cd
 * @property string $group_name
 * @property string|null $model_group
 */
class MtMasterCarModelGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mt_master_car_model_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['make_cd', 'group_name'], 'required'],
            [['group_name', 'model_group'], 'string'],
            [['make_cd'], 'string', 'max' => 20],
            [['make_cd', 'group_name'], 'unique', 'targetAttribute' => ['make_cd', 'group_name']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'make_cd' => 'Make Cd',
            'group_name' => 'Group Name',
            'model_group' => 'Model Group',
        ];
    }
}
