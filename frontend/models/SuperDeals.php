<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "super_deals".
 *
 * @property int $super_deals_id
 * @property string $urn_no
 * @property string|null $status
 * @property string|null $schedule_status
 * @property float|null $regular_price
 * @property float|null $sale_price
 * @property string|null $super_deals_start_date
 * @property string|null $super_deals_end_date
 * @property string|null $super_deals_schedule_start_date
 * @property string|null $super_deals_schedule_end_date
 */
class SuperDeals extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'super_deals';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['urn_no'], 'required'],
            [['regular_price', 'sale_price'], 'number'],
            [['super_deals_start_date', 'super_deals_end_date', 'super_deals_schedule_start_date', 'super_deals_schedule_end_date'], 'safe'],
            [['urn_no'], 'string', 'max' => 20],
            [['status', 'schedule_status'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'super_deals_id' => 'Super Deals ID',
            'urn_no' => 'Urn No',
            'status' => 'Status',
            'schedule_status' => 'Schedule Status',
            'regular_price' => 'Regular Price',
            'sale_price' => 'Sale Price',
            'super_deals_start_date' => 'Super Deals Start Date',
            'super_deals_end_date' => 'Super Deals End Date',
            'super_deals_schedule_start_date' => 'Super Deals Schedule Start Date',
            'super_deals_schedule_end_date' => 'Super Deals Schedule End Date',
        ];
    }

    public function getsuperdeals($urn_no) {
        $response_array = [];

        $super_deals_memcache_key = "/super_deals/getsuperdeals/urn_no/" . $urn_no;
        $super_deals_memcache = Yii::$app->cache->get($super_deals_memcache_key);
        if ($super_deals_memcache !== false) {
            $response_array = $super_deals_memcache;
        } else {
            $super_deals_model = $this->find()->where(['and', ['=', 'urn_no', $urn_no], ['<=', 'super_deals_start_date', date("Y-m-d H:i:s")], ['>=', 'super_deals_end_date', date("Y-m-d H:i:s")]])->one();
            if ($super_deals_model) {
                $got_super_deals = '0';
                if ($super_deals_model['urn_no'] === $urn_no) {
                    if ($super_deals_model['schedule_status'] === '1') {
                        if (date("Y-m-d H:i:s") >= $super_deals_model['super_deals_schedule_start_date'] && date("Y-m-d H:i:s") <= $super_deals_model['super_deals_schedule_end_date']) {
                            $got_super_deals = '1';
                        }
                    } else {
                        $got_super_deals = '1';
                    }
                } else {
                    $got_super_deals = '0';
                }
                
                $response_array = ['got_super_deals' => $got_super_deals,
                                        'super_deals_schedule_start_date' => $super_deals_model['super_deals_schedule_start_date'],
                                        'super_deals_schedule_end_date' => $super_deals_model['super_deals_schedule_end_date'],
                                        'sale_price' => $super_deals_model['sale_price'],
                                        'regular_price' => $super_deals_model['regular_price'],
                                        'schedule_status' => $super_deals_model['schedule_status'],
                                        ];
                
                Yii::$app->cache->set($super_deals_memcache_key, $response_array, 600);
            } else {
                $response_array = ['got_super_deals' => '0',
                                        'super_deals_schedule_start_date' => '',
                                        'super_deals_schedule_end_date' => '',
                                        'sale_price' => '0',
                                        'regular_price' => '0',
                                        'schedule_status' => '0',
                                        ];
            }
        }

        return $response_array;
    }

    public function getsimilarsuperdeals($make_cd, $model_cd) {
        $response_array = [];
        $advert_image = '';

        $similar_super_deals_memcache_key = "/super_deals/getsimilarsuperdeals/" . $make_cd . "/" . $model_cd;
        $similar_super_deals_memcache = Yii::$app->cache->get($similar_super_deals_memcache_key);
        if ($similar_super_deals_memcache !== false) {
            $response_array = $similar_super_deals_memcache;
        } else {
            $super_deals_model = $this->find()->where("super_deals_start_date <= :date AND super_deals_end_date >= :date", [':date'=> date("Y-m-d H:i:s")])->orderBy('super_deals_start_date DESC')->all();
            if ($super_deals_model) {
                foreach ($super_deals_model as $super_deals_data_loop) {
                    
                    $trans_order_model = TransOrder::model()->getadvertinfo($super_deals_data_loop->urn_no);
                    if (isset($trans_order_model->make_cd) && $trans_order_model->make_cd == $make_cd && $trans_order_model->model_cd == $model_cd) {
                        
                        if ($trans_order_model->is_new == 1) {
                            $advert_image = Yii::app()->general->advertimageurl($super_deals_data_loop->urn_no . '.jpg', 'medium', strtotime($super_deals_data_loop->upddate),$trans_order_model->is_new);
                        } else {
                            $advert_image = Yii::app()->general->advertimageurl($super_deals_data_loop->urn_no . '.jpg', 'webad', strtotime($super_deals_data_loop->upddate),$trans_order_model->is_new);
                        }
                        
                        $seo_url = Yii::app()->general->generateseo('car', $trans_order_model->urn_no, $trans_order_model->car_info);
                    
                        $response_array[] = array(  'urn_no' => $super_deals_data_loop->urn_no,
                                                    'image' => $advert_image,
                                                    'seourl' => $seo_url,
                                                    'car_info' => $trans_order_model->car_info,
                                                    'sale_price' => 'RM' . number_format($super_deals_data_loop->sale_price),
                                                    'regular_price' => 'RM' . number_format($super_deals_data_loop->regular_price),
                                                );
                    }
                }
                Yii::app()->cache->set($similar_super_deals_memcache_key, $response_array, 600);
            }
        }

        return $response_array;
    }
}
