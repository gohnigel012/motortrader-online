<?php
namespace frontend\components;
use Yii;
use yii\base\Component;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\web\UploadedFile;
use yii\helpers\Url;
use linslin\yii2\curl;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Fpath extends Component {
	public $cdn;
    public $css;
    public $images;
    public $assets;
    public $mtsa;
	public $mountimages;
	public $s3;
    public $gookantei;
	public $s3_flag = '0';
	public $s3_domain = '';

    public function assets($image_name) {
        $display_image_name = Yii::$app->fpath->assets['path'] . '/' . $image_name;
        if (file_exists($display_image_name)) {
            $display_image_name = Yii::$app->fpath->assets['url'] . '/' . $image_name;
        } else {
            $display_image_name = false;
        }

        return $display_image_name;
    }

    public function images($image_name) {
        $display_image_name = Yii::$app->fpath->images['path'] . '/' . $image_name;
        if (file_exists($display_image_name)) {
            $display_image_name = Yii::$app->fpath->images['url'] . '/' . $image_name;
        } else {
            $display_image_name = false;
        }

        return $display_image_name;
    }
    
    public function css($css_name) {
        $display_image_name = Yii::$app->fpath->css['path'] . '/' . $css_name;
        if (file_exists($display_image_name)) {
            $display_image_name = Yii::$app->fpath->css['url'] . '/' . $css_name;
        } else {
            $display_image_name = false;
        }

        return $display_image_name;
    }
    
    public function mountimages($image_name) {
		
		$user_id = Yii::$app->general->getuserid();
		$target_osc_folder = $user_id . '-' . md5($user_id . 'car');
		
		$display_image_name = rtrim(Yii::$app->fpath->mountimages['path'], '/') . '/freepost-tmp/' . $target_osc_folder . '/webad-' . $image_name;
		if (file_exists($display_image_name)) {
			$display_image_name = rtrim(Yii::$app->fpath->mountimages['url'], '/') . '/freepost-tmp/' . $target_osc_folder . '/webad-' . $image_name;
		} else {
			$display_image_name = 'https://s3.ap-south-1.amazonaws.com/take-my-order/default/missing.png';
		}

        return $display_image_name;
    }
}