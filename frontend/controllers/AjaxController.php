<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use app\models\MasterCarModel;
use app\models\MasterCarModelGroup;
use app\models\SuperDeals;
use app\models\TransOrder;
use app\models\TransStatisticCount;

/**
 * Ajax controller
 */
class AjaxController extends Controller
{
	public function actionNews() {
		$response_str = Yii::$app->api->wpnews();
		echo $response_str;
	}

	public function actionModel() {
		$response_array = ['result' => 0, 'model' => [], 'message' => 'Unknown error occurred, please try again.'];	

		$from_osc = (isset($_REQUEST['is_osc']) ? (int)$_REQUEST['is_osc'] : 0);

		$master_car_model = new MasterCarModel();
		$master_car_model_group = new MasterCarModelGroup();
		$trans_statistic_count = new TransStatisticCount();

		if (isset($_REQUEST['make']) && trim($_REQUEST['make'])) {
			$response_array['result'] = 1;

			$model_array = [];
			$car_model_group_array = [];
			$display_number_model_group = 0;
			
			$master_car_memcache_key = "/master_car_model/make/" . $_REQUEST['make'] . '/osc/' . $from_osc;
			
			$master_car_group_model_memcache_key = "/master_car_group_model/make/" . $_REQUEST['make'] . '/osc/' . $from_osc;
			
			$master_car_memcache = Yii::$app->cache->get($master_car_memcache_key);
			$master_car_group_model_memcache = Yii::$app->cache->get($master_car_group_model_memcache_key);
			if ($master_car_memcache !== false && $master_car_group_model_memcache !== false) {
				$model_array = $master_car_memcache;
				$car_model_group_array = $master_car_group_model_memcache;
			} else {

				$tmp_model_array = $master_car_model->getmodelbymake($_REQUEST['make']);
				$model_group_array = $master_car_model_group->getmodelgroup($_REQUEST['make']);
				if (count($tmp_model_array)) {
					foreach ($tmp_model_array as $tmp_model_id_loop => $tmp_model_data_loop) {
						if ($from_osc) {
							$model_array[] = ['id' => $tmp_model_id_loop, 'name' => $tmp_model_data_loop];
						} else {
							$display_number = $trans_statistic_count->displaycount('car_model', $tmp_model_id_loop);
							$is_model_group = false;
							
							if ($display_number !== false) {
								if (count($model_group_array)) {
									foreach ($model_group_array as $model_group_array_loop => $model_group_array_data_loop) {
										// foreach ($model_group_array_data_loop['model_group'] as $model_group_key_loop => $model_group_data_loop) {
										// 	if ($tmp_model_id_loop == $model_group_data_loop) {
										// 		$is_model_group = true;
										// 	}
										// }
									}
									
									if ($is_model_group == false) {
										$model_array[] = ['id' => $tmp_model_id_loop, 'name' => $tmp_model_data_loop . ' ' . $display_number];
									}
								} else {
									$model_array[] = ['id' => $tmp_model_id_loop, 'name' => $tmp_model_data_loop . ' ' . $display_number];
								}
							}
						}
					}
				}
				
				if (count($model_group_array)) {
					$count = 0;
					foreach ($model_group_array as $model_group_array_loop => $model_group_array_data_loop) {
						$count++;
						$group_name = $model_group_array_data_loop['group_name'];
						// $model_group_list = $model_group_array_data_loop['model_group_list'];
						$display_number_model_group = 0;
						// foreach ($model_group_array_data_loop['model_group'] as $model_group_key_loop => $model_group_data_loop) {
						// 	if (count($tmp_model_array)) {
						// 		foreach ($tmp_model_array as $tmp_model_id_loop => $tmp_model_data_loop) {
						// 			$display_number = $trans_statistic_count->displaycount('car_model', $tmp_model_id_loop);
						// 			if ($display_number !== false) {
						// 				if ($tmp_model_id_loop == $model_group_data_loop) {
						// 					$display_number = str_replace(array('(', ')'),"",$display_number);
						// 					$display_number_model_group += $display_number;
						// 					$is_model_group = true;
						// 				}
						// 			}
						// 		}
						// 	}
						// }
						
						if ($is_model_group == true && $display_number_model_group != 0) {
							$car_model_group_array[$count] = ['id' => $model_group_list, 'name' => $group_name . ' ' . '(' . $display_number_model_group . ')'];
						}
					}
				} 

				Yii::$app->cache->set($master_car_memcache_key, $model_array, 240);
				Yii::$app->cache->set($master_car_group_model_memcache_key, $car_model_group_array, 240);
			}
			
			$response_array['message'] = 'Success';
			$response_array['model'] = $model_array;
			$response_array['car_model_group'] = $car_model_group_array;
		}
		
		echo json_encode($response_array);
	}

	public function actionLogin() {
		$response_array = [
			'result'=> 0,
			'message'=> 'Unknown error occurred, please try again.',
		];

		if (isset($_SESSION['user_id']) && trim($_SESSION['user_id'])) {
			$response_array['message'] = 'You have logged in.';
			$response_array['result'] = 1;

		} else if (!isset($_REQUEST['username']) || trim($_REQUEST['username']) == '') {
			$response_array['message'] = 'Invalid username.';

		} else if (!isset($_REQUEST['password']) || trim($_REQUEST['password']) == '') {
			$response_array['message'] = 'Invalid password.';

		} else {
			$username = trim($_REQUEST['username']);
			$password = trim($_REQUEST['password']);

			if ($user_id = Yii::$app->api->login($username, $password, $response_array['message'])) {
				Yii::$app->general->setuserid($user_id, $username);
				
				$response_array['result'] = 1;
				$response_array['message'] = 'Login success';
			}
		}

		echo json_encode($response_array);
	}

	public function actionRegister() {
		$response_array = [
			'result'=> 0,
			'message'=> 'Unknown error occurred, please try again.',
		];

		if (isset($_SESSION['user_id']) && trim($_SESSION['user_id'])) {
			$response_array['message'] = 'You have logged in.';
			$response_array['result'] = 1;


		} else if (!isset($_REQUEST['username']) || trim($_REQUEST['username']) == '') {
			$response_array['message'] = 'Invalid username.';

		} else if (!isset($_REQUEST['password']) || trim($_REQUEST['password']) == '') {
			$response_array['message'] = 'Invalid password.';

		} else if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
			$response_array['message'] = 'Invalid email.';

		} else if (!isset($_REQUEST['phone']) || trim($_REQUEST['phone']) == '') {
			$response_array['message'] = 'Invalid phone.';

		} else {

			$username = trim($_REQUEST['username']);
			$password = trim($_REQUEST['password']);
			$email = trim($_REQUEST['email']);
			$phone = trim($_REQUEST['phone']);

			$uuid = '';
			$social = '';

			if (isset($_REQUEST['uuid']) && isset($_REQUEST['social']) &&
				trim($_REQUEST['uuid']) && trim($_REQUEST['social'])
			) {
				$uuid = $_REQUEST['uuid'];
				$social = $_REQUEST['social'];
			}

			if ($user_id = Yii::$app->api->register($username, $password, $email, $phone, 'private', $social, $uuid, $response_array['message'])) {
				Yii::$app->general->setuserid($user_id, $username);

				$response_array['result'] = 1;
				$response_array['message'] = 'Register success';
			}
		}

		echo json_encode($response_array);
	}

	public function actionRemovecompare() {
		$response_array = ['result' => 0, 'message' => 'Unknown error occurred, please try again.', 'html' => '', 'count' => ''];

		$trans_order = new TransOrder();		

		if (isset($_REQUEST['urn']) && trim($_REQUEST['urn'])) {
			if (Yii::$app->general->advcompareremove($_REQUEST['urn'])) {
				$response_array['message'] = 'Success';
				$response_array['result'] = 1;
				$compare_total = 0;
				
				$compare_array = Yii::$app->general->advfromcompare();
				if (count($compare_array)) {
					$trans_advert_criteria = ['and', 'urn_no', $compare_array];
					$compare_total = $trans_order->find()->where($trans_advert_criteria)->count();
				}
				
				$response_array['html'] = $this->render('compare', ['urn_no' => $_REQUEST['urn']], true);
			
				if (count($compare_array) > 0) {
					$response_array['count'] = $compare_total;
				} else {
					$response_array['count'] = 0;
				}
			}
		}
		
		echo json_encode($response_array);
	}

	public function actionLoadcompare() {
		$response_array = ['result' => 0, 'message' => 'Unknown error occurred, please try again.', 'html' => '', 'count' => ''];

		$response_array['message'] = 'Success';
		$response_array['result'] = 1;
		$compare_total = 0;

		$trans_order = new TransOrder();
		$super_deals = new SuperDeals();

		$compare_array = Yii::$app->general->advfromcompare();

		if (count($compare_array)) {
			$trans_advert_criteria = ['and', 'urn_no', $compare_array];
			$compare_total= $trans_order->find()->where($trans_advert_criteria)->count();
		}
		
		$response_array['html'] = $this->render('compare', ['trans_order' => $trans_order, 'super_deals' => $super_deals], true);
		$response_array['count'] = $compare_total;
		
		echo json_encode($response_array);
	}
}
?>