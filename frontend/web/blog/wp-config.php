<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mt_blog' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'xWDlY9uQ<f`Fe8j$q$Lo,2Y2U1+4<WFq.</XMpY5w|%v6#]~HPeoNqblboPZjV/J' );
define( 'SECURE_AUTH_KEY',  'PBO%3AfC&I,BLR uHjjIiJh{,0(v_C0CtCgi&oyI]2L1r~fX/6{__JF2N3F X8^e' );
define( 'LOGGED_IN_KEY',    '?CZ3jj H`_JuUK_5w ARqSi;b:zx~0GPC/Frl3uxZ4-.UiLX7f&gB.`!!0 Tz@Wb' );
define( 'NONCE_KEY',        'jAxLG7M$b.UG{NMNnv$((>1DKy}9mi .v%6-?PoRC6~U5:gw-qn@Dy QWx=X(8?o' );
define( 'AUTH_SALT',        'dCr`Ve+)mNwwy@+8I4.zhKbCF.8k.~sa*q[,5hIC;;?UcSPL3kFY=K?cF9jR.{`(' );
define( 'SECURE_AUTH_SALT', ';t-)jVJum&wrSVK5^ MLqKD*eX#Mi2b&]$n<(P#k%_EQHJEo.I{=VV`H r-fs0};' );
define( 'LOGGED_IN_SALT',   'rNFu)E(/uJm}gL09GTz,X<378V_R]~H#IjmE@ETGqR]=zLB!l.q%kE3l}Y}eZo1(' );
define( 'NONCE_SALT',       'LK]])!L.{eaKjg@}KLb|P9>SOZ;7B vSByW1^:0B=+4{oSEUBqX(.Q3v}T)+IR})' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
