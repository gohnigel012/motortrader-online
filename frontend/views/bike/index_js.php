<?php
use app\models\MasterGeneral;

$master_general = new MasterGeneral();
?>
<script>
	jQuery("#link-slider").protoSlider({
		'fadeintime': 1000
	});

	window.addEventListener("load", function(event) {
	    lazyload();
	});

	jQuery('#bike-filter-open').click(function () {
		if(jQuery('#bike-filter-show').hasClass('d-none')){
			jQuery('#bike-filter-show').removeClass('d-none');
			jQuery('#bike-filter-open').attr("src", "/images/close.png");
		} else {
			jQuery('#bike-filter-show').addClass('d-none');
			jQuery('#bike-filter-open').attr("src", "/images/plus.png");
		}
	});

	jQuery('#bike-condition-open').click(function () {
		if(jQuery('#bike-condition-show').hasClass('d-none')){
			jQuery('#bike-condition-show').removeClass('d-none');
			jQuery('#bike-condition-open').attr("src", "/images/close.png");
		} else {
			jQuery('#bike-condition-show').addClass('d-none');
			jQuery('#bike-condition-open').attr("src", "/images/plus.png");
		}
	});

	jQuery('#bike-state-open').click(function () {
		if(jQuery('#bike-state-show').hasClass('d-none')){
			jQuery('#bike-state-show').removeClass('d-none');
			jQuery('#bike-state-open').attr("src", "/images/close.png");
		} else {
			jQuery('#bike-state-show').addClass('d-none');
			jQuery('#bike-state-open').attr("src", "/images/plus.png");
		}
	});

	$( "#slider-range" ).slider({
      range: true,
      min: 100,
      max: 9999999,
      values: [ <?php echo isset($_REQUEST['minPrice']) && $_REQUEST['minPrice'] != null ? $_REQUEST['minPrice'] : 50000; ?>, <?php echo isset($_REQUEST['maxPrice']) && $_REQUEST['maxPrice'] != null ? $_REQUEST['maxPrice'] : 1251000; ?> ],
      slide: function( event, ui ) {
        $( "#minPrice" ).text( ui.values[ 0 ] );
        $( "#maxPrice" ).text( ui.values[ 1 ] );
      }
    });

    $( "#minPrice" ).text( $( "#slider-range" ).slider( "values", 0 ))  ;
    $( "#maxPrice" ).text( $( "#slider-range" ).slider( "values", 1 ))  ;

    jQuery('#custom-handle').mouseup(function() {
		window.location = "bike?minPrice=" + $( "#slider-range" ).slider( "values", 0 ) + "&maxPrice=" + $( "#slider-range" ).slider( "values", 1 );
	});

	jQuery('#custom-handle-2').mouseup(function() {
		window.location = "bike?minPrice=" + $( "#slider-range" ).slider( "values", 0 ) + "&maxPrice=" + $( "#slider-range" ).slider( "values", 1 );
	});

    $( "#slider-range-2" ).slider({
      range: true,
      min: new Date().getFullYear() - 20,
      max: new Date().getFullYear(),
      values: [ <?php echo isset($_REQUEST['minYear']) && $_REQUEST['minYear'] != null ? $_REQUEST['minYear'] : date('Y', strtotime("-12 year")); ?>, <?php echo isset($_REQUEST['maxYear']) && $_REQUEST['maxYear'] != null ? $_REQUEST['maxYear'] : date('Y', strtotime("-2 year")); ?> ],
      slide: function( event, ui ) {
        $( "#minYear" ).text( ui.values[ 0 ] );
        $( "#maxYear" ).text( ui.values[ 1 ] );
      }
    });

	$( "#minYear" ).text( $( "#slider-range-2" ).slider( "values", 0 ));
    $( "#maxYear" ).text( $( "#slider-range-2" ).slider( "values", 1 ));

     jQuery('#custom-handle-3').mouseup(function() {
		window.location = "bike?minYear=" + $( "#slider-range-2" ).slider( "values", 0 ) + "&maxYear=" + $( "#slider-range-2" ).slider( "values", 1 );
	});

	jQuery('#custom-handle-4').mouseup(function() {
		window.location = "bike?minYear=" + $( "#slider-range-2" ).slider( "values", 0 ) + "&maxYear=" + $( "#slider-range-2" ).slider( "values", 1 );
	});    

    $( "#slider-range-3" ).slider({
      range: true,
      min: 0,
      max: 100000,
      values: [ <?php echo isset($_REQUEST['min_mileage']) && $_REQUEST['min_mileage'] != null ? $_REQUEST['min_mileage'] : 10; ?>, <?php echo isset($_REQUEST['max_mileage']) && $_REQUEST['max_mileage'] != null ? $_REQUEST['max_mileage'] : 6000; ?> ],
      slide: function( event, ui ) {
        $( "#minMileage" ).text( ui.values[ 0 ] );
        $( "#maxMileage" ).text( ui.values[ 1 ] );
      }
    });

    getMileageCode($( "#slider-range-3" ).slider( "values", 0 ), $( "#slider-range-3" ).slider( "values", 1 ));

    $( "#minMileage" ).text( $( "#slider-range-3" ).slider( "values", 0 ));
    $( "#maxMileage" ).text( $( "#slider-range-3" ).slider( "values", 1 ));

    jQuery('#custom-handle-5').mouseup(function() {
		window.location = "bike?mileage=" + mileage_code + "&min_mileage=" + $( "#slider-range-3" ).slider( "values", 0 ) + "&max_mileage=" + $( "#slider-range-3" ).slider( "values", 1 );
	});

	jQuery('#custom-handle-6').mouseup(function() {
		window.location = "bike?mileage=" + mileage_code + "&min_mileage=" + $( "#slider-range-3" ).slider( "values", 0 ) + "&max_mileage=" + $( "#slider-range-3" ).slider( "values", 1 );
	});


	function getMileageCode($minMileage, $maxMileage) {
		mileage_code = '';
<?php
	$mileage_array = $master_general->getMileage();

	foreach($mileage_array as $mileage_array_id_loop => $mileage_array_data_loop) {
		preg_match_all('!\d+(?:,\d+)*!', $mileage_array_data_loop, $matches);
		if($mileage_array_data_loop != 'More than 500,000 km') {		
?>
		if ($minMileage >= parseInt('<?php echo str_replace(',', '', $matches[0][0]); ?>') && $maxMileage < parseInt('<?php echo str_replace(',', '', $matches[0][1]); ?>') ) {
	    	mileage_code = '<?php echo $mileage_array_id_loop; ?>';
	    }
<?php	
		} else {
?>
		if ($minMileage >= parseInt('<?php echo str_replace(',', '', $matches[0][0]); ?>') && $maxMileage < parseInt('<?php echo str_replace(',', '', $matches[0][0]); ?>') ) {
	    	mileage_code = '<?php echo $mileage_array_id_loop; ?>';
	    }
<?php

		}
	}
?>
	}
</script>