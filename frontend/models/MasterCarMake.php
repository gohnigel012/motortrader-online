<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_car_make".
 *
 * @property string $make_cd
 * @property string $make_name
 * @property float|null $sort_index 12.12.11 added
 * @property string|null $upd_user
 * @property string|null $upd_date
 * @property string|null $disp_flg 1:Show to system, 0:Disabled
 * @property string|null $file_name
 */
class MasterCarMake extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_car_make';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['make_cd', 'make_name'], 'required'],
            [['sort_index'], 'number'],
            [['upd_date'], 'safe'],
            [['make_cd', 'upd_user'], 'string', 'max' => 20],
            [['make_name', 'file_name'], 'string', 'max' => 255],
            [['disp_flg'], 'string', 'max' => 1],
            [['make_cd'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'make_cd' => 'Make Cd',
            'make_name' => 'Make Name',
            'sort_index' => 'Sort Index',
            'upd_user' => 'Upd User',
            'upd_date' => 'Upd Date',
            'disp_flg' => 'Disp Flg',
            'file_name' => 'File Name',
        ];
    }

    public function getmake() {
        $make_array = [];

        $master_car_make_memcache_key = "/master_car_make";
        $master_car_make_memcache = Yii::$app->cache->get($master_car_make_memcache_key);
        if ($master_car_make_memcache !== false) {
            $make_array = $master_car_make_memcache;
        } else {
            $master_car_make = $this->find()->where(['disp_flg' => 1])->orderBy('make_name')->all();
            if (count($master_car_make)) {
                foreach ($master_car_make as $master_car_data_loop) {
                    $make_array[$master_car_data_loop->make_cd] = $master_car_data_loop->make_name;
                }
            }

            Yii::$app->cache->set($master_car_make_memcache_key, $make_array, 3600);
        }
        
        return $make_array;
    }

    public function getmakename($make_cd) {
        $response = false;

        $master_car_make_memcache_key = "/master_car_make/cd/" . $make_cd;
        $master_car_make_memcache = Yii::$app->cache->get($master_car_make_memcache_key);
        if ($master_car_make_memcache !== false) {
            $response = $master_car_make_memcache;
        } else {
            $master_car_make = $this->find()->where(['make_cd' => (string)$make_cd])->one();
            if ($master_car_make) {
                $response = $master_car_make->make_name;

                Yii::$app->cache->set($master_car_make_memcache_key, $response, 43200);
            }
        }
        
        return $response;
    }

    public function getmakecd($make_name) {
        $response = false;

        $master_car_make_memcache_key = "/master_car_make/makename/" . $make_name;
        $master_car_make_memcache = Yii::$app->cache->get($master_car_make_memcache_key);
        if ($master_car_make_memcache !== false) {
            $response = $master_car_make_memcache;
        } else {
            $master_car_make = $this->find()->where(['ilike', 'make_name', $make_name])->orderBy('make_name')->one();
            if ($master_car_make) {
                $response = $master_car_make->make_cd;
                Yii::$app->cache->set($master_car_make_memcache_key, $response, 43200);
            }
        }
        return $response;
    }

}
