<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

?>
<div id="main-header">
<?php
    NavBar::begin([
        'brandLabel' => '<img src="/images/logo.png" alt="Motor Trader logo" id="logo">',
        'brandUrl' => ['/'],
        'innerContainerOptions' => ['class' => 'container-fluid'],
        'options' => [
            'class' => 'navbar navbar-expand-sm navbar-light justify-content-between',
        ],
    ]);

    $menuItems = [
        [
            'label' => 'Buy',
            'items' => [
                    '<div class="row"><div class="col">',
                    '<div class="dropdown-header">Cars for sale</div>',
                    ['label' => 'Used cars', 'url' => '/car?type=used'],
                    ['label' => 'Recond cars', 'url' => '/car?type=recond'],
                    ['label' => 'New cars', 'url' => '/car?type=new'],
                    '</div><div class="col">',
                    '<div class="dropdown-header">Motorbike for sale</div>',
                    ['label' => 'Used motorbike', 'url' => '/bike?type=used'],
                    ['label' => 'Recond motorbike', 'url' => '/bike?type=recond'],
                    ['label' => 'New motorbike', 'url' => '/bike?type=new'],
                    '</div><div class="col">',
                    '<div class="dropdown-header">Commercial cars for sale</div>',
                    ['label' => 'Used commercial cars', 'url' => '/car?bodyType=bt9&type=used'],
                    ['label' => 'Recond commercial cars', 'url' => '/car?bodyType=bt9&type=recond'],
                    ['label' => 'New commercial cars', 'url' => '/car?bodyType=bt9&type=new'],
                    '</div><div class="col">',
                    '<div class="dropdown-header">Others for sale</div>',
                    ['label' => 'Autoparts', 'url' => '/autopart'],
                    ['label' => 'Number plates', 'url' => '/numberplate'],
                    ['label' => 'Phone numbers', 'url' => '/specialnumber'],
                    '</div><div class="col">',
                    '<div class="dropdown-header">All Dealers</div>',
                    ['label' => 'Dealership', 'url' => '/dealer'],
                    ['label' => 'Private dealers', 'url' => '/dealer'],
                    '</div></div>',
            ],
        ], 
        [
            'label' => 'Sell',
            'items' => [
                    '<div class="row">',
                    '<div class="col-6">',
                    '<div class="dropdown-header">Sell your cars</div>',
                    ['label' => 'How to sell', 'url' => '/site/login'],
                    ['label' => 'Dealers', 'url' => '/site/login'],
                    ['label' => 'Private dealer', 'url' => 'https://mtsa.com.my'],
                    '</div><div class="col-6">',
                    '<div class="dropdown-header">Other services</div>',
                    ['label' => 'Build websites', 'url' => '/'],
                    '</div></div>',
            ],
        ],
        [
            'label' => 'Insights',
            'items' => [
                    ['label' => 'All insights', 'url' => 'https://news.motortrader.com.my'],
                    ['label' => 'News', 'url' => 'https://news.motortrader.com.my'],
                    ['label' => 'New car', 'url' => 'https://news.motortrader.com.my'],
                    ['label' => 'Advice', 'url' => 'https://news.motortrader.com.my'],
                    ['label' => 'Promotion', 'url' => 'https://news.motortrader.com.my/category/features'],
                    ['label' => 'Review', 'url' => 'https://autocarmalaysia.com'],
            ],
        ],
        [
            'label' => 'Tools',
            'items' => [
                    ['label' => 'Comparison', 'url' => '/site/compare'],
                    ['label' => 'Car loan calculator', 'url' => '/calculator/calculator'],
                    ['label' => 'Road tax calculator', 'url' => '/calculator/roadtax'],
                    ['label' => 'Check summons', 'url' => 'https://www.jpj.gov.my/web/main-site/semakan-saman'],
            ],
        ],
        [
            'label' => 'Promotions',
            'items' => [
                    ['label' => 'Motor Trader Promotion', 'url' => 'https://news.motortrader.com.my/category/motor-trader-campaign/'],
                    ['label' => 'Job Vacany @ Car Dealership', 'url' => 'https://news.motortrader.com.my/category/jobs-opening/'],
            ],
        ],
    ];

    $nextLink = [
        ['label' => '<img src="/images/search.png" class="main-header__comparison-images" alt="Search Images">', 'url' => ['/search']],
        ['label' => '<img src="/images/bookmark.png" class="main-header__comparison-images" alt="Comparison Images">', 'url' => ['/bookmark']],
        ['label' => 'Login',
         'items' => [
            ['label' => 'Dealer Login', 'url' => 'https://www.mtsa.com.my'],
            ['label' => 'User Login', 'url' => '/site/login', 'options' => [ 'style' => 'padding-top: 30px;' ] ],
         ],
        ],
    ];

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav mr-auto'],
        'items' => $menuItems,
        'encodeLabels' => false,
    ]);

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav ml-auto'],
        'items' => $nextLink,
        'encodeLabels' => false,
    ]);

    NavBar::end();
?>
</div>
<div class="main-header-responsive">
    <a href="/"><img src="/images/logo.png" alt="Motor Trader logo" id="logo-responsive"></a>
    <button type="button" class="btn btn-default header__nav">
        <span class="header__burger"></span>
        <span class="header__burger header__burger--adjust"></span>
        <span class="header__burger"></span>
    </button>
</div>