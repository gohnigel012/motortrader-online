<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mt_news' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '65rc{~U-;~s:)}q|x:[UHNeH,e*rl<[S8%?Y=L2fJ5qH 3$L|/Dvo1E(KW._&u-Q' );
define( 'SECURE_AUTH_KEY',  'i!bpqBgDCYJK$q1sA9EbO]oQl;DiOd!:1!<{krG1l7Y]o=)[,^w+RR+YM&<pZn:7' );
define( 'LOGGED_IN_KEY',    'YX/l}$>n%],Rp~bFoG!wy}*5 XE.{1eb9ZJxUZl.j;O}Uz.UX@U6D?|G)v?kS=0W' );
define( 'NONCE_KEY',        'N(Uyb;r+0q/TA(c?1[AtMh}l>31r<kyB!b@~<_;]E@0:(=XK)z4TorRf,pfMlLIG' );
define( 'AUTH_SALT',        '&J4NCzFPeqjAT<M33J Gdp7Bvmb4l{/4Z#&e$~[6IB1f4%YOFD(!9Byz.}l>9&-^' );
define( 'SECURE_AUTH_SALT', '%]9%44#)_(B t)/ji4uB(98(*->MmnN}K`P9o.&N<JPKsUq]hoF;+4V-#)u7elT<' );
define( 'LOGGED_IN_SALT',   'gfq]1us}D6EQ08*^<OB:sL*g!9TN_4hT&od0tIYY{I73CTGYp.kX#~2Cw7%P5GHq' );
define( 'NONCE_SALT',       'yJ.K7eJ$cs#i TRSDI3U)QcOsL1{;Sr$.5V3TN!`3ewUF&*A>fT,N ,waN!)N}.m' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
