<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $master_dealer_model->dealer_name;
$this->params['breadcrumbs'][] = ['label' => 'Motor Trader Dealer', 'url' => ['/dealer']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-home">
	<div class="dealer-view">
		<div class="row">
			<div class="col-lg-7">
<?php 
	if ($master_dealer_model->master_dealer_logo != '') {
		$dealer_logo_image = Yii::$app->general->dealer_cover_logo_image($master_dealer_model->master_dealer_logo, strtotime("now"), $master_dealer_model->dealer_id);
		if ($dealer_logo_image) {
?>				
				<p><img src="/images/ajax-loader.gif" data-src="<?php echo (@getimagesize($dealer_logo_image)) ? $dealer_logo_image : '/images/dealerexample.png'; ?>" alt="Dealer" class="dealer-view__image lazyload"></p>
<?php 
		} 
	} else {
?>
				<p><img src="/images/ajax-loader.gif" data-src="/images/dealerexample.png" alt="Dealer" class="dealer-view__image lazyload"></p>
<?php			
	}	
?>				
			</div>
			<div class="col-lg-5">
				<h1 class="banner-search__title"><?php echo $master_dealer_model->dealer_name;?></h1>
				<p>(1188991-U)</p>
				<p class="car-view__rating-list">
<?php 
	$dealer_seo_url = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name);
	switch ($average_total_overall) {
		case '1':
			echo '	<img src="/images/star.png">
					<img src="/images/star-empty.png">
					<img src="/images/star-empty.png">
					<img src="/images/star-empty.png">
					<img src="/images/star-empty.png">
					<span class="car-view__rating"> 1.0 </span>
					<a class="car-view__link car-view__link--padding" href="' . $dealer_seo_url . '&type=nav-dealer-review">
						<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
					</a>	
				';
		break;
		case '1.5':
			echo '	<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star-empty.png">
					<img src="/images/star-empty.png">
					<img src="/images/star-empty.png">
					<span class="car-view__rating"> 1.5 </span>
					<a class="car-view__link car-view__link--padding" href="' . $dealer_seo_url . '%type=nav-dealer-review">
						<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
					</a>
				';
		break;
		case '2':
			echo '	<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star-empty.png">
					<img src="/images/star-empty.png">
					<img src="/images/star-empty.png">
					<span class="car-view__rating"> 2.0 </span>
					<a class="car-view__link car-view__link--padding" href="' . $dealer_seo_url . '&type=nav-dealer-review">
						<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
					</a>
				';
		break;
		case '2.5':
			echo '	<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star-empty.png">
					<img src="/images/star-empty.png">
					<span class="car-view__rating"> 2.5 </span>
					<a class="car-view__link car-view__link--padding" href="' . $dealer_seo_url . '&type=nav-dealer-review">
						<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
					</a>
				';
		break;
		case '3':
			echo '	<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star-empty.png">
					<img src="/images/star-empty.png">
					<span class="car-view__rating"> 3.0 </span>
					<a class="car-view__link car-view__link--padding" href="' . $dealer_seo_url . '&type=nav-dealer-review">
						<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
					</a>
				';
		break;
		case '3.5':
			echo '	<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star-empty.png">
					<span class="car-view__rating"> 3.5</span>
					<a class="car-view__link car-view__link--padding" href="' . $dealer_seo_url . '&type=nav-dealer-review">
						<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
					</a>
				';
		break;
		case '4':
			echo '	<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star-empty.png">
					<span class="car-view__rating"> 4.0</span>
					<a class="car-view__link car-view__link--padding" href="' . $dealer_seo_url . '&type=nav-dealer-review">
						<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
					</a>
				';
		break;
		case '4.5':
			echo '	<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<span class="car-view__rating"> 4.5</span>
					<a class="car-view__link car-view__link--padding" href="' . $dealer_seo_url . '&type=nav-dealer-review">
						<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
					</a>
				';
		break;
		case '5':
			echo '	<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<span class="car-view__rating"> 5.0</span>
					<a class="car-view__link car-view__link--padding" href="' . $dealer_seo_url . '&type=nav-dealer-review">
						<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
					</a>
				';
		break;
		case '0':
			echo '	<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 0.0</span>
							<a class="car-view__link car-view__link--padding" href="' . $dealer_seo_url . '&type=nav-dealer-review">
						<span>No review yet</span>
					</a>';
		break;
	}
?>				
				</p>
				<div class="row">
					<div class="col-lg-6">
						<div class="car-view__dealer">
							<div class="row">
								<div class="col-2">
									<p><img class="featured-ads-section__icon" src="/images/dealer.png"></p>
								</div>
								<div class="col-10">
									<p><small>Seller type</small></p>
									<p>
<?php
	switch ($master_dealer_model->dealer_type) {
		case '2':
			echo '<span>Dealer</span>';
		break;
		case '3':
			echo '<span>Private</span>';
		break;
		default:
			echo '<span>Dealer</span>';
		break;
	}
?>								
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="row">
	                        <div class="col-2">
	                            <p><img class="featured-ads-section__icon" src="/images/location.png" alt="location" /></p>
	                        </div>
	                        <div class="col-10">
	                            <span class="featured-ads-section__text">Location</span>
	                            <p><?php echo $master_dealer_model->location_name; ?></p>
	                        </div>
	                    </div>
					</div>
				</div>
				<p><button class="btn car-view__search-btn car-view__search-btn--position"><span class="car-view__search-btn-text">Waze</span></button>
				<button class="btn car-view__search-btn car-view__search-btn--position"><span class="car-view__search-btn-text">Google Map</span></button></p>
				<p><button class="btn car-view__search-btn car-view__search-btn--red car-view__search-btn--position"><span class="car-view__search-btn-text">Contact</span></button> 
					<button class="btn car-view__search-btn car-view__search-btn--red car-view__search-btn--position"><span class="car-view__search-btn-text">Visit Website</span></button></p>
			</div>
		</div>
	</div>
	<div class="superdeals-featured-cnabadv superdeals-featured-cnabadv--background-color">
        <div class="superdeals-featured-cnabadv__content">
        	<div class="tab dealer-view__tab text-center">
		        <button class="tablinks active" id="icon-box-sale">
		            <span>For sale</span>
		        </button>
		        <button class="tablinks" id="icon-box-review" onclick="scrolltoelement('#dealer-sale');">
		            <span>Reviews</span>
		        </button>
		    </div>
	        <div class="dealer-view__sale" id="dealer-sale">
				<div class="row">
					<div class="col-lg-8">
						<p class="dealer-view__show">Showing <?php echo $pdata['total']; ?> results</p>
					</div>
					<div class="col-lg-4 text-right">
						<p class="dealer-view__sort">
							Sort by
	<?php
		if (count($trans_advert_model) > 0) {
	?> 					
						<select onchange="location = this.value;">
		        			<option <?php if ($pdata['sort'] == '') echo "selected"; ?> disabled>Select one</option>
		        			<option <?php if($pdata['sort'] == 'updatehigh') echo "selected"; ?> value="<?php echo $pdata['url_sort_update_low']; ?>">Formest first</option>
		        			<option <?php if($pdata['sort'] == 'updatelow') echo "selected"; ?> value="<?php echo $pdata['url_sort_update_high']; ?>">Recent first</option>
		        			<option <?php if($pdata['sort'] == 'pricelow') echo "selected"; ?> value="<?php echo $pdata['url_sort_price_low']; ?>">Price (lowest first)</option>
		        			<option <?php if($pdata['sort'] == 'pricehigh') echo "selected"; ?> value="<?php echo $pdata['url_sort_price_high']; ?>">Price (highest first)</option>
		        			<option <?php if($pdata['sort'] == 'yearlow') echo "selected"; ?> value="<?php echo $pdata['url_sort_year_low']; ?>">Year (lowest first)</option>
		        			<option <?php if($pdata['sort'] == 'yearhigh') echo "selected"; ?> value="<?php echo $pdata['url_sort_year_high']; ?>">Year (highest first)</option>
		        		</select>
	<?php
		}
	?>	        		
						</p>
					</div>
				</div>
        		<div class="row">
<?php
		if (count($trans_advert_model)) {

			$gk_frame_array =  ['1'=>'-',
								'2'=>'Needs Rectification',
								'3'=>'Acceptable',
								'4'=>'Good',
								'5'=>'Excellent'
								];

			$gk_engine_array = ['1'=>'-',
								'2'=>'-',
								'3'=>'-',
								'4'=>'-',
								'5'=>'Good'
								];

			$count_dealer = 0;

			foreach ($trans_advert_model as $trans_advert_data_loop) {
				if ($trans_advert_data_loop->classification_cd === '1167') {
					$seo_url = Yii::$app->general->generateseo('bike', $trans_advert_data_loop->urn_no, $trans_advert_data_loop->make_name . '-' . $trans_advert_data_loop->model_name);
					$seo_title = $trans_advert_data_loop->make_name . ' ' . $trans_advert_data_loop->model_name;
					$advert_no_image = '/images/motorcycle.png';
				} else if ($trans_advert_data_loop->classification_cd === '1166') {
					$seo_url = Yii::$app->general->generateseo('autopart', $trans_advert_data_loop->urn_no, $trans_advert_data_loop->make_name);
					$seo_title = $trans_advert_data_loop->make_name;
					$advert_no_image = '/images/autopart.png';
				} else if($trans_advert_data_loop->classification_cd === '1165') {
					$seo_url = Yii::$app->general->generateseo('numberplate', $trans_advert_data_loop->urn_no, $trans_advert_data_loop->special_no);
					if ( $trans_advert_data_loop->additional_desc != "" ) {
						if (strlen($trans_advert_data_loop->additional_desc) > 120) {
							$seo_title = substr($trans_advert_data_loop->additional_desc, 0, 120) . ' ...';
						} else {
							$seo_title = $trans_advert_data_loop->additional_desc;
						}
					} else if($trans_advert_data_loop->qe_add_desc != "" ) {
						if (strlen($trans_advert_data_loop->qe_add_desc) > 120) {
							$seo_title = substr($trans_advert_data_loop->qe_add_desc, 0, 120) . ' ...';
						} else {
							$seo_title = $trans_advert_data_loop->qe_add_desc;
						}

					} else if($trans_advert_data_loop->feature_desc != "" ) {
						if (strlen($trans_advert_data_loop->feature_desc) > 120) {
							$seo_title = substr($trans_advert_data_loop->feature_desc, 0, 120) . ' ...';
						} else {
							$seo_title = $trans_advert_data_loop->feature_desc;
						}
					}
				} else if($trans_advert_data_loop->classification_cd === '2493') {
					$seo_url = Yii::$app->general->generateseo('special-phone-number', $trans_advert_data_loop->urn_no, $trans_advert_data_loop->special_no);
					$seo_title = $trans_advert_data_loop->special_no;
				} else {
					$seo_url = Yii::$app->general->generateseo('car', $trans_advert_data_loop->urn_no, $trans_advert_data_loop->make_name . '-' . $trans_advert_data_loop->model_name);
					$seo_title = $trans_advert_data_loop->make_name . ' ' . $trans_advert_data_loop->model_name;

					if($trans_advert_data_loop->bodytype_code == 'bt9'){
						$advert_no_image = '/images/bus.png';
					} else {
						$advert_no_image = '/images/mercedes.png';
					}
				}
				
				$advert_image = Yii::$app->general->advertimageurl($trans_advert_data_loop->urn_no . '.jpg', 'wmfull', strtotime($trans_advert_data_loop->upd_data_date));
				
	            $count_dealer++;

				$voc = '';
				$extra_info_array = [];

				$extra_info_array = $trans_order_extra_info->getadvertextrainfo($trans_advert_data_loop->urn_no);

				if (isset($extra_info_array['vehicle_cert']) && $extra_info_array['vehicle_cert']!= '') {
					$voc = $extra_info_array['vehicle_cert'];
				}
				
				$year_make = ((int)date("Y") - (int)$trans_advert_data_loop->year_make);
				
				$super_deals_model = $super_deals->getsuperdeals($trans_advert_data_loop->urn_no);	
			
				$has_super_deals = $super_deals_model['got_super_deals'];																				
?>        		
	        		<div class="col-lg-3">
	        			<div class="superdeals-featured-cnabadv__featured-ads-section-listing superdeals-featured-cnabadv__featured-ads-section-listing--change <?php echo $has_super_deals === '1' ? 'superdeals-featured-cnabadv__featured-ads-section-listing--border' : ''; ?>">
<?php
				if($trans_advert_data_loop->classification_cd !== '1165' && $trans_advert_data_loop->classification_cd !== '2493') {
?>        				
	        				<p class="text-center car-view__position">
	        					<a href="<?php echo $seo_url; ?>">
	        						<img class="featured-ads-section__img" src="<?php echo (@getimagesize($advert_image)) ? $advert_image : $advert_no_image; ?>" alt="<?php echo addslashes($trans_advert_data_loop->make_name . ' ' . $trans_advert_data_loop->model_name); ?>">
	        					</a>
	        					<span class="car-list__bookmark <?php echo (in_array($trans_advert_data_loop->urn_no, $pdata['bookmark_array']) ? 'car-list__show-bookmark' : '') ?>" data-urn="<?php echo $trans_advert_data_loop->urn_no; ?>"><img class="car-list__none" src="/images/bookmark.png"><img class="car-list__all" src="/images/bookmarked.png"></span>
	        				</p>
<?php
				} else {
?>
							<a class="dealer-view__numberplate-link" href="<?php echo $seo_url; ?>">
		        				<div class="dealer-view__numberplate car-view__position">
									<h2 class="dealer-view__numberplate-text"><?php echo substr($trans_advert_data_loop->special_no, 0, 15); ?></h2>
									<span class="car-list__bookmark <?php echo (in_array($trans_advert_data_loop->urn_no, $pdata['bookmark_array']) ? 'car-list__show-bookmark' : '') ?>" data-urn="<?php echo $trans_advert_data_loop->urn_no; ?>"><img class="car-list__none" src="/images/bookmark.png"><img class="car-list__all" src="/images/bookmarked.png"></span>
								</div>
							</a>
<?php
				}
?>						
        					<p class="text-center featured-ads-section__title"><a href="<?php echo $seo_url; ?>"><?php echo $seo_title; ?></a></p>
<?php
				if (isset($super_deals_model) && date("Y-m-d H:i:s") >= $super_deals_model['super_deals_schedule_start_date'] && date("Y-m-d H:i:s") <= $super_deals_model['super_deals_schedule_end_date']) {
					if ($super_deals_model['sale_price'] == 0) {
						$price = 'POA';
						$month = '';
					} else {
						if (trim($voc)) {
							$price = '<small class="featured-ads-section__price-currency">VOC RM</small> ' . number_format($super_deals_model['sale_price']);
						} else {
							$price = '<small class="featured-ads-section__price-currency">RM</small>  ' . number_format($super_deals_model['sale_price']); 
						}

						$deposit = 10;
						$interest = 3.69;
						$term = 7;
						$interestRate = $interest / 100;    
						$oriPrice = str_replace(",","", $super_deals_model['sale_price']);
						$depositAmount = $oriPrice * ($deposit / 100);
						$loanPaidAmount = $oriPrice - $depositAmount; 
						$interestPaidAmount = ($term * $interestRate) * $loanPaidAmount;                            
						$month = ($interestPaidAmount + $loanPaidAmount) / ($term * 12);
						$month = number_format(round($month));
					}

					if ($super_deals_model['sale_price'] != 0) {
						if ($super_deals_model['sale_price'] != $super_deals_model['regular_price']) { 
							$regular_price = number_format($super_deals_model['regular_price']);
						}
					}

					if($trans_advert_data_loop->classification_cd == '1141'){
?>
						<div class="row">
		                <div class="col-6">
		                    <p><small>RM <?php echo $month; ?>/month</small></p>
		                </div>
		                <div class="col-6 text-right">
		                    <p class="featured-ads-section__price"><?php echo $price; ?></p>
		                    <p class="featured-ads-section__price"><?php echo $regular_price; ?></p>
		                </div>
		            </div>
<?php
					} else {
?>
						<p class="featured-ads-section__price text-right"><?php echo $price; ?></p>
	                    <p class="featured-ads-section__price text-right"><?php echo $regular_price; ?></p>
<?php
					}
				} else {
					if ($trans_advert_data_loop->price == 0) {
						$price = 'POA';
						$month = '';
					} else {
						if (trim($voc)) {
							$price = '<small class="featured-ads-section__price-currency">VOC RM</small> ' . number_format($trans_advert_data_loop->price);
						} else {
							$price = '<small class="featured-ads-section__price-currency">RM</small>  ' . number_format($trans_advert_data_loop->price); 
						}

						$deposit = 10;
						$interest = 3.69;
						$term = 7;
						$interestRate = $interest / 100;    
						$oriPrice = str_replace(",","", $trans_advert_data_loop->price);
						$depositAmount = $oriPrice * ($deposit / 100);
						$loanPaidAmount = $oriPrice - $depositAmount; 
						$interestPaidAmount = ($term * $interestRate) * $loanPaidAmount;                            
						$month = ($interestPaidAmount + $loanPaidAmount) / ($term * 12);
						$month = number_format(round($month));
					}

					if($trans_advert_data_loop->classification_cd == '1141'){
?>	
							<div class="row">
				                <div class="col-4">
				                    <p><small>RM <?php echo $month; ?>/month</small></p>
				                </div>
				                <div class="col-8 text-right">
				                    <p class="featured-ads-section__price"><?php echo $price; ?></p>
				                </div>
				            </div>
<?php
					} else {
?>
							<p class="featured-ads-section__price text-right"><?php echo $price; ?></p>
<?php					
					}
				}
?>		            
				            <hr>
<?php
					if($trans_advert_data_loop->classification_cd == '1141' || $trans_advert_data_loop->classification_cd == '1167') {
?>
							<div class="row">
		                        <div class="col-6">
		                            <div class="row">
		                                <div class="col-5">
		                                    <p><img class="featured-ads-section__icon" src="/images/calendar.png" alt="calendar" /></p>
		                                </div>
		                                <div class="col-7">
		                                    <span class="featured-ads-section__text">Year</span>
		                                    <p><?php echo $trans_advert_data_loop->year_make; ?></p>
		                                </div>
		                            </div>
		                        </div>
		                        <div class="col-6">
		                            <div class="row">
		                                <div class="col-5">
		                                    <p><img class="featured-ads-section__icon" src="/images/location.png" alt="location" /></p>
		                                </div>
		                                <div class="col-7">
		                                    <span class="featured-ads-section__text">Location</span>
		                                    <p><?php echo $trans_advert_data_loop->location_name; ?></p>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="row">
		                        <div class="col-6">
		                            <div class="row">
		                                <div class="col-5">
		                                    <p><img class="featured-ads-section__icon" src="/images/mileage.png" alt="mileage" /></p>
		                                </div>
		                                <div class="col-7">
		                                    <span class="featured-ads-section__text">Mileage</span>
		                                    <p><?php echo $trans_advert_data_loop->mileage_name; ?></p>
		                                </div>
		                            </div>
		                        </div>
		                        <div class="col-6">
		                            <div class="row">
		                                <div class="col-5">
		                                    <p><img class="featured-ads-section__icon" src="/images/transmission.png" alt="location" /></p>
		                                </div>
		                                <div class="col-7">
		                                    <span class="featured-ads-section__text">Transmission</span>
		                                    <p><?php echo $trans_advert_data_loop->transmission_name; ?></p>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
<?php
					} else if($trans_advert_data_loop->classification_cd == '1166') {
?>
							<div class="row">
	                                <div class="col-5">
	                                    <p><img class="featured-ads-section__icon" src="/images/location.png" alt="location" /></p>
	                                </div>
	                                <div class="col-7">
	                                    <span class="featured-ads-section__text">Location</span>
	                                    <p><?php echo $trans_advert_data_loop['location_name']; ?></p>
	                                </div>
	                            </div>
			                    <div>
			                        <div class="row">
			                            <div class="col-5">
			                                <p><img class="featured-ads-section__icon" src="/images/autoparticon.png" alt="category" /></p>
			                            </div>
			                            <div class="col-7">
			                                <span class="featured-ads-section__text">Category</span>
<?php
		$autoparts_category_array = $trans_statistic_count->listcount('autoparts_rank');
		foreach ($autoparts_category_array as $autoparts_category_array_name_loop => $autoparts_category_array_count_loop) {
			if ($autoparts_category_array_count_loop > 0) {
				if (preg_match('%' . $autoparts_category_array_name_loop . '%', $trans_advert_data_loop['model_name'])) {
?>
											<p><?php echo $autoparts_category_array_name_loop; ?></p>
<?php
				}
			}
		}
?>			                                
			                                
			                            </div>
			                        </div>
			                    </div>	
<?php						
					} else {
?>
								<div class="row">
		                            <div class="col-5">
		                                <p><img class="featured-ads-section__icon" src="/images/location.png" alt="location" /></p>
		                            </div>
		                            <div class="col-7">
		                                <span class="featured-ads-section__text">Location</span>
		                                <p><?php echo $trans_advert_data_loop->location_name; ?></p>
		                            </div>
		                        </div>
<?php						
					}
?>
				             
	        			</div>
	        		</div>
<?php
		}
	}
?>        		
        		</div>
<?php
	if ($pdata['total'] > 0) {
		$pagination_range = 5;

        $start_page = $pdata['page'] - floor($pagination_range / 2);
        if ($start_page < 1) {
            $start_page = 1;
        }

        $total_end_page = ceil($pdata['total'] / $pdata['limit']);

        $end_page = $pdata['page'] + floor($pagination_range / 2);
        if ($end_page > $total_end_page) {
            $end_page = $total_end_page;
        }

        if (($end_page - $pdata['page']) < floor($pagination_range / 2) && $pagination_range < floor($pdata['total'] / $pdata['limit'])) {
            $start_page = ceil($pdata['total'] / $pdata['limit']) - $pagination_range;
        }

        if ($end_page < $pagination_range && floor($pdata['total'] / $pdata['limit']) >= $pagination_range) {
            $end_page = $pagination_range;
        }
?>
				<div>
					<p class="text-center featured-ads-section__page">
<?php
		if ($pdata['page'] > 1) {
			$processed_link = str_replace('__PAGE__', ($pdata['page'] - 1), $pdata['url_pagination']);
?>					
						<span class="<?php echo ($start_page > 1 ? '' : 'hidden-md-up'); ?>"><a href="<?php echo $processed_link; ?>"><img src="/images/previous.png" alt="Previous"> </a>
<?php
		}
?>					
					<span><?php echo $pdata['page'] . ' / ' . $end_page; ?></span>
<?php
		if ($pdata['page'] < ceil($pdata['total'] / $pdata['limit']) ) {
			$processed_link = str_replace('__PAGE__', ($pdata['page'] + 1), $pdata['url_pagination']);
?>					
						<a href="<?php echo $processed_link; ?>"><img src="/images/next.png" alt="Next"></a></span>
<?php
		}
?>				
					</p>
				</div>
<?php
	}
?>        	
    		</div>
    	</div> 
    	<div class="d-none dealer-view__sale" id="dealer-review">
    		<p class="text-center">
<?php 
	switch ($average_total_overall) {
		case '1':
			echo '	<img src="/images/star.png">
					<img src="/images/star-empty.png">
					<img src="/images/star-empty.png">
					<img src="/images/star-empty.png">
					<img src="/images/star-empty.png">
					<span class="car-view__rating">1.0</span>
				';
		break;
		case '1.5':
			echo '	<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star-empty.png">
					<img src="/images/star-empty.png">
					<img src="/images/star-empty.png">
					<span class="car-view__rating">1.5</span>
				';
		break;
		case '2':
			echo '	<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star-empty.png">
					<img src="/images/star-empty.png">
					<img src="/images/star-empty.png">
					<span class="car-view__rating">2.0</span>
				';
		break;
		case '2.5':
			echo '	<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star-empty.png">
					<img src="/images/star-empty.png">
					<span class="car-view__rating">2.5</span>
				';
		break;
		case '3':
			echo '	<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star-empty.png">
					<img src="/images/star-empty.png">
					<span class="car-view__rating">3.0</span>
				';
		break;
		case '3.5':
			echo '	<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star-empty.png">
					<span class="car-view__rating">3.5</span>
				';
		break;
		case '4':
			echo '	<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star-empty.png">
					<span class="car-view__rating">4.0</span>
				';
		break;
		case '4.5':
			echo '	<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<span class="car-view__rating">4.5</span>
				';
		break;
		case '5':
			echo '	<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<img src="/images/star.png">
					<span class="car-view__rating">5.0</span>
				';
		break;
		case '0':
			echo '	<img src="/images/star-empty.png">
					<img src="/images/star-empty.png">
					<img src="/images/star-empty.png">
					<img src="/images/star-empty.png">
					<img src="/images/star-empty.png">
					<span class="car-view__rating"> 0.0</span>
				';
		break;
	}
?>
    	</p>
    	<div class="text-center">
<?php 
	$count_master_dealer_review_info = $master_dealer_review_info->countdealerreviewinfo($master_dealer_model->dealer_id);	
?>									
				<p><?php echo $count_master_dealer_review_info ? $count_master_dealer_review_info : '0'; ?> lifetime reviews. A dealership's rating is calculated by averaging scores from reviews received in the past 24 months</p>
			</div>
			<div class="text-center dealer-review">
				<a class="btn car-view__search-btn" href="javascript:void(0)" onclick="popupreviewform()">
					<span class="car-view__search-btn-text">Write a Review</span>
				</a>
			</div>	
	    </div>
	</div>
</div>