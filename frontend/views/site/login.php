<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = 'Motor Trader Login';
?>
<div class="page-home">
	<h1 class="banner-search__title text-center">Sell your car with us</h1>
	<h2 class="text-center">Are you a dealer? <a class="superdeals-featured-cnabadv__link" href="/site/faq">Click here</a> to get more info about dealer account.</h2>
	<div class="user-login">
		<div class="row">
			<div class="col-lg-6">
				<div class="user-login__column-left">
					<form name="user-login__form-login" id="frm-login">
						<h3 class="text-center user-login__title">Sign In</h3>
						<div class="form-group">
							<p><input type="text" name="username" class="form-control banner-search__text banner-search__text--resize banner-search__text--margin" placeholder="Username" value="" autocomplete="off"></p>
							<p><input type="password" name="password" class="form-control banner-search__text banner-search__text--resize" placeholder="Password" value="" autocomplete="off"></p>
							<p class="user-login__flex"><button class="btn btn-default banner-search__search-btn"><span class="banner-search__search-btn-text">Sign in</span></button> <span><a href="/site/forgotPassword" class="superdeals-featured-cnabadv__link">Forgot password?</a></span></p>
						</div>
						<p class="user-login__divider"><span>Or</span></p>
						<p class="text-center user-login__alternate"><a class="user-login__facebook"><img src="/images/loginfacebook.png" alt="Facebook"> Log in with Facebook</a></p>
						<p class="text-center user-login__alternate user-login__alternate--color"><a class="user-login__google"><img src="/images/logingoogle.png" alt="Google"> Log in with Google</a></p>
					</form>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="user-login__column-right">
					<form name="user-login__form-register" id="frm-register">
						<h3 class="text-center user-login__title">Register an account</h3>
						<div class="form-group">
							<p><input type="text" name="username" class="form-control banner-search__text banner-search__text--resize banner-search__text--margin" placeholder="Username" value="" autocomplete="off"></p>
							<p><input type="password" name="password" class="form-control banner-search__text banner-search__text--resize banner-search__text--margin" placeholder="Password" value="" autocomplete="off"></p>
							<p><input type="text" name="email" class="form-control banner-search__text banner-search__text--resize banner-search__text--margin" placeholder="Email address" value="" autocomplete="off"></p>
							<p><input type="text" name="phone" class="form-control banner-search__text banner-search__text--resize" placeholder="Phone number" value="" autocomplete="off"></p>
							<p class="user-login__terms"><input type="checkbox" name="terms" id="register-agree"> <label class="form-check-label" for="register-agree">I agree that I have read and accepted the <a class="superdeals-featured-cnabadv__link">Terms and Conditions</a></label></p>
							<p><button class="btn btn-default banner-search__search-btn"><span class="banner-search__search-btn-text">Register</span></button></p>
						</div>
						<p class="user-login__divider"><span>Or</span></p>
						<p class="text-center user-login__alternate"><a class="user-login__facebook"><img src="/images/loginfacebook.png" alt="Facebook"> Log in with Facebook</a></p>
						<p class="text-center user-login__alternate user-login__alternate--color"><a class="user-login__google"><img src="/images/logingoogle.png" alt="Google"> Log in with Google</a></p>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>