<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trans_statistic_count".
 *
 * @property string $trans_statistic_count_key
 * @property string $trans_statistic_count_ref
 * @property int|null $trans_statistic_count
 * @property string|null $trans_statistic_count_created
 */
class TransStatisticCount extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trans_statistic_count';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['trans_statistic_count_key', 'trans_statistic_count_ref'], 'required'],
            [['trans_statistic_count'], 'default', 'value' => null],
            [['trans_statistic_count'], 'integer'],
            [['trans_statistic_count_created'], 'safe'],
            [['trans_statistic_count_key', 'trans_statistic_count_ref'], 'string', 'max' => 125],
            [['trans_statistic_count_key', 'trans_statistic_count_ref'], 'unique', 'targetAttribute' => ['trans_statistic_count_key', 'trans_statistic_count_ref']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'trans_statistic_count_key' => 'Trans Statistic Count Key',
            'trans_statistic_count_ref' => 'Trans Statistic Count Ref',
            'trans_statistic_count' => 'Trans Statistic Count',
            'trans_statistic_count_created' => 'Trans Statistic Count Created',
        ];
    }

    public function getcount($group, $key) {
        $response = false;

        $trans_statistic_count_memcache_key = "/trans_statistic_count/getcount/group/" . $group . '/key/' . $key;
        $trans_statistic_count_memcache = Yii::$app->cache->get($trans_statistic_count_memcache_key);
        if ($trans_statistic_count_memcache !== false) {
            $response = $trans_statistic_count_memcache;
        } else {
            $trans_statistic_count_model = $this->find()->where(['trans_statistic_count_key' => (string)$group, 'trans_statistic_count_ref' => (string)$key])->one();
            if ($trans_statistic_count_model) {
                $cache_sec = 300;

                $passed_sec = time() - strtotime($trans_statistic_count_model->trans_statistic_count_created);
                if ($passed_sec < 0) {
                    $passed_sec = 0;
                } else if ($passed_sec > $cache_sec) {
                    $passed_sec = $cache_sec;
                }

                $response = $trans_statistic_count_model->trans_statistic_count;
            }

            Yii::$app->cache->set($trans_statistic_count_memcache_key, $response, 120);
        }

        return $response;
    }

    public function displaycount($group, $key)
    {
        $response = $this->getcount($group, $key);

        if ($response !== false && (int)$response > 0) {
            return ' (' . $response . ')';
        } else {
            return false;
        }
    }

    public function listcount($group, $limit = null) {
        $response = false;

        $trans_statistic_count_memcache_key = "/trans_statistic_count/listcount/group/" . $group . "/limit/" . $limit;
        $trans_statistic_count_memcache = Yii::$app->cache->get($trans_statistic_count_memcache_key);
        if ($trans_statistic_count_memcache !== false) {
            $response = $trans_statistic_count_memcache;
        } else {

            $filter_array = $this->find()->where(['trans_statistic_count_key' => (string)$group])->orderBy('trans_statistic_count DESC');

            if ($limit !== null) {
                $filter_array->limit($limit);
            }

            $trans_statistic_count_model = $filter_array->all();
            if (count($trans_statistic_count_model)) {
                
                $response = [];
                $time = false;
                foreach ($trans_statistic_count_model as $trans_statistic_count_data_loop) {
                    $response[$trans_statistic_count_data_loop->trans_statistic_count_ref] = $trans_statistic_count_data_loop->trans_statistic_count;
                    
                    if ($time == false || $time > strtotime($trans_statistic_count_data_loop->trans_statistic_count_created)) {
                        $time = strtotime($trans_statistic_count_data_loop->trans_statistic_count_created);
                    }
                }

                $cache_sec = 300;

                $passed_sec = time() - $time;
                if ($passed_sec < 0) {
                    $passed_sec = 0;
                } else if ($passed_sec > $cache_sec) {
                    $passed_sec = $cache_sec;
                }

            }
            
            Yii::$app->cache->set($trans_statistic_count_memcache_key, $response, 60);
        }

        return $response;
    }
}
