<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Motor Trader Autoparts';
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-home">
	<div class="jumbotron">
		<p class="text-left">
<?php			
	if (isset($_REQUEST['model']) && trim($_REQUEST['model'])) {
?>			
			<?php echo $pdata['total']; ?>  Car Parts & Accessories for <?php echo $pdata['car_make_name']; ?> <?php echo $pdata['car_model_name']; ?>  - Page <?php echo $pdata['page']; ?>
<?php
	} else if (isset($_REQUEST['make']) && trim($_REQUEST['make'])) { 
?>	
			<?php echo $pdata['total']; ?>  Car Parts & Accessories for <?php echo $pdata['car_make_name']; ?> - Page <?php echo $pdata['page']; ?>
<?php
	} else {
?>	
			<?php echo $pdata['total']; ?> <?php echo $pdata['page_label']; ?> for sale in <?php echo $pdata['page_location']; ?>  - Page <?php echo $pdata['page']; ?>
<?php
	} 
?>			
		</p>
	</div>

	 <div class="proto-slide" id="link-slider">
    	<div class="proto-slide-box">
    		<ul>
		    	<li><a href="/car" class="btn <?php if(Url::current() == '/car/index') echo "active"; ?> choose__icon"><img src="/images/caricon.png" alt="Car icon"> Cars</a></li>
		    	<li><a href="/autopart" class="btn active choose__icon"><img src="/images/autoparticon.png" alt="Autoparts icon"> Autoparts</a></li>
			    <li><a href="/numberplate" class="btn choose__icon"><img src="/images/numberplateicon.png" alt="Number plate icon"> Number plates</a></li>
			    <li><a href="/specialnumber" class="btn choose__icon"><img src="/images/phoneicon.png" alt="Phone number icon"> Phone number</a></li>
			    <li><a href="/car?bodyType=bt9" class="btn <?php if(Url::current() == '/car/index?bodyType=bt9') echo "active"; ?> choose__icon"><img src="/images/commercialicon.png" alt="Commercial cars icon"> Commercial cars</a></li>
			   	<li><a href="/bike" class="btn choose__icon"><img src="/images/motorbikeicon.png" alt="Motorbike icon"> Motorbike</a></li>
			   	<li><a href="/homeAndLI" class="btn choose__icon"><img src="/images/itineryicon.png" alt="Home and itinery icon"> Home and Living</a></li>
			   	
		   </ul>
	   </div>
	   <div class="proto-slide-arrow proto-slide-arrow-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>
		<div class="proto-slide-arrow proto-slide-arrow-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
	</div>

	<div class="list-collapse">
		<ul class="text-center">
	    	<li data-toggle="collapse" data-target="#buy"><a href="#" class="btn choose__icon-responsive"><img src="/images/autoparticon.png" alt="Autopart icon"> Autoparts</a></li>
    	 	<div id="buy" class="collapse">
    	 		<li><a href="/car" class="btn <?php if(strpos(Url::current(), '/car/index') !== false && Url::current() != '/car/index?bodyType=bt9') echo "active"; ?> choose__icon-responsive"><img src="/images/caricon.png" alt="Car icon"> Cars</a></li>
		    	<li><a href="/autopart" class="btn choose__icon-responsive"><img src="/images/autoparticon.png" alt="Autoparts icon"> Autoparts</a></li>
			    <li><a href="/numberplate" class="btn choose__icon-responsive"><img src="/images/numberplateicon.png" alt="Number plate icon"> Number plates</a></li>
			    <li><a href="/contactNumber" class="btn choose__icon-responsive"><img src="/images/phoneicon.png" alt="Phone number icon"> Phone number</a></li>
			    <li><a href="/car?bodyType=bt9" class="btn <?php if(Url::current() == '/car/index?bodyType=bt9') echo "active"; else echo ""; ?> choose__icon-responsive"><img src="/images/commercialicon.png" alt="Commercial cars icon"> Commercial cars</a></li>
			   	<li><a href="/bike" class="btn choose__icon-responsive"><img src="/images/motorbikeicon.png" alt="Motorbike icon"> Motorbike</a></li>
			   	<li><a href="/homeAndLI" class="btn choose__icon-responsive"><img src="/images/itineryicon.png" alt="Home and itinery icon"> Home and Living</a></li>
		   </div>
	   </ul>
	</div>

	<div class="car-search__row">
		<div class="row no-gutters">
			<div class="col-lg-3">
				<div class="car-search__column">
					<div class="row no-gutters">
						<div class="col-lg-12 col-7">
							<div class="car-search__border">
								<img src="/images/search.png" class="car-search-feedback" alt="Search icon">
								<input type="text" name="keyword" id="" class="form-control car-search-text" placeholder="Search" value="<?php echo (isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '') ?>" autocomplete="off" onkeyup="if (event.keyCode === 13) { window.location = 'autopart?keyword=' + this.value; }"/>
							</div>
						</div>
						<div class="col-lg-12 col-5">
							<div class="car-search__column-responsive">
								<div class="row">
									<div class="col-6">
										<p>
											<span class="header__burger"></span>
									        <span class="header__burger header__burger--adjust"></span>
									        <span class="header__burger"></span>
										</p>
									</div>
									<div class="col-6">
										<p class="car-search__filter-text">Filter</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="car-search__desktop">
						<div class="car-search__border">
							<p class="car-search__clear">
								<span style="font-size: 25px;">Filter</span> <span><a href="/autopart" class="car-search__link">Clear all</a></span>
							</p>
						</div>
						<div class="car-search__border">
							<p class="car-search__clear car-search__clear--margin">
								Show with <img id="autopart-filter-open" <?php echo ( (isset($_REQUEST['superdeals']) && $_REQUEST['superdeals'] != '' ) || ( isset($_REQUEST['video']) && $_REQUEST['video'] != '' ) ) ? 'src="/images/close.png"' : 'src="/images/plus.png"'; ?> >
							</p>
							<p class="<?php echo ( (isset($_REQUEST['superdeals']) && $_REQUEST['superdeals'] != '' ) || ( isset($_REQUEST['video']) && $_REQUEST['video'] != '' ) ) ? '' : 'd-none'; ?>" id="autopart-filter-show">
								<a class="btn car-search__button <?php echo (isset($_REQUEST['superdeals']) && $_REQUEST['superdeals'] != '') ? 'active' : ''; ?>" href="<?php echo Yii::$app->general->generatecarsseo('autopart', 0, 0, '', 0, '', '', '', 1, []); ?>">
									Super Deals
								</a> 
								<a class="btn car-search__button <?php echo (isset($_REQUEST['video']) && $_REQUEST['video'] != '') ? 'active' : ''; ?>" href="<?php echo Yii::$app->general->generatecarsseo('autopart', 0, 0, '', 0, '', '', '', 0, ['video' => 1]); ?>">
									Video
								</a> 
							</p>
						</div>
						<div class="car-search__border">
							<p class="car-search__clear car-search__clear--margin">Condition <img id="autopart-condition-open" <?php echo (isset($_REQUEST['type']) && $_REQUEST['type'] != '') ? 'src="/images/close.png"' : 'src="/images/plus.png"'; ?>></p>
							<p class="<?php echo (isset($_REQUEST['type']) && $_REQUEST['type'] != '') ? '' : 'd-none'; ?>" id="autopart-condition-show">
								<a class="btn car-search__button <?php echo (isset($_REQUEST['type']) && $_REQUEST['type'] == 'used') ? 'active' : ''; ?>" href="<?php echo $pdata['url_type_used']; ?>">Used</a> 
								<a class="btn car-search__button <?php echo (isset($_REQUEST['type']) && $_REQUEST['type'] == 'new') ? 'active' : ''; ?>" href="<?php echo $pdata['url_type_new']; ?>">New</a> 
								<a class="btn car-search__button <?php echo (isset($_REQUEST['type']) && $_REQUEST['type'] == 'recond') ? 'active' : ''; ?>" href="<?php echo $pdata['url_type_recond']; ?>">Recon</a></p>
						</div>
						<div class="car-search__border">
							<p class="car-search__clear car-search__clear--margin">Car make <img id="autopart-make-open" <?php echo ((isset($_REQUEST['make']) && $_REQUEST['make'] != '' )) ? 'src="/images/close.png"' : 'src="/images/plus.png"'; ?>></p>
<?php
	$make_array = $master_car_make->getmake();

	if (count($make_array)) {
?>		
							<p class="<?php echo ( (isset($_REQUEST['make']) && $_REQUEST['make'] != '' ) ) ? '' : 'd-none'; ?>" id="autopart-make-show">
<?php
		foreach ($make_array as $make_id_loop => $make_data_loop) {
?>				
								<a class="btn car-search__button <?php echo ( (isset($_REQUEST['make']) && $_REQUEST['make'] == $make_id_loop ) ) ? 'active' : ''; ?>" href="/autopart?make=<?php echo $make_id_loop ?>" id="car-search__load-<?php echo $make_id_loop ?>"><?php echo $make_data_loop; ?></a>
<?php
		}
?>
							</p>
<?php
	}
?>
						</div>
<?php
	if ($pdata['car_make_name']) {
		$model_group_array = $master_car_model_group->getmodelgroup($pdata['car_make_cd']);
		$display_car_array = $trans_order->getpopularmodel($pdata['car_make_cd']);

		$model_group_list = [];
		$model_group_total = 0;
		if (count($model_group_array)) {
			foreach ($model_group_array as $model_group_array_loop => $model_group_array_data_loop) {
				$group_name = $model_group_array_data_loop['group_name'];
				$model_group_total = 0;
				foreach ($model_group_array_data_loop['model_group'] as $model_group_key_loop => $model_group_data_loop) {
					if (count($display_car_array)) {
						foreach ($display_car_array as $display_car_model_data_loop => $total) {
							if ($display_car_model_data_loop == $model_group_data_loop) {
								$model_group_total += $total;
								$model_group_list[$group_name] = $display_car_model_data_loop;
								$display_car_array = $model_group_list + $display_car_array;
								unset($display_car_array[$display_car_model_data_loop]);
							}
						}
					}
				}
			}
		}

		if (count($display_car_array)) {
			$count = 0;
?>					
						<div class="car-search__border">
							<p class="car-search__clear car-search__clear--margin">Car models <img id="autopart-model-open" <?php echo ( (isset($_REQUEST['model']) && $_REQUEST['model'] != '' ) ) ? 'src="/images/close.png"' : 'src="/images/plus.png"'?>></p>
							<p class="<?php echo ( (isset($_REQUEST['model']) && $_REQUEST['model'] != '' ) ) ? '' : 'd-none'?>" id="autopart-model-show">
<?php
				foreach ($display_car_array as $display_car_model_data_loop => $model_cd) {
					$count++;
					
					$car_model_name = $master_car_model->getmodelname($display_car_model_data_loop);
					if (isset($car_model_name) && trim($car_model_name)) {
						$model_name = $car_model_name;
						$model_link = Yii::$app->general->generateseo('brand_a', $pdata['filter']['make'] . '&model=' . $model_cd);
					} else {
						$model_name = $display_car_model_data_loop;
						$model_link = Yii::$app->general->generateseo('brand_a', $pdata['filter']['make'] . '&model=' . $model_cd);
					}
?>						
								<a class="btn car-search__button <?php echo ( (isset($_REQUEST['model']) && $_REQUEST['model'] == $model_cd ) ) ? 'active' : ''?>" href="<?php echo $model_link; ?>"><?php echo $model_name; ?></a>
<?php
				}
?>						
							</p>
						</div>	
<?php
		}
	}
?>						
						<div class="car-search__border">
							<p class="car-search__clear">Category <img id="autopart-category-open" <?php echo ((isset($_REQUEST['category']) && $_REQUEST['category'] != '' )) ? 'src="/images/close.png"' : 'src="/images/plus.png"'; ?>></p>
<?php
	$autoparts_category_array = $trans_statistic_count->listcount('autoparts_rank');
	if ($autoparts_category_array !== false && count($autoparts_category_array)) {
		$count = 0;
		$count2 = 0;
?>
							<p class="<?php echo ( (isset($_REQUEST['category']) && $_REQUEST['category'] != '' ) ) ? '' : 'd-none'?>" id="autopart-category-show">
<?php
		foreach ($autoparts_category_array as $autoparts_category_array_name_loop => $autoparts_category_array_count_loop) {
			if ($autoparts_category_array_count_loop > 0) {
				$temp_category = explode(">", $autoparts_category_array_name_loop);

				$filter_category = '';
				
				switch(trim($temp_category[0])){
					case 'Rims & Tyres':
						$filter_category = 1;
						break;
					case 'Rims & Tires':
						$filter_category = 1;
						break;	
					case 'Car security & Navigation system':
						$filter_category = 2;
						break;
					case 'In car entertainment & Car navigation system':
						$filter_category = 2;
						break;	
					case 'Performance Part':
						$filter_category = 3;
						break;
					case 'Exterior & Body Parts':
						$filter_category = 4;
						break;
					case 'Engine & Transmission':
						$filter_category = 5;
						break;
					case 'Car Care':
						$filter_category = 7;
						break;
					case 'Interior Accesories':
						$filter_category = 8;
						break;
					case 'Int. Accessories':
						$filter_category = 8;
						break;	
					case 'Oils, Coolants & Fluids':
						$filter_category = 9;
						break;
					case 'Clothing':
						$filter_category = 10;
						break;
					case 'Other Accesories':
						$filter_category = 11;
						break;
					case 'Half-cut':
						$filter_category = 12;
						break;	
				}
?>
							<a class="btn car-search__button <?php echo ( (isset($_REQUEST['category']) && $_REQUEST['category'] == $filter_category ) ) ? 'active' : ''?>" href="<?php echo Yii::$app->urlManager->createAbsoluteUrl(['autopart', 'category' => $filter_category]); ?>"><?php echo $autoparts_category_array_name_loop; ?></a>
<?php
			}
		}
?>
						</p>
<?php
	}
?>				
						</div>
<?php
	if ($pdata['filter']['category']) {
		$sub_category_array = $master_car_sub_part->getSubCategory($pdata['filter']['category']);

		if(count($sub_category_array)) {
?>						
						<div class="car-search__border">
							<p class="car-search__clear">Sub category <img id="autopart-subcategory-open" <?php echo ((isset($_REQUEST['subcategory']) && $_REQUEST['subcategory'] != '' )) ? 'src="/images/close.png"' : 'src="/images/plus.png"'; ?>></p>
							<p class="<?php echo ( (isset($_REQUEST['subcategory']) && $_REQUEST['subcategory'] != '' ) ) ? '' : 'd-none'?>" id="autopart-subcategory-show">
<?php
			foreach($sub_category_array as $model_id_loop => $model_data_loop) {
?>
							<a class="btn car-search__button <?php echo ( (isset($_REQUEST['subcategory']) && $_REQUEST['subcategory'] == $model_id_loop ) ) ? 'active' : ''?>" href="<?php echo Yii::$app->urlManager->createAbsoluteUrl(['autopart', 'category' => $pdata['filter']['category'], 'subcategory' => $model_id_loop]); ?>"><?php echo $model_data_loop; ?></a>
<?php			
			}
		}
?>	
							</p>							
						</div>
<?php
	}
?>
						<div class="car-search__border">
							<p class="car-search__clear car-search__clear--margin">Price</p>
							<div id="slider-range">
								<div id="custom-handle" class="ui-slider-handle"></div>
								<div id="custom-handle-2" class="ui-slider-handle"></div>
							</div>
							<p class="car-search__margin">
								<a class="btn car-search__slide" href="#">
									<span class="car-search__slide-rm">RM</span><br>
									<span id="minPrice"></span>
								</a>
								<img src="/images/blueline.png" class="car-search__slide-to" alt="To">
								<a class="btn car-search__slide" href="#">
									<span class="car-search__slide-rm">RM</span><br>
									<span id="maxPrice"></span>
								</a>
							</p>
						</div>
						<div class="car-search__border">
							<p class="car-search__clear car-search__clear--margin">State <img id="autopart-state-open" <?php echo (isset($_REQUEST['location']) && $_REQUEST['location'] != '' ) ? 'src="/images/close.png"' : 'src="/images/plus.png"'; ?>></p>
<?php
	$location_array = $master_general->getLocation();
	if (count($location_array)) {
?>			
							<p class="<?php echo (isset($_REQUEST['location']) && $_REQUEST['location'] != '' ) ? '' : 'd-none'; ?>" id="autopart-state-show">
<?php
		foreach ($location_array as $location_array_id_loop => $location_array_data_loop) {
			$filter_location='';
			switch($location_array_data_loop){
				case 'Kuala Lumpur':
					$filter_location = 'kuala_lumpur';
					break;
				case 'Selangor':
					$filter_location = 'selangor';
					break;
				case 'Johor':
					$filter_location = 'johor';
					break;
				case 'Melaka':
					$filter_location = 'melaka';
					break;
				case 'Perak':
					$filter_location = 'perak';
					break;
				case 'Sarawak':
					$filter_location = 'sarawak';
					break;
				case 'Sabah':
					$filter_location = 'sabah';
					break;
				case 'Kedah':
					$filter_location = 'kedah';
					break;
				case 'Kelantan':
					$filter_location = 'kelantan';
					break;
				case 'Negeri Sembilan':
					$filter_location = 'negeri_sembilan';
					break;
				case 'Penang':
					$filter_location = 'penang';
					break;
				case 'Pahang':
					$filter_location = 'pahang';
					break;
				case 'Terengganu':
					$filter_location = 'terengganu';
					break;
				case 'Perlis':
					$filter_location = 'perlis';
					break;	
			}

			if (trim($filter_location)) {
?>				
								<a class="btn car-search__button <?php echo (isset($_REQUEST['location']) && $_REQUEST['location'] == $location_array_id_loop ) ? 'active' : ''; ?>" href="<?php echo Yii::$app->general->generatecarsseo('autopart', '', '', $filter_location, 0); ?>"><?php echo $location_array_data_loop; ?></a>
<?php
			}
		}
?>
							</p>
<?php
	}
?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-9">
				<div class="car-list">
					<div class="car-list__head">
						<div class="row">
							<div class="col-lg-6">
								<p class="car-list__show">Showing <?php echo $pdata['total']; ?> results</p>
							</div>
							<div class="col-lg-6">
								<p class="car-list__sort">
									Sort by 
									<select onchange="location = this.value;">
										<option <?php if ($pdata['sort'] == '') echo "selected"; ?> disabled>Select one</option>
										<option <?php if($pdata['sort'] == 'updatelow') echo "selected"; ?> value="<?php echo $pdata['url_sort_update_low']; ?>">Recent (latest first)</option>
										<option <?php if($pdata['sort'] == 'updatehigh') echo "selected"; ?> value="<?php echo $pdata['url_sort_update_high']; ?>">Recent (earliest first)</option>
										<option <?php if ($pdata['sort'] == 'pricehigh') echo "selected"; ?> value="<?php echo $pdata['url_sort_price_high']; ?>">Price (highest first)</option>
										<option <?php if ($pdata['sort'] == 'pricelow') echo "selected"; ?> value="<?php echo $pdata['url_sort_price_low']; ?>">Price (lowest first)</option>
									</select>
								</p>
							</div>
						</div>
					</div>
					<div class="row">
<?php
	if (count($trans_advert_model)) {
		$count_advert = 0;
		foreach ($trans_advert_model as $trans_advert_data_loop) {

			$count_advert++;
			
			$seo_url = Yii::$app->general->generateseo('autopart', $trans_advert_data_loop['urn_no'], $trans_advert_data_loop['make_name']);
			
			$advert_image = Yii::$app->general->advertimageurl($trans_advert_data_loop['urn_no'] . '.jpg', 'webad', strtotime($trans_advert_data_loop['upd_data_date']));
			
			$master_dealer_model = $master_dealer->getDealer($trans_advert_data_loop['dealer_id']);
			
			$trans_advert_super_deals_model = $super_deals->getsuperdeals($trans_advert_data_loop['urn_no']);	
			
			$has_super_deals = $trans_advert_super_deals_model['got_super_deals'];
?>				
						<div class="col-lg-4">
							<div class="superdeals-featured-cnabadv__featured-ads-section-listing superdeals-featured-cnabadv__featured-ads-section-listing--adjust <?php echo $has_super_deals === '1' ? 'superdeals-featured-cnabadv__featured-ads-section-listing--border' : ''; ?>">
								<p class="car-list__position">
									<a href="<?php echo $seo_url; ?>">
										<img class="featured-ads-section__img featured-ads-section__img--adjust lazyload" src="<?php echo Yii::$app->fpath->images("ajax-loader.gif"); ?>" data-src="<?php echo @getimagesize($advert_image) ? $advert_image : '/images/autopart.png'; ?>" alt="<?php echo $trans_advert_data_loop['make_name'] ; ?>">
									</a>

<?php 		
				if($has_super_deals === '1') { 
?>
									<button class="btn superdeals-featured-cnabadv__insights-btn superdeals-featured-cnabadv__insights-btn--position">Super Deals</button>
<?php 
				} 
?>
									<span class="car-list__bookmark <?php echo (in_array($trans_advert_data_loop['urn_no'], $pdata['bookmark_array']) ? 'car-list__show-bookmark' : '') ?>" data-urn="<?php echo $trans_advert_data_loop['urn_no'] ?>"><img class="car-list__none" src="/images/bookmark.png"><img class="car-list__all" src="/images/bookmarked.png"></span>
<?php
				$autopart_video = Yii::$app->api->getvideo($trans_advert_data_loop['urn_no']);
				if ($autopart_video !== false)	{
?>
								<div class="adv-video-icon">
									<div onclick="popupvideo('<?php echo $autopart_video['video_url'] ?>', '<?php echo addslashes($autopart_video['video_title']) ?>')">
										<img src="/images/vid_ico.png" alt="Video icon">
									</div>
								</div>
<?php
				}
?>	
								</p>								
								 <p class="text-center featured-ads-section__title text-right"><a href="<?php echo $seo_url; ?>"><?php echo $trans_advert_data_loop['make_name']; ?></a></p>
<?php
			if (isset($trans_advert_super_deals_model) && date("Y-m-d H:i:s") >= $trans_advert_super_deals_model['super_deals_schedule_start_date'] && date("Y-m-d H:i:s") <= $trans_advert_super_deals_model['super_deals_schedule_end_date']) {
?>
								<div>
<?php
				if ($trans_advert_data_loop['price'] == 0) {
					echo '<p class="featured-ads-section__price text-right">POA</p>';
				} else {
					if ($trans_advert_super_deals_model['sale_price'] == '0') {
						echo '<p class="featured-ads-section__price text-right">POA</p>';
					} else {					
?>
									<p class="featured-ads-section__price text-right"><small class="featured-ads-section__price-currency">RM</small> <?php echo number_format($trans_advert_super_deals_model['sale_price']); ?></p>
<?php
					if ($trans_advert_super_deals_model['sale_price'] != $trans_advert_super_deals_model['regular_price']) { 			
?>
									<p class="featured-ads-section__price text-right"><small class="featured-ads-section__price-currency">RM</small> <?php echo number_format($trans_advert_super_deals_model['regular_price']); ?></p>
<?php
						}
?>					
<?php
					}
				}
?>
								</div>
<?php
			} else {
?>
								<div>
<?php
				if ($trans_advert_data_loop['price'] == 0) {
					echo '<p class="featured-ads-section__price text-right">POA</p>';
				} else {
					echo '<p class="featured-ads-section__price text-right"><small class="featured-ads-section__price-currency">RM</small> ' . number_format($trans_advert_data_loop['price']) . '</p>'; 
				}
?>						
								</div>
						     

				
<?php			
			}
?>
								<hr/>
	                            <div class="row no-gutters">
	                                <div class="col-2">
	                                    <p><img class="featured-ads-section__icon" src="/images/location.png" alt="location" /></p>
	                                </div>
	                                <div class="col-10">
	                                    <span class="featured-ads-section__text">Location</span>
	                                    <p><?php echo $trans_advert_data_loop['location_name']; ?></p>
	                                </div>
	                            </div>
		                        <div class="row no-gutters">
		                            <div class="col-2">
		                                <p><img class="featured-ads-section__icon" src="/images/autoparticon.png" alt="category" /></p>
		                            </div>
		                            <div class="col-10">
		                                <span class="featured-ads-section__text">Category</span>
<?php
		foreach ($autoparts_category_array as $autoparts_category_array_name_loop => $autoparts_category_array_count_loop) {
			if ($autoparts_category_array_count_loop > 0) {
				if (preg_match('%' . $autoparts_category_array_name_loop . '%', $trans_advert_data_loop['model_name'])) {
?>
											<p><?php echo $autoparts_category_array_name_loop; ?></p>
<?php
				}
			}
		}
?>			                                
			                                
		                            </div>
		                        </div>	 
							</div>
						</div>	
<?php 
		}
	}
?>
						
					</div>
<?php
	if ($pdata['total'] > 0) {
		$pagination_range = 5;

        $start_page = $pdata['page'] - floor($pagination_range / 2);
        if ($start_page < 1) {
            $start_page = 1;
        }

        $total_end_page = ceil($pdata['total'] / $pdata['limit']);

        $end_page = $pdata['page'] + floor($pagination_range / 2);
        if ($end_page > $total_end_page) {
            $end_page = $total_end_page;
        }

        if (($end_page - $pdata['page']) < floor($pagination_range / 2) && $pagination_range < floor($pdata['total'] / $pdata['limit'])) {
            $start_page = ceil($pdata['total'] / $pdata['limit']) - $pagination_range;
        }

        if ($end_page < $pagination_range && floor($pdata['total'] / $pdata['limit']) >= $pagination_range) {
            $end_page = $pagination_range;
        }
?>
					<div>
						<p class="text-center featured-ads-section__page">
<?php
		if ($pdata['page'] > 1) {
			$processed_link = str_replace('__PAGE__', ($pdata['page'] - 1), $pdata['url_pagination']);
?>					
							<span class="<?php echo ($start_page > 1 ? '' : 'd-none-lg-up'); ?>"><a href="<?php echo $processed_link; ?>"><img src="/images/previous.png" alt="Previous"></a>
<?php
		}
?>					
					<span><?php echo $pdata['page'] . ' / ' . $end_page; ?></span>
<?php
		if ($pdata['page'] < ceil($pdata['total'] / $pdata['limit']) ) {
			$processed_link = str_replace('__PAGE__', ($pdata['page'] + 1), $pdata['url_pagination']);
?>					
							<a href="<?php echo $processed_link; ?>"> <img src="/images/next.png" alt="Next"></a></span>
<?php
		}
?>				
						</p>
					</div>
<?php
	}
?>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="popupFrame">
	<div id="videoForm">
		<div id="title">
			<span id="video-title">Video</span>
			<div id="closeBtn" onClick="closeVideo()">X</div>
		</div>
		<div id="video-popup-frame-container">		
			<object id="popup-video-object" controls autoplay muted width="100%" height="100%" class="video img img-fluid video-popup-frame"></object>
			<video id="popup-video-video" controls autoplay muted width="100%" height="100%" class="video img img-fluid video-popup-frame"></video>
		</div>
	</div>
</div>	