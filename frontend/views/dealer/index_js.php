<script type="text/javascript">
	jQuery("#link-slider").protoSlider({
		'fadeintime': 1000
	});
	
	window.addEventListener("load", function(event) {
        lazyload();
    });

    jQuery('#dealer-state-open').click(function () {
		if(jQuery('#dealer-state-show').hasClass('d-none')){
			jQuery('#dealer-state-show').removeClass('d-none');
			jQuery('#dealer-state-open').attr("src", "/images/close.png");
		} else {
			jQuery('#dealer-state-show').addClass('d-none');
			jQuery('#dealer-state-open').attr("src", "/images/plus.png");
		}
	});
	jQuery('#dealer-dealer-open').click(function () {
		if(jQuery('#dealer-dealer-show').hasClass('d-none')){
			jQuery('#dealer-dealer-show').removeClass('d-none');
			jQuery('#dealer-dealer-open').attr("src", "/images/close.png");
		} else {
			jQuery('#dealer-dealer-show').addClass('d-none');
			jQuery('#dealer-dealer-open').attr("src", "/images/plus.png");
		}
	});
</script>