<script>
	window.addEventListener("load", function(event) {
	    lazyload();
	});

	function showCarousel(index) {
        $('#demo').carousel(parseInt(index));
    }

    var total = $('.carousel-indicators-items').length;
    var currentIndex = $('div.active').index() + 1;
    $('.carousel-text').html(currentIndex + '/'  + total);

    // This triggers after each slide change
    $('.carousel').on('slid.bs.carousel', function () {
      currentIndex = $('div.active').index() + 1;

      // Now display this wherever you want
      var text = currentIndex + '/' + total;
      $('.carousel-text').html(text);
    });

</script>