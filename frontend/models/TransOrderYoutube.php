<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trans_order_youtube".
 *
 * @property string $urn_no
 * @property string|null $youtube_id
 * @property string|null $trans_order_youtube_title
 * @property string|null $trans_order_youtube_tag
 * @property string|null $trans_order_youtube_created
 * @property float $mtcs_credits
 * @property string|null $youtube_thumbnail
 * @property string|null $youtube_status
 * @property string|null $trans_order_youtube_description
 * @property string|null $trans_order_youtube_status
 * @property string|null $trans_order_youtube_filename
 */
class TransOrderYoutube extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trans_order_youtube';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['urn_no', 'mtcs_credits'], 'required'],
            [['trans_order_youtube_created'], 'safe'],
            [['mtcs_credits'], 'number'],
            [['urn_no'], 'string', 'max' => 20],
            [['youtube_id', 'trans_order_youtube_title', 'trans_order_youtube_tag', 'youtube_thumbnail', 'trans_order_youtube_filename'], 'string', 'max' => 255],
            [['youtube_status', 'trans_order_youtube_status'], 'string', 'max' => 32],
            [['trans_order_youtube_description'], 'string', 'max' => 500],
            [['urn_no'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'urn_no' => 'Urn No',
            'youtube_id' => 'Youtube ID',
            'trans_order_youtube_title' => 'Trans Order Youtube Title',
            'trans_order_youtube_tag' => 'Trans Order Youtube Tag',
            'trans_order_youtube_created' => 'Trans Order Youtube Created',
            'mtcs_credits' => 'Mtcs Credits',
            'youtube_thumbnail' => 'Youtube Thumbnail',
            'youtube_status' => 'Youtube Status',
            'trans_order_youtube_description' => 'Trans Order Youtube Description',
            'trans_order_youtube_status' => 'Trans Order Youtube Status',
            'trans_order_youtube_filename' => 'Trans Order Youtube Filename',
        ];
    }
}
