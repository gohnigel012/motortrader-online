<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_car_variant".
 *
 * @property string $make_cd
 * @property string $model_cd
 * @property string $variant_cd
 * @property string $variant_name
 * @property float|null $sort_index 12.12.11 added
 * @property string|null $upd_user
 * @property string|null $upd_date
 * @property string|null $disp_flg 1:Show to system, 0:Disabled
 */
class MasterCarVariant extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_car_variant';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['make_cd', 'model_cd', 'variant_cd', 'variant_name'], 'required'],
            [['sort_index'], 'number'],
            [['upd_date'], 'safe'],
            [['make_cd', 'model_cd', 'variant_cd', 'upd_user'], 'string', 'max' => 20],
            [['variant_name'], 'string', 'max' => 255],
            [['disp_flg'], 'string', 'max' => 1],
            [['make_cd', 'model_cd', 'variant_cd'], 'unique', 'targetAttribute' => ['make_cd', 'model_cd', 'variant_cd']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'make_cd' => 'Make Cd',
            'model_cd' => 'Model Cd',
            'variant_cd' => 'Variant Cd',
            'variant_name' => 'Variant Name',
            'sort_index' => 'Sort Index',
            'upd_user' => 'Upd User',
            'upd_date' => 'Upd Date',
            'disp_flg' => 'Disp Flg',
        ];
    }

    public function getvariantname($variant_cd) {
        $master_car_variant_model = $this->find()->where(['variant_cd' => $variant_cd])->one();
        if ($master_car_variant_model) {
            return $master_car_variant_model->variant_name;
        }
        
        return false;
    }

    public function getvariantcd($model_name) {
        $master_car_variant_model = $this->find()->where(['ilike', 'variant_name', $model_name])->one();
        if ($master_car_variant_model) {
            return $master_car_variant_model->variant_cd;
        }
        
        return false;
    }
}
