<?php
use app\models\TransStatisticCount;
use app\models\MasterCarMake;
use app\models\MasterCarModel;
use app\models\MasterCarModelGroup;
use app\models\MasterGeneral;

	$master_car_make = new MasterCarMake();
	$master_car_model = new MasterCarModel();
	$master_car_model_group = new MasterCarModelGroup();
	$master_general = new MasterGeneral();
	$trans_statistic_count = new TransStatisticCount();

	$location_array = $master_general->getLocation();
	$body_type_array = $master_general->getBodyType();
	$color_array = $master_general->getColor();
	$mileage_array = $master_general->getMileage();
	$transmission_array = $master_general->getTransmission();
	$color_available_array = ['black', 'grey', 'white', 'red', 'blue', 'yellow', 'green', 'pink', 'orange', 'gold', 'silver', 'gray', 'brown', 'other'];

	$default_type = 'all';
	$default_dealers = 'all';

	$default_min_year = (isset($_REQUEST['minYear']) && is_numeric($_REQUEST['minYear']) ? $_REQUEST['minYear'] : '');
    $default_max_year = (isset($_REQUEST['maxYear']) && is_numeric($_REQUEST['maxYear']) ? $_REQUEST['maxYear'] : '');

	$default_min_price = (isset($_REQUEST['minPrice']) && is_numeric($_REQUEST['minPrice']) ? $_REQUEST['minPrice'] : '' );
	$default_max_price = (isset($_REQUEST['maxPrice']) && is_numeric($_REQUEST['maxPrice']) ? $_REQUEST['maxPrice'] : '' );

	$default_keyword = (isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '');
	$default_city = (isset($_REQUEST['cityname']) ? ucfirst(strtolower($_REQUEST['cityname'])) : '');

	$price_range_array = Yii::$app->general->filterPriceRange();
	$desc_price_range_array = Yii::$app->general->filterPriceRange(false);

	$year_range_array = Yii::$app->general->filterYearRange();
	$desc_year_range_array = Yii::$app->general->filterYearRange(false);

	$make_array = $master_car_make->getmake();
?>
<div class="modal fade" id="squarespaceModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="modal-title" id="modal-head-title"> Advanced Search - Cars </h3>
                    </div>
                    <div class="col-md-6">
                        <p class="text-right"><button type="button" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button></p>
                    </div>
                    <form name="frm-home-modal" id="modal-search-car" action="/car" method="get"> 
                    	<div class="form-group">
<?php
	$count_car_type = 0;
	foreach (['all'=>'All Cars', 'used' => 'Used', 'new' =>'New', 'recond' => 'Recond'] as $car_type_value_loop => $car_type_data_loop){
		$count_car_type++;
?>
							<label>
								<input id="car_type_<?php echo $count_car_type ?>" data-name="header-car-type" data-value="<?php echo $car_type_value_loop ?>" data-index="<?php echo $count_car_type ?>" name="type" type="radio" value="<?php echo $car_type_value_loop ?>" <?php echo ($car_type_value_loop == $default_type ? 'checked' : '') ?> onchange="headerUpdateCarType('radio')"> 
								<span><?php echo $car_type_data_loop; ?>
							</label>
<?php
	}
?>                    		
                    	</div>
                    	<div class="form-group">
							<div class="row">
								<div class="col-xs-2">
									<p><span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span></p>
								</div>
								<div class="col-xs-10">
									<p><input type="text" name="keyword" id="model-search-keyword"  class="form-control" placeholder="eg. 2017 Toyota vios" value="<?php echo $default_keyword; ?>"></p>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6">
										<select name="make" id="home-popup-make" class="form-control">
											<option value="">All Make</option>
<?php
	$popular_array = [];
	$popular_array['1010'] = 'TOYOTA';
	$popular_array['1020'] = 'HONDA';
	$popular_array['9009'] = 'PERODUA';
	$popular_array['2100'] = 'PROTON';
	$popular_array['1015'] = 'NISSAN';
	$popular_array['2025'] = 'MERCEDES-BENZ';
	$popular_array['2015'] = 'BMW';

	if (count($popular_array)) {
?>
											<optgroup label="------ Popular Make ------">
<?php
		foreach ($popular_array as $popular_make_id_loop => $popular_make_data_loop) {
			$display_number = $trans_statistic_count->displaycount('car_make', $popular_make_id_loop);
			if ($display_number !== false) {
?>
												<option value="<?php echo $popular_make_id_loop ?>"><?php echo $popular_make_data_loop ?> <?php echo $display_number ?></option>
<?php
			}
		}
?>
											</optgroup>		
<?php
	}
?>
											<optgroup label="--------------------------">
<?php
	
	foreach ($make_array as $car_make_value_loop => $car_make_loop) {
		$display_number = $trans_statistic_count->displaycount('car_make', $car_make_value_loop);
		if ($display_number !== false) {
?>
												<option value="<?php echo $car_make_value_loop ?>" <?php echo (isset($_REQUEST['make']) && $car_make_value_loop == $_REQUEST['make'] ? 'selected' : '') ?>><?php echo $car_make_loop; ?> <?php echo $display_number; ?></option>
<?php
		}
	}
?>

											</optgroup>									
										</select>
									</div>
									<div class="col-sm-6">
										<select name="model" id="home-popup-model" class="form-control">
											<option value="">All Models</option>
<?php
	if (isset($_REQUEST['make']) && trim($_REQUEST['make'])) {
		
		$model_array = $master_car_model->getmodelbymake($_REQUEST['make']);
		$model_group_array = $master_car_model_group->getmodelgroup($_REQUEST['make']);
		$car_model_array = [];
		$car_model_group_array = [];
		$display_number_model_group = 0;
		
		if (count($model_array)) {
			foreach ($model_array as $car_model_value_loop => $car_model_data_loop){
				$display_number = $trans_statistic_count->displaycount('car_model', $car_model_value_loop);
				$is_model_group = false;
				if ($display_number !== false) {
					if (count($model_group_array)) {
						foreach ($model_group_array as $model_group_array_loop => $model_group_array_data_loop) {
							foreach ($model_group_array_data_loop['model_group'] as $model_group_key_loop => $model_group_data_loop) {
								if ($car_model_value_loop == $model_group_data_loop) {
									$is_model_group = true;
								}
							}
						}
						
						if ($is_model_group == false) {
							$car_model_array[$car_model_value_loop] = $car_model_data_loop;
						}
					} else {
						$car_model_array[$car_model_value_loop] = $car_model_data_loop;
					}
				}
			}
		}
		
		if (count($model_group_array)) {
			$count = 0;
			foreach ($model_group_array as $model_group_array_loop => $model_group_array_data_loop) {
				$count++;
				$group_name = $model_group_array_data_loop['group_name'];
				$model_group_list = $model_group_array_data_loop['model_group_list'];
				$display_number_model_group = 0;
				foreach ($model_group_array_data_loop['model_group'] as $model_group_key_loop => $model_group_data_loop) {
					if (count($model_array)) {
						foreach ($model_array as $car_model_value_loop => $car_model_data_loop) {
							$display_number = $trans_statistic_count->displaycount('car_model', $car_model_value_loop);
							if ($display_number !== false) {
								if ($car_model_value_loop == $model_group_data_loop) {
									$display_number = str_replace(['(', ')'],"",$display_number);
									$display_number_model_group += $display_number;
									$is_model_group = true;
								}
							}
						}
					}
				}
				
				if ($is_model_group == true && $display_number_model_group != 0) {
					$car_model_group_array[$model_group_list] = $group_name . ' ' . '(' . $display_number_model_group . ')';
				}
			}
		} 
			
		if (count($car_model_group_array) !== 0) {
?>
												<optgroup label="------ Group ------">	
												</optgroup>
<?php			
			foreach ($car_model_group_array as $car_model_group_key_loop => $car_model_group_data_loop) {			
?>
												<option value="<?php echo $car_model_group_key_loop; ?>" <?php echo (isset($_REQUEST['model']) && $car_model_group_key_loop == $_REQUEST['model'] ? 'selected' : ''); ?>><?php echo $car_model_group_data_loop; ?></option>
<?php
			}
?>
												<optgroup label="--------------------------">
												</optgroup>			
<?php
		}
		foreach ($car_model_array as $car_model_array_value_loop => $car_model_array_data_loop){
			$display_number = $trans_statistic_count->displaycount('car_model', $car_model_array_value_loop);
			if ($display_number !== false) {
?>
												<option value="<?php echo $car_model_array_value_loop; ?>" <?php echo (isset($_REQUEST['model']) && $car_model_array_value_loop == $_REQUEST['model'] ? 'selected' : ''); ?>><?php echo $car_model_array_data_loop; ?> <?php echo $display_number; ?></option>
<?php
			}
		}
	}
?>
										</select>			
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<select name="location" id="home-popup-location" class="form-control">
											<option value="">All Locations</option>
<?php
	foreach ($location_array as $car_location_value_loop => $car_location_loop){
?>
											<option value="<?php echo $car_location_value_loop; ?>" <?php echo (isset($_REQUEST['location']) && $_REQUEST['location'] == $car_location_value_loop ? 'selected' : '');?>><?php echo ($car_location_loop); ?></option>
<?php
	}
?>													
										</select>
									</div>
									<div class="col-sm-6">
										<select name="bodyType" id="home-popup-type"  class="form-control">
											<option value="">All Body Types</option>
<?php
	foreach ($body_type_array as $car_body_value_loop => $car_body_loop){
?>
												
											<option value="<?php echo $car_body_value_loop; ?>" <?php echo (isset($_REQUEST['bodyType']) && $_REQUEST['bodyType'] == $car_body_value_loop ? 'selected' : '');?>><?php echo ($car_body_loop); ?></option>
<?php
	}										
?>											
										</select>
									</div>
								</div>
								<div class="form-group">
									<p>Search City :</p>
									<p><input type="text" name="cityname" id="home-popup-cityname" class="form-control" placeholder="eg. City Name" value="<?php echo $default_city; ?>"></p>
								</div>
								<div class="form-group">
									<p>Price Range :</p>
									<div class="row">
										<div class="col-sm-6">
											<select name="minPrice" id="modal-price-min" class="form-control">
												<option value="">No Min</option>
<?php
	foreach ($price_range_array as $price_range_value_loop => $price_range_label_loop) {
?>
												<option value="<?php echo $price_range_value_loop; ?>" <?php echo ($default_min_price == $price_range_value_loop ? 'selected': ''); ?>><?php echo $price_range_label_loop; ?></option>
<?php
	}
?>												
											</select>
										</div>
<div class="col-sm-6">
											<select name="maxPrice" id="modal-price-max" class="form-control">
												<option value="">No Max</option>
<?php
	foreach ($desc_price_range_array as $price_range_value_loop => $price_range_label_loop) {
?>
												<option value="<?php echo $price_range_value_loop; ?>" <?php echo ($default_max_price == $price_range_value_loop ? 'selected': ''); ?>><?php echo $price_range_label_loop; ?></option>
<?php
	}
?>												
											</select>
										</div>			
									</div>
								</div>
								<div class="form-group">
									<p>Year : </p>
									<div class="row">
										<div class="col-xs-6">
											<select name="minYear" id="modal-year-min" class="form-control">
												<option value="">No Min</option>
<?php
	foreach ($year_range_array as $year_range_value_loop => $year_range_label_loop) {
?>
												<option value="<?php echo $year_range_value_loop; ?>" <?php echo ($default_min_year == $year_range_value_loop ? 'selected': ''); ?>><?php echo $year_range_label_loop; ?></option>
<?php
	}
?>												
											</select>
										</div>
										<div class="col-xs-6">
											<select name="maxYear" id="modal-year-max" class="form-control">
												<option value="">No Max</option>
<?php
	foreach ($desc_year_range_array as $year_range_value_loop => $year_range_label_loop) {
?>
												<option value="<?php echo $year_range_value_loop ?>" <?php echo ($default_max_year == $year_range_value_loop ? 'selected': '') ?>><?php echo $year_range_label_loop ?></option>
<?php
	}
?>
											</select>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-xs-6">
											<p>Mileage : </p>
											<select name="mileage" id="home-popup-mileage" class="form-control">
												<option value="">All Mileage</option>
<?php
	foreach($mileage_array as $mileage_loop_key => $mileage_loop_name) {
?>
													
												<option value="<?php echo $mileage_loop_key; ?>" <?php echo (isset($_REQUEST['mileage']) && $_REQUEST['mileage'] == $mileage_loop_key ? 'selected' : ''); ?>><?php echo ($mileage_loop_name); ?></option>
<?php
	}	
?>												
											</select>
										</div>
										<div class="col-xs-6">
											<p>Transmission : </p>
											<select name="transmission" id="home-popup-transmission" class="form-control">
												<option value="">All Transmission</option>
<?php
	foreach($transmission_array as $transmission_loop_key => $transmission_loop_name) {
?>
													
												<option value="<?php echo $transmission_loop_key; ?>" <?php echo (isset($_REQUEST['transmission']) && $_REQUEST['transmission'] == $transmission_loop_key ? 'selected' : '');?>><?php echo ($transmission_loop_name); ?></option>
<?php
	}	
?>
											</select>
										</div>
									</div>
								</div>
								<div class="form-group">
									<p>Color : </p>
									<select name="color" id="home-popup-color" class="form-control">
											<option value="">Color</option>
<?php
	foreach($color_array as $color_loop_key => $color_loop_name) {
		if (in_array(strtolower($color_loop_name), $color_available_array)) { 
?>
													
											<option value="<?php echo $color_loop_key; ?>" <?php echo (isset($_REQUEST['color']) && $_REQUEST['color'] == $color_loop_key ? 'selected' : '')?>><?php echo ($color_loop_name); ?></option>
<?php
		}
	}	
?>
										</select>
								</div>
							</div>
						</div>
						<div class="modal-button-search">
							<button class="form-control margin-bottom-10" type="submit" id="search-btn-modal"> Search </button>
							<button type="reset" class="form-control" id="reset-btn-modal" onclick="resetAdvanceSearch()">Reset</button>
						</div>
                    </form>                       
                </div>
            </div>
        </div>
    </div>
</div>