<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use app\models\MasterGeneral;
use app\models\MasterDealer;
use app\models\MasterDealerReview;
use app\models\MasterDealerReviewInfo;
use app\models\SuperDeals;
use app\models\TransOrder;
use app\models\TransOrderExtraInfo;
use app\models\TransStatisticCount;

/**
 * Dealer controller
 */
class DealerController extends Controller
{
	/**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
    	$master_dealer = new MasterDealer();
    	$master_general = new MasterGeneral();
    	$master_dealer_review = new MasterDealerReview();
    	$master_dealer_review_info = new MasterDealerReviewInfo();
    	$trans_order = new TransOrder();

    	$pdata = [];

    	$pdata['avanser_array'] = Yii::$app->general->avanser();

		$dealer_array = [];

    	$pdata['limit'] = 9;
		if (isset($_REQUEST['limit']) && $_REQUEST['limit']) {
			$pdata['limit'] = $_REQUEST['limit'];
		}

		$pdata['filter']['location'] = '';
		if (isset($_REQUEST['location'])) {
			$pdata['filter']['location'] = $_REQUEST['location'];
		}
		
		$pdata['sort'] = 'totalstockdesc';
		if (isset($_REQUEST['sort']) && in_array($_REQUEST['sort'], array('nameasc', 'namedesc', 'totalstockasc', 'totalstockdesc'))) {
			$pdata['sort'] = $_REQUEST['sort'];
	    }
		
		$pdata['filter']['keyword'] = '';
		if (isset($_REQUEST['keyword'])) {
			$pdata['filter']['keyword'] = trim($_REQUEST['keyword']);
		}

		$pdata['filter']['dealers_type'] = 'all';
		if (isset($_REQUEST['dealers_type']) && in_array($_REQUEST['dealers_type'], ['1', '2'])) {
			$pdata['filter']['dealers_type'] = $_REQUEST['dealers_type'];
		}

    	$offset = 0;

    	$pdata['total'] = 0;
    	$pdata['page'] = 1;

    	if (isset($_REQUEST['page']) && (int)$_REQUEST['page'] == $_REQUEST['page']) {
			$pdata['page'] = (int)$_REQUEST['page'];
			if ($pdata['page'] > 0) {
				$offset = $pdata['limit'] * ($pdata['page'] - 1);
			} else {
				$offset = 0;
			}
		}

    	$master_dealer_select_array = [];
		$master_dealer_select_array[] = '*';
		$master_dealer_select_array[] = "(CASE WHEN last_posted IS NULL THEN '2011-01-01 00:00:00' ELSE last_posted END) as plast_posted";

		$master_dealer_criteria = $master_dealer->find()->select($master_dealer_select_array)->where("dealer_name <> '-' AND dealer_id NOT IN ('T99001','T888') AND total_stock > 0 ");

		$master_dealer_params_array = [];
		if (isset($_REQUEST['keyword']) && trim($_REQUEST['keyword'])) {
			$master_dealer_criteria->andWhere(" dealer_name ilike :dealer_name ");
			$master_dealer_params_array[':dealer_name'] = "%" . $_REQUEST['keyword'] . "%";
		}
		
		$pdata['page_location'] = 'Malaysia';

		if (isset($_REQUEST['location']) && trim($_REQUEST['location'])) {
			$master_dealer_criteria->andWhere(" location_cd ilike :location_cd ");
			$master_dealer_params_array[':location_cd'] = "%" . $_REQUEST['location'] . "%";
			
			$tmp_location_array = Yii::$app->general->location();
			if (isset($tmp_location_array[$pdata['filter']['location']])) {
				$pdata['page_location'] = $tmp_location_array[$pdata['filter']['location']] . ', Malaysia';
			}
		}

		switch ($pdata['filter']['dealers_type']) {
			case '1':
				$master_dealer_criteria->andWhere("dealer_type = '1'");
				break;
			case '2':
				$master_dealer_criteria->andWhere("dealer_type = '2'");
				break;
		}

		$master_dealer_criteria->params($master_dealer_params_array);

		$pdata['total'] = $master_dealer_criteria->count();

		$master_dealer_criteria->offset($offset);
		$master_dealer_criteria->limit($pdata['limit']);

		switch($pdata['sort']) {
			case 'totalstockasc':
	        	$master_dealer_criteria->orderBy('total_stock ASC');
	        break;
			case 'namedesc':
	        	$master_dealer_criteria->orderBy('dealer_name DESC');
	        break;
			case 'nameasc':
	        	$master_dealer_criteria->orderBy('dealer_name ASC');
	        break;
	        default:
	        	$master_dealer_criteria->orderBy('plast_posted DESC, total_stock DESC');
	        break;
        }

        $master_dealer_model = $master_dealer_criteria->all();

        if (count($master_dealer_model)) {
    		foreach ($master_dealer_model as $master_dealer_data_loop) {

    			$dealer_array[$master_dealer_data_loop->dealer_id] = [	
    				'dealer_id' => $master_dealer_data_loop->dealer_id,
    				'dealer_name' => $master_dealer_data_loop->dealer_name,
					'address' => $master_dealer_data_loop->address,
					'phone' => $master_dealer_data_loop->phone,
					'location_name' => $master_dealer_data_loop->location_name,
					'total_stock' => $master_dealer_data_loop->total_stock,
					'dealer_type' => $master_dealer_data_loop->dealer_type,
					'master_dealer_logo' => $master_dealer_data_loop->master_dealer_logo,
				];
    		}
    	}

    	$recent_view_array = Yii::$app->general->advfromrecent('car');
		$pdata['recentview'] = [];
		if (count($recent_view_array)) {
			foreach ($recent_view_array as $recent_urn_loop) {
				$recent_view_info =$trans_order->loadinfo($recent_urn_loop);
				if ($recent_view_info !== false) {
					$pdata['recentview'][] = $recent_view_info;
				}
			}
		}

		$url_parse = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
		parse_str($url_parse, $url_array);

		$url_array['sort'] = 'totalstockasc';
		$pdata['url_sort_totalstock_low'] = Yii::$app->urlManager->createAbsoluteUrl('/dealer') . '?' . http_build_query($url_array);				
		
		$url_array['sort'] = 'totalstockdesc';
		$pdata['url_sort_totalstock_high'] = Yii::$app->urlManager->createAbsoluteUrl('/dealer') . '?' . http_build_query($url_array);	
		
		$url_array['sort'] = 'nameasc';
		$pdata['url_sort_name_low'] = Yii::$app->urlManager->createAbsoluteUrl('/dealer') . '?' . http_build_query($url_array);
		
		$url_array['sort'] = 'namedesc';
		$pdata['url_sort_name_high'] = Yii::$app->urlManager->createAbsoluteUrl('/dealer') . '?' . http_build_query($url_array);
		
		$url_array['sort'] = $pdata['sort'];

		$url_array['page'] = '1';
		
		$url_array['page'] = '__PAGE__';
		$pdata['url_pagination'] = Yii::$app->urlManager->createAbsoluteUrl('/dealer') . '?' . http_build_query($url_array);

    	 return $this->render('index', ['dealer_array' => $dealer_array, 'pdata' => $pdata, 'master_general' => $master_general, 'master_dealer_review' => $master_dealer_review, 'master_dealer_review_info' => $master_dealer_review_info,]);
    }

    public function actionView() {
    	$master_dealer = new MasterDealer();
    	$master_dealer_review = new MasterDealerReview();
    	$master_dealer_review_info = new MasterDealerReviewInfo();
    	$super_deals = new SuperDeals();
    	$trans_order = new TransOrder();
    	$trans_order_extra_info = new TransOrderExtraInfo();
    	$trans_statistic_count = new TransStatisticCount();

    	$pdata = [];

    	$total_overall = 0;
		$average_total_overall = 0;
		$count_master_dealer_review_info = 0;
		$master_dealer_review_info_model = false;

		$pdata['limit'] = 16;
		$pdata['offset'] = 0;

		$pdata['filter']['keyword'] = (isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '');
		$pdata['filter']['minPrice'] = (isset($_REQUEST['minPrice']) && is_numeric($_REQUEST['minPrice']) ? $_REQUEST['minPrice'] : '' );
		$pdata['filter']['maxPrice'] = (isset($_REQUEST['maxPrice']) && is_numeric($_REQUEST['maxPrice']) ? $_REQUEST['maxPrice'] : '' );

		if (isset($_REQUEST['limit']) && $_REQUEST['limit']) {
			$limit = $_REQUEST['limit'];
			$pdata['offset'] = 0;
		}

		$pdata['page'] = 1;
		if (isset($_REQUEST['page']) && (int)$_REQUEST['page'] == $_REQUEST['page']) {
			$pdata['page'] = (int)$_REQUEST['page'];

			if ($pdata['page'] > 0) {
				$pdata['offset'] = $pdata['limit'] * ($pdata['page'] - 1);
			} else {
				$pdata['offset'] = 0;
			}
		}

		$pdata['filter']['type'] = 'all';
		// if (isset($_REQUEST['type']) && in_array(ltrim($_REQUEST['type'], "/"), array('all', 'cars', 'autoparts', 'numberplate', 'bike'))) {
		if (isset($_REQUEST['type']) && in_array(ltrim($_REQUEST['type'], "/"), ['all', 'cars', 'autoparts', 'numberplate', 'bike'])) {
            $pdata['filter']['type'] = ltrim($_REQUEST['type'], "/");
        }

		$pdata['sort'] = 'updatehigh';
		if (isset($_REQUEST['sort']) && in_array($_REQUEST['sort'], ['pricehigh', 'pricelow', 'yearhigh', 'yearlow', 'updatelow', 'updatehigh'])) {
			$pdata['sort'] = $_REQUEST['sort'];
		}

		$offset = 0;

		$pdata['total'] = 0;
		$pdata['page'] = 1;
		
		if (isset($_REQUEST['page']) && (int)$_REQUEST['page'] == $_REQUEST['page']) {
			$pdata['page'] = (int)$_REQUEST['page'];

			if ($pdata['page'] > 0) {
				$offset = $pdata['limit'] * ($pdata['page'] - 1);
			} else {
				$offset = 0;
			}
		}

    	if (isset($_REQUEST['dealer_id'])) {
    		$master_dealer_model = $master_dealer->findOne($_REQUEST['dealer_id']);

    		if ($master_dealer_model) {
    			$dealer_array = [];
				if ($master_dealer_model->dealer_id == 'T1859') {
					$dealer_array[] = $master_dealer_model->dealer_id;
					$dealer_array[] = 'T26774';
					$dealer_array[] = 'T26773';
					$dealer_array[] = 'T26393';
					$dealer_array[] = 'T28344';
					$dealer_array[] = 'T29266';
					$dealer_array[] = 'T26779';
					$dealer_array[] = 'T26775';
					$dealer_array[] = 'T26778';
					$dealer_array[] = 'T26777';
				} else if ($master_dealer_model->dealer_id == 'T31556') {
					$dealer_array[] = $master_dealer_model->dealer_id;
					$dealer_array[] = 'T17737';
					$dealer_array[] = 'T30162';
				} else {
					$dealer_array_criteria = $master_dealer->find()->where(" dealer_hierachy_parent = :dealer_hierachy_parent ");
					
					$dealer_array_params_array = [];
					$dealer_array_params_array[':dealer_hierachy_parent'] = $master_dealer_model->dealer_id;
					$dealer_array_criteria->params($dealer_array_params_array);
					
					$child_model = $dealer_array_criteria->all();
					$dealer_array[] = $master_dealer_model->dealer_id;
					if (count($child_model)) {
						foreach ($child_model as $child_data_loop) {
							$dealer_array[] = $child_data_loop->dealer_id;
						}
					}
				}

				$trans_advert_params_array = [];
				$trans_advert_criteria = $trans_order->find()->where(" dealer_id IN ('" . implode("','", $dealer_array) . "') AND status = '40'");

				switch ($pdata['filter']['type']) {
					case 'car':
						$trans_advert_criteria->andWhere("classification_cd = '1141' ");
						break;
					// case 'autopart':
					// 	$trans_advert_criteria->andWhere("classification_cd = '1166' ");
					// 	break;
					// case 'numberplate':
					// 	$trans_advert_criteria->andWhere(" AND advert_category = '3' ");
					// 	break;
					case 'bike':
						$trans_advert_criteria->andWhere("classification_cd = '1167' ");
						break;	
				}

				switch ($pdata['filter']['type']) {
					case 'car':
						$pdata['action_route'] = 'car';
						$pdata['page_label'] = 'Cars';
						
						$trans_advert_criteria->andWhere("classification_cd = '1141'");
						break;
					// case 'autopart':
					// 	$pdata['action_route'] = 'autoparts';
					// 	$pdata['page_label'] = 'Auto Parts';
						
					// 	$trans_advert_criteria->andWhere(" AND advert_category ='2' AND classification_cd = '1166' ");
					// 	break;
					// case 'numberplate':
					// 	$pdata['action_route'] = 'numberplate';
					// 	$pdata['page_label'] = 'Number Plate';
						
					// 	$trans_advert_criteria->andWhere(" AND advert_category ='3' AND (classification_cd = '1165' OR classification_cd ='2493') ");
					// 	break;
					case 'bike':
						$pdata['action_route'] = 'bike';
						$pdata['page_label'] = 'Motorcycle';
						
						$trans_advert_criteria->andWhere("classification_cd = '1167' ");
						break;	
					default:
						$pdata['action_route'] = 'all';
						$pdata['page_label'] = 'All items';
						break;
				}

				if (isset($_REQUEST['keyword']) && trim($_REQUEST['keyword'])){
					$pdata['filter']['keyword'] = $_REQUEST['keyword'];
					
					$keyword_temp_array = [];
					$count_keyword = 0;
					foreach (explode(" ", $pdata['filter']['keyword']) as $keyword_loop) {
						$count_keyword++;
						
						$keyword_temp_array[] = " model_name ILIKE :keyword_" . $count_keyword ;
						$trans_advert_params_array[':keyword_' . $count_keyword] = "%" . $keyword_loop . "%";
					}
					
					$trans_advert_criteria->andWhere(implode(" AND ", $keyword_temp_array));
				}

				if ((isset($_REQUEST['minPrice']) && trim($_REQUEST['minPrice'])) || (isset($_REQUEST['maxPrice']) && trim($_REQUEST['maxPrice']))){
					$trans_advert_criteria->andWhere(" t.poa_flg <> '1' ");
				}

				if (isset($_REQUEST['minPrice']) && trim($_REQUEST['minPrice'])) {
					$min_price = str_replace(",", "", $_REQUEST['minPrice']);
					if (is_numeric($min_price)) {
						$pdata['filter']['minPrice'] = $min_price;
						$trans_advert_criteria->andWhere(" (CASE WHEN price = 'POA' OR poa_flg = '1' THEN '-1' ELSE t.price END) >= :minPrice");
						$trans_advert_params_array[':minPrice'] = $pdata['filter']['minPrice'];
					}
				}

				if (isset($_REQUEST['maxPrice']) && trim($_REQUEST['maxPrice'])) {
					$max_price = str_replace(",", "", $_REQUEST['maxPrice']);
					if (is_numeric($max_price)) {
						$pdata['filter']['maxPrice'] = $max_price;
						$trans_advert_criteria->addCondition(" (CASE WHEN price = 'POA' OR poa_flg = '1' THEN '-1' ELSE t.price END) <= :maxPrice");
						$trans_advert_params_array[':maxPrice'] = $pdata['filter']['maxPrice'];
					}
				}

				$pdata['avanser_array'] = Yii::$app->general->avanser();

				$pdata['seourl'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name);

				$trans_advert_criteria->params($trans_advert_params_array);
				$pdata['total'] = $trans_advert_criteria->count();

				$trans_advert_select_array = [];
				$trans_advert_select_array[] = '*';

				if (in_array($pdata['sort'], ['pricelow', 'pricehigh'])) {

					$trans_advert_select_array[] = "(CASE WHEN price = 'POA' OR poa_flg = '1' THEN '9999999999' ELSE price END) as plprice";
					$trans_advert_select_array[] = "(CASE WHEN price = 'POA' OR poa_flg = '1' THEN '0' ELSE price END) as phprice";
				}

				$trans_advert_criteria->select($trans_advert_select_array);

				switch ($pdata['sort']) {
					case 'pricehigh':
			        	$trans_advert_criteria->orderBy("phprice DESC, upd_data_date DESC");
			        break;
			        case 'pricelow':
			        	$trans_advert_criteria->orderBy("plprice, upd_data_date DESC");
			        break;
			        case 'yearhigh':
			        	$trans_advert_criteria->orderBy("year_make DESC, upd_data_date DESC");
			        break;
			        case 'yearlow':
			        	$trans_advert_criteria->orderBy("year_make, upd_data_date DESC");
			        break;
			        case 'updatelow':
			        	$trans_advert_criteria->orderBy("upd_data_date ");
			        break;
			        default:
			        	$trans_advert_criteria->orderBy("upd_data_date DESC ");
		        	break;
		        }

		        $trans_advert_criteria->limit($pdata['limit']);
				$trans_advert_criteria->offset($pdata['offset']);

				$trans_advert_model = $trans_advert_criteria->all();

				$url_parse = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
				parse_str($url_parse, $url_array);

				unset($url_array);

				/*
					Remember to put default at the last
				*/
				switch ($pdata['filter']['type']) {
					case 'car':
						$url_array['sort'] = 'pricehigh';
						$pdata['url_sort_price_high'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);
						
						$url_array['sort'] = 'pricelow';
						$pdata['url_sort_price_low'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = 'yearhigh';
						$pdata['url_sort_year_high'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = 'yearlow';
						$pdata['url_sort_year_low'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = 'updatehigh';
						$pdata['url_sort_update_low'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = 'updatelow';
						$pdata['url_sort_update_high'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = $pdata['sort'];

						$pdata['url_pagination'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array) . '&page=__PAGE__';
						break;
					case 'autopart':

						$url_array['sort'] = 'pricehigh';
						$pdata['url_sort_price_high'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);
						
						$url_array['sort'] = 'pricelow';
						$pdata['url_sort_price_low'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = 'yearhigh';
						$pdata['url_sort_year_high'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = 'yearlow';
						$pdata['url_sort_year_low'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = 'updatehigh';
						$pdata['url_sort_update_low'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = 'updatelow';
						$pdata['url_sort_update_high'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = $pdata['sort'];

						$pdata['url_pagination'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array) . '&page=__PAGE__';
						break;
					case 'numberplate':

						$url_array['sort'] = 'pricehigh';
						$pdata['url_sort_price_high'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);
						
						$url_array['sort'] = 'pricelow';
						$pdata['url_sort_price_low'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = 'yearhigh';
						$pdata['url_sort_year_high'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = 'yearlow';
						$pdata['url_sort_year_low'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = 'updatehigh';
						$pdata['url_sort_update_low'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = 'updatelow';
						$pdata['url_sort_update_high'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = $pdata['sort'];
						
						$pdata['url_pagination'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array) . '&page=__PAGE__';
						break;
					case 'bike':

						$url_array['sort'] = 'pricehigh';
						$pdata['url_sort_price_high'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);
						
						$url_array['sort'] = 'pricelow';
						$pdata['url_sort_price_low'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = 'yearhigh';
						$pdata['url_sort_year_high'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = 'yearlow';
						$pdata['url_sort_year_low'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = 'updatehigh';
						$pdata['url_sort_update_low'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = 'updatelow';
						$pdata['url_sort_update_high'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = $pdata['sort'];
						
						$pdata['url_pagination'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array) . '&page=__PAGE__';
						break;	
					default:

						$url_array['sort'] = 'pricehigh';
						$pdata['url_sort_price_high'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);
						
						$url_array['sort'] = 'pricelow';
						$pdata['url_sort_price_low'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = 'yearhigh';
						$pdata['url_sort_year_high'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);
				
						$url_array['sort'] = 'yearlow';
						$pdata['url_sort_year_low'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = 'updatehigh';
						$pdata['url_sort_update_low'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = 'updatelow';
						$pdata['url_sort_update_high'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

						$url_array['sort'] = $pdata['sort'];
						

						$url_array['page'] = '__PAGE__';
						$pdata['url_pagination'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);
						break;
				}

				$master_dealer_review_model = $master_dealer_review->find()->where(['dealer_id' => $master_dealer_model->dealer_id])->one();

    			if (isset($master_dealer_review_model)) {
					if ($master_dealer_review_model->master_dealer_review_status == '1') {
						
						$count_master_dealer_review_info = $master_dealer_review_info->countdealerreviewinfo($master_dealer_review_model->dealer_id);

						$master_dealer_review_info_model = $master_dealer_review_info->find()->where(['dealer_id' => $master_dealer_model->dealer_id])->orderBy('master_dealer_review_info_id DESC')->all();
						
						$from_date = date("Y-m-d", strtotime("- 24 months"));
						$to_date = date("Y-m-d");
						
						$master_dealer_review_info_model_average_overall = $master_dealer_review_info->find()->where(['and', ['>=', 'review_created', date("Y-m-d 00:00:00", strtotime($from_date))], ['<=', 'review_created', date("Y-m-d 23:59:59", strtotime($to_date))], ['dealer_id' => $master_dealer_review_model->dealer_id]])->all();
						$count = count($master_dealer_review_info_model_average_overall);
						if (count($master_dealer_review_info_model_average_overall) > 0) {                   
							foreach($master_dealer_review_info_model_average_overall as $master_dealer_review_info_model_average_overall_data_loop) {  
								$total_overall += $master_dealer_review_info_model_average_overall_data_loop->review_overall_rating;
							}
						}
						
						if ($count > 0)  $average_total_overall = round(($total_overall / $count));
					}
				} else {
					$master_dealer_review_info_model = '';
				}

				$url_array['type'] = 'all';
				
				if (isset($url_array['page'])) unset($url_array['page']);
				
				$pdata['url_type_all'] = Yii::$app->general->generateseo('dealer', $master_dealer_model->dealer_id, $master_dealer_model->dealer_name) . '&' . http_build_query($url_array);

				$pdata['bookmark_array'] = Yii::$app->general->advfrombookmark();

    			return $this->render('view', ['master_dealer_model' => $master_dealer_model, 'trans_advert_model' => $trans_advert_model, 'pdata' => $pdata, 'master_dealer_review_model' => $master_dealer_review_model, 'master_dealer_review_info_model' => $master_dealer_review_info_model, 'average_total_overall' => $average_total_overall, 'count_master_dealer_review_info' => $count_master_dealer_review_info, 'trans_order_extra_info' => $trans_order_extra_info, 'super_deals' => $super_deals, 'master_dealer_review_info' => $master_dealer_review_info, 'trans_statistic_count' => $trans_statistic_count]);
    		} else {
    			return $this->redirect('/dealer');
    		}
    		
    	} else {
    		return $this->redirect('/dealer');
    	}
    }
}