<?php
namespace frontend\components;
use Yii;
use yii\base\Component;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\web\UploadedFile;
use yii\web\Cookie;

class Fcookie extends Component {
	public function hasCookie($name) {
        return !empty(Yii::$app->getRequest()->getCookies()->getValue($name));
    }

    public function getCookie($name) {
        return Yii::$app->getRequest()->getCookies()->getValue($name);
    }

    public function setCookie($name, $value) {
        Yii::$app->request->cookies->readOnly = false;

        $cookie = new Cookie([
            'name' => $name,
            'value' => $value,
        ]);
        
        Yii::$app->getResponse()->getCookies()->add($cookie);

    }

    public function removeCookie($name) {
        Yii::$app->getResponse()->getCookies()->remove(new Cookie($this->identityCookie));
    }
}
