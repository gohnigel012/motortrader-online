<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_car_sub_part".
 *
 * @property string $sub_part_cd
 * @property string $part_cd
 * @property string|null $sub_part_name
 * @property int|null $sorting
 * @property int|null $disp_flg
 */
class MasterCarSubPart extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_car_sub_part';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sub_part_cd', 'part_cd'], 'required'],
            [['sorting', 'disp_flg'], 'default', 'value' => null],
            [['sorting', 'disp_flg'], 'integer'],
            [['sub_part_cd', 'part_cd'], 'string', 'max' => 30],
            [['sub_part_name'], 'string', 'max' => 300],
            [['sub_part_cd', 'part_cd'], 'unique', 'targetAttribute' => ['sub_part_cd', 'part_cd']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sub_part_cd' => 'Sub Part Cd',
            'part_cd' => 'Part Cd',
            'sub_part_name' => 'Sub Part Name',
            'sorting' => 'Sorting',
            'disp_flg' => 'Disp Flg',
        ];
    }

    public function getSubCategory($category) {
        $return_array = [];
        
        $master_car_sub_part_memcache_key = "/master_car_sub_part/subcategory/category/" . $category;
        $master_car_sub_part_memcache = Yii::$app->cache->get($master_car_sub_part_memcache_key);

        if ($master_car_sub_part_memcache !== false) {
            $return_array = $master_car_sub_part_memcache;
        } else {
            $master_car_sub_part_model = $this->find()->where(['part_cd' => $category, 'disp_flg' => '1'])->orderBy('sub_part_name')->all();
            if ($master_car_sub_part_model) {
                foreach ($master_car_sub_part_model as $master_car_sub_part_data_loop) {
                    $return_array[$master_car_sub_part_data_loop->sub_part_cd] = $master_car_sub_part_data_loop->sub_part_name;
                }
            }

            Yii::$app->cache->set($master_car_sub_part_memcache_key, $return_array, 3600);
        }

        return $return_array;
    }
}
