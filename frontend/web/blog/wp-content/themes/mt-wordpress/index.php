<!DOCTYPE html>
<html>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="This website is based on the existing MotorTrader website that is currently being maintained by Cloudkia Solutions Sdn Bhd. This website sells advertisements which shows sales of different cars from different vendors.">
    <meta name="csrf-param" content="_csrf-frontend">
	<meta name="csrf-token" content="2QE3j4y1PpaWV1WH41VByzypoPLOyCqDDXV86RyT_xHvSkDK-eYIydAaIc6QGieZeNzCl7qHebF0IxXYacXGaA==">
    <title>MotorTrader News</title>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri().'/css/bootstrap.css'; ?>">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri().'/css/bootstrap.min.css'; ?>">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri().'/css/site.css'; ?>">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">

	<?php wp_head(); ?>
</head>
	<body>

	<?php wp_body_open(); ?>

	<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
		<?php if(function_exists('bcn_display'))
		{
			bcn_display();
		}?>
		</div>

		<div class="insights">
			<?php the_content(__('(more...)')); ?>
		</div>
	<?php endwhile; else: ?>
		<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
	<?php endif; ?>

	<?php get_footer(); ?>

	</body>
</html>
<script src="<?php echo get_stylesheet_directory_uri().'/js/jquery.js'; ?>"></script>
<script src="<?php echo get_stylesheet_directory_uri().'/js/bootstrap.js'; ?>"></script>