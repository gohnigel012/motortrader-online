<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use app\models\MasterDealer;
use app\models\MasterDealerReview;
use app\models\MasterDealerReviewinfo;
use app\models\MasterGeneral;
use app\models\SuperDeals;
use app\models\TransOrder;
use app\models\TransOrderExtraInfo;
use app\models\TransOrderPicture;

/**
 * Bike controller
 */
class BikeController extends Controller
{
	/**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
    	$master_dealer = new MasterDealer();
    	$master_general = new MasterGeneral();
    	$super_deals = new SuperDeals();
    	$trans_order = new TransOrder();
    	$trans_order_extra_info = new TransOrderExtraInfo();

    	$location_array = $master_general->getLocation();

    	$url_filter_params = [];

    	$pdata = [];

    	$pdata['page_location'] = 'Malaysia';
		$pdata['page'] = 1;
		$pdata['limit'] = 9;
		$pdata['offset'] = 0;
		
		$pdata['autoscroll'] = false;
		
		if (isset($_REQUEST['limit']) && $_REQUEST['limit']) {
			$limit = $_REQUEST['limit'];
			$pdata['offset'] = 0;
		}

		$pdata['page'] = 1;
		if (isset($_REQUEST['page']) && (int)$_REQUEST['page'] == $_REQUEST['page']) {
			
			$pdata['autoscroll'] = 'result';
			
			$pdata['page'] = (int)$_REQUEST['page'];

			if ($pdata['page'] > 0) {
				$pdata['offset'] = $pdata['limit'] * ($pdata['page'] - 1);
			} else {
				$pdata['offset'] = 0;
			}
		}

		if ($pdata['page'] > 1) {
			$url_filter_params['page'] = $pdata['page'];
		}

		$trans_advert_criteria = $trans_order->find()->where("t.urn_no is not null AND t.status = '40' AND (t.classification_cd = '1167')");
		$trans_advert_params_array = [];

		$pdata['filter']['location'] = '';
		$pdata['filter']['locationname'] = '';
		
		$pdata['filter']['superdeals'] = '0';
		if (isset($_REQUEST['superdeals'])) {
			$pdata['filter']['superdeals'] = $_REQUEST['superdeals'];
		}

		$pdata['filter']['mileage'] = '';
		if (isset($_REQUEST['mileage'])) {
			$pdata['filter']['mileage'] = trim($_REQUEST['mileage']);
		}
		
		if (isset($_REQUEST['location']) && isset($location_array[$_REQUEST['location']])) {
			$pdata['filter']['location'] = $_REQUEST['location'];
			$pdata['filter']['locationname'] = strtolower(preg_replace('~[^\\pL\d]+~u', '_', $location_array[$pdata['filter']['location']]));

		} else if (isset($_REQUEST['locationname']) && trim($_REQUEST['locationname'])) {
			$pdata['filter']['locationname'] = ltrim($_REQUEST['locationname'], "/");
		}

		if (trim($pdata['filter']['location'])){
			$pdata['filter']['location'] = $pdata['filter']['location'];

			$trans_advert_criteria->andWhere(" location_cd = :location ");
			$trans_advert_params_array[':location'] = $pdata['filter']['location'];

			$tmp_location_array = Yii::$app->general->location();
			if (isset($tmp_location_array[$pdata['filter']['location']])) {
				$pdata['page_location'] = $tmp_location_array[$pdata['filter']['location']] . ', Malaysia';

				$meta_keyword_array[] = $tmp_location_array[$pdata['filter']['location']];
			}
		}
		
		$pdata['filter']['type'] = 'all';
		if (isset($_REQUEST['type']) && in_array($_REQUEST['type'], ['all', 'used', 'new', 'recond'])) {
			$pdata['filter']['type'] = $_REQUEST['type'];
		}
		
		$pdata['filter']['dealers_type'] = 'all';
		if (isset($_REQUEST['dealers_type']) && in_array($_REQUEST['dealers_type'], ['1', '2'])) {
			$pdata['filter']['dealers_type'] = $_REQUEST['dealers_type'];
		}

		$pdata['filter']['mileage'] = false;
		if (isset($_REQUEST['mileage']) && trim($_REQUEST['mileage'])){
			$pdata['filter']['mileage'] = $_REQUEST['mileage'];
			$trans_advert_criteria->andWhere("mileage_code = :mileage");
			$trans_advert_params_array[':mileage'] = $pdata['filter']['mileage'];
		}

		$pdata['sort'] = 'updatehigh';
		if (isset($_REQUEST['sort']) && in_array($_REQUEST['sort'], ['pricehigh', 'pricelow', 'yearhigh', 'yearlow', 'updatelow', 'updatehigh', 'lth', 'htl'])) {
			if (in_array($_REQUEST['sort'], ['pricehigh'])) {
				$pdata['sort'] = 'pricehigh';
			} else if (in_array($_REQUEST['sort'], ['pricelow'])) {
				$pdata['sort'] = 'pricelow';
			} else {
				$pdata['sort'] = $_REQUEST['sort'];
			}
	    }

	    $trans_advert_select_array = [];
		$trans_advert_select_array[] = '*';

	    if (in_array($pdata['sort'], ['pricelow', 'pricehigh'])) {
	    	$trans_advert_select_array[] = "(CASE WHEN price = '0' OR poa_flg = '1' THEN 9999999999 ELSE price::numeric END) as plprice";
			$trans_advert_select_array[] = "(CASE WHEN price = '0' OR poa_flg = '1' THEN 0 ELSE price::numeric END) as phprice";
		}

		$trans_advert_criteria->select($trans_advert_select_array)->from('trans_order t');

		if (isset($_REQUEST['minPrice']) && trim($_REQUEST['minPrice'])) {
			$min_price = str_replace(",", "", $_REQUEST['minPrice']);
			if (is_numeric($min_price)) {
				$pdata['filter']['minPrice'] = $min_price;
				// $trans_advert_criteria->andWhere(" CAST(t.price AS DECIMAL) >= :minPrice");
				$trans_advert_criteria->andWhere(" (CASE WHEN price = 'POA' OR poa_flg = '1' THEN -1 ELSE CAST(t.price AS DECIMAL) END) >= :minPrice");
				$trans_advert_params_array[':minPrice'] = $pdata['filter']['minPrice'];
			}
		}

		if (isset($_REQUEST['maxPrice']) && trim($_REQUEST['maxPrice'])) {
			$max_price = str_replace(",", "", $_REQUEST['maxPrice']);
			if (is_numeric($max_price)) {
				$pdata['filter']['maxPrice'] = $max_price;
				// $trans_advert_criteria->andWhere(" CAST(t.price AS DECIMAL) <= :maxPrice ");
				$trans_advert_criteria->andWhere(" (CASE WHEN price = 'POA' OR poa_flg = '1' THEN -1 ELSE CAST(t.price AS DECIMAL) END) <= :maxPrice");
				$trans_advert_params_array[':maxPrice'] = $pdata['filter']['maxPrice'];
			}
		}

		if (isset($_REQUEST['minYear']) && trim($_REQUEST['minYear']) ) {
			$pdata['filter']['minYear'] = $_REQUEST['minYear'];
			$trans_advert_criteria->andWhere("CAST(CASE WHEN t.year_make ~ '^[0-9]+$' THEN t.year_make ELSE '1979' END AS INTEGER) >= :minYear");
			$trans_advert_params_array[':minYear'] = $pdata['filter']['minYear'];
		
		}
		
		if (isset($_REQUEST['maxYear']) && trim($_REQUEST['maxYear']) ){
			$pdata['filter']['maxYear'] = $_REQUEST['maxYear'];
			$trans_advert_criteria->andWhere("CAST(CASE WHEN t.year_make ~ '^[0-9]+$' THEN t.year_make ELSE '1979' END AS INTEGER) <= :maxYear");
			$trans_advert_params_array[':maxYear'] = $pdata['filter']['maxYear'];
		
		}

		if (isset($pdata['filter']['minYear']) && isset($pdata['filter']['maxYear']) && $pdata['filter']['minYear'] == $pdata['filter']['maxYear']) {
			$pdata['page_year'] = $pdata['filter']['minYear'];
		}

		switch ($pdata['filter']['dealers_type']) {
			case '1':
				$trans_advert_criteria->innerJoin("master_dealer md", "md.dealer_id = t.dealer_id");
				$trans_advert_criteria->andWhere("md.dealer_type = '1'");
				break;
			case '2':
				$trans_advert_criteria->andWhere("md.dealer_type = '2'");
				break;
		}

		$pdata['filter']['from_date'] = false;
		if (isset($_REQUEST['days']) && (int)$_REQUEST['days']) {
			if (in_array($_REQUEST['days'], [1, 7, 30, 90])) {
				$pdata['filter']['from_date'] = date('Y-m-d H:i:s', strtotime('- ' . $_REQUEST['days'] . ' days'));
			}
			
			$trans_advert_criteria->andWhere("upd_data_date >= :from ");
			$trans_advert_params_array[':from'] = $pdata['filter']['from_date'];
		}

		$pdata['filter']['video_file'] = false;
		if (isset($_REQUEST['video']) && (int)$_REQUEST['video']) {
			$pdata['filter']['video_file'] = $_REQUEST['video'];
			
			$trans_advert_criteria->innerJoin("trans_order_youtube tay", "tay.urn_no = t.urn_no ");
		}

		if ($pdata['filter']['superdeals'] == '1') {
			$has_join_table = true;
			if ($has_join_table) {
				$trans_advert_criteria->innerJoin("super_deals sd", "sd.urn_no = t.urn_no ");
				$trans_advert_criteria->andWhere(" sd.super_deals_schedule_start_date <= :date ");
				$trans_advert_criteria->andWhere(" sd.super_deals_schedule_end_date >= :date ");
				$trans_advert_params_array[':date'] = date("Y-m-d H:i:s");
			} else {
				$trans_advert_criteria->innerJoin("super_deals sd", "sd.urn_no = t.urn_no ");
				$trans_advert_criteria->andWhere(" sd.super_deals_schedule_start_date <= :date ");
				$trans_advert_criteria->andWhere(" sd.super_deals_schedule_end_date >= :date ");
				$trans_advert_params_array[':date'] = date("Y-m-d H:i:s");
			}
		}

		$pdata['filter']['keyword'] = '';
		if (isset($_REQUEST['keyword'])) {
			$pdata['filter']['keyword'] = trim($_REQUEST['keyword']);

			if (!isset($_REQUEST['sort']) || $_REQUEST['sort'] == '') {
				$pdata['sort'] = 'alow';
		    }
		}
		
		if (trim($pdata['filter']['keyword']) && trim($pdata['filter']['keyword'])) {
			$trans_advert_criteria->andWhere(" ( REPLACE(REPLACE(make_name, '\n', ''), ' ', '') ilike :keyword OR REPLACE(make_name, '\n', ' ') ilike :keyword OR make_name ilike :keyword ) OR ( REPLACE(REPLACE(model_name, '\n', ''), ' ', '') ilike :keyword OR REPLACE(model_name, '\n', ' ') ilike :keyword OR model_name ilike :keyword )");
			$trans_advert_params_array[':keyword'] = "%" . trim($pdata['filter']['keyword']) . "%";
		}
		
		if (count($trans_advert_params_array)) $trans_advert_criteria->params = $trans_advert_params_array;
		
		$pdata['featured'] = $trans_order->featured('1167', 12, '', '', '', '');

		$pdata['seller_type'] = '';
		if (isset($_REQUEST['seller_type']) && in_array($_REQUEST['seller_type'], ['dealer', 'private'])) {
			$pdata['seller_type'] = $_REQUEST['seller_type'];

			switch ($pdata['seller_type']) {
				case 'dealer':
					$trans_advert_criteria->innerJoin("master_dealer md", "md.dealer_id = t.dealer_id");
					$trans_advert_criteria->andWhere(['in', 'md.dealer_type', ['1']]);
					break;
				case 'private':
					$trans_advert_criteria->innerJoin("master_dealer md", "md.dealer_id = t.dealer_id");
					$trans_advert_criteria->andWhere(['in', 'md.dealer_type', ['2']]);
					break;
			}
		}

		$pdata['page_label'] = 'motorcycle';
		$pdata['page_location'] = 'Malaysia';
		
		$pdata['action_route'] = 'bike';

		switch ($pdata['filter']['type']) {
			case 'bike':
					$trans_advert_criteria->andWhere(" is_used_flg = '1' ");
					$pdata['page_label'] = 'All Motorcycle';
					$meta_keyword_array[] = 'All Motorcycle';

				break;			
			case 'used':
					$trans_advert_criteria->andWhere(" is_used_flg = '1' ");
					$pdata['page_label'] = 'Used Motorcycle';
					$meta_keyword_array[] = 'Used Motorcycle';
					
				break;
			case 'new':
					$trans_advert_criteria->andWhere(" is_used_flg = '2' ");
					$pdata['page_label'] = 'New Motorcycle';
					$meta_keyword_array[] = 'New Motorcycle';

				break;
			case 'recond':
					$trans_advert_criteria->andWhere(" is_used_flg = '3' ");
					$pdata['page_label'] = 'Recond Motorcycle';
					$meta_keyword_array[] = 'Recond Motorcycle';
					
				break;
		}

		$pdata['total'] = $trans_advert_criteria->count();

		switch ($pdata['sort']) {
			case 'pricehigh':
			case 'htl':
	        	$trans_advert_criteria->orderBy("phprice DESC, upd_data_date DESC");
	        break;
	        case 'pricelow':
	        case 'lth':
	        	$trans_advert_criteria->orderBy("plprice, upd_data_date DESC");
	        break;
	        case 'yearhigh':
	        	$trans_advert_criteria->orderBy("year_make DESC, upd_data_date DESC");
	        break;
	        case 'yearlow':
	        	$trans_advert_criteria->orderBy("year_make, upd_data_date DESC");
	        break;
	        case 'updatelow':
	        	$trans_advert_criteria->orderBy("upd_data_date ");
	        break;
	        default:
	        	$trans_advert_criteria->orderBy("upd_data_date DESC ");
        	break;
        }
		
		$trans_advert_criteria->limit($pdata['limit']);
		$trans_advert_criteria->offset($pdata['offset']);

		$trans_advert_model = $trans_advert_criteria->all();
		
		$recent_view_array = Yii::$app->general->advfromrecent('bike');
		$pdata['recentview'] = [];
		if (count($recent_view_array)) {
			foreach ($recent_view_array as $recent_urn_loop) {
				$recent_view_info = $trans_order->loadinfo($recent_urn_loop);
				if ($recent_view_info !== false) {
					$pdata['recentview'][] = $recent_view_info;
				}
			}
		}

		$pdata['bookmark_array'] = Yii::$app->general->advfrombookmark();
		
		$pdata['compare_array'] = Yii::$app->general->advfromcompare();

		/*
			Define URL
			Remember to put default at the last
		*/

		$url_parse = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
		parse_str($url_parse, $url_array);
		unset($url_array['page']);
		unset($url_array['sort']);

		// $url_array['autoscroll'] = 'result';
		
		$pdata['url_superdeals_on'] = Yii::$app->general->generatecarsseo('bike', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', 1, $url_array);
		$pdata['url_superdeals_off'] = Yii::$app->general->generatecarsseo('bike', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', 0, $url_array);

		$url_array['type'] = 'all';
		$pdata['url_type_all'] = Yii::$app->general->generatecarsseo('bike', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', $pdata['filter']['superdeals'], $url_array);

		$url_array['type'] = 'used';
		$pdata['url_type_used'] = Yii::$app->general->generatecarsseo('bike', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', $pdata['filter']['superdeals'], $url_array);

		$url_array['type'] = 'new';
		$pdata['url_type_new'] = Yii::$app->general->generatecarsseo('bike', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', $pdata['filter']['superdeals'], $url_array);

		$url_array['type'] = 'recond';
		$pdata['url_type_recond'] = Yii::$app->general->generatecarsseo('bike', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', $pdata['filter']['superdeals'], $url_array);
		
		$url_array['type'] = $pdata['filter']['type'];
		
		$url_array['sort'] = 'pricehigh';
		$pdata['url_sort_price_high'] = Yii::$app->general->generatecarsseo('bike', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', $pdata['filter']['superdeals'], $url_array);

		$url_array['sort'] = 'pricelow';
		$pdata['url_sort_price_low'] = Yii::$app->general->generatecarsseo('bike', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', $pdata['filter']['superdeals'], $url_array);

		$url_array['sort'] = 'yearhigh';
		$pdata['url_sort_year_high'] = Yii::$app->general->generatecarsseo('bike', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', $pdata['filter']['superdeals'], $url_array);

		$url_array['sort'] = 'yearlow';
		$pdata['url_sort_year_low'] = Yii::$app->general->generatecarsseo('bike', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', $pdata['filter']['superdeals'], $url_array);

		$url_array['sort'] = 'updatehigh';
		$pdata['url_sort_update_low'] = Yii::$app->general->generatecarsseo('bike', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', $pdata['filter']['superdeals'], $url_array);

		$url_array['sort'] = 'updatelow';
		$pdata['url_sort_update_high'] = Yii::$app->general->generatecarsseo('bike', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', $pdata['filter']['superdeals'], $url_array);

		$url_array['sort'] = $pdata['sort'];

		$pdata['url_pagination'] = Yii::$app->general->generatecarsseo('bike', 0, 0, $pdata['filter']['locationname'], '__PAGE__', '', '', '', $pdata['filter']['superdeals'], $url_array);

    	return $this->render('index', ['master_dealer' => $master_dealer, 'master_general' => $master_general, 'super_deals' => $super_deals, 'trans_order_extra_info' => $trans_order_extra_info, 'trans_advert_model' => $trans_advert_model,  'pdata' => $pdata]);
    }

    public function actionView()
    {
    	$master_dealer = new MasterDealer();
    	$master_dealer_review = new MasterDealerReview();
    	$master_dealer_review_info = new MasterDealerReviewInfo();
    	$master_general = new MasterGeneral();
    	$super_deals = new SuperDeals();
    	$trans_order = new TransOrder();
    	$trans_order_extra_info = new TransOrderExtraInfo();
    	$trans_order_picture = new TransOrderPicture();

    	$pdata = [];

    	if (isset($_REQUEST['urn_no'])) {

    		if($trans_order_picture->findOne($_REQUEST['urn_no'])) {
    			$trans_advert_model =  $trans_order->find()->from("trans_order")->where(['trans_order.urn_no' => $_REQUEST['urn_no']])->innerJoinWith("transOrderPictures")->one();
    		} else {
    			$trans_advert_model = $trans_order->findOne($_REQUEST['urn_no']);
    		}

    		if ($trans_advert_model) {

    			$pdata['master_dealer_model'] =  $master_dealer->findOne($trans_advert_model->dealer_id);

    			if($pdata['master_dealer_model']) {	
					$pdata['dealer_seourl'] = Yii::$app->general->generateseo('dealer', $trans_advert_model->dealer_id, $pdata['master_dealer_model']->dealer_id, $pdata['master_dealer_model']->dealer_name);
				} else {
					$pdata['master_dealer_model'] = false;
					$pdata['dealer_seourl'] = Yii::$app->general->generateseo('dealer', $trans_advert_model->dealer_id, 'private');
				}

				$pdata['bike_video'] = Yii::$app->api->getvideo($trans_advert_model->urn_no);

				$pdata['upd_data_date'] = strtotime($trans_advert_model->upd_data_date);

				$pdata['voc_file'] = '';
				$pdata['voc_approved_status'] = false;
				$voc_file = $trans_order_extra_info->getvalue($trans_advert_model->urn_no, 'vehicle_cert_approved');
				if ($voc_file !== false) {
					$pdata['voc_file'] = $voc_file;
				}

				$pdata['picture_array'] = [];

				$image_path = rtrim(Yii::$app->fpath->mountimages['path'], "/") . '/advert/wmfull/';

				foreach ($trans_advert_model->transOrderPictures as $trans_order_picture) {
					if (trim($trans_order_picture->picture_no)) {
						$pdata['picture_array'][$trans_order_picture->picture_no] = ['url' => Yii::$app->general->advertimageurl($trans_order_picture->picture_file, 'wmfull', $pdata['upd_data_date'],$trans_order_picture->is_new), 'thumb' => Yii::$app->general->advertimageurl($trans_order_picture->picture_file, 'webad', $pdata['upd_data_date'],$trans_order_picture->is_new), 'filename' => $trans_order_picture->picture_file];
					}

					if (!count($pdata['picture_array'])) {
					$pdata['picture_array'][1] = [
						'url' => Yii::$app->general->advertimageurl($ttrans_order_picture->picture_file, 'wmfull', $pdata['upd_data_date'], $trans_order_picture->is_new), 
						'thumb' => Yii::$app->general->advertimageurl($trans_order_picture->picture_file, 'webad', $pdata['upd_data_date'], $trans_order_picture->is_new), 
						'filename' => ''];
					}
				}

				$pdata['voc_picture_array'] = [];
				if (trim($pdata['voc_file'])) {
					$pdata['voc_picture_array'] = ['url' => Yii::$app->general->advertimagecerturl($pdata['voc_file'], $trans_advert_model->urn_no, 'carcert', $pdata['upd_data_date']), 'thumb' => Yii::$app->general->advertimagecerturl($pdata['voc_file'], $trans_advert_model->urn_no, 'carcert', $pdata['upd_data_date']), 'filename' => $pdata['voc_file']];
				}

				switch($trans_advert_model->is_used_flg){
					case '1':
						$pdata['condition'] = 'Used Motorcycle';
						break;
					case '2':
						$pdata['condition'] = 'New Motorcycle';
						break;
					case '3':
						$pdata['condition'] = 'Recon Motorcycle';
						break;
					default:
						$pdata['condition'] = 'Used Motorcycle';
						break;
				}

				$pdata['similar'] = $trans_order->similar('1167', 6, $trans_advert_model->make_cd, $trans_advert_model->model_cd, '', '');

				$pdata['bookmark_array'] = Yii::$app->general->advfrombookmark();

				$pdata['mileage_array'] = $master_general->getMileage();

    			return $this->render('view', ['master_dealer_review' => $master_dealer_review, 'master_dealer_review_info' => $master_dealer_review_info, 'super_deals' => $super_deals, 'trans_advert_model' => $trans_advert_model, 'pdata' => $pdata]);
    		} else {
    			$this->redirect(Yii::$app->urlManager->createAbsoluteUrl('site/error', ['urn_no' => $_REQUEST['urn_no']]));
    		}

    		
    	} else {
			$this->redirect(Yii::$app->urlManager->createAbsoluteUrl('bike'));
		}
    }
}