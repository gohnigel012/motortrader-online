<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'My Favourites';
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-home">
	<h1 class="text-center banner-search__title banner-search__title--adjust">My Favourites</h1>

	 <div class="tab banner-search__icon banner-search__icon--adjust text-center">
        <button class="tablinks banner-search__icon-box banner-search__icon-box--adjust active" id="icon-box-car">
            <span>Cars</span>
        </button>
        <button class="tablinks banner-search__icon-box banner-search__icon-box--adjust" id="icon-box-motorcycle">
            <span>Motorbikes</span>
        </button>
        <button class="tablinks banner-search__icon-box banner-search__icon-box--adjust" id="icon-box-commercial-car">
            <span>Commercial cars</span>
        </button>
        <button class="tablinks banner-search__icon-box banner-search__icon-box--adjust" id="icon-box-auto-parts">
            <span>Auto parts</span>
        </button>
        <button class="tablinks banner-search__icon-box banner-search__icon-box--adjust" id="icon-box-number-plates">
            <span>Number plates</span>
        </button>
        <button class="tablinks banner-search__icon-box banner-search__icon-box--adjust" id="icon-box-phone-numbers">
            <span>Phone numbers</span>
        </button>
    </div>

    <div class="bookmark__list">
    	<div class="row">
<?php
	if (count($trans_advert_model)) {
		foreach ($trans_advert_model as $trans_advert_data_loop) {
			if($trans_advert_data_loop->classification_cd != '1165' && $trans_advert_data_loop->classification_cd != '2493') {
				$advert_image = Yii::$app->general->advertimageurl($trans_advert_data_loop->urn_no . '.jpg', 'wmfull', strtotime($trans_advert_data_loop->upd_data_date), $trans_advert_data_loop->transOrderPictures[0]->is_new);
			}

			$year_make = ((int)date("Y") - (int)$trans_advert_data_loop->year_make);

			$voc = '';
			$extra_info_array = [];

			$extra_info_array = $trans_order_extra_info->getadvertextrainfo($trans_advert_data_loop->urn_no);

			if (isset($extra_info_array['vehicle_cert']) && $extra_info_array['vehicle_cert']!= '') {
				$voc = $extra_info_array['vehicle_cert'];
			}

			$trans_advert_super_deals_model = $super_deals->getsuperdeals($trans_advert_data_loop->urn_no);
			
			$has_super_deals = $trans_advert_super_deals_model['got_super_deals'];

			if ($trans_advert_data_loop->classification_cd == '1141' && $trans_advert_data_loop->bodytype_code != 'bt9') {
				$seo_url = Yii::$app->general->generateseo('car', $trans_advert_data_loop->urn_no, $trans_advert_data_loop->make_name . '-' . $trans_advert_data_loop->model_name);
?>    		
    		<div class="col-lg-3" data-urn="<?php echo $trans_advert_data_loop->urn_no; ?>">
    			<div class="superdeals-featured-cnabadv__featured-ads-section-listing superdeals-featured-cnabadv__featured-ads-section-listing--adjust superdeals-featured-cnabadv__featured-ads-section-listing--taller <?php echo $has_super_deals === '1' ? 'superdeals-featured-cnabadv__featured-ads-section-listing--border' : ''; ?> superdeals-featured-cnabadv__featured-ads-section-listing-car">
    				<p class="car-list__position">
                    	<a href="<?php echo $seo_url; ?>">
                    		<img class="featured-ads-section__img lazyload" src="<?php echo Yii::$app->fpath->images("ajax-loader.gif"); ?>" data-src="<?php echo (@getimagesize($advert_image)) ? $advert_image : '/images/mercedes.png'; ?>" alt="<?php echo $trans_advert_data_loop->make_name . ' ' . $trans_advert_data_loop->model_name; ?>">
                    	</a>
<?php 		
				if($has_super_deals === '1') { 
?>
						<button class="btn superdeals-featured-cnabadv__insights-btn superdeals-featured-cnabadv__insights-btn--position">Super Deals</button>
<?php 
				} 
?>
						<span class="car-list__bookmark <?php echo (in_array($trans_advert_data_loop->urn_no, $pdata['bookmark_array']) ? 'car-list__show-bookmark ' : '') ?>" data-urn="<?php echo $trans_advert_data_loop->urn_no; ?>" onclick="removeBookmark('<?php echo $trans_advert_data_loop->urn_no ?>');"><img class="car-list__none" src="/images/bookmark.png"><img class="car-list__all" src="/images/bookmarked.png"></span>
					</p>
					<p class="text-center featured-ads-section__title"><a href="<?php echo $seo_url; ?>"><?php echo $trans_advert_data_loop->make_name . ' ' . $trans_advert_data_loop->model_name; ?></a></p>
<?php
				if (isset($trans_advert_super_deals_model) && date("Y-m-d H:i:s") >= $trans_advert_super_deals_model['super_deals_schedule_start_date'] && date("Y-m-d H:i:s") <= $trans_advert_super_deals_model['super_deals_schedule_end_date']) {
?>                    
        			<div class="row">

 <?php
					if ($trans_advert_super_deals_model['sale_price'] == 0) {
						$car_price =  'POA';
					} else {
						if (trim($voc)) {
							$car_price =  number_format($trans_advert_super_deals_model['sale_price']); 
						} else {
							$car_price =  number_format($trans_advert_super_deals_model['sale_price']); 
						}
						
						$deposit = 10;
						$interest = 3.69;
					if ($trans_advert_data_loop->is_used_flg == '2') {		
						$term = 9;
					}else{
						$term = 7;
					}
						$interestRate = $interest / 100;    
						$oriPrice = str_replace(",","", $trans_advert_super_deals_model['sale_price']);
						$depositAmount = $oriPrice * ($deposit / 100);
						$loanPaidAmount = $oriPrice - $depositAmount; 
						$interestPaidAmount = ($term * $interestRate) * $loanPaidAmount;                            
						$month = ($interestPaidAmount + $loanPaidAmount) / ($term * 12);
						$month = number_format(round($month));
					}
					
					if ($trans_advert_super_deals_model['sale_price'] != $trans_advert_super_deals_model['regular_price']) {
						$car_price = number_format($trans_advert_super_deals_model['regular_price']);
					} 			
?>					
					</div>						
<?php
				} else {

?>
            		<div class="row">

<?php
					if ($trans_advert_data_loop->price == 0) {
						$car_price =  'POA';
					} else {
						if (trim($voc)) {
							$car_price = '<small class="featured-ads-section__price-currency">RM</small> ' . number_format($trans_advert_data_loop->price); 
						} else {
							$car_price = '<small class="featured-ads-section__price-currency">RM</small> ' . number_format($trans_advert_data_loop->price); 
						}
						
						$deposit = 10;
						$interest = 3.69;
					if ($trans_advert_data_loop->is_used_flg == '2') {		
						$term = 9;
					} else {
						$term = 7;
					}
						$interestRate = $interest / 100;    
						$oriPrice = str_replace(",","", $trans_advert_data_loop->price);
						$depositAmount = $oriPrice * ($deposit / 100);
						$loanPaidAmount = $oriPrice - $depositAmount; 
						$interestPaidAmount = ($term * $interestRate) * $loanPaidAmount;                            
						$month = ($interestPaidAmount + $loanPaidAmount) / ($term * 12);
						$month = number_format(round($month));
					}
				}

				if ($year_make < 13 || $year_make == date("Y")) {				
?>                    	
                		<div class="col-4">
<?php
					if (isset($trans_advert_super_deals_model) && date("Y-m-d H:i:s") >= $trans_advert_super_deals_model['super_deals_schedule_start_date'] && date("Y-m-d H:i:s") <= $trans_advert_super_deals_model['super_deals_schedule_end_date']) {
						if ($trans_advert_super_deals_model['sale_price'] == 0) {
?>				
							<p class="featured-ads-section__month">-</p>
<?php				
						} else {
?>
                    		<p class="featured-ads-section__month">RM <?php echo $month; ?> / month</p>
<?php
						} 
					} else {											
						if ($trans_advert_data_loop->price == 0) {
?>				
							<p class="featured-ads-section__month">-</p>
<?php	
						} else {
?>				
					 		<p class="featured-ads-section__month">RM <?php echo $month; ?> / month</p>
<?php
						}										
					}
?>
						</div>
<?php							
				} else {
?>
						<div class="col-4">
							<p class="featured-ads-section__month">-</p>
						</div>
<?php		
				}
?>	                           
                        <div class="col-8 text-right">
                            <p class="featured-ads-section__price"><?php echo $car_price; ?></p>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-6">
                            <div class="row">
                                <div class="col-3">
                                    <p><img class="featured-ads-section__icon" src="images/calendar.png" alt="calendar" /></p>
                                </div>
                                <div class="col-9">
                                    <span class="featured-ads-section__text">Year</span>
                                    <p><?php echo $trans_advert_data_loop->year_make; ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="row">
                                <div class="col-3">
                                    <p><img class="featured-ads-section__icon" src="images/location.png" alt="location" /></p>
                                </div>
                                <div class="col-9">
                                    <span class="featured-ads-section__text">Location</span>
                                    <p><?php echo $trans_advert_data_loop->location_name; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="row">
                                <div class="col-3">
                                    <p><img class="featured-ads-section__icon" src="images/mileage.png" alt="mileage" /></p>
                                </div>
                                <div class="col-9">
                                    <span class="featured-ads-section__text">Mileage</span>
                                    <p><?php echo $trans_advert_data_loop->mileage_name; ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="row">
                                <div class="col-3">
                                    <p><img class="featured-ads-section__icon" src="images/transmission.png" alt="transmission" /></p>
                                </div>
                                <div class="col-9">
                                    <span class="featured-ads-section__text">Transmission</span>
                                    <p><?php echo $trans_advert_data_loop->transmission_name; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                   <p class="text-center"><button class="btn car-view__search-btn car-view__search-btn--margin car-view__search-btn--red"><span class="car-view__search-btn-text">Compare</span></button></p>
	    		</div>
	    	</div>
<?php
			} else if ($trans_advert_data_loop->classification_cd == '1167') {
				$seo_url = Yii::$app->general->generateseo('bike', $trans_advert_data_loop->urn_no, $trans_advert_data_loop->make_name . '-' . $trans_advert_data_loop->model_name);
?>
			<div class="col-lg-3" data-urn="<?php echo $trans_advert_data_loop->urn_no; ?>">
				<div class="superdeals-featured-cnabadv__featured-ads-section-listing superdeals-featured-cnabadv__featured-ads-section-listing--adjust superdeals-featured-cnabadv__featured-ads-section-listing--taller <?php echo $has_super_deals === '1' ? 'superdeals-featured-cnabadv__featured-ads-section-listing--border' : ''; ?> superdeals-featured-cnabadv__featured-ads-section-listing-bike d-none">
					<p class="car-list__position">
	                	<a href="<?php echo $seo_url; ?>">
	                		<img class="featured-ads-section__img lazyload" src="<?php echo Yii::$app->fpath->images("ajax-loader.gif"); ?>" data-src="<?php echo (@getimagesize($advert_image)) ? $advert_image : '/images/motorcycle.png'; ?>" alt="<?php echo $trans_advert_data_loop->make_name . ' ' . $trans_advert_data_loop->model_name; ?>">
	                	</a>
<?php 		
				if($has_super_deals === '1') { 
?>
							<button class="btn superdeals-featured-cnabadv__insights-btn superdeals-featured-cnabadv__insights-btn--position">Super Deals</button>
<?php 
				} 
?>
							<span class="car-list__bookmark <?php echo (in_array($trans_advert_data_loop->urn_no, $pdata['bookmark_array']) ? 'car-list__show-bookmark ' : '') ?>" data-urn="<?php echo $trans_advert_data_loop->urn_no; ?>" onclick="removeBookmark('<?php echo $trans_advert_data_loop->urn_no ?>');"><img class="car-list__none" src="/images/bookmark.png"><img class="car-list__all" src="/images/bookmarked.png"></span>
						</p>
						<p class="text-center featured-ads-section__title"><a href="<?php echo $seo_url; ?>"><?php echo $trans_advert_data_loop->make_name . ' ' . $trans_advert_data_loop->model_name; ?></a></p>
<?php
				if (isset($trans_advert_super_deals_model) && date("Y-m-d H:i:s") >= $trans_advert_super_deals_model['super_deals_schedule_start_date'] && date("Y-m-d H:i:s") <= $trans_advert_super_deals_model['super_deals_schedule_end_date']) {
?>                    
        				<div class="row">

 <?php
					if ($trans_advert_super_deals_model['sale_price'] == 0) {
						$car_price =  'POA';
					} else {
						if (trim($voc)) {
							$car_price =  number_format($trans_advert_super_deals_model['sale_price']); 
						} else {
							$car_price =  number_format($trans_advert_super_deals_model['sale_price']); 
						}
						
						$deposit = 10;
						$interest = 3.69;
					if ($trans_advert_data_loop->is_used_flg == '2') {		
						$term = 9;
					}else{
						$term = 7;
					}
						$interestRate = $interest / 100;    
						$oriPrice = str_replace(",","", $trans_advert_super_deals_model['sale_price']);
						$depositAmount = $oriPrice * ($deposit / 100);
						$loanPaidAmount = $oriPrice - $depositAmount; 
						$interestPaidAmount = ($term * $interestRate) * $loanPaidAmount;                            
						$month = ($interestPaidAmount + $loanPaidAmount) / ($term * 12);
						$month = number_format(round($month));
					}
					
					if ($trans_advert_super_deals_model['sale_price'] != $trans_advert_super_deals_model['regular_price']) {
						$car_price = number_format($trans_advert_super_deals_model['regular_price']);
					} 			
?>
					</div>						
<?php
				} else {

?>
        			<div class="row">

<?php
					if ($trans_advert_data_loop->price == 0) {
						$car_price =  'POA';
					} else {
						if (trim($voc)) {
							$car_price = '<small class="featured-ads-section__price-currency">RM</small> ' . number_format($trans_advert_data_loop->price); 
						} else {
							$car_price = '<small class="featured-ads-section__price-currency">RM</small> ' . number_format($trans_advert_data_loop->price); 
						}
						
						$deposit = 10;
						$interest = 3.69;
					if ($trans_advert_data_loop->is_used_flg == '2') {		
						$term = 9;
					}else{
						$term = 7;
					}
						$interestRate = $interest / 100;    
						$oriPrice = str_replace(",","", $trans_advert_data_loop->price);
						$depositAmount = $oriPrice * ($deposit / 100);
						$loanPaidAmount = $oriPrice - $depositAmount; 
						$interestPaidAmount = ($term * $interestRate) * $loanPaidAmount;                            
						$month = ($interestPaidAmount + $loanPaidAmount) / ($term * 12);
						$month = number_format(round($month));
					}
				}

		if ($year_make < 13 || $year_make == date("Y")) {				
?>                    	
            			<div class="col-4">
<?php
			if (isset($trans_advert_super_deals_model) && date("Y-m-d H:i:s") >= $trans_advert_super_deals_model['super_deals_schedule_start_date'] && date("Y-m-d H:i:s") <= $trans_advert_super_deals_model['super_deals_schedule_end_date']) {
				if ($trans_advert_super_deals_model['sale_price'] == 0) {
?>				
							<p class="featured-ads-section__month">-</p>
<?php				
				} else {
?>
                			<p class="featured-ads-section__month">RM <?php echo $month; ?> / month</p>
<?php
				} 
			} else {											
				if ($trans_advert_data_loop->price == 0) {
?>				
							<p class="featured-ads-section__month">-</p>
<?php	
				} else {
?>				
				 			<p class="featured-ads-section__month">RM <?php echo $month; ?> / month</p>
<?php
				}										
			}
?>
						</div>
<?php							
		} else {
?>
						<div class="col-4">
							<p class="featured-ads-section__month">-</p>
						</div>
<?php		
		}
?>	                           
                        <div class="col-8 text-right">
                            <p class="featured-ads-section__price"><?php echo $car_price; ?></p>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-6">
                            <div class="row">
                                <div class="col-3">
                                    <p><img class="featured-ads-section__icon" src="images/calendar.png" alt="calendar" /></p>
                                </div>
                                <div class="col-9">
                                    <span class="featured-ads-section__text">Year</span>
                                    <p><?php echo $trans_advert_data_loop->year_make; ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="row">
                                <div class="col-3">
                                    <p><img class="featured-ads-section__icon" src="images/location.png" alt="location" /></p>
                                </div>
                                <div class="col-9">
                                    <span class="featured-ads-section__text">Location</span>
                                    <p><?php echo $trans_advert_data_loop->location_name; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="row">
                                <div class="col-3">
                                    <p><img class="featured-ads-section__icon" src="images/mileage.png" alt="mileage" /></p>
                                </div>
                                <div class="col-9">
                                    <span class="featured-ads-section__text">Mileage</span>
                                    <p><?php echo $trans_advert_data_loop->mileage_name; ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="row">
                                <div class="col-3">
                                    <p><img class="featured-ads-section__icon" src="images/transmission.png" alt="transmission" /></p>
                                </div>
                                <div class="col-9">
                                    <span class="featured-ads-section__text">Transmission</span>
                                    <p><?php echo $trans_advert_data_loop->transmission_name; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                     <p class="text-center"><button class="btn car-view__search-btn car-view__search-btn--red"><span class="car-view__search-btn-text">Compare</span></button></p>
	    		</div>
	    	</div>
<?php
			} else if ($trans_advert_data_loop->classification_cd == '1141' && $trans_advert_data_loop->bodytype_code == 'bt9') {
				$seo_url = Yii::$app->general->generateseo('car', $trans_advert_data_loop->urn_no, $trans_advert_data_loop->make_name . '-' . $trans_advert_data_loop->model_name);
?>    		
			<div class="col-lg-3" data-urn="<?php echo $trans_advert_data_loop->urn_no; ?>">
				<div class="superdeals-featured-cnabadv__featured-ads-section-listing superdeals-featured-cnabadv__featured-ads-section-listing--adjust superdeals-featured-cnabadv__featured-ads-section-listing--taller <?php echo $has_super_deals === '1' ? 'superdeals-featured-cnabadv__featured-ads-section-listing--border' : ''; ?> d-none superdeals-featured-cnabadv__featured-ads-section-listing-commercial-car">
					<p class="car-list__position">
	                	<a href="<?php echo $seo_url; ?>">
	                		<img class="featured-ads-section__img lazyload" src="<?php echo Yii::$app->fpath->images("ajax-loader.gif"); ?>" data-src="<?php echo (@getimagesize($advert_image)) ? $advert_image : '/images/bus.png'; ?>" alt="<?php echo $trans_advert_data_loop->make_name . ' ' . $trans_advert_data_loop->model_name; ?>">
	                	</a>
<?php 		
				if($has_super_deals === '1') { 
?>
						<button class="btn superdeals-featured-cnabadv__insights-btn superdeals-featured-cnabadv__insights-btn--position">Super Deals</button>
<?php 
				} 
?>
						<span class="car-list__bookmark <?php echo (in_array($trans_advert_data_loop->urn_no, $pdata['bookmark_array']) ? 'car-list__show-bookmark ' : '') ?>" data-urn="<?php echo $trans_advert_data_loop->urn_no; ?>" onclick="removeBookmark('<?php echo $trans_advert_data_loop->urn_no ?>');"><img class="car-list__none" src="/images/bookmark.png"><img class="car-list__all" src="/images/bookmarked.png"></span>
					</p>
					<p class="text-center featured-ads-section__title"><a href="<?php echo $seo_url; ?>"><?php echo $trans_advert_data_loop->make_name . ' ' . $trans_advert_data_loop->model_name; ?></a></p>
<?php
				if (isset($trans_advert_super_deals_model) && date("Y-m-d H:i:s") >= $trans_advert_super_deals_model['super_deals_schedule_start_date'] && date("Y-m-d H:i:s") <= $trans_advert_super_deals_model['super_deals_schedule_end_date']) {
?>                    
    				<div class="row">

 <?php
					if ($trans_advert_super_deals_model['sale_price'] == 0) {
						$car_price =  'POA';
					} else {
						if (trim($voc)) {
							$car_price =  number_format($trans_advert_super_deals_model['sale_price']); 
						} else {
							$car_price =  number_format($trans_advert_super_deals_model['sale_price']); 
						}
						
						$deposit = 10;
						$interest = 3.69;
					if ($trans_advert_data_loop->is_used_flg == '2') {		
						$term = 9;
					}else{
						$term = 7;
					}
						$interestRate = $interest / 100;    
						$oriPrice = str_replace(",","", $trans_advert_super_deals_model['sale_price']);
						$depositAmount = $oriPrice * ($deposit / 100);
						$loanPaidAmount = $oriPrice - $depositAmount; 
						$interestPaidAmount = ($term * $interestRate) * $loanPaidAmount;                            
						$month = ($interestPaidAmount + $loanPaidAmount) / ($term * 12);
						$month = number_format(round($month));
					}
					
					if ($trans_advert_super_deals_model['sale_price'] != $trans_advert_super_deals_model['regular_price']) {
						$car_price = number_format($trans_advert_super_deals_model['regular_price']);
					} 			
?>
					</div>							
<?php
				} else {

?>
    				<div class="row">

<?php
					if ($trans_advert_data_loop->price == 0) {
						$car_price =  'POA';
					} else {
						if (trim($voc)) {
							$car_price = '<small class="featured-ads-section__price-currency">RM</small> ' . number_format($trans_advert_data_loop->price); 
						} else {
							$car_price = '<small class="featured-ads-section__price-currency">RM</small> ' . number_format($trans_advert_data_loop->price); 
						}
						
						$deposit = 10;
						$interest = 3.69;
					if ($trans_advert_data_loop->is_used_flg == '2') {		
						$term = 9;
					}else{
						$term = 7;
					}
						$interestRate = $interest / 100;    
						$oriPrice = str_replace(",","", $trans_advert_data_loop->price);
						$depositAmount = $oriPrice * ($deposit / 100);
						$loanPaidAmount = $oriPrice - $depositAmount; 
						$interestPaidAmount = ($term * $interestRate) * $loanPaidAmount;                            
						$month = ($interestPaidAmount + $loanPaidAmount) / ($term * 12);
						$month = number_format(round($month));
					}
				}

		if ($year_make < 13 || $year_make == date("Y")) {				
?>                    	
            				<div class="col-4">
<?php
			if (isset($trans_advert_super_deals_model) && date("Y-m-d H:i:s") >= $trans_advert_super_deals_model['super_deals_schedule_start_date'] && date("Y-m-d H:i:s") <= $trans_advert_super_deals_model['super_deals_schedule_end_date']) {
				if ($trans_advert_super_deals_model['sale_price'] == 0) {
?>				
								<p class="featured-ads-section__month">-</p>
<?php				
				} else {
?>
                				<p class="featured-ads-section__month">RM <?php echo $month; ?> / month</p>
<?php
				} 
			} else {											
				if ($trans_advert_data_loop->price == 0) {
?>				
								<p class="featured-ads-section__month">-</p>
<?php	
				} else {
?>				
				 				<p class="featured-ads-section__month">RM <?php echo $month; ?> / month</p>
<?php
				}										
			}
?>
							</div>
<?php							
		} else {
?>
							<div class="col-4">
								<p class="featured-ads-section__month">-</p>
							</div>
<?php		
		}
?>	                           
	                        <div class="col-8 text-right">
	                            <p class="featured-ads-section__price"><?php echo $car_price; ?></p>
	                        </div>
	                    </div>
	                    <hr/>
	                    <div class="row">
	                        <div class="col-6">
	                            <div class="row">
	                                <div class="col-3">
	                                    <p><img class="featured-ads-section__icon" src="images/calendar.png" alt="calendar" /></p>
	                                </div>
	                                <div class="col-9">
	                                    <span class="featured-ads-section__text">Year</span>
	                                    <p><?php echo $trans_advert_data_loop->year_make; ?></p>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="col-6">
	                            <div class="row">
	                                <div class="col-3">
	                                    <p><img class="featured-ads-section__icon" src="images/location.png" alt="location" /></p>
	                                </div>
	                                <div class="col-9">
	                                    <span class="featured-ads-section__text">Location</span>
	                                    <p><?php echo $trans_advert_data_loop->location_name; ?></p>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class="col-6">
	                            <div class="row">
	                                <div class="col-3">
	                                    <p><img class="featured-ads-section__icon" src="images/mileage.png" alt="mileage" /></p>
	                                </div>
	                                <div class="col-9">
	                                    <span class="featured-ads-section__text">Mileage</span>
	                                    <p><?php echo $trans_advert_data_loop->mileage_name; ?></p>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="col-6">
	                            <div class="row">
	                                <div class="col-3">
	                                    <p><img class="featured-ads-section__icon" src="images/transmission.png" alt="transmission" /></p>
	                                </div>
	                                <div class="col-9">
	                                    <span class="featured-ads-section__text">Transmission</span>
	                                    <p><?php echo $trans_advert_data_loop->transmission_name; ?></p>
	                                </div>
	                            </div>
	                        </div>
	                    </div>	 	
	                    <p class="text-center"><button class="btn car-view__search-btn car-view__search-btn--red car-view__search-btn--margin"><span class="car-view__search-btn-text">Compare</span></button></p>
	    			</div>
	    		</div>
	    	</div>
<?php
			} else if ($trans_advert_data_loop->classification_cd == '1166') {
				$seo_url = Yii::$app->general->generateseo('autopart', $trans_advert_data_loop->urn_no, $trans_advert_data_loop->make_name);
?>
			<div class="col-lg-3" data-urn="<?php echo $trans_advert_data_loop->urn_no; ?>">
				<div class="superdeals-featured-cnabadv__featured-ads-section-listing superdeals-featured-cnabadv__featured-ads-section-listing--adjust superdeals-featured-cnabadv__featured-ads-section-listing--taller <?php echo $has_super_deals === '1' ? 'superdeals-featured-cnabadv__featured-ads-section-listing--border' : ''; ?> d-none superdeals-featured-cnabadv__featured-ads-section-listing-autopart">
					<p class="car-list__position">
						<a href="<?php echo $seo_url; ?>">
							<img class="featured-ads-section__img featured-ads-section__img--adjust lazyload" src="<?php echo Yii::$app->fpath->images("ajax-loader.gif"); ?>" data-src="<?php echo @getimagesize($advert_image) ? $advert_image : '/images/autopart.png'; ?>" alt="<?php echo $trans_advert_data_loop['make_name'] ; ?>">
						</a>

<?php 		
				if($has_super_deals === '1') { 
?>
									<button class="btn superdeals-featured-cnabadv__insights-btn superdeals-featured-cnabadv__insights-btn--position">Super Deals</button>
<?php 
				} 
?>
									<span class="car-list__bookmark <?php echo (in_array($trans_advert_data_loop['urn_no'], $pdata['bookmark_array']) ? 'car-list__show-bookmark' : '') ?>" data-urn="<?php echo $trans_advert_data_loop['urn_no'] ?>" onclick="removeBookmark('<?php echo $trans_advert_data_loop->urn_no ?>');"><img class="car-list__none" src="/images/bookmark.png"><img class="car-list__all" src="/images/bookmarked.png"></span>
								</p>								
								 <p class="text-center featured-ads-section__title text-right"><a href="<?php echo $seo_url; ?>"><?php echo $trans_advert_data_loop['make_name']; ?></a></p>
<?php
			if (isset($trans_advert_super_deals_model) && date("Y-m-d H:i:s") >= $trans_advert_super_deals_model['super_deals_schedule_start_date'] && date("Y-m-d H:i:s") <= $trans_advert_super_deals_model['super_deals_schedule_end_date']) {
?>
								<div>
<?php
				if ($trans_advert_data_loop['price'] == 0) {
					echo '<p class="featured-ads-section__price text-right">POA</p>';
				} else {
					if ($trans_advert_super_deals_model['sale_price'] == '0') {
						echo '<p class="featured-ads-section__price text-right">POA</p>';
					} else {					
?>
									<p class="featured-ads-section__price text-right"><small class="featured-ads-section__price-currency">RM</small> <?php echo number_format($trans_advert_super_deals_model['sale_price']); ?></p>
<?php
					if ($trans_advert_super_deals_model['sale_price'] != $trans_advert_super_deals_model['regular_price']) { 			
?>
									<p class="featured-ads-section__price text-right"><small class="featured-ads-section__price-currency">RM</small> <?php echo number_format($trans_advert_super_deals_model['regular_price']); ?></p>
<?php
						}
?>					
<?php
					}
				}
?>
								</div>
<?php
			} else {
?>
						<div>
<?php
				if ($trans_advert_data_loop['price'] == 0) {
					echo '<p class="featured-ads-section__price text-right">POA</p>';
				} else {
					echo '<p class="featured-ads-section__price text-right"><small class="featured-ads-section__price-currency">RM</small> ' . number_format($trans_advert_data_loop['price']) . '</p>'; 
				}
?>						
						</div>
				
<?php			
			}
?>
					<hr/>
                    <div class="row">
                        <div class="col-4">
                            <p><img class="featured-ads-section__icon" src="images/location.png" alt="location" /></p>
                        </div>
                        <div class="col-8">
                            <span class="featured-ads-section__text">Location</span>
                            <p><?php echo $trans_advert_data_loop['location_name']; ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <p><img class="featured-ads-section__icon" src="/images/autoparticon.png" alt="category" /></p>
                        </div>
                        <div class="col-8">
                            <span class="featured-ads-section__text">Category</span>
<?php
		$autoparts_category_array = $trans_statistic_count->listcount('autoparts_rank');
		foreach ($autoparts_category_array as $autoparts_category_array_name_loop => $autoparts_category_array_count_loop) {

			if ($autoparts_category_array_count_loop > 0) {
				if (preg_match('%' . $autoparts_category_array_name_loop . '%', $trans_advert_data_loop['model_name'])) {
?>
							<p><?php echo $autoparts_category_array_name_loop; ?></p>
<?php
				}
			}
		}
?>			                                
			                                
                        </div>
                    </div>
                    <p class="text-center"><button class="btn car-view__search-btn car-view__search-btn--red car-view__search-btn--margin"><span class="car-view__search-btn-text">Compare</span></button></p> 
				</div>
			</div>
<?php				
			} else if($trans_advert_data_loop->classification_cd === '1165') {
					$seo_url = Yii::$app->general->generateseo('numberplate', $trans_advert_data_loop->urn_no, $trans_advert_data_loop->special_no);
?>
			<div class="col-lg-3" data-urn="<?php echo $trans_advert_data_loop->urn_no; ?>">
				<div class="superdeals-featured-cnabadv__featured-ads-section-listing superdeals-featured-cnabadv__featured-ads-section-listing--adjust superdeals-featured-cnabadv__featured-ads-section-listing--taller <?php echo $has_super_deals === '1' ? 'superdeals-featured-cnabadv__featured-ads-section-listing--border' : ''; ?> d-none superdeals-featured-cnabadv__featured-ads-section-listing-numberplate">
					<a class="numberplate-search__link" href="<?php echo $seo_url; ?>">
						<div class="numberplate-search__image car-list__position">
							<h2 class="numberplate-search__image-text"><?php echo substr($trans_advert_data_loop->special_no, 0, 15); ?></h2>
<?php 		
				if($has_super_deals === '1') { 
?>
								<p>
									<button class="btn superdeals-featured-cnabadv__insights-btn superdeals-featured-cnabadv__insights-btn--position">Super Deals</button>
								</p>
<?php 
				} 
?>
								<p class="car-list__bookmark <?php echo (in_array($trans_advert_data_loop->urn_no, $pdata['bookmark_array']) ? 'car-list__show-bookmark' : '') ?>" data-urn="<?php echo $trans_advert_data_loop->urn_no ?>" onclick="removeBookmark('<?php echo $trans_advert_data_loop->urn_no ?>');"><img class="car-list__none" src="/images/bookmark.png"><img class="car-list__all" src="/images/bookmarked.png"></p>								
							</div>
						</a>
						<p class="text-center featured-ads-section__title">
							<a href="<?php echo $seo_url; ?>">
<?php
			if ( $trans_advert_data_loop->additional_desc != "" ) {
				if (strlen($trans_advert_data_loop->additional_desc) > 120) {
					echo substr($trans_advert_data_loop->additional_desc, 0, 120) . ' ...';
				} else {
					echo $trans_advert_data_loop->additional_desc;
				}
			} else if($trans_advert_data_loop->qe_add_desc != "" ) {
				if (strlen($trans_advert_data_loop->qe_add_desc) > 120) {
					echo substr($trans_advert_data_loop->qe_add_desc, 0, 120) . ' ...';
				} else {
					echo $trans_advert_data_loop->qe_add_desc;
				}

			} else if($trans_advert_data_loop->feature_desc != "" ) {
				if (strlen($trans_advert_data_loop->feature_desc) > 120) {
					echo substr($trans_advert_data_loop->feature_desc, 0, 120) . ' ...';
				} else {
					echo $trans_advert_data_loop->feature_desc;
				}
			}
?>
							</a>
						</p>					
<?php
			if (isset($trans_advert_super_deals_model) && date("Y-m-d H:i:s") >= $trans_advert_super_deals_model['super_deals_schedule_start_date'] && date("Y-m-d H:i:s") <= $trans_advert_super_deals_model['super_deals_schedule_end_date']) {
?>		
						<div>
<?php				
				if ($trans_advert_data_loop->price == 0) {
					$display_price = 'POA';
?>
						<p class="featured-ads-section__price text-right"><?php echo $display_price; ?></p>
<?php					
				} else {
					if ($trans_advert_super_deals_model['sale_price'] == '0') {
						echo 'POA';
					} else {					
						$display_price = '<small>RM </small> ' . number_format($trans_advert_super_deals_model['sale_price']);				
?>
						<p class="featured-ads-section__price text-right"><?php echo $display_price; ?></p>
<?php
						if ($trans_advert_super_deals_model['sale_price'] != $trans_advert_super_deals_model['regular_price']) { 			
?>													
						<p class="featured-ads-section__price text-right"><?php echo number_format($trans_advert_super_deals_model['regular_price']); ?></p>
<?php
						}
					}
				}
?>							
					</div>	
<?php 		
			} else {
				if ($trans_advert_data_loop->price == 0) {
					$display_price = 'POA';
				} else {
					$display_price = '<small class="featured-ads-section__price-currency">RM </small>' . number_format($trans_advert_data_loop->price);
				}
?>
					<div><p class="featured-ads-section__price text-right"><?php echo $display_price; ?></p></div>
<?php
			}
?>				
					<hr/>
                    <div class="row">
                        <div class="col-4">
                            <p><img class="featured-ads-section__icon" src="images/location.png" alt="location" /></p>
                        </div>
                        <div class="col-8">
                            <span class="featured-ads-section__text">Location</span>
                            <p><?php echo $trans_advert_data_loop->location_name; ?></p>
                        </div>
                    </div>
				</div>
			</div>
<?php
			} else if($trans_advert_data_loop->classification_cd === '2493') {
					$seo_url = Yii::$app->general->generateseo('special-phone-number', $trans_advert_data_loop->urn_no, $trans_advert_data_loop->special_no);
?>
			<div class="col-lg-3" data-urn="<?php echo $trans_advert_data_loop->urn_no; ?>">
				<div class="superdeals-featured-cnabadv__featured-ads-section-listing superdeals-featured-cnabadv__featured-ads-section-listing--adjust <?php echo $has_super_deals === '1' ? 'superdeals-featured-cnabadv__featured-ads-section-listing--border' : ''; ?> d-none superdeals-featured-cnabadv__featured-ads-section-listing-specialnumber">
					<a class="numberplate-search__link" href="<?php echo $seo_url; ?>">
						<div class="numberplate-search__image car-list__position">
							<h2 class="numberplate-search__image-text"><?php echo substr($trans_advert_data_loop->special_no, 0, 15); ?></h2>
<?php 		
				if($has_super_deals === '1') { 
?>
							<p>
								<button class="btn superdeals-featured-cnabadv__insights-btn superdeals-featured-cnabadv__insights-btn--position">Super Deals</button>
							</p>
<?php 
				} 
?>
							<p class="car-list__bookmark <?php echo (in_array($trans_advert_data_loop->urn_no, $pdata['bookmark_array']) ? 'car-list__show-bookmark' : '') ?>" data-urn="<?php echo $trans_advert_data_loop->urn_no ?>" onclick="removeBookmark('<?php echo $trans_advert_data_loop->urn_no ?>');"><img class="car-list__none" src="/images/bookmark.png"><img class="car-list__all" src="/images/bookmarked.png"></p>					
						</div>
					</a>
					<p class="text-center featured-ads-section__title">
						<a href="<?php echo $seo_url; ?>">
<?php
			if ( $trans_advert_data_loop->additional_desc != "" ) {
				if (strlen($trans_advert_data_loop->additional_desc) > 120) {
					echo substr($trans_advert_data_loop->additional_desc, 0, 120) . ' ...';
				} else {
					echo $trans_advert_data_loop->additional_desc;
				}
			} else if($trans_advert_data_loop->qe_add_desc != "" ) {
				if (strlen($trans_advert_data_loop->qe_add_desc) > 120) {
					echo substr($trans_advert_data_loop->qe_add_desc, 0, 120) . ' ...';
				} else {
					echo $trans_advert_data_loop->qe_add_desc;
				}

			} else if($trans_advert_data_loop->feature_desc != "" ) {
				if (strlen($trans_advert_data_loop->feature_desc) > 120) {
					echo substr($trans_advert_data_loop->feature_desc, 0, 120) . ' ...';
				} else {
					echo $trans_advert_data_loop->feature_desc;
				}
			}
?>
						</a>
					</p>					
<?php
			if (isset($trans_advert_super_deals_model) && date("Y-m-d H:i:s") >= $trans_advert_super_deals_model['super_deals_schedule_start_date'] && date("Y-m-d H:i:s") <= $trans_advert_super_deals_model['super_deals_schedule_end_date']) {
?>		
					<div>
<?php				
				if ($trans_advert_data_loop->price == 0) {
					$display_price = 'POA';
?>
						<p class="featured-ads-section__price text-right"><?php echo $display_price; ?></p>
<?php					
				} else {
					if ($trans_advert_super_deals_model['sale_price'] == '0') {
						echo 'POA';
					} else {					
						$display_price = '<small>RM </small> ' . number_format($trans_advert_super_deals_model['sale_price']);				
?>
						<p class="featured-ads-section__price text-right"><?php echo $display_price; ?></p>
<?php
						if ($trans_advert_super_deals_model['sale_price'] != $trans_advert_super_deals_model['regular_price']) { 			
?>													
						<p class="featured-ads-section__price text-right"><?php echo number_format($trans_advert_super_deals_model['regular_price']); ?></p>
<?php
						}
					}
				}
?>							
					</div>	
<?php 		
			} else {
				if ($trans_advert_data_loop->price == 0) {
					$display_price = 'POA';
				} else {
					$display_price = '<small class="featured-ads-section__price-currency">RM </small>' . number_format($trans_advert_data_loop->price);
				}
?>
					<div><p class="featured-ads-section__price text-right"><?php echo $display_price; ?></p></div>
<?php
			}
?>				
					<hr/>
                    <div class="row">
                        <div class="col-4">
                            <p><img class="featured-ads-section__icon" src="images/location.png" alt="location" /></p>
                        </div>
                        <div class="col-8">
                            <span class="featured-ads-section__text">Location</span>
                            <p><?php echo $trans_advert_data_loop->location_name; ?></p>
                        </div>
                    </div>
				</div>
			</div>
<?php
			}
		}
	}
?>
    	</div>
    </div>
</div>