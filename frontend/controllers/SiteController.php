<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\MasterCarMake;
use app\models\MasterCarModelGroup;
use app\models\SuperDeals;
use app\models\TransStatisticCount;
use app\models\TransOrder;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        
        $response_str = Yii::$app->api->wpnews();
        $news_array = json_decode($response_str, true);

        // echo "<pre>";
        // print_r($news_array);
        // exit;

        $master_car_make = new MasterCarMake();
        $master_car_model_group = new MasterCarModelGroup();
        $super_deals = new SuperDeals();
        $trans_statistic_count = new TransStatisticCount();
        $trans_order = new TransOrder();

        $pdata = [];
        
        $featured_cars = $trans_order->featured(1141, 6);
        
        $pdata['superdeals_featured_cnabadv_list'] = ['Featured Cars' => $featured_cars];
        
        return $this->render('index', [
            'news_array' => $news_array,
            'master_car_make' => $master_car_make,
            'master_car_model_group' => $master_car_model_group,
            'super_deals' => $super_deals,
            'trans_statistic_count' => $trans_statistic_count,
            'pdata' => $pdata,
        ]);
    }

     /**
     * Displays login page.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (in_array($_SERVER['REMOTE_ADDR'], ['127.0.0.1', '188.166.181.222', '58.71.205.150', '2001:d08:e2:5e77:d1fe:3726:e0ec:109e', '2001:d08:e2:5e77:912a:4142:613e:3351'])) {
            if (!Yii::$app->general->getuserid()) {
                return $this->render('login');
            } else {
                return $this->redirect(Yii::$app->createAbsoluteUrl('user/index'));
            }
            
        } else {
            $start_date = date('2019-09-01 00:00:00');
            $end_date = date('2019-09-13 23:59:59');
            $current_date = date("Y-m-d 00:00:00");;
            if ($current_date >= $start_date && $current_date <= $end_date) { 
                $this->redirect(Yii::$app->createAbsoluteUrl('site/oscmaintenance'));
            } else {
                if (!Yii::$app->general->getuserid()) {
                    return $this->render('login');
                }
                else {
                    return $this->redirect(Yii::$app->createAbsoluteUrl('user/index'));
                }
            }
        }
    }

    /**
     * Displays terms and condtions.
     *
     * @return mixed
     */
    public function actionTermsAndConditions()
    {
        return $this->render('termsAndConditions');
    }

    /**
     * Displays contact.
     *
     * @return mixed
     */
    public function actionContactUs()
    {
        return $this->render('contactUs');
    }

    /**
     * Displays privacy policy.
     *
     * @return mixed
     */
    public function actionPrivacy()
    {
        return $this->render('privacy');
    }

    /**
     * Displays sitemap.
     *
     * @return mixed
     */
    public function actionSitemap()
    {
        return $this->render('sitemap');
    }

    /**
     * Displays sitemap.
     *
     * @return mixed
     */
    public function actionFaq()
    {
        return $this->render('faq');
    }
}
