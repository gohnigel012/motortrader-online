<script type="text/javascript">
	jQuery("#link-slider").protoSlider({
		'fadeintime': 1000
	});

	window.addEventListener("load", function(event) {
        lazyload();
    });

    jQuery('#numberplate-filter-open').click(function () {
		if(jQuery('#numberplate-filter-show').hasClass('d-none')){
			jQuery('#numberplate-filter-show').removeClass('d-none');
			jQuery('#numberplate-filter-open').attr("src", "/images/close.png");
		} else {
			jQuery('#numberplate-filter-show').addClass('d-none');
			jQuery('#numberplate-filter-open').attr("src", "/images/plus.png");
		}
	});

	jQuery('#numberplate-alphabet-open').click(function () {
		if(jQuery('#numberplate-alphabet-show').hasClass('d-none')){
			jQuery('#numberplate-alphabet-show').removeClass('d-none');
			jQuery('#numberplate-alphabet-open').attr("src", "/images/close.png");
		} else {
			jQuery('#numberplate-alphabet-show').addClass('d-none');
			jQuery('#numberplate-alphabet-open').attr("src", "/images/plus.png");
		}
	});

	jQuery('#numberplate-state-open').click(function () {
		if(jQuery('#numberplate-state-show').hasClass('d-none')){
			jQuery('#numberplate-state-show').removeClass('d-none');
			jQuery('#numberplate-state-open').attr("src", "/images/close.png");
		} else {
			jQuery('#numberplate-state-show').addClass('d-none');
			jQuery('#numberplate-state-open').attr("src", "/images/plus.png");
		}
	});

	$( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 99999,
      values: [ <?php echo isset($_REQUEST['minPrice']) && $_REQUEST['minPrice'] != null ? $_REQUEST['minPrice'] : 30000; ?>, <?php echo isset($_REQUEST['maxPrice']) && $_REQUEST['maxPrice'] != null ? $_REQUEST['maxPrice'] : 65000; ?> ],
      slide: function( event, ui ) {
        $( "#minPrice" ).text( ui.values[ 0 ] );
        $( "#maxPrice" ).text( ui.values[ 1 ] );
      }
    });

    $( "#minPrice" ).text( $( "#slider-range" ).slider( "values", 0 ))  ;
    $( "#maxPrice" ).text( $( "#slider-range" ).slider( "values", 1 ))  ;

    jQuery('#custom-handle').mouseup(function() {
		window.location = "numberplate?minPrice=" + $( "#slider-range" ).slider( "values", 0 ) + "&maxPrice=" + $( "#slider-range" ).slider( "values", 1 );
	});

	jQuery('#custom-handle-2').mouseup(function() {
		window.location = "numberplate?minPrice=" + $( "#slider-range" ).slider( "values", 0 ) + "&maxPrice=" + $( "#slider-range" ).slider( "values", 1 );
	});

	function popupvideo(_url, _title, _urn) {
		
		var _urn = _urn || '';

		ga('send', 'event', 'Auto Parts', 'video', _urn, {
			nonInteraction: true
		});

		var _title = _title || 'Video';

		jQuery("#videoForm #video-title").text(_title);

		if (_url.indexOf('?autoplay=1&mute=1') > 0 ) {
			jQuery("#popup-video-object").css("display","block");
			jQuery("#popup-video-object").attr('data', _url);
			jQuery("#popup-video-video").css("display","none");	
		} else {
			jQuery("#popup-video-object").css("display","none");
			jQuery("#popup-video-video").css("display","block");
			jQuery("#popup-video-video").attr('src', _url);	
		}

		jQuery("#popupFrame").css("display","block");

		jQuery("#popup-video").prop('muted', true)
	}
	
	function closeVideo(){
		jQuery("#popupFrame").css("display","none");
		jQuery("#popup-video-object").attr('data', '');
		jQuery("#popup-video-video").attr('src', '');	
	}	
</script>