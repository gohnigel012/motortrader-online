<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trans_order_picture".
 *
 * @property string $urn_no
 * @property string $urn_no_seq
 * @property float $picture_no 1～
 * @property string|null $picture_title
 * @property string|null $picture_file
 * @property string|null $upd_user
 * @property string|null $upd_date
 * @property string|null $fp_flg
 * @property int|null $is_new
 */
class TransOrderPicture extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trans_order_picture';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['urn_no', 'urn_no_seq', 'picture_no'], 'required'],
            [['picture_no'], 'number'],
            [['upd_date'], 'safe'],
            [['is_new'], 'default', 'value' => null],
            [['is_new'], 'integer'],
            [['urn_no', 'upd_user'], 'string', 'max' => 20],
            [['urn_no_seq'], 'string', 'max' => 5],
            [['picture_title'], 'string', 'max' => 50],
            [['picture_file'], 'string', 'max' => 255],
            [['fp_flg'], 'string', 'max' => 1],
            [['urn_no', 'urn_no_seq', 'picture_no'], 'unique', 'targetAttribute' => ['urn_no', 'urn_no_seq', 'picture_no']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'urn_no' => 'Urn No',
            'urn_no_seq' => 'Urn No Seq',
            'picture_no' => 'Picture No',
            'picture_title' => 'Picture Title',
            'picture_file' => 'Picture File',
            'upd_user' => 'Upd User',
            'upd_date' => 'Upd Date',
            'fp_flg' => 'Fp Flg',
            'is_new' => 'Is New',
        ];
    }
}
