<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $trans_advert_model->special_no;
$this->params['breadcrumbs'][] = ['label' => 'Phone numbers', 'url' => ['/specialnumber']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-home">
	<div class="car-view">
		<div class="row">
			<div class="col-lg-6">
				<div class="numberplate-view__numberplate">
					<h2 class="numberplate-view__numberplate-text"><?php echo $trans_advert_model->special_no; ?></h2>
				</div>
			</div>
			<div class="col-lg-6">
				<p class="car-view__flex">Last updated on <?php echo date("j F Y", strtotime($trans_advert_model->upd_data_date)); ?> <a href="">Report this ad</a></p>
				<h1 class="banner-search__title banner-search__title--gap">
<?php
	if ( $trans_advert_model->additional_desc != "" ) {
		if (strlen($trans_advert_model->additional_desc) > 120) {
			echo substr($trans_advert_model->additional_desc, 0, 120) . ' ...';
		} else {
			echo $trans_advert_model->additional_desc;
		}
	} else if($trans_advert_model->qe_add_desc != "" ) {
		if (strlen($trans_advert_model->qe_add_desc) > 120) {
			echo substr($trans_advert_model->qe_add_desc, 0, 120) . ' ...';
		} else {
			echo $trans_advert_model->qe_add_desc;
		}

	} else if($trans_advert_model->feature_desc != "" ) {
		if (strlen($trans_advert_model->feature_desc) > 120) {
			echo substr($trans_advert_model->feature_desc, 0, 120) . ' ...';
		} else {
			echo $trans_advert_model->feature_desc;
		}
	}
?>
				</h1>
				<div class="row no-gutters">
                    <div class="col-1">
                        <p><img class="featured-ads-section__icon" src="/images/location.png" alt="location" /></p>
                    </div>
                    <div class="col-11">
                        <span class="featured-ads-section__text">Location</span>
                        <p><a href="/numberplate?location=<?php echo $trans_advert_model->location_name; ?>" class="car-view__link"><?php echo $trans_advert_model->location_name; ?></a></p>
                    </div>
                </div>
<?php
	$trans_advert_super_deals_model = $super_deals->getsuperdeals($trans_advert_model->urn_no);	
	$has_super_deals = $trans_advert_super_deals_model['got_super_deals'];

	$display_price = 'POA';

	if (isset($trans_advert_super_deals_model) && date("Y-m-d H:i:s") >= $trans_advert_super_deals_model['super_deals_schedule_start_date'] && date("Y-m-d H:i:s") <= $trans_advert_super_deals_model['super_deals_schedule_end_date']) {
		if ($trans_advert_model->price == 0) {
?>
               <p class="car-view__price"><span class="car-view__price-figure"><?php echo $display_price; ?></span></p>
<?php
		} else {
			if ($trans_advert_super_deals_model['sale_price'] == '0') {
				echo $display_price;
			} else {
				$display_price = number_format($trans_advert_super_deals_model['sale_price']); 
?>				
			<p class="car-view__price"><small>RM</small> <span class="car-view__price-figure"><?php echo $display_price; ?></span></p>
<?php
				if ($trans_advert_super_deals_model['sale_price'] != $trans_advert_super_deals_model['regular_price']) { 			
?>
					<p class="car-view__price"><small>RM</small> <span class="car-view__price-figure"><?php echo number_format($trans_advert_super_deals_model['regular_price']); ?></span></p>				
<?php
				}
			}
		}
	} else {
		if ($trans_advert_model->price == 0) {
			//
		} else {
			$display_price = number_format($trans_advert_model->price); 
		}
?>
			<p class="car-view__price"><small class="car-view__price-currency">RM</small> <span class="car-view__price-figure"><?php echo $display_price; ?></span></p>
<?php
	}
?>                
			</div>
		</div>
	</div>
	<div class="car-view__fixed">
		<div class="row no-gutters">
			<div class="col-lg-7">
				<p>
					<a class="btn car-view__fixed-button">
						<img src="/images/bookmark.png">
					</a>
					<a class="btn car-view__fixed-button">
						<img src="/images/share.png">
					</a>
					<a class="btn car-view__fixed-button">
						<img src="/images/dualcar.png">
					</a>
					<span class="car-view__fixed-text car-view__fixed-text--stylise">
<?php
	if ( $trans_advert_model->additional_desc != "" ) {
		if (strlen($trans_advert_model->additional_desc) > 120) {
			echo substr($trans_advert_model->additional_desc, 0, 120) . ' ...';
		} else {
			echo $trans_advert_model->additional_desc;
		}
	} else if($trans_advert_model->qe_add_desc != "" ) {
		if (strlen($trans_advert_model->qe_add_desc) > 120) {
			echo substr($trans_advert_model->qe_add_desc, 0, 120) . ' ...';
		} else {
			echo $trans_advert_model->qe_add_desc;
		}

	} else if($trans_advert_model->feature_desc != "" ) {
		if (strlen($trans_advert_model->feature_desc) > 120) {
			echo substr($trans_advert_model->feature_desc, 0, 120) . ' ...';
		} else {
			echo $trans_advert_model->feature_desc;
		}
	}
?>	    					
    				</span>
    			</p>
	    	</div>
	    	<div class="col-lg-5">
				<div class="row no-gutters">
	    			<div class="col-lg-4">
	    				<div class="car-view__fixed-price">
		    				<p class="car-view__fixed-text text-right"><small class="car-view__fixed-text--position">RM</small> <span class="car-view__fixed-text--enlarge"><?php echo $display_price; ?></span></p>
	    				</div>
	    			</div>
	    			<div class="col-lg-8">
	    				<p>
		    				<button class="btn car-view__search-btn"><span class="car-view__search-btn-text">Contact</span></button>
							<button class="btn car-view__search-btn"><span class="car-view__search-btn-text">WhatsApp</span></button>
						</p>
	    			</div>
	    		</div>
	    	</div>
		</div>
	</div>
	<div class="superdeals-featured-cnabadv superdeals-featured-cnabadv--background-color">		
		<div class="superdeals-featured-cnabadv-container">
			<h1 class="banner-search__title banner-search__title--position"><a href="<?php echo $pdata['dealer_seourl']; ?>"><?php echo $pdata['dealer_type'] !== false ? $pdata['dealer_type']->dealer_name : 'Private Dealer'; ?></a></h1>
			<p class="car-view__rating-list">
<?php 
	$master_dealer_review_model = $master_dealer_review->getdealerreviewinfo($trans_advert_model->dealer_id);
	if ($master_dealer_review_model) {
		if ($master_dealer_review_model->master_dealer_review_status == '1') {
			
			$count_master_dealer_review_info = $master_dealer_review_info->countdealerreviewinfo($master_dealer_review_model->dealer_id);

			$average_total_overall = $master_dealer_review_info->getaveragerating($master_dealer_review_model->dealer_id);

			switch ($average_total_overall) {
				case '1':
					echo '	<img src="/images/star.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 1.0 </span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '?type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>
						';
				break;
				case '1.5':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 1.5 </span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '?type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '2':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 2.0 </span>
								<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '?type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '2.5':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 2.5 </span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '?type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '3':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 3.0 </span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '?type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '3.5':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 3.5</span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '?type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '4':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 4.0</span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '?type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '4.5':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<span class="car-view__rating"> 4.5</span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '?type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '5':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<span class="car-view__rating"> 5.0</span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '?type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>
						';
				break;
				case '0':
					echo '	<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 0.0</span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '?type=nav-dealer-review">
								<span>No review yet</span>
							</a>';
				break;
			}
		}
	} else {
		echo '	<img src="/images/star-empty.png">
				<img src="/images/star-empty.png">
				<img src="/images/star-empty.png">
				<img src="/images/star-empty.png">
				<img src="/images/star-empty.png">
				<span class="car-view__rating"> 0.0</span>
				<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '?type=nav-dealer-review">
					<span>No review yet</span>
				</a>';
	}
?>				
			</p>
			<div class="row">
				<div class="col-lg-7">
					<div class="car-view__dealer">
						<div class="row no-gutters">
							<div class="col-1">
								<p><img class="featured-ads-section__icon" src="/images/dealer.png"></p>
							</div>
							<div class="col-11">
								<p class="car-view__dealer-type"><?php echo $pdata['dealer_type'] !== false ? 'Dealer' : 'Private'; ?></p>
							</div>
						</div>
					</div>
					<div class="car-view__dealer">
						<div class="row no-gutters">
							<div class="col-1">
								<p><img class="featured-ads-section__icon" src="/images/location.png"></p>
							</div>
							<div class="col-11">
								<p class="car-view__dealer-type"><?php echo $pdata['dealer_type']->location_name; ?></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5">
					<p class="text-center"><button class="btn car-view__search-btn car-view__search-btn--align"><span class="car-view__search-btn-text">Drive there</span></button> <button class="btn car-view__search-btn car-view__search-btn--red car-view__search-btn--align"><span class="car-view__search-btn-text">Contact</span></button></p>
				</div>
			</div>
		</div>
	</div>
	<div class="superdeals-featured-cnabadv">
		<p class="banner-search__title banner-search__title--margin text-center">Description</p>
        <div class="container">
        	<div class="car-view__tab-show">
        		<p>
<?php
	$description = '';

	if ($pdata['dealer_type'] !== false) {
		if (trim($trans_advert_model->qe_add_desc) != '') {
			$temp = Yii::$app->general->avanserreplace($pdata['dealer_type']->dealer_id, $trans_advert_model->qe_add_desc);
			$description .= nl2br(Yii::$app->general->censoredfilter((is_string($temp) ? $temp : $trans_advert_model->qe_add_desc)));
		}
		
		if (trim($trans_advert_model->additional_desc) != '' ) {
			$temp = Yii::$app->general->avanserreplace($pdata['dealer_type']->dealer_id, $trans_advert_model->additional_desc);
			$description .= nl2br(Yii::$app->general->censoredfilter(($temp != 0 ? $temp : $trans_advert_model->additional_desc)));
		}
		
		if (trim($trans_advert_model->feature_desc) != '') {
			$temp = Yii::$app->general->avanserreplace($pdata['dealer_type']->dealer_id, $trans_advert_model->feature_desc);
			$description .= nl2br(Yii::$app->general->censoredfilter(($temp != 0 ? $temp : $trans_advert_model->feature_desc)));
		}
	} else {
		if (trim($trans_advert_model->qe_add_desc) != '') {
			$description .= nl2br(Yii::$app->general->censoredfilter($trans_advert_model->qe_add_desc));
		}
		
		if (trim($trans_advert_model->additional_desc) != '' ) {
			$description .= nl2br(Yii::$app->general->censoredfilter($trans_advert_model->additional_desc));
		}
		
		if (trim($trans_advert_model->feature_desc) != '') {
			$description .= nl2br(Yii::$app->general->censoredfilter($trans_advert_model->feature_desc));
		}
	}

	$censored_string = Yii::$app->general->censoredfilter($description, '-', $matched_phone);

	if ($matched_phone !== false) {
		$matched_phone = Yii::$app->general->filterphonex($matched_phone);
	}

	$filterphone_string = Yii::$app->general->filterphone($censored_string, '<a href="javascript:void(0)" onclick="showphoneindetails(this)" class="showphoneindetails-pre" data-phone="__PHONE__">' . $matched_phone . '</a>');

	echo $filterphone_string;

?>        			
        		</p>
        	</div>
        </div>
    </div>
<?php
	if (isset($pdata['similar']) && count($pdata['similar'])) {	
?>
     <div class="superdeals-featured-cnabadv superdeals-featured-cnabadv--background-color">
        <div class="superdeals-featured-cnabadv__box">
<?php
	foreach ($pdata['similar'] as $pdata_loop) {
		$min_max_price[] = $pdata_loop['price'];
	}
?>       	
        	<h2 class="text-center banner-search__title banner-search__title--margin">Number plates between RM<?php echo number_format(min($min_max_price)); ?> to RM<?php echo number_format(max($min_max_price)); ?></h2>
        	<div class="row no-gutters">
<?php
	$count = 0;
	foreach ($pdata['similar'] as $featured_data_loop) {
		$count++;

		$trans_advert_super_deals_model = $super_deals->getsuperdeals($featured_data_loop['urn_no']);
			
		$has_super_deals = $trans_advert_super_deals_model['got_super_deals'];
		
		if ($featured_data_loop['poa'] == 1) {
			$price = 'POA';
		} else {
			$price = '<small class="featured-ads-section__price-currency">RM </small>' . number_format($featured_data_loop['price']);
		}
?>
				<div class="col-lg-4">
	        		<div class="superdeals-featured-cnabadv__featured-ads-section-listing superdeals-featured-cnabadv__featured-ads-section-listing--center margin-bottom-10 <?php echo $has_super_deals === '1' ? 'superdeals-featured-cnabadv__featured-ads-section-listing--border' : ''; ?>">
	        			<a class="dealer-view__numberplate-link" href="<?php echo $featured_data_loop['seourl']; ?>?utm_medium=FeaturedAds_Detail">
	        				<div class="dealer-view__numberplate car-view__position">
								<h2 class="dealer-view__numberplate-text"><?php echo $featured_data_loop['special_no']; ?></h2>
								<p class="car-list__bookmark <?php echo (in_array($featured_data_loop['urn_no'], $pdata['bookmark_array']) ? 'car-list__show-bookmark' : '') ?>" data-urn="<?php echo $featured_data_loop['urn_no']; ?>"><img class="car-list__none" src="/images/bookmark.png"><img class="car-list__all" src="/images/bookmarked.png"></p>
							</div>
						</a>
	        			 <p class="text-center featured-ads-section__title">
	        			 	<a href="<?php echo $featured_data_loop['seourl']; ?>?utm_medium=FeaturedAds_Detail">
<?php
	if ( $trans_advert_model->additional_desc != "" ) {
		if (strlen($trans_advert_model->additional_desc) > 120) {
			echo substr($trans_advert_model->additional_desc, 0, 120) . ' ...';
		} else {
			echo $trans_advert_model->additional_desc;
		}
	} else if($trans_advert_model->qe_add_desc != "" ) {
		if (strlen($trans_advert_model->qe_add_desc) > 120) {
			echo substr($trans_advert_model->qe_add_desc, 0, 120) . ' ...';
		} else {
			echo $trans_advert_model->qe_add_desc;
		}

	} else if($trans_advert_model->feature_desc != "" ) {
		if (strlen($trans_advert_model->feature_desc) > 120) {
			echo substr($trans_advert_model->feature_desc, 0, 120) . ' ...';
		} else {
			echo $trans_advert_model->feature_desc;
		}
	}
?>	        			
							</a>
						</p>
	                    <p class="featured-ads-section__price text-right"><?php echo $price; ?></p>
	                    <hr/>
	                    <div class="row">
	                        <div class="col-4">
	                            <p><img class="featured-ads-section__icon" src="/images/location.png" alt="location" /></p>
	                        </div>
	                        <div class="col-8">
	                            <span class="featured-ads-section__text">Location</span>
	                            <p><?php echo $trans_advert_model->location_name; ?></p>
	                        </div>
	                    </div>
                	</div>
                </div>
<?php
	}
?>        		
    		</div>
    		<p class="text-center car-view__link-outer"><a href="<?php echo Yii::$app->general->generatecarsseo('specialnumber', 0, 0, '', 0, '', '', '', 0, ['minPrice' => min($min_max_price), 'maxPrice' => max($min_max_price),]); ?>" class="car-view__link">View all</a></p>
    	</div>
<?php
	}
?>
	</div>
</div>