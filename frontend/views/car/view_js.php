<script>
	window.addEventListener("load", function(event) {
        lazyload();
    });

    function showCarousel(index) {
        $('#demo').carousel(parseInt(index));
    }

    var total = $('.carousel-indicators-items').length;
    var currentIndex = $('div.active').index() + 1;
    $('.carousel-text').html(currentIndex + '/'  + total);

    // This triggers after each slide change
    $('.carousel').on('slid.bs.carousel', function () {
      currentIndex = $('div.active').index() + 1;

      // Now display this wherever you want
      var text = currentIndex + '/' + total;
      $('.carousel-text').html(text);
    });

    jQuery("#icon-box-desc").click(function() {
    	jQuery("#icon-box-desc").addClass("active");
    	jQuery("#icon-box-loan").removeClass("active");
    	jQuery("#icon-box-voc").removeClass("active");
    	jQuery("#car-desc").removeClass("d-none");
    	jQuery("#car-view__loan").addClass("d-none");
    	jQuery("#car-view__voc").addClass("d-none");
    });

    jQuery("#icon-box-loan").click(function() {
    	jQuery("#icon-box-desc").removeClass("active");
    	jQuery("#icon-box-loan").addClass("active");
    	jQuery("#icon-box-voc").removeClass("active");
    	jQuery("#car-desc").addClass("d-none");
    	jQuery("#car-view__loan").removeClass("d-none");
    	jQuery("#car-view__voc").addClass("d-none");
    });

    jQuery("#icon-box-voc").click(function() {
    	jQuery("#icon-box-desc").removeClass("active");
    	jQuery("#icon-box-loan").removeClass("active");
    	jQuery("#icon-box-voc").addClass("active");
    	jQuery("#car-desc").addClass("d-none");
    	jQuery("#car-view__loan").addClass("d-none");
    	jQuery("#car-view__voc").removeClass("d-none");
    });
</script>