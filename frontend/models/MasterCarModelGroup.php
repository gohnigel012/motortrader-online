<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_car_model_group".
 *
 * @property int $master_car_model_group_id
 * @property string|null $make_cd
 * @property string|null $group_name
 * @property string|null $disp_flg 1:Show to system, 0:Disabled
 */
class MasterCarModelGroup extends \yii\db\ActiveRecord
{
  /**
   * {@inheritdoc}
   */
  public static function tableName()
  {
      return 'master_car_model_group';
  }

  /**
   * {@inheritdoc}
   */
  public function rules()
  {
      return [
          [['group_name'], 'string'],
          [['make_cd'], 'string', 'max' => 20],
          [['disp_flg'], 'string', 'max' => 1],
      ];
  }

  /**
   * {@inheritdoc}
   */
  public function attributeLabels()
  {
      return [
          'master_car_model_group_id' => 'Master Car Model Group ID',
          'make_cd' => 'Make Cd',
          'group_name' => 'Group Name',
          'disp_flg' => 'Disp Flg',
      ];
  }

  public function getMasterCarModelGroupItems()
  {
      return $this->hasOne(MasterCarModelGroupItems::className(), ['master_car_model_group_id' => 'master_car_model_group_id']);
  }

  public function getmodelgroup($make_cd) { 
     $master_car_model_group_array = []; 

     $master_car_model_group_memcache_key = "/getmodelgroup/" . $make_cd; 
     $master_car_model_group_memcache = Yii::$app->cache->get($master_car_model_group_memcache_key); 
     if ($master_car_model_group_memcache !== false) { 
         $master_car_model_group_array = $master_car_model_group_memcache; 
     } else { 
         $master_car_model_group = $this->find()->from('master_car_model_group mcmg')->innerJoin('master_car_model_group_items mcmgi', 'mcmgi.master_car_model_group_id::INTEGER = mcmg.master_car_model_group_id')->where(['mcmg.make_cd' => (string)$make_cd])->with('masterCarModelGroupItems')->all();

         if (count($master_car_model_group)) { 
             $count = 0; 
             foreach ($master_car_model_group as $master_car_model_group_data_loop) { 
                 $count++;
                  
                 $master_car_model_group_array[$count] = [ 
                                                            'group_name' => $master_car_model_group_data_loop->group_name,
                                                            'model_group' => explode(",", $master_car_model_group_data_loop->masterCarModelGroupItems->model_group),
                                                            'model_group_list' => $master_car_model_group_data_loop->masterCarModelGroupItems->model_group,
                                                         ]; 
             } 
         } 

         Yii::$app->cache->set($master_car_model_group_memcache_key, $master_car_model_group_array, 240); 
     } 
      
     return $master_car_model_group_array; 
  }

  public function getmodelgroupname($make_cd, $model_cd) {
    $response = false;

    $master_car_model_group_memcache_key = "/getmodelgroupname/cd/" . $make_cd . "/" . $model_cd;
    $master_car_model_group_memcache = Yii::$app->cache->get($master_car_model_group_memcache_key);
    if ($master_car_model_group_memcache !== false) {
      $response = $master_car_model_group_memcache;
    } else {
      $master_car_model_group = $this->find()->where(['make_cd' => trim($make_cd), 'model_group' => trim($model_cd)])->one();
      if ($master_car_model_group) {
        $response = $master_car_model_group->group_name;

        Yii::$app->cache->set($master_car_model_group_memcache_key, $response, 240);
      }
    }
    
    return $response;
  }  
}
