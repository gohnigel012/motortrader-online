<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_car_model_group_items".
 *
 * @property int $master_car_model_group_items_id
 * @property string|null $master_car_model_group_id
 * @property string|null $make_cd
 * @property string|null $model_group
 */
class MasterCarModelGroupItems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_car_model_group_items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_group'], 'string'],
            [['master_car_model_group_id', 'make_cd'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'master_car_model_group_items_id' => 'Master Car Model Group Items ID',
            'master_car_model_group_id' => 'Master Car Model Group ID',
            'make_cd' => 'Make Cd',
            'model_group' => 'Model Group',
        ];
    }
}
