<script>
	window.addEventListener("load", function(event) {
	    lazyload();
	});

	jQuery('#icon-box-car').click(function() {
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-car").removeClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-commercial-car").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-autopart").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-numberplate").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-bike").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-specialnumber").addClass("d-none");
        jQuery("#icon-box-car").addClass("active");
        jQuery("#icon-box-commercial-car").removeClass("active");
        jQuery("#icon-box-auto-parts").removeClass("active");
        jQuery("#icon-box-number-plates").removeClass("active");
        jQuery("#icon-box-motorcycle").removeClass("active");
        jQuery("#icon-box-phone-numbers").removeClass("active");
    });

    jQuery('#icon-box-motorcycle').click(function() {
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-bike").removeClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-autopart").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-car").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-commercial-car").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-numberplate").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-specialnumber").addClass("d-none");
        jQuery("#icon-box-motorcycle").addClass("active");
        jQuery("#icon-box-car").removeClass("active");
        jQuery("#icon-box-commercial-car").removeClass("active");
        jQuery("#icon-box-auto-parts").removeClass("active");
        jQuery("#icon-box-number-plates").removeClass("active");
        jQuery("#icon-box-phone-numbers").removeClass("active");
    });

    jQuery('#icon-box-commercial-car').click(function() {
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-car").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-commercial-car").removeClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-autopart").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-numberplate").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-bike").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-specialnumber").addClass("d-none");
        jQuery("#icon-box-car").removeClass("active");
        jQuery("#icon-box-commercial-car").addClass("active");
        jQuery("#icon-box-auto-parts").removeClass("active");
        jQuery("#icon-box-number-plates").removeClass("active");
        jQuery("#icon-box-motorcycle").removeClass("active");
        jQuery("#icon-box-phone-numbers").removeClass("active");
    });

    jQuery('#icon-box-auto-parts').click(function() {
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-autopart").removeClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-car").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-commercial-car").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-numberplate").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-bike").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-specialnumber").addClass("d-none");
        jQuery("#icon-box-auto-parts").addClass("active");
        jQuery("#icon-box-commercial-car").removeClass("active");
        jQuery("#icon-box-car").removeClass("active");
        jQuery("#icon-box-number-plates").removeClass("active");
        jQuery("#icon-box-motorcycle").removeClass("active");
        jQuery("#icon-box-phone-numbers").removeClass("active");
        
    });

    jQuery('#icon-box-number-plates').click(function() {
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-numberplate").removeClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-autopart").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-car").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-commercial-car").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-bike").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-specialnumber").addClass("d-none");
        jQuery("#icon-box-number-plates").addClass("active");
        jQuery("#icon-box-phone-numbers").removeClass("active");
        jQuery("#icon-box-car").removeClass("active");
        jQuery("#icon-box-commercial-car").removeClass("active");
        jQuery("#icon-box-auto-parts").removeClass("active");
        jQuery("#icon-box-motorcycle").removeClass("active");
        jQuery("#icon-box-phone-numbers").removeClass("active");
    });

    jQuery('#icon-box-phone-numbers').click(function() {
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-specialnumber").removeClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-numberplate").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-autopart").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-car").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-commercial-car").addClass("d-none");
        jQuery(".superdeals-featured-cnabadv__featured-ads-section-listing-bike").addClass("d-none");
        jQuery("#icon-box-number-plates").removeClass("active");
        jQuery("#icon-box-phone-numbers").addClass("active");
        jQuery("#icon-box-car").removeClass("active");
        jQuery("#icon-box-commercial-car").removeClass("active");
        jQuery("#icon-box-auto-parts").removeClass("active");
        jQuery("#icon-box-motorcycle").removeClass("active");
    });

    function removeBookmark(_urn) {
        jQuery.ajax({
            type: 'post',
            url: DEFINE_AJAX_REMOVE_BOOKMARK_URL,
            dataType: 'json',
            data: {'urn_no': _urn},
            timeout: 120000,
            beforeSend : function(){
                
            },
            success: function(data) {
                if (data.result == '1') {
                	jQuery(".col-lg-3[data-urn=" + _urn + "]").remove();
                } else {
                    //
                }
            }
        });
	}
</script>