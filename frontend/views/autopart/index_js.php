<script>
	jQuery("#link-slider").protoSlider({
		'fadeintime': 1000
	});
	
	window.addEventListener("load", function(event) {
	    lazyload();
	});

	jQuery('#autopart-filter-open').click(function () {
		if(jQuery('#autopart-filter-show').hasClass('d-none')){
			jQuery('#autopart-filter-show').removeClass('d-none');
			jQuery('#autopart-filter-open').attr("src", "/images/close.png");
		} else {
			jQuery('#autopart-filter-show').addClass('d-none');
			jQuery('#autopart-filter-open').attr("src", "/images/plus.png");
		}
	});

	jQuery('#autopart-condition-open').click(function () {
		if(jQuery('#autopart-condition-show').hasClass('d-none')){
			jQuery('#autopart-condition-show').removeClass('d-none');
			jQuery('#autopart-condition-open').attr("src", "/images/close.png");
		} else {
			jQuery('#autopart-condition-show').addClass('d-none');
			jQuery('#autopart-condition-open').attr("src", "/images/plus.png");
		}
	});

	jQuery('#autopart-make-open').click(function () {
		if(jQuery('#autopart-make-show').hasClass('d-none')){
			jQuery('#autopart-make-show').removeClass('d-none');
			jQuery('#autopart-make-open').attr("src", "/images/close.png");
		} else {
			jQuery('#autopart-make-show').addClass('d-none');
			jQuery('#autopart-make-open').attr("src", "/images/plus.png");
		}
	});

	jQuery('#autopart-model-show').click(function () {
		if(jQuery('#autopart-model-show').hasClass('d-none')){
			jQuery('#autopart-model-show').removeClass('d-none');
			jQuery('#autopart-model-show').attr("src", "/images/close.png");
		} else {
			jQuery('#autopart-model-show').addClass('d-none');
			jQuery('#autopart-model-show').attr("src", "/images/plus.png");
		}
	});

	jQuery('#autopart-category-open').click(function () {
		if(jQuery('#autopart-category-show').hasClass('d-none')){
			jQuery('#autopart-category-show').removeClass('d-none');
			jQuery('#autopart-category-open').attr("src", "/images/close.png");
		} else {
			jQuery('#autopart-category-show').addClass('d-none');
			jQuery('#autopart-category-open').attr("src", "/images/plus.png");
		}
	});

	jQuery('#autopart-subcategory-open').click(function () {
		if(jQuery('#autopart-subcategory-show').hasClass('d-none')){
			jQuery('#autopart-subcategory-show').removeClass('d-none');
			jQuery('#autopart-subcategory-open').attr("src", "/images/close.png");
		} else {
			jQuery('#autopart-subcategory-show').addClass('d-none');
			jQuery('#autopart-subcategory-open').attr("src", "/images/plus.png");
		}
	});

	jQuery('#autopart-state-open').click(function () {
		if(jQuery('#autopart-state-show').hasClass('d-none')){
			jQuery('#autopart-state-show').removeClass('d-none');
			jQuery('#autopart-state-open').attr("src", "/images/close.png");
		} else {
			jQuery('#autopart-state-show').addClass('d-none');
			jQuery('#autopart-state-open').attr("src", "/images/plus.png");
		}
	});

	$( "#slider-range" ).slider({
      range: true,
      min: 40,
      max: 3000,
      values: [ <?php echo isset($_REQUEST['minPrice']) && $_REQUEST['minPrice'] != null ? $_REQUEST['minPrice'] : 90; ?>, <?php echo isset($_REQUEST['maxPrice']) && $_REQUEST['maxPrice'] != null ? $_REQUEST['maxPrice'] : 1200; ?> ],
      slide: function( event, ui ) {
        $( "#minPrice" ).text( ui.values[ 0 ] );
        $( "#maxPrice" ).text( ui.values[ 1 ] );
      }
    });

    $( "#minPrice" ).text( $( "#slider-range" ).slider( "values", 0 ))  ;
    $( "#maxPrice" ).text( $( "#slider-range" ).slider( "values", 1 ))  ;

    jQuery('#custom-handle').mouseup(function() {
		window.location = "autopart?minPrice=" + $( "#slider-range" ).slider( "values", 0 ) + "&maxPrice=" + $( "#slider-range" ).slider( "values", 1 );
	});

	jQuery('#custom-handle-2').mouseup(function() {
		window.location = "autopart?minPrice=" + $( "#slider-range" ).slider( "values", 0 ) + "&maxPrice=" + $( "#slider-range" ).slider( "values", 1 );
	});

	function popupvideo(_url, _title, _urn) {
		
		var _urn = _urn || '';

		ga('send', 'event', 'Auto Parts', 'video', _urn, {
			nonInteraction: true
		});

		var _title = _title || 'Video';

		jQuery("#videoForm #video-title").text(_title);

		if (_url.indexOf('?autoplay=1&mute=1') > 0 ) {
			jQuery("#popup-video-object").css("display","block");
			jQuery("#popup-video-object").attr('data', _url);
			jQuery("#popup-video-video").css("display","none");	
		} else {
			jQuery("#popup-video-object").css("display","none");
			jQuery("#popup-video-video").css("display","block");
			jQuery("#popup-video-video").attr('src', _url);	
		}

		jQuery("#popupFrame").css("display","block");

		var video = jQuery("#popup-video")[0];
		if (video.paused) {
		    video.play();
		}

		jQuery("#popup-video").prop('muted', true)
	}
	
	function closeVideo(){
		jQuery("#popupFrame").css("display","none");
		jQuery("#popup-video-object").attr('data', '');
		jQuery("#popup-video-video").attr('src', '');	
	}	

</script>