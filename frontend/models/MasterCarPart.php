<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_car_part".
 *
 * @property string|null $part_cd
 * @property string|null $part_name
 * @property int|null $sorting
 * @property int|null $disp_flg
 */
class MasterCarPart extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_car_part';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sorting', 'disp_flg'], 'default', 'value' => null],
            [['sorting', 'disp_flg'], 'integer'],
            [['part_cd'], 'string', 'max' => 30],
            [['part_name'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'part_cd' => 'Part Cd',
            'part_name' => 'Part Name',
            'sorting' => 'Sorting',
            'disp_flg' => 'Disp Flg',
        ];
    }
}
