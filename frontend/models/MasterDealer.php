<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_dealer".
 *
 * @property string $dealer_id
 * @property string $dealer_name
 * @property string $password
 * @property string|null $email_add
 * @property string $plan_type
 * @property string|null $credit_status
 * @property string|null $status
 * @property string|null $sales_person_id
 * @property string|null $address
 * @property string|null $town
 * @property string|null $state
 * @property string|null $country
 * @property string|null $postal_cd
 * @property string|null $phone
 * @property string|null $fax
 * @property string|null $test_user_flag
 * @property int|null $failed_login
 * @property string|null $system_status
 * @property int|null $reg_ad_count
 * @property int|null $reg_ad_limit
 * @property int|null $publish_ad_count
 * @property int|null $publish_ad_limit
 * @property string|null $sales_area
 * @property string|null $upd_user
 * @property string|null $upd_date
 * @property string|null $dpl_url
 * @property string|null $dpl_pic_file1
 * @property string|null $dpl_pic_file2
 * @property string|null $address_hide_flg
 * @property string|null $name_hide_flg
 * @property string|null $dealer_type
 * @property int|null $publish_ls_count
 * @property int|null $publish_ls_limit
 * @property string|null $pr_pic_file1
 * @property string|null $map_pic_file1
 * @property string|null $pr_pic_file2
 * @property string|null $map_pic_file2
 * @property string|null $pr_pic_file3
 * @property string|null $map_pic_file3
 * @property string|null $dealer_hierachy_parent
 * @property int|null $dealer_group_id
 * @property int|null $dealer_credits
 * @property float|null $dealer_average_credits
 * @property string|null $dealer_reg_time
 * @property string|null $credit_expiry_date
 * @property string $dealer_email_verification
 * @property string|null $dealer_display_name
 * @property string|null $google_analytics
 * @property string|null $google_analytics_url
 * @property int|null $total_stock
 * @property string|null $trusted
 * @property string|null $trusted_file
 * @property string|null $last_posted
 * @property string|null $mt_sales_representative
 * @property string|null $master_dealer_url
 * @property string|null $master_dealer_logo
 * @property string|null $master_dealer_banner
 * @property string|null $location_cd
 * @property string|null $location_name
 */
class MasterDealer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_dealer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dealer_id', 'dealer_name', 'password', 'plan_type'], 'required'],
            [['failed_login', 'reg_ad_count', 'reg_ad_limit', 'publish_ad_count', 'publish_ad_limit', 'publish_ls_count', 'publish_ls_limit', 'dealer_group_id', 'dealer_credits', 'total_stock'], 'default', 'value' => null],
            [['failed_login', 'reg_ad_count', 'reg_ad_limit', 'publish_ad_count', 'publish_ad_limit', 'publish_ls_count', 'publish_ls_limit', 'dealer_group_id', 'dealer_credits', 'total_stock'], 'integer'],
            [['upd_date', 'dealer_reg_time', 'credit_expiry_date', 'last_posted'], 'safe'],
            [['dealer_average_credits'], 'number'],
            [['dealer_email_verification'], 'string'],
            [['dealer_id', 'sales_person_id', 'postal_cd', 'sales_area', 'upd_user', 'dealer_hierachy_parent'], 'string', 'max' => 20],
            [['dealer_name', 'town', 'state', 'country', 'dealer_display_name'], 'string', 'max' => 80],
            [['password', 'credit_status'], 'string', 'max' => 40],
            [['email_add', 'address', 'master_dealer_url', 'master_dealer_logo', 'master_dealer_banner', 'location_name'], 'string', 'max' => 255],
            [['plan_type', 'status', 'location_cd'], 'string', 'max' => 10],
            [['phone', 'fax'], 'string', 'max' => 30],
            [['test_user_flag', 'system_status', 'address_hide_flg', 'name_hide_flg', 'dealer_type', 'google_analytics', 'trusted'], 'string', 'max' => 1],
            [['dpl_url', 'google_analytics_url'], 'string', 'max' => 500],
            [['dpl_pic_file1', 'dpl_pic_file2', 'pr_pic_file1', 'map_pic_file1', 'pr_pic_file2', 'map_pic_file2', 'pr_pic_file3', 'map_pic_file3', 'trusted_file'], 'string', 'max' => 50],
            [['mt_sales_representative'], 'string', 'max' => 32],
            [['dealer_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dealer_id' => 'Dealer ID',
            'dealer_name' => 'Dealer Name',
            'password' => 'Password',
            'email_add' => 'Email Add',
            'plan_type' => 'Plan Type',
            'credit_status' => 'Credit Status',
            'status' => 'Status',
            'sales_person_id' => 'Sales Person ID',
            'address' => 'Address',
            'town' => 'Town',
            'state' => 'State',
            'country' => 'Country',
            'postal_cd' => 'Postal Cd',
            'phone' => 'Phone',
            'fax' => 'Fax',
            'test_user_flag' => 'Test User Flag',
            'failed_login' => 'Failed Login',
            'system_status' => 'System Status',
            'reg_ad_count' => 'Reg Ad Count',
            'reg_ad_limit' => 'Reg Ad Limit',
            'publish_ad_count' => 'Publish Ad Count',
            'publish_ad_limit' => 'Publish Ad Limit',
            'sales_area' => 'Sales Area',
            'upd_user' => 'Upd User',
            'upd_date' => 'Upd Date',
            'dpl_url' => 'Dpl Url',
            'dpl_pic_file1' => 'Dpl Pic File1',
            'dpl_pic_file2' => 'Dpl Pic File2',
            'address_hide_flg' => 'Address Hide Flg',
            'name_hide_flg' => 'Name Hide Flg',
            'dealer_type' => 'Dealer Type',
            'publish_ls_count' => 'Publish Ls Count',
            'publish_ls_limit' => 'Publish Ls Limit',
            'pr_pic_file1' => 'Pr Pic File1',
            'map_pic_file1' => 'Map Pic File1',
            'pr_pic_file2' => 'Pr Pic File2',
            'map_pic_file2' => 'Map Pic File2',
            'pr_pic_file3' => 'Pr Pic File3',
            'map_pic_file3' => 'Map Pic File3',
            'dealer_hierachy_parent' => 'Dealer Hierachy Parent',
            'dealer_group_id' => 'Dealer Group ID',
            'dealer_credits' => 'Dealer Credits',
            'dealer_average_credits' => 'Dealer Average Credits',
            'dealer_reg_time' => 'Dealer Reg Time',
            'credit_expiry_date' => 'Credit Expiry Date',
            'dealer_email_verification' => 'Dealer Email Verification',
            'dealer_display_name' => 'Dealer Display Name',
            'google_analytics' => 'Google Analytics',
            'google_analytics_url' => 'Google Analytics Url',
            'total_stock' => 'Total Stock',
            'trusted' => 'Trusted',
            'trusted_file' => 'Trusted File',
            'last_posted' => 'Last Posted',
            'mt_sales_representative' => 'Mt Sales Representative',
            'master_dealer_url' => 'Master Dealer Url',
            'master_dealer_logo' => 'Master Dealer Logo',
            'master_dealer_banner' => 'Master Dealer Banner',
            'location_cd' => 'Location Cd',
            'location_name' => 'Location Name',
        ];
    }

    public function getDealer($id) {
        $response = false;
        
        $master_dealer_id_memcache_key = "/master_dealer/getDealer" . $id;
        $master_dealer_id_memcache = Yii::$app->cache->get($master_dealer_id_memcache_key);
        if ($master_dealer_id_memcache !== false) {
            $response = $master_dealer_id_memcache;
        } else {
            $master_dealer_model = $this->find()->where(['dealer_id' => $id])->one();
            if ($master_dealer_model) {
                $response = $master_dealer_model;
            }
            Yii::$app->cache->set($master_dealer_id_memcache_key, $response, 600);
        }
        return $response;
    }
}
