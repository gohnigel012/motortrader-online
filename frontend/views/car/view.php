<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
	
$filter_type='';
switch($pdata['condition']){
	case 'Used Car':
		$filter_type = 'used';
		break;
	case 'New Car':
		$filter_type = 'new';
		break;
	case 'Recon Car':
		$filter_type = 'recond';
		break;
}

$this->title = ($trans_advert_model->make_name == '' ? '-' : $trans_advert_model->make_name) . ' ' . ($trans_advert_model->model_name == '' ? '-' : $trans_advert_model->model_name);
$this->params['breadcrumbs'][] = ['label' => 'Cars', 'url' => ['/car']];
$this->params['breadcrumbs'][] = ['label' => $pdata['condition'], 'url' => ['/car?type=' . $filter_type]];
$this->params['breadcrumbs'][] = ['label' => $trans_advert_model->make_name ? $trans_advert_model->make_name : '-', 'url' => ['/car?type=' . $filter_type .'&make=' . $trans_advert_model->make_cd]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-home">
	<div class="car-view">
		<div class="row">
			<div class="col-lg-6">
<?php
	if (isset($pdata['picture_array']) && count($pdata['picture_array'])) {	
		$count = 0;
?>				
				<div id="demo" class="carousel slide" data-ride="carousel">

					<!-- Indicators -->
					<ul class="carousel-indicators">
<?php
		if ($pdata['car_video'] !== false)	{
?>
						<li data-target="#demo" data-slide-to="0" class="active carousel-indicators-items"></li>
<?php
			foreach ($pdata['picture_array'] as $index_loop => $data_loop) {
?>
						<li data-target="#demo" data-slide-to="<?php echo $index_loop; ?>" class="<?php echo $pdata['car_video'] === false && $index_loop == 1 ? 'active' : ''; ?> carousel-indicators-items"></li>
<?php				
			}
		} else {
			foreach ($pdata['picture_array'] as $index_loop => $data_loop) {
?>
					    <li data-target="#demo" data-slide-to="<?php echo $index_loop - 1; ?>" class="<?php echo $pdata['car_video'] === false && $index_loop == 1 ? 'active' : ''; ?> carousel-indicators-items"></li>
<?php
			}
		}
?>
					</ul>

				    <!-- The slideshow -->
				    <div class="carousel-inner">

<?php
		$trans_advert_super_deals_model = $super_deals->getsuperdeals($trans_advert_model->urn_no);
			
		$has_super_deals = $trans_advert_super_deals_model['got_super_deals'];

		if ($pdata['car_video'] !== false)	{
?>
					    <div class="carousel-item active <?php echo $has_super_deals === '1' ? 'superdeals-featured-cnabadv__featured-ads-section-listing--border' : ''; ?>">
<?php
			if (trim($pdata['car_video']['youtube_id'])) {
?>			
							<object id="advert-video" controls autoplay muted width="100%" class="video img img-fluid video-vertical-frame" data="<?php echo $pdata['car_video']['video_url'] ?>"></object>
<?php
			} else {
?>
							<video id="advert-video" controls autoplay muted width="100%" class="video img img-fluid video-vertical-frame" src="<?php echo $pdata['car_video']['video_url'] ?>"></video>
<?php
			}
?>
					    </div>
<?php
	}
?>

<?php
		foreach ($pdata['picture_array'] as $index_loop => $data_loop) {
?>
					    <div class="carousel-item <?php echo $pdata['car_video'] === false && $index_loop == 1 ? 'active' : ''; ?>" <?php echo $has_super_deals === '1' ? 'superdeals-featured-cnabadv__featured-ads-section-listing--border' : ''; ?>>
					      <img src="/images/ajax-loader.gif" data-src="<?php echo (@getimagesize($pdata['picture_array'][$index_loop]['url'])) ? $pdata['picture_array'][$index_loop]['url'] : $trans_advert_model->bodytype_code == 'bt9' ? '/images/bus.png' : '/images/mercedes.png'; ?>" class="car-view__image lazyload">
					    </div>
<?php
		}
?>
					</div>

<?php 		
		if($has_super_deals === '1') { 
?>
					<button class="btn superdeals-featured-cnabadv__insights-btn superdeals-featured-cnabadv__insights-btn--position">Super Deals</button>
<?php 
		} 
?>					
					  
					<!-- Left and right controls -->
					<a class="carousel-control-prev" href="#demo" data-slide="prev">
					    <span class="carousel-control-prev-icon"></span>
					</a>
					<a class="carousel-control-next" href="#demo" data-slide="next">
					    <span class="carousel-control-next-icon"></span>
					</a>

					<p class="carousel-text"></p>
				</div>
<?php
	}
?>

			</div>
			<div class="col-lg-6">
				<p class="car-view__flex">Last updated on <?php echo date("j F Y", strtotime($trans_advert_model->upd_data_date)) ?> <a href="">Report this ad</a></p>
				<h1 class="banner-search__title"><?php echo $trans_advert_model->make_name . ' ' . $trans_advert_model->model_name; ?></h1>
				<p class="car-view__variant"><?php echo $trans_advert_model->variant_name; ?></p>
				<div class="row">
					<div class="col-lg-6">
						<div class="row">
							<div class="col-6">
								<div class="row no-gutters">
									<div class="col-4">
				                        <p><img class="featured-ads-section__icon" src="/images/calendar.png" alt="calendar" /></p>
				                    </div>
				                    <div class="col-8">
				                        <span class="featured-ads-section__text">Year</span>
				                        <p>
<?php
	if(trim($trans_advert_model->year_make) && $trans_advert_model->year_make != '') {
?>			                        	
			                        		<a href="/car?make=<?php echo $trans_advert_model->make_cd; ?>&model=<?php echo $trans_advert_model->model_cd; ?>&minYear=<?php echo $trans_advert_model->year_make; ?>&maxYear=<?php echo $trans_advert_model->year_make; ?>" class="car-view__link"><?php echo trim($trans_advert_model->year_make) ? $trans_advert_model->year_make : '-'; ?></a>
<?php
	} else {
?>
										<?php echo trim($trans_advert_model->year_make) ? $trans_advert_model->year_make : '-'; ?></a>		
<?php
	}
?>			                        	
				                        </p>
				                    </div>
								</div>
							</div>
							<div class="col-6">
	                            <div class="row no-gutters">
	                                <div class="col-4">
	                                    <p><img class="featured-ads-section__icon" src="/images/location.png" alt="location" /></p>
	                                </div>
	                                <div class="col-8">
	                                    <span class="featured-ads-section__text">Location</span>
	                                    <p><a href="/car?make=<?php echo $trans_advert_model->make_cd; ?>&model=<?php echo $trans_advert_model->model_cd; ?>&location=<?php echo $trans_advert_model->location_cd; ?>" class="car-view__link"><?php echo $trans_advert_model->location_name; ?></a></p>
	                                </div>
	                            </div>
	                        </div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="row">
							<div class="col-6">
		                        <div class="row no-gutters">
		                            <div class="col-4">
		                                <p><img class="featured-ads-section__icon" src="/images/mileage.png" alt="mileage" /></p>
		                            </div>
		                            <div class="col-8">
		                                <span class="featured-ads-section__text">Mileage</span>
		                                <p><?php echo $trans_advert_model->mileage_name ? $trans_advert_model->mileage_name : '-'; ?></p>
		                            </div>
		                        </div>
	                    	</div>
	                    	<div class="col-6">
		                        <div class="row no-gutters">
		                            <div class="col-4">
		                                <p><img class="featured-ads-section__icon" src="/images/transmission.png" alt="transmission" /></p>
		                            </div>
		                            <div class="col-8">
		                                <span class="featured-ads-section__text">Transmission</span>
		                                <p><?php echo $trans_advert_model->transmission_name ? $trans_advert_model->transmission_name : '-'; ?></p>
		                            </div>
		                        </div>
	                    	</div>
						</div>
					</div>
				</div>
<?php
	$month_round = 0;
	$display_price = 'POA';
	$year_make = ((int)date("Y") - (int)$trans_advert_model->year_make);

	if (isset($trans_advert_super_deals_model) && date("Y-m-d H:i:s") >= $trans_advert_super_deals_model['super_deals_schedule_start_date'] && date("Y-m-d H:i:s") <= $trans_advert_super_deals_model['super_deals_schedule_end_date']) {

		if ($trans_advert_model->price != 0) {

			$display_price = number_format($trans_advert_super_deals_model['sale_price']); 

			$deposit = 10;
			$interest = 3.69;
			if ($trans_advert_model->is_used_flg == '2') {		
				$term = 9;
			} else {
				$term = 7;
			}
			$interestRate = $interest / 100;    
			$oriPrice = str_replace(",","", $trans_advert_model->price);
			$depositAmount = $oriPrice * ($deposit / 100);
			$loanPaidAmount = $oriPrice - $depositAmount; 
			$interestPaidAmount = ($term * $interestRate) * $loanPaidAmount;                            
			$month = ($interestPaidAmount + $loanPaidAmount) / ($term * 12);
			$month_round = number_format(round($month));
		}
?>			
				<div class="row">
					<div class="col-lg-4">
						<p class="car-view__price"><?php trim($pdata['voc_file']) ? 'VOC' : '' ?> RM </small> <span class="car-view__price-figure"><?php echo $display_price; ?></span></p></p>
						<p class="car-view__price-type">All inclusive price <img src="/images/info.png" alt="Information"></p>
<?php
		if ($trans_advert_super_deals_model['sale_price'] != $trans_advert_super_deals_model['regular_price']) { 			
?>										
						<p class="price-before-super-deals">RM <?php echo number_format($trans_advert_super_deals_model['regular_price']); ?></p>	
<?php
		} 			
?>					
					</div>
					<div class="col-lg-4">
						<p class="car-view__price"><small class="car-view__price-currency">RM</small> <span class="car-view__price-figure"><?php echo $month_round; ?></span> per month</p>
						<p class="car-view__price-type">As-low-as monthly payment</p>
					</div>
					<div class="col-lg-4">
						<p class="car-view__price"><small class="car-view__price-currency">RM</small> <span class="car-view__price-figure"><?php echo $pdata['roadtax_amount']; ?></span></p>
						<p class="car-view__price-type">Road tax per year</p>
					</div>
				</div>
<?php
	} else {
		if ($trans_advert_model->price != 0) {

			$display_price = number_format($trans_advert_model->price);
			
			$deposit = 10;
			$interest = 3.69;
			if ($trans_advert_model->is_used_flg == '2') {		
				$term = 9;
			} else {
				$term = 7;
			}
			$interestRate = $interest / 100;    
			$oriPrice = str_replace(",","", $trans_advert_model->price);
			$depositAmount = $oriPrice * ($deposit / 100);
			$loanPaidAmount = $oriPrice - $depositAmount; 
			$interestPaidAmount = ($term * $interestRate) * $loanPaidAmount;                            
			$month = ($interestPaidAmount + $loanPaidAmount) / ($term * 12);
			$month_round = number_format(round($month));
		}
?>
				<div class="row">
					<div class="col-lg-4">
						<p class="car-view__price"><small class="car-view__price-currency"> <?php trim($pdata['voc_file']) ? 'VOC' : '' ?> RM </small> <span class="car-view__price-figure"><?php echo $display_price; ?></span></p>
						<p class="car-view__price-type">All inclusive price <img src="/images/info.png" alt="Information"></p>
					</div>
					<div class="col-lg-4">
						<p class="car-view__price"><small class="car-view__price-currency">RM</small> <span class="car-view__price-figure"><?php echo $month_round; ?></span> per month</p>
						<p class="car-view__price-type">As-low-as monthly payment</p>
					</div>
					<div class="col-lg-4">
						<p class="car-view__price"><small class="car-view__price-currency">RM</small> <span class="car-view__price-figure"><?php echo $pdata['roadtax_amount']; ?></span></p>
						<p class="car-view__price-type">Road tax per year</p>
					</div>
				</div>
<?php
	}
?>
				<p class="text-right"><img src="/images/previous.png"> <img src="/images/next.png"></p>
				<div class="row">
<?php
		$count = 0;
		if ($pdata['car_video'] !== false)	{
?>
					<div class="col-3">
						<p><img src="/images/vid_ico.png" alt="Video" class="car-view__video" onmouseover="showCarousel('0');"></img></p>
					</div>
<?php
			foreach ($pdata['picture_array'] as $index_loop => $data_loop) {
?>					
					<div class="col-3">
						<p><img src="/images/ajax-loader.gif" data-src="<?php echo (@getimagesize($pdata['picture_array'][$index_loop]['url'])) ? $pdata['picture_array'][$index_loop]['url'] : $trans_advert_model->bodytype_code == 'bt9' ? '/images/bus.png' : '/images/mercedes.png'; ?>" alt="Car" class="car-view__image car-view__image--margin lazyload" onmouseover="showCarousel('<?php echo $index_loop; ?>');"></p>
					</div>
<?php
			}

		} else {
			foreach ($pdata['picture_array'] as $index_loop => $data_loop) {
?>					
					<div class="col-3">
						<p><img src="/images/ajax-loader.gif" data-src="<?php echo (@getimagesize($pdata['picture_array'][$index_loop]['url'])) ? $pdata['picture_array'][$index_loop]['url'] : $trans_advert_model->bodytype_code == 'bt9' ? '/images/bus.png' : '/images/mercedes.png'; ?>" class="car-view__image car-view__image--margin lazyload" onmouseover="showCarousel('<?php echo $index_loop - 1; ?>');"></p>
					</div>
<?php
			}
		}
?>
		
				</div>
			</div>
		</div>
	</div>
    <div class="car-view__fixed">
    	<div class="row no-gutters">
    		<div class="col-lg-7">
				<p>
					<a class="btn car-view__fixed-button">
						<img src="/images/bookmark.png">
					</a> 
					<a class="btn car-view__fixed-button">
						<img src="/images/share.png">
					</a> 
					<a class="btn car-view__fixed-button">
						<img src="/images/dualcar.png">
					</a> 
					<span class="car-view__fixed-text car-view__fixed-text--stylise">
						<?php echo $trans_advert_model->make_name . ' ' . $trans_advert_model->model_name; ?>
					</span>
				</p>
	    	</div>
	    	<div class="col-lg-5">
	    		<div class="row no-gutters">
	    			<div class="col-lg-4">
	    				<div class="car-view__fixed-price">
		    				<p class="car-view__fixed-text">
		    					<small class="car-view__fixed-text--position">RM </small>
		    					<span class="car-view__fixed-text--enlarge"><?php echo $display_price; ?></span></p>
		    				<p class="car-view__fixed-text car-view__fixed-text--resize">RM<?php echo $month_round; ?> per month</p>
	    				</div>
	    			</div>
	    			<div class="col-lg-8">
	    				<p>
		    				<button class="btn car-view__search-btn"><span class="car-view__search-btn-text">Contact</span></button>
							<button class="btn car-view__search-btn"><span class="car-view__search-btn-text">WhatsApp</span></button>
						</p>
	    			</div>
	    		</div>
	    	</div>
    	</div>
    </div>
	<div class="superdeals-featured-cnabadv superdeals-featured-cnabadv--background-color">
		<h1 class="banner-search__title banner-search__title--position text-center">Car details</h1>
		<div class="car-view__details">
			<div class="row">
				<div class="col-lg-6">
					<div class="row">
						<div class="col-6">
							<div class="row no-gutters">
								<div class="col-3">
									<p><img class="car-view__details-image" src="/images/caricon.png" alt="Car condition"></p>
								</div>
								<div class="col-9">
									<p><small>Condition</small></p>							
									<p><a href="/car?type=<?php echo $filter_type; ?>"><?php echo $pdata['condition'] ? $pdata['condition'] : '-'; ?></a></p>
								</div>
							</div>
						</div>
						<div class="col-6">
							<div class="row no-gutters">
								<div class="col-3">
									<p><img class="car-view__details-image" src="/images/registration.png" alt="Registration"></p>
								</div>
								<div class="col-9">
									<p><small>Registration</small></p>
									<p>
										<?php echo trim($trans_advert_model->year_reg) ? $trans_advert_model->year_reg : '-'; ?>
<?php
	if (trim($pdata['voc_file']) && $pdata['voc_approved_status'] == false) {
?>								
									 	<img src="/images/verified.png" alt="Verified"> Verified
<?php
	}
?>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="row">
						<div class="col-6">
							<div class="row no-gutters">
								<div class="col-3">
									<p><img class="car-view__details-image" src="/images/engine.png" alt="Engine"></p>
								</div>
								<div class="col-9">
									<p><small>Engine</small></p>
									<p><?php echo $trans_advert_model->engine_name ? $trans_advert_model->engine_name : '-'; ?></p>
								</div>
							</div>
						</div>
<?php
	if ($trans_advert_model->bodytype_name != 'carPost') {
		$filter_bodyType='';
			switch($trans_advert_model->bodytype_name){
				case 'Sedan':
					$filter_bodyType = 'bt1';
					break;
				case 'Wagon':
					$filter_bodyType = 'bt2';
					break;
				case 'Coupe':
					$filter_bodyType = 'bt3';
					break;
				case 'Convertible':
					$filter_bodyType = 'bt4';
					break;
				case 'Hatchback':
					$filter_bodyType = 'bt5';
					break;
				case 'MPV':
					$filter_bodyType = 'bt6';
					break;
				case 'SUV':
					$filter_bodyType = 'bt7';
					break;
				case 'Pickup':
					$filter_bodyType = 'bt8';
					break;
				case 'Commercial':
					$filter_bodyType = 'bt9';
					break;	
				case 'Others':
					$filter_bodyType = 'bt10';
					break;
			}		
?>					
						<div class="col-6">
							<div class="row no-gutters">
								<div class="col-3">
									<p><img class="car-view__details-image" src="/images/caricon.png" alt="Type"></p>
								</div>
								<div class="col-9">
									<p><small>Type</small></p>
									<p><a href="<?php echo Yii::$app->general->generatecarsseo('car',0, 0, '', 0, '', '', '', 0, ['bodyType' => $filter_bodyType]); ?>"><?php echo ($trans_advert_model->bodytype_name) ? $trans_advert_model->bodytype_name : '-'; ?></a></p>
								</div>
							</div>
						</div>
<?php
	}
?>
					</div>
				</div>
			</div>
		</div>
		<div class="car-view__details-highlights">
			<div class="row no-gutters">
				<div class="col-2 col-lg-1">
					<p><img class="car-view__details-image" src="/images/star.png" alt="Featured highlights"></p>
				</div>
				<div class="col-10 col-lg-11">
					<p><small>Featured highlights</small></p>
					<div class="car-view__highlights">
						<p><?php
							$description = '';

							$description .= nl2br(Yii::$app->general->censoredfilter($trans_advert_model->feature_desc));

							$censored_string = Yii::$app->general->censoredfilter($description, '-');

							$filterphone_string = Yii::$app->general->filterphone($censored_string, '<a href="javascript:void(0)" onclick="showphoneindetails(this)" class="showphoneindetails-pre" data-phone="__PHONE__">Click to Show Phone Number</a>');

							echo $filterphone_string;
						 ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="superdeals-featured-cnabadv">
		<div class="superdeals-featured-cnabadv-container">
			<h1 class="banner-search__title banner-search__title--position"><a href="<?php echo $pdata['dealer_seourl']; ?>"><?php echo $pdata['master_dealer_model'] !== false ? $pdata['master_dealer_model']->dealer_name : 'Private Dealer'; ?></a></h1>
			<p class="car-view__rating-list">
<?php 
	$master_dealer_review_model = $master_dealer_review->getdealerreviewinfo($trans_advert_model->dealer_id);
	if ($master_dealer_review_model) {
		if ($master_dealer_review_model->master_dealer_review_status == '1') {
			
			$count_master_dealer_review_info = $master_dealer_review_info->countdealerreviewinfo($master_dealer_review_model->dealer_id);

			$average_total_overall = $master_dealer_review_info->getaveragerating($master_dealer_review_model->dealer_id);

			switch ($average_total_overall) {
				case '1':
					echo '	<img src="/images/star.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 1.0 </span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '&type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>
						';
				break;
				case '1.5':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 1.5 </span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '%type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '2':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 2.0 </span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '&type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '2.5':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 2.5 </span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '&type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '3':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 3.0 </span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '&type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '3.5':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 3.5</span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '&type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '4':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 4.0</span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '&type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '4.5':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<span class="car-view__rating"> 4.5</span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '&type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '5':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<span class="car-view__rating"> 5.0</span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '&type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>
						';
				break;
				case '0':
					echo '	<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 0.0</span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '&type=nav-dealer-review">
								<span>No review yet</span>
							</a>';
				break;
			}
		}
	} else {
		echo '	<img src="/images/star-empty.png">
				<img src="/images/star-empty.png">
				<img src="/images/star-empty.png">
				<img src="/images/star-empty.png">
				<img src="/images/star-empty.png">
				<span class="car-view__rating"> 0.0</span>
				<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '&type=nav-dealer-review">
					<span>No review yet</span>
				</a>';
	}
?>
			</p>
			<div class="row">
				<div class="col-lg-7">
					<div class="car-view__dealer">
						<div class="row no-gutters">
							<div class="col-1">
								<p><img class="featured-ads-section__icon" src="/images/dealer.png"></p>
							</div>
							<div class="col-11">
								<p class="car-view__dealer-type"><?php echo $pdata['master_dealer_model'] !== false ? 'Dealer' : 'Private'; ?></p>
							</div>
						</div>
					</div>
<?php
	if ($pdata['master_dealer_model'] !== false) {
?>				
					<div class="car-view__dealer">
						<div class="row">
							<div class="col-1">
								<p><img class="featured-ads-section__icon" src="/images/location.png"></p>
							</div>
							<div class="col-11">
								<p class="car-view__dealer-type"><?php echo $pdata['master_dealer_model']->location_name; ?></p>
							</div>
						</div>
					</div>				
					<div class="car-view__dealer">
						<div class="row">
							<div class="col-1">
								<p><img class="featured-ads-section__icon" src="/images/caricon.png"></p>
							</div>
							<div class="col-11">
								<p class="car-view__dealer-type"><a class="car-view__link" href="<?php echo $pdata['dealer_seourl']; ?>">View all <?php echo $pdata['master_dealer_model']->total_stock; ?> cars / items</a></p>
							</div>
						</div>
					</div>
<?php
	}
?>
				</div>
				<div class="col-lg-5">
					<p class="text-center"><button class="btn car-view__search-btn car-view__search-btn--align"><span class="car-view__search-btn-text">Drive there</span></button> <button class="btn car-view__search-btn car-view__search-btn--red car-view__search-btn--align"><span class="car-view__search-btn-text">Contact</span></button></p>
				</div>
			</div>
		</div>	
	</div>
	<div class="superdeals-featured-cnabadv superdeals-featured-cnabadv--background-color superdeals-featured-cnabadv--padding">
		<div class="tab car-view__tab text-center">
            <button class="tablinks active" id="icon-box-desc">
                <span>Description</span>
            </button>
            <button class="tablinks" id="icon-box-loan">
                <span>Loan calculator</span>
            </button>
            <button class="tablinks" id="icon-box-voc">
                <span>View VOC</span>
            </button>
        </div>
        <div class="container">
        	<div class="car-view__tab-show" id="car-desc">	
        		<p>
<?php
	$description = '';

	if ($pdata['master_dealer_model'] !== false) {
		if (trim($trans_advert_model->qe_add_desc) != '') {
			$temp = Yii::$app->general->avanserreplace($pdata['master_dealer_model']->dealer_id, $trans_advert_model->qe_add_desc);
			$description .= nl2br(Yii::$app->general->censoredfilter((is_string($temp) ? $temp : $trans_advert_model->qe_add_desc)));
		}
		
		if (trim($trans_advert_model->additional_desc) != '' ) {
			$temp = Yii::$app->general->avanserreplace($pdata['master_dealer_model']->dealer_id, $trans_advert_model->additional_desc);
			$description .= nl2br(Yii::$app->general->censoredfilter(($temp != 0 ? $temp : $trans_advert_model->additional_desc)));
		}
		
		if (trim($trans_advert_model->feature_desc) != '') {
			$temp = Yii::$app->general->avanserreplace($pdata['master_dealer_model']->dealer_id, $trans_advert_model->feature_desc);
			$description .= nl2br(Yii::$app->general->censoredfilter(($temp != 0 ? $temp : $trans_advert_model->feature_desc)));
		}
	} else {

		if (trim($trans_advert_model->qe_add_desc) != '') {
			$description .= nl2br(Yii::$app->general->censoredfilter($trans_advert_model->qe_add_desc));
		}
		
		if (trim($trans_advert_model->additional_desc) != '' ) {
			$description .= nl2br(Yii::$app->general->censoredfilter($trans_advert_model->additional_desc));
		}
		
		if (trim($trans_advert_model->feature_desc) != '') {
			$description .= nl2br(Yii::$app->general->censoredfilter($trans_advert_model->feature_desc));
		}
	}

	$censored_string = Yii::$app->general->censoredfilter($description, '-', $matched_phone);

	if ($matched_phone !== false) {
		$matched_phone = Yii::$app->general->filterphonex($matched_phone);
	}

	$filterphone_string = Yii::$app->general->filterphone($censored_string, '<a href="javascript:void(0)" onclick="showphoneindetails(this)" class="showphoneindetails-pre" data-phone="__PHONE__">' . $matched_phone . '</a>');

	echo $filterphone_string;
?>
				 </p>
        	</div>
        </div>
        <div class="container">
        	<div class="d-none car-view__tab-show" id="car-view__loan">
<?php
	if ($year_make < 13 || $year_make == date("Y")) {		
		if ($trans_advert_model->price != 0) {
?>        	
	        	<form>
	        		<div class="form-group">
	        			<label for="calculator-price">Car Price (RM):</label>
	        			<input type="text" class="form-control car-view__calculate" id="calculator-price" placeholder="eg. 80000" value="<?php echo str_replace(",", "", $display_price); ?>" onKeyUp="calculate()">
	        		</div>
	        		 <div class="form-group">
		    			<label for="calculator-deposit">Deposit (RM):</label>
		    			<input type="text" class="form-control car-view__calculate" id="calculator-deposit" placeholder="eg. 8000" value="<?php echo $depositAmount; ?>" onKeyUp="calculate()">
	   				</div>
	   				 <div class="form-group">
		    			<label for="calculator-interest">Interest Rate (%):</label>
		    			<input type="text" class="form-control car-view__calculate" id="calculator-interest" placeholder="eg. 3.2" value="<?php echo $interest; ?>" onKeyUp="calculate()">
	   				</div>
	   				<div class="form-group">
	    				<label for="calculator-terms">Long Term (years):</label>
	    				<input type="text" class="form-control car-view__calculate" id="calculator-terms" placeholder="eg. 7" value="<?php echo $term; ?>" onKeyUp="calculate()">
	    			</div>
	        	</form>
	        	<div id="loan-calculator-right-panel">
	    			<div id="loan-calculator-right-inner">
	    				<h5>Car Loan monthly installment*</h5>
	   					<h3 id="calculator-result">RM <?php echo number_format($month, 2); ?></h3>
	   					<h4>/month</h4>
	    					<span id="loan-calculator-right-desc">The calculations given by the car loan calculator tool are only a guide.
	   						Please speak to an independent financial advisor for professional guidance</span>
					</div>
				</div>
<?php
		}
	}
?>
        	</div>			
        </div>
        <div class="container">
        	<div class="d-none car-view__tab-show" id="car-view__voc">
<?php
	if (trim($pdata['voc_file'])) {
?>        	
        	<p><img src="<?php echo (@getimagesize($pdata['voc_picture_array']['url'])) ? $pdata['voc_picture_array']['url'] : '/images/close.png' ; ?>" class="img img-fluid" alt="VOC image"></p>
<?php
	} else {
?>
			<p>There is no VOC data available</p>
<?php		
	}
?>
        	</div>
        </div>
	</div>
	<div class="superdeals-featured-cnabadv">
        <div class="superdeals-featured-cnabadv__box">
<?php
	foreach ($pdata['similar'] as $pdata_loop) {
		$min_max_price[] = $pdata_loop['price'];
	}
?>
            <h2 class="text-center banner-search__title banner-search__title--margin">Browse cars between RM<?php echo number_format(min($min_max_price)); ?> to RM<?php echo number_format(max($min_max_price)); ?></h2>
            <div class="row no-gutters">
<?php
	$count = 0;
	foreach ($pdata['similar'] as $featured_data_loop) {
		$count++;

		$trans_advert_super_deals_model = $super_deals->getsuperdeals($featured_data_loop['urn_no']);
			
		$has_super_deals = $trans_advert_super_deals_model['got_super_deals'];
		
		if ($featured_data_loop['poa'] == 1) {
			$price = 'POA';
		} else {
			$price = '<small class="featured-ads-section__price-currency">RM</small> ' . number_format($featured_data_loop['price']);
		}

		$deposit = 10;
		$interest = 3.69;
		if ($featured_data_loop['is_used_flg'] == '2') {		
			$term = 9;
		} else {
			$term = 7;
		}
		$interestRate = $interest / 100;    
		$oriPrice = str_replace(",","", number_format($featured_data_loop['price']));
		$depositAmount = $oriPrice * ($deposit / 100);
		$loanPaidAmount = $oriPrice - $depositAmount; 
		$interestPaidAmount = ($term * $interestRate) * $loanPaidAmount;                            
		$month = ($interestPaidAmount + $loanPaidAmount) / ($term * 12);
		$month = number_format(round($month));
?>            	
            	<div class="col-lg-4">
            		<div class="superdeals-featured-cnabadv__featured-ads-section-listing superdeals-featured-cnabadv__featured-ads-section-listing--center  margin-bottom-10 <?php echo $has_super_deals === '1' ? 'superdeals-featured-cnabadv__featured-ads-section-listing--border' : ''; ?>">
	                    <p class="text-center car-view__position">
	                    	<a href="<?php echo $featured_data_loop['seourl']; ?>&utm_medium=FeaturedAds_Detail">
	                    		<img class="featured-ads-section__img" src="<?php echo (@getimagesize($featured_data_loop['image'])) ? $featured_data_loop['image'] : $featured_data_loop['bodytype_code'] == 'bt9' ? '/images/bus.png' : '/images/mercedes.png'; ?>" alt="<?php echo addslashes($featured_data_loop['make_name'] . ' ' . $featured_data_loop['model_name']); ?>">
	                    	</a>
	                    	<span class="car-list__bookmark <?php echo (in_array($featured_data_loop['urn_no'], $pdata['bookmark_array']) ? 'car-list__show-bookmark' : '') ?>" data-urn="<?php echo $featured_data_loop['urn_no']; ?>"><img class="car-list__none" src="/images/bookmark.png"><img class="car-list__all" src="/images/bookmarked.png"></span>
	                    </p>
	                    <p class="text-center featured-ads-section__title">
	                    	<a href="<?php echo $featured_data_loop['seourl']; ?>&utm_medium=FeaturedAds_Detail"><?php echo $featured_data_loop['make_name'] . ' ' . $featured_data_loop['model_name']; ?></a>
	                    </p>
	                    <div class="row">
	                        <div class="col-4">
	                            <p><small>RM <?php echo $month; ?>/month</small></p>
	                        </div>
	                        <div class="col-8 text-right">
	                            <p class="featured-ads-section__price"><?php echo $price; ?></p>
	                        </div>
	                    </div>
	                    <hr/>
	                    <div class="row">
	                        <div class="col-6">
	                            <div class="row">
	                                <div class="col-4">
	                                    <p><img class="featured-ads-section__icon" src="/images/calendar.png" alt="calendar" /></p>
	                                </div>
	                                <div class="col-8">
	                                    <span class="featured-ads-section__text">Year</span>
	                                    <p><?php echo $featured_data_loop['year_make']; ?></p>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="col-6">
	                            <div class="row">
	                                <div class="col-4">
	                                    <p><img class="featured-ads-section__icon" src="/images/location.png" alt="location" /></p>
	                                </div>
	                                <div class="col-8">
	                                    <span class="featured-ads-section__text">Location</span>
	                                    <p><?php echo $featured_data_loop['location_name']; ?></p>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class="col-6">
	                            <div class="row">
	                                <div class="col-4">
	                                    <p><img class="featured-ads-section__icon" src="/images/mileage.png" alt="mileage" /></p>
	                                </div>
	                                <div class="col-8">
	                                    <span class="featured-ads-section__text">Mileage</span>
	                                    <p><?php echo $featured_data_loop['mileage_name']; ?></p>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="col-6">
	                            <div class="row">
	                                <div class="col-4">
	                                    <p><img class="featured-ads-section__icon" src="/images/transmission.png" alt="location" /></p>
	                                </div>
	                                <div class="col-8">
	                                    <span class="featured-ads-section__text">Transmission</span>
	                                    <p><?php echo $featured_data_loop['transmission_name']; ?></p>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
            		</div>
                </div>
<?php
	}
?>               
          	</div>
        </div>
        <p class="text-center car-view__link-outer"><a href="/car" class="car-view__link">View all</a></p>
    </div>
<?php
	$dissuitableapcmmadvert = $trans_advert_model->displaysuitableapcmm($trans_advert_model->make_cd, $trans_advert_model->model_cd);

	if (isset($dissuitableapcmmadvert) && count($dissuitableapcmmadvert)) {
?>
    <div class="superdeals-featured-cnabadv superdeals-featured-cnabadv--background-color">
        <div class="superdeals-featured-cnabadv__box">
            <h2 class="text-center banner-search__title banner-search__title--margin"><a href="<?php echo '/autopart?make=' . $trans_advert_model->make_cd . '&model=' . $trans_advert_model->model_cd; ?>">Accessories for <?php echo $trans_advert_model->make_name . ' ' . $trans_advert_model->model_name; ?></a></h2>
            <div class="row no-gutters">
<?php
	$count = 0;
	foreach ($dissuitableapcmmadvert as $featured_data_loop) {
		$count++;
		
		if ($featured_data_loop['poa'] == 1) {
			$price = 'POA';
		} else {
			$price = '<small class="featured-ads-section__price-currency">RM</small> ' . number_format($featured_data_loop['price']);
		}
?>            	
            	<div class="col-lg-4">
            		<div class="superdeals-featured-cnabadv__featured-ads-section-listing superdeals-featured-cnabadv__featured-ads-section-listing--center">
	                    <p class="text-center car-view__position">
	                    	<a href="<?php echo $featured_data_loop['seourl']; ?>&utm_medium=FeaturedAds_Detail">
	                    		<img class="featured-ads-section__img" src="<?php echo (@getimagesize($featured_data_loop['image'])) ? $featured_data_loop['image'] : '/images/autopart.png'; ?>" alt="<?php echo addslashes($featured_data_loop['make_name']); ?>">
	                    	</a>
	                    	<span class="car-list__bookmark <?php echo (in_array($featured_data_loop['urn_no'], $pdata['bookmark_array']) ? 'car-list__show-bookmark' : '') ?>" data-urn="<?php echo $featured_data_loop['urn_no']; ?>"><img class="car-list__none" src="/images/bookmark.png"><img class="car-list__all" src="/images/bookmarked.png"></span>
	                    </p>
	                    <p class="text-center featured-ads-section__title"><a href="<?php echo $featured_data_loop['seourl']; ?>&utm_medium=FeaturedAds_Detail"><?php echo $featured_data_loop['make_name']; ?></a></p>
	                    <p class="featured-ads-section__price text-right"><?php echo $price; ?></p>
	                    <hr/>
	                    <div class="row no-gutters">
	                        <div class="col-2">
	                            <p><img class="featured-ads-section__icon" src="/images/location.png" alt="location" /></p>
	                        </div>
	                        <div class="col-10">
	                            <span class="featured-ads-section__text">Location</span>
	                            <p><?php echo $featured_data_loop['location_name']; ?></p>
	                        </div>
	                    </div>
	                    <div class="row no-gutters">
	                        <div class="col-2">
	                            <p><img class="featured-ads-section__icon" src="/images/autoparticon.png" alt="category" /></p>
	                        </div>
	                        <div class="col-10">
	                            <span class="featured-ads-section__text">Category</span>
<?php
		$autoparts_category_array = $trans_statistic_count->listcount('autoparts_rank');
		if ($autoparts_category_array !== false && count($autoparts_category_array)) {

			foreach ($autoparts_category_array as $autoparts_category_array_name_loop => $autoparts_category_array_count_loop) {
				if ($autoparts_category_array_count_loop > 0) {
					if (preg_match('%' . $autoparts_category_array_name_loop . '%', $featured_data_loop['model_name'])) {
?>
									<p><?php echo $autoparts_category_array_name_loop; ?></p>
<?php
					}
				}
			}
		}
?>
	                        </div>
	                    </div>
            		</div>
                </div>
<?php
	}
?>	                
          	</div>
        </div>
        <p class="text-center car-view__link-outer"><a href="<?php echo Yii::$app->general->generatecarsseo('autopart', 0, 0, '', 0, '', '', '', 0, ['make' => $trans_advert_model->make_cd, 'model' => $trans_advert_model->model_cd]); ?>" class="car-view__link">View all</a></p>
    </div>
<?php
	}
?>
</div>