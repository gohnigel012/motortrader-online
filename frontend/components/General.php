<?php
namespace frontend\components;
use Yii;
use yii\base\Component;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\web\UploadedFile;
use yii\helpers\Url;
use linslin\yii2\curl;
use app\models\MasterDealer;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class General extends Component {
    public $domain = 'https://www.motortrader.com.my';
    public $recent_size = 5;
    public $s3_flag = '0';
    public $s3_domain = '';
    public $advert;
    public $compile_css = '0';

    public function getuserid() {
        if (isset($_SESSION['user_id']) && trim($_SESSION['user_id'])) {
            return $_SESSION['user_id'];
        } else {
            return false;
        }
    }

    public function setuserid($user_id, $user_name = '') {
        $_SESSION['user_id'] = $user_id;
        $_SESSION['user_name'] = $user_name;

        if (trim($_SESSION['user_name']) == '') {
            $_SESSION['user_name'] = $_SESSION['user_id'];
        }
    }

    public function setasus($asus) {
        $_SESSION['asus'] = $asus;
    }

    public function getasus() {
        if (isset($_SESSION['asus']) && trim($_SESSION['asus'])) {
            return $_SESSION['asus'];
        } else {
            return false;
        }
    }

    public static function slugify($text) {
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        // trim
        $text = trim($text, '-');
        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        // lowercase
        $text = strtolower($text);
        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
        
        if (empty($text)) {
            return 'n-a';
        }
        return $text;
    }

    public function generateseo($type, $refer_id, $seo_string='', $subpage='') {
        
        $seo_url = $this->domain;
        
        $processed_seo_string = $this->slugify($seo_string);

        $master_dealer = new MasterDealer();

        switch ($type) {
            case 'dealer':
                if ($subpage == 'all') {
                    $subpage = '';
                }
                
                $master_dealer_model = $master_dealer->getDealer($refer_id);
                if ($master_dealer_model) {
                    // if ($master_dealer_model->master_dealer_url != '') {
                    //     $seo_url .= "/" . $master_dealer_model->master_dealer_url . "/" . (trim($subpage) ? $subpage . '/': '');
                    //     // $seo_url .= "/dealer/" . $refer_id . "/" . $processed_seo_string . "/" . (trim($subpage) ? $subpage . '/': '') . "index.html";
                    // } else {
                        $seo_url .= "/dealer/view?dealer_id=" . $refer_id . "&dealer_name=" .  $processed_seo_string .(trim($subpage) ? $subpage . '/': '') ;
                    // }
                } else {
                    $seo_url .= "/dealer/view?dealer_id=" . $refer_id . "&dealer_name=" . $processed_seo_string .(trim($subpage) ? $subpage . '/': '');
                }
                
                break;
            case 'interactive':
                if ($subpage == 'all') {
                    $subpage = '';
                }

                $seo_url .= "/dealer/interactive" . (trim($subpage) ? $subpage . '/': '');
                break;
            case 'brand':
                $seo_url .= "/car?make=" . $refer_id;
                break;
             case 'brand_a':
                $seo_url .= "/autopart?make=" . $refer_id;
                break;
            case 'car':
                $seo_url .= "/car/view?urn_no=" . $refer_id . "&car_name=" . $processed_seo_string . (trim($subpage) ? $subpage . '/': '');
                break;
            case 'numberplate':
                $seo_url .= "/numberplate/view?urn_no=" . $refer_id . "&number_plate=" . $processed_seo_string . (trim($subpage) ? $subpage . '/': '');
                break;
            case 'carpart':
            case 'autopart':
            case 'carparts':
            case 'autoparts':
                $seo_url .= "/autopart/view?urn_no=" . $refer_id . "&auto_part=" . $processed_seo_string . (trim($subpage) ? $subpage . '/': '');
                break;
            case 'bike':
                $seo_url .= "/bike/view?urn_no=" . $refer_id . "&bike_name=" . $processed_seo_string . (trim($subpage) ? $subpage . '/': '');
                break;
             case 'specialnumber':
                $seo_url .= "/specialnumber/view?urn_no=" . $refer_id . "&phone_number=" . $processed_seo_string . (trim($subpage) ? $subpage . '/': '');
                break;
        }

        return $seo_url;
    }

    public function generatecarsseo($route, $trusted = 0, $gookantei = 0, $location = '', $page = 0, $make = '', $model = '', $variant = '', $superdeals = 0, $others_params=[]) {

        $_location = $location;
        if ($location != '') {

            if (isset($others_params['location'])) unset($others_params['location']);
            
            $location_seo_array = $this->locationseoname();
            if (isset($location_seo_array)) {
                foreach ($location_seo_array as $location_seo_cd => $location_seo_name) {
                    if($location == $location_seo_name){
                        $_location = $location_seo_cd;
                    }
                }
                
            } else {
                if (!in_array($_location, ['kuala_lumpur', 'selangor', 'negeri_sembilan', 'melaka', 'pahang', 'johor', 'kedah', 'kelantan', 'perak', 'perlis', 'penang', 'pulau_pinang', 'terengganu', 'sabah', 'sarawak'])) {
                    $_location = '';
                }
            }
        }

        $route = Yii::$app->urlManager->createAbsoluteUrl('') . ltrim($route, '/');
        
        if (count($others_params)) {
            $others_params = Yii::$app->general->filterurlparams($others_params);
        }

        if (trim($_location)) {
            $route .= '?location=' . $_location;

            if (count($others_params)) {
                unset($others_params['page']);
                $route .= '&' . http_build_query($others_params);
            }
        } else if ($page) {
            $route .= '?page=' . $page;

            if (count($others_params)) {
                unset($others_params['page']);
                $route .= '&' . http_build_query($others_params);
            }
        } else if (trim($make)) {
            if (isset($others_params['make'])) unset($others_params['make']);
            $route .= '?make=' . $make;

            if (count($others_params)) {
                unset($others_params['page']);
                $route .= '&' . http_build_query($others_params);
            }
        } else if (trim($model)) {
            if (isset($others_params['model'])) unset($others_params['model']);
            $route .= '?model=' . $model;
        } else if (trim($variant)) {
            $route .= '?variant=' . $variant;
        } else if ($superdeals) {
            $route .= '?superdeals=' . $superdeals;
        } else if (count($others_params)) {
            $route .= '?' . http_build_query($others_params);
        }
        
        return $route;
    }

    public function advertimageurl($image_name, $type='wmfull', $lastdate='', $is_new=0) {
        if ($is_new == 1) {
            
            $temp_image_name = $image_name;
            
            if (in_array($type, ['wmfull', 'wmoriginal'])) {
                $target_folder = 'wmoriginal';
                $target_image = mb_substr($temp_image_name, 0, 4) . '/' . $temp_image_name;
            } else if (in_array($type, ['small', 'thumb'])) {
                $target_folder = 'small';
                $extension_pos = strrpos($copy_picture_file, '.');
                $thumb = substr($temp_image_name, 0, $extension_pos) . '_thumb' . substr($temp_image_name, $extension_pos);
                $target_image = mb_substr($temp_image_name, 0, 4) . '/' . $thumb;
            } else if (in_array($type, ['medium', 'webad'])) {
                $target_folder = 'medium';
                $target_image = mb_substr($temp_image_name, 0, 4) . '/' . $temp_image_name;
            } else if (in_array($type, ['full', 'original'])) {
                $target_folder = 'original';
                $target_image = mb_substr($temp_image_name, 0, 4) . '/' . $temp_image_name;
            }
            $display_image_name = rtrim($this->s3_domain) . '/photos/' . $target_folder . '/' . $target_image . '?' . $lastdate;
        } else {
            $type = 'wmfull';
            if (in_array($type, ['wmfull', 'wmoriginal'])) {
                $type = 'wmfull';
            } else if (in_array($type, ['small', 'thumb'])) {
                $type = 'thumb';
            } else if (in_array($type, ['medium', 'webad'])) {
                $type = 'webad';
            } else if (in_array($type, ['full', 'original'])) {
                $type = 'full';
            }
            $display_image_name = rtrim($this->s3_domain) . '/advert/' . $type . '/' . $image_name . '?' . $lastdate;
        }
        return $display_image_name;
    }

    public function advertimagecerturl($image_name, $urn_no, $type='carcert', $lastdate='') {
        
        if ($this->s3_flag == '1') {

            $display_image_name = rtrim($this->s3_domain) . '/' . $type . '/' . $urn_no . '/' . $image_name . '?' . $lastdate;
            
            // $display_image_name = rtrim($this->s3_domain) . '/advert/full/' . $image_name . '?' . $upd_time;
        } else {
            if (trim($lastdate) == '') {
                $lastdate = date('YmW');
            }
            
            $display_image_name = Yii::$app->fpath->mountimages['path'] . '/advert/' . $type . '/' . $image_name;
            if (file_exists($display_image_name)) {
                return Yii::$app->fpath->mountimages['domain'] . '/advertimage/' . $lastdate . '/' . $type . '/' . $image_name;
            } else {
                return 'https://s3.ap-south-1.amazonaws.com/take-my-order/default/missing.png';
            }
        }
        
        return $display_image_name;
    }

    public function advertvideourl($video_name, $type='video') {
        
        if ($this->s3_flag == '1') {

            $display_video_url = rtrim($this->s3_domain) . '/' . $type . '/' . $video_name;
            
            // $display_video_url = rtrim($this->s3_domain) . '/advert/full/' . $video_name . '?' . $upd_time;
        } else {
            if (trim($lastdate) == '') {
                $lastdate = date('YmW');
            }
            
            $display_video_url = Yii::$app->fpath->mountimages['path'] . '/advert/' . $type . '/' . $video_name;
            if (file_exists($display_video_url)) {
                return Yii::$app->fpath->mountimages['domain'] . '/advertimage/' . $lastdate . '/' . $type . '/' . $video_name;
            } else {
                return 'https://s3.ap-south-1.amazonaws.com/take-my-order/default/missing.png';
            }
        }
        
        return $display_video_url;
    }

    public function avanser() {
        return [];
        $beta_dealer = [];
        
        $beta_dealer = [
                    'T1859' => [
                                    'dealer_name' => 'KEONG HENG AUTO SDN BHD',
                                    'avanserPhone' => '011-17225636'
                                ],
                    'T23431' => [
                                    'dealer_name' => 'WELLCAR AUTO SDN BHD',
                                    'avanserPhone' => '011-17227009'
                                ],
                    // 'T26243C001' => array(
                    //     'dealer_name' => 'SYARIKAT WANA MOTORS SDN BHD',
                    //     'avanserPhone' => '011-17227006'
                    // ),
                    // 'T8964' => array(
                    //     'dealer_name' => 'HO YEE AUTO SDN BHD',
                    //     'avanserPhone' => '011-17227008'
                    // ),
                    // 'T1145' => array(
                    //     'dealer_name' => 'AUTO EXTREME SDN BHD',
                    //     'avanserPhone' => '011-17226180'
                    // ),
                    'T27523' => [
                                    'dealer_name' => 'KNT MOTORS SDN BHD',
                                    'avanserPhone' => '011-17227018'
                                ],
                    'T27693' => [
                                    'dealer_name' => 'PW CAR COMMERCE SDN BHD',
                                    'avanserPhone' => '011-17227010'
                                ],
                    // 'T26387' => array(
                    //     'dealer_name' => 'SYARIKAT WANA MOTORS SDN BHD - OLD KLANG ROAD BRANCH',
                    //     'avanserPhone' => '011-17227020'
                    // ),
                    // 'T1203' => array(
                    //     'dealer_name' => 'BEAU CAR STATION SDN BHD',
                    //     'avanserPhone' => '011-17227030'
                    // ),
                    'T21992' => [
                                    'dealer_name' => 'AMIRA VENTURES SDN BHD',
                                    'avanserPhone' => '011-17227050'
                                ],
                    // 'T2937' => array(
                    //     'dealer_name' => 'ZUKIS MOTOR SDN BHD',
                    //     'avanserPhone' => '011-17227060'
                    // ),
                    'T28545' => [
                                    'dealer_name' => 'MAX GAIN CONCEPTS SDN BHD',
                                    'avanserPhone' => '011-17226900'
                                ],
                    // 'T24130' => array(
                    //     'dealer_name' => 'HAMZA MOTORS SDN BHD',
                    //     'avanserPhone' => '011-17226213'
                    // ),
                    'T1936' => [
                                    'dealer_name' => 'LS MOTOR',
                                    'avanserPhone' => '011-17227605'
                                ],
                    'T10026' => [
                                    'dealer_name' => 'SMARTS-S AUTO TRADING SDN BHD',
                                    'avanserPhone' => '01117225247'
                                ],
                    // 'T2469' => array(
                    //     'dealer_name' => 'STAR CITY AUTO SDN BHD',
                    //     'avanserPhone' => '01117225650'
                    // ),
                    'T29250' => [
                                    'dealer_name' => 'DOMINATING CYG',
                                    'avanserPhone' => '01117226114'
                                ],
                    'MT0032073' => [
                                        'dealer_name' => 'xxxxxxxxxx',
                                        'avanserPhone' => 'XXXXXXXXXX'
                                    ],
                    'MT0032096' => [
                                        'dealer_name' => 'xxxxxxxxxx',
                                        'avanserPhone' => 'XXXXXXXXXX'
                                    ],
                ];
            
        return $beta_dealer;
    }

    public function avanserreplace($dealer_id, $description) {
        $beta_dealer = $this->avanser();
        if (isset($beta_dealer[$dealer_id])) {
            $pattern = [
                            '/\d{3}\s\d{3}\s\d{4}/',
                            '/\d{3}\s\-\s\d{3}\s\d{4}/',
                            '/\d{3}-\d{7}/',
                            '/\d{3}-\d{8}/',
                            '/\d{3}-\d{8,10}/',
                            '/\d{10}/',
                            '/\d{3}-\d{3}\s\d{4}/',
                            '/\d{3}\s\d{7}/',
                            '/\d{3}\s\d{4}\s\d{3}/',
                        ];
            // $pattern = '/\d{3}\s\d{3}\s\d{4}|\d{3}-\d{7}|\d{10}|\d{3}-\d{3}\s\d{4}|\d{3}\s\d{7}|\d{3}\s\d{4}\s\d{3}/';
            $avanser = $beta_dealer[$dealer_id];
            if (is_array($avanser)) {
                return preg_replace($pattern, $avanser['avanserPhone'], $description);
            }
        }

        return $description;
    }

    public function censoredfilter($word, $replace_to = '-', &$phone = false) {
        $target_replace_from_array = ['carlist', 'car-list', '01117557693', '011-17557693', '0107158610', '0107158910'];
        $target_replace_to_array = [];

        foreach ($target_replace_from_array as $target_replace_from_data_loop) {
            $target_replace_to_array[] = str_pad("", strlen($target_replace_from_data_loop), $replace_to);
        }
        
        $response = str_ireplace($target_replace_from_array, $target_replace_to_array, $word);

        $pattern = array(
            '/\d{10}/',
            '/\d{4}-\d{8,10}/',
            '/\d{3}\s\d{3}\s\d{4}/',
            '/\d{3}\s\-\s\d{3}\s\d{4}/',
            '/\d{2}\s\d{2}\-\d{3}\s\d{4}/',
            '/\d{3}-\d{8,10}/',
            '/\d{3}-\d{7}/',
            '/\d{3}-\s\d{3}\s\d{4}/',
            '/\d{3}-\d{3}\s\d{4}/',
            '/\d{3}-\d{4}\s\d{3}/',
            '/\d{3}\s\d{7}/',
            '/\d{3}\s\d{4}\s\d{3}/',
        );

        foreach ($pattern as $pattern_data_loop) {
            if (preg_match($pattern_data_loop, $response, $matched)) {
                if (count($matched)) {
                    $phone = $matched[0];
                }

                break;
            }
        }

        return $response;
    }

    public function filterphonex($phone) {
        $phone1 = substr($phone, 0, 3);
        $phone2 = substr($phone, 3, -3);
        $phone2 = preg_replace('/[0-9]/i', 'X', $phone2);
        $phone3 = substr($phone, -3);
        return $phone1 . $phone2 . $phone3;
    }

    public function location() {
        return [    '' => 'All Location',
                    'L00004' => 'Johor',
                    'L00014' => 'Kedah',
                    'L00007' => 'Kelantan',
                    'L00001' => 'Kuala Lumpur',
                    'L00005' => 'Melaka',
                    'L00015' => 'Negeri Sembilan',
                    'L00009' => 'Pahang',
                    'L00008' => 'Penang',
                    'L00006' => 'Perak',
                    'L00011' => 'Perlis',
                    // 'L00002' => 'Petaling Jaya',
                    'L00012' => 'Sabah',
                    'L00013' => 'Sarawak',
                    'L00003' => 'Selangor',
                    'L00010' => 'Terengganu',
                ];
    }

    public function locationseoname() {
        return [   'L00004' => 'johor',
                    'L00014' => 'kedah',
                    'L00007' => 'kelantan',
                    'L00001' => 'kuala_lumpur',
                    'L00005' => 'melaka',
                    'L00015' => 'negeri_sembilan',
                    'L00009' => 'pahang',
                    'L00008' => 'penang',
                    'L00006' => 'perak',
                    'L00011' => 'perlis',
                    // 'L00002' => 'Petaling Jaya',
                    'L00012' => 'sabah',
                    'L00013' => 'sarawak',
                    'L00003' => 'selangor',
                    'L00010' => 'terengganu',
                ];
    }

     public function advtorecent($type, $urn) {
        if (in_array($type, ['car', 'numberplate', 'autopart', 'bike', 'special-phone-number'])) {
            if (!Yii::$app->fcookie->hasCookie('recent_' . $type)) {
                Yii::$app->fcookie->setCookie('recent_' . $type, json_encode([]));
            }
            
            $current_urn_cookie = Yii::$app->fcookie->getCookie('recent_' . $type);
            $current_urn_array = json_decode($current_urn_cookie, 1);
            if (!is_array($current_urn_array)) {
                $current_urn_array = [];
            }
            
            $pos = array_search($urn, $current_urn_array);
            if (is_numeric($pos) && isset($current_urn_array[$pos])) {
                unset($current_urn_array[$pos]);
            }

            if (!in_array($urn, $current_urn_array)) {
                array_unshift($current_urn_array, $urn);
            }
            
            if (count($current_urn_array) > $this->recent_size) {
                $current_urn_array = array_slice($current_urn_array, 0, $this->recent_size);
            }

            Yii::$app->fcookie->setCookie('recent_' . $type, json_encode($current_urn_array));

            return true;
        }

        return false;
    }

    public function advfromrecent($type) {
        $response_array = [];

        if (in_array($type, ['car', 'numberplate', 'autopart', 'bike', 'special-phone-number'])) {
            if (!Yii::$app->fcookie->hasCookie('recent_' . $type)) {
                return $response_array;
            }
            
            $current_urn_cookie = Yii::$app->fcookie->getCookie('recent_' . $type);
            $response_array = json_decode($current_urn_cookie, 1);
            if (!is_array($response_array)) {
                $response_array = [];
            }
        }

        if (count($response_array) > $this->recent_size) {
            $response_array = array_slice($response_array, 0, $this->recent_size);
        }

        return $response_array;
    }

    public function advtobookmark($urn) {
        if (!Yii::$app->fcookie->hasCookie('bookmark')) {
            Yii::$app->fcookie->setCookie('bookmark', json_encode([]));
        }
        
        $current_urn_cookie = Yii::$app->fcookie->getCookie('bookmark');
        $current_urn_array = json_decode($current_urn_cookie, 1);
        if (!is_array($current_urn_array)) {
            $current_urn_array = [];
        }

        if (!in_array($urn, $current_urn_array)) {
            array_unshift($current_urn_array, $urn);
        }
        
        if (count($current_urn_array) > $this->recent_size) {
            array_slice($current_urn_array, $this->recent_size);
        }

        Yii::$app->fcookie->setCookie('bookmark', json_encode($current_urn_array));

        return true;
    }

    public function advfrombookmark() {
        $response_array = [];

        if (!Yii::$app->fcookie->hasCookie('bookmark')) {
            return $response_array;
        }
        
        $current_urn_cookie = Yii::$app->fcookie->getCookie('bookmark');
        $response_array = json_decode($current_urn_cookie, 1);
        if (!is_array($response_array)) {
            $response_array = [];
        }

        return $response_array;
    }

    public function advbookmarkremove($urn) {
        if (!Yii::$app->fcookie->hasCookie('bookmark')) {
            return false;
        }
        
        $current_urn_cookie = Yii::$app->fcookie->getCookie('bookmark');
        $current_urn_array = json_decode($current_urn_cookie, 1);
        if (!is_array($current_urn_array)) {
            return false;
        }
        
        $pos = array_search($urn, $current_urn_array);
        if (isset($current_urn_array[$pos])) {
            unset($current_urn_array[$pos]);
        }

        Yii::$app->fcookie->setCookie('bookmark', json_encode($current_urn_array));
        
        return true;
    }

    public function advfromcompare() {
        $response_array = [];

        if (!Yii::$app->fcookie->hasCookie('compare')) {
            return $response_array;
        }
        
        $current_urn_cookie = Yii::$app->fcookie->getCookie('compare');
        $response_array = json_decode($current_urn_cookie, 1);
        if (!is_array($response_array)) {
            $response_array = [];
        }

        return $response_array;
    }

    public function advcompareremove($urn) {
        if (!Yii::$app->fcookie->hasCookie('compare')) {
            return false;
        }
        
        $current_urn_cookie = Yii::$app->fcookie->getCookie('compare');
        $current_urn_array = json_decode($current_urn_cookie, 1);
        if (!is_array($current_urn_array)) {
            return false;
        }
        
        $pos = array_search($urn, $current_urn_array);
        if (isset($current_urn_array[$pos])) {
            unset($current_urn_array[$pos]);
        }

        Yii::$app->fcookie->setCookie('compare', json_encode($current_urn_array));
        
        return true;
    }

    public function filterurlparams($url_array) {
        if (count($url_array)) {
            if (isset($url_array['page']) && $url_array['page'] == '') {
                unset($url_array['page']);
            }
            if (isset($url_array['condition']) && $url_array['condition'] == '') {
                unset($url_array['condition']);
            }
            if (isset($url_array['minPrice']) && $url_array['minPrice'] == '') {
                unset($url_array['minPrice']);
            }
            if (isset($url_array['maxPrice']) && $url_array['maxPrice'] == '') {
                unset($url_array['maxPrice']);
            }
            if (isset($url_array['minYear']) && $url_array['minYear'] == '') {
                unset($url_array['minYear']);
            }
            if (isset($url_array['maxYear']) && $url_array['maxYear'] == '') {
                unset($url_array['maxYear']);
            }
            if (isset($url_array['location']) && $url_array['location'] == '') {
                unset($url_array['location']);
            }
            if (isset($url_array['make']) && $url_array['make'] == '') {
                unset($url_array['make']);
            }
            if (isset($url_array['model']) && $url_array['model'] == '') {
                unset($url_array['model']);
            }
            if (isset($url_array['type']) && $url_array['type'] == '') {
                unset($url_array['type']);
            }
            if (isset($url_array['keyword']) && $url_array['keyword'] == '') {
                unset($url_array['keyword']);
            }
            if (isset($url_array['bodyType']) && $url_array['bodyType'] == '') {
                unset($url_array['bodyType']);
            }
            if (isset($url_array['cityname']) && $url_array['cityname'] == '') {
                unset($url_array['cityname']);
            }
            if (isset($url_array['mileage']) && $url_array['mileage'] == '') {
                unset($url_array['mileage']);
            }
            if (isset($url_array['transmission']) && $url_array['transmission'] == '') {
                unset($url_array['transmission']);
            }
            if (isset($url_array['color']) && $url_array['color'] == '') {
                unset($url_array['color']);
            }
        }
        
        return $url_array;
    }

    public function filterPriceRange($acs = true) {
        $price_range_array = [];
        if ($acs == true) {
            $price_range_array['5000'] = 'RM5,000';
            $price_range_array['10000'] = 'RM10,000';
            $price_range_array['20000'] = 'RM20,000';
            $price_range_array['30000'] = 'RM30,000';
            $price_range_array['40000'] = 'RM40,000';
            $price_range_array['50000'] = 'RM50,000';
            $price_range_array['60000'] = 'RM60,000';
            $price_range_array['70000'] = 'RM70,000';
            $price_range_array['80000'] = 'RM80,000';
            $price_range_array['90000'] = 'RM90,000';
            $price_range_array['100000'] = 'RM100,000';
            $price_range_array['110000'] = 'RM110,000';
            $price_range_array['120000'] = 'RM120,000';
            $price_range_array['130000'] = 'RM130,000';
            $price_range_array['140000'] = 'RM140,000';
            $price_range_array['150000'] = 'RM150,000';
            $price_range_array['160000'] = 'RM160,000';
            $price_range_array['170000'] = 'RM170,000';
            $price_range_array['180000'] = 'RM180,000';
            $price_range_array['190000'] = 'RM190,000';
            $price_range_array['200000'] = 'RM200,000';
            $price_range_array['210000'] = 'RM210,000';
            $price_range_array['220000'] = 'RM220,000';
            $price_range_array['230000'] = 'RM230,000';
            $price_range_array['240000'] = 'RM240,000';
            $price_range_array['250000'] = 'RM250,000';
            $price_range_array['260000'] = 'RM260,000';
            $price_range_array['270000'] = 'RM270,000';
            $price_range_array['280000'] = 'RM280,000';
            $price_range_array['290000'] = 'RM290,000';
            $price_range_array['300000'] = 'RM300,000';
            $price_range_array['350000'] = 'RM350,000';
            $price_range_array['400000'] = 'RM400,000';
        } else {
            $price_range_array['5000'] = 'RM5,000';
            $price_range_array['10000'] = 'RM10,000';
            $price_range_array['20000'] = 'RM20,000';
            $price_range_array['30000'] = 'RM30,000';
            $price_range_array['40000'] = 'RM40,000';
            $price_range_array['50000'] = 'RM50,000';
            $price_range_array['60000'] = 'RM60,000';
            $price_range_array['70000'] = 'RM70,000';
            $price_range_array['80000'] = 'RM80,000';
            $price_range_array['90000'] = 'RM90,000';
            $price_range_array['100000'] = 'RM100,000';
            $price_range_array['110000'] = 'RM110,000';
            $price_range_array['120000'] = 'RM120,000';
            $price_range_array['130000'] = 'RM130,000';
            $price_range_array['140000'] = 'RM140,000';
            $price_range_array['150000'] = 'RM150,000';
            $price_range_array['160000'] = 'RM160,000';
            $price_range_array['170000'] = 'RM170,000';
            $price_range_array['180000'] = 'RM180,000';
            $price_range_array['190000'] = 'RM190,000';
            $price_range_array['200000'] = 'RM200,000';
            $price_range_array['210000'] = 'RM210,000';
            $price_range_array['220000'] = 'RM220,000';
            $price_range_array['230000'] = 'RM230,000';
            $price_range_array['240000'] = 'RM240,000';
            $price_range_array['250000'] = 'RM250,000';
            $price_range_array['260000'] = 'RM260,000';
            $price_range_array['270000'] = 'RM270,000';
            $price_range_array['280000'] = 'RM280,000';
            $price_range_array['290000'] = 'RM290,000';
            $price_range_array['300000'] = 'RM300,000';
            $price_range_array['350000'] = 'RM350,000';
            $price_range_array['400000'] = 'RM400,000';
        }
        return $price_range_array;
    }

    public function filterYearRange($acs = true) {
        $year_range_array = [];
        $current_year = $count_year=date("Y");
        if ($acs == true) {
            for ($count_year=30;$count_year>0;$count_year--) {
                $tmp_year = $current_year-$count_year;
                $year_range_array[$tmp_year] = $tmp_year;
            }
        } else {
            for ($count_year=0; $count_year < 30; $count_year++) {
                $tmp_year = $current_year-$count_year;
                $year_range_array[$tmp_year] = $tmp_year;
            }
        }
        return $year_range_array;
    }

    public function nextadvert($model) {

        $uniq_ud = time() . uniqid();
        $next_advert_memcache_key = "/general/next/advert/" . $uniq_ud;
        
        Yii::$app->cache->set($next_advert_memcache_key, $model, 900);

        return $uniq_ud;
    }

    public function filterphone($content, $replace_to) {
        $pattern = [
                    '/\d{10}/',
                    '/\d{4}-\d{8,10}/',
                    '/\d{3}\s\d{3}\s\d{4}/',
                    '/\d{3}\s\-\s\d{3}\s\d{4}/',
                    '/\d{2}\s\d{2}\-\d{3}\s\d{4}/',
                    '/\d{3}-\d{8,10}/',
                    '/\d{3}-\d{7}/',
                    '/\d{3}-\s\d{3}\s\d{4}/',
                    '/\d{3}-\d{3}\s\d{4}/',
                    '/\d{3}-\d{4}\s\d{3}/',
                    '/\d{3}\s\d{7}/',
                    '/\d{3}\s\d{4}\s\d{3}/',
                ];
        
        $replace_array = [];
        
        $count_replace = 0;
        foreach ($pattern as $pattern_data_loop) {
            if (preg_match_all($pattern_data_loop, $content, $match_array)) {
                if (isset($match_array[0]) && count($match_array[0])) {
                    foreach ($match_array[0] as $match_data_loop) {
                        $count_replace++;
                        
                        $replace_array["__PHONE__" . $count_replace . "__"] = $match_data_loop;
                        
                        $content = str_replace($match_data_loop, "__PHONE__" . $count_replace . "__", $content);

                    }
                }
            }
        }
        
        if (count($replace_array)) {
            foreach ($replace_array as $replace_key_loop => $replace_data_loop) {
                $temp_replace = str_replace('__PHONE__', $replace_data_loop, $replace_to);
                $content = str_replace($replace_key_loop, $temp_replace, $content);
            }
        }
        
        return $content;
    }

    public function dealer_cover_logo_image($image_name, $upd_time='', $dealer_id) {
        
        if ($this->s3_flag == '1') {
            $display_image_name = rtrim($this->s3_domain) . '/dealers/' . $dealer_id . '/' . $image_name . '?' . $upd_time;
        } else {
            $display_image_name = '';
        }
        
        return $display_image_name;
    }
}
?>