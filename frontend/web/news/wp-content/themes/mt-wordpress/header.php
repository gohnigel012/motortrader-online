<header id="main-header">
	<nav id="w0" class="navbar navbar-expand-sm navbar-light justify-content-between">
		<div class="container-fluid">
				<a class="navbar-brand" href="/">
					<img src="/wp-content/themes/mt-wordpress/images/logo.png" alt="Motor Trader logo" id="logo">
				</a>
				<button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#w0-collapse" aria-controls="w0-collapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
			<div id="w0-collapse" class="collapse navbar-collapse">
				<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
			</div>
			<ul id="w7" class="navbar-nav ml-auto nav">
				<li class="nav-item">
					<a class="nav-link" href="/search">
						<img src="/wp-content/themes/mt-wordpress/images/search.png" class="main-header__comparison-images" alt="Search Images">
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="/bookmark">
						<img src="/wp-content/themes/mt-wordpress/images/bookmark.png" class="main-header__comparison-images" alt="Comparison Images">
					</a>
				</li>
				<li class="dropdown nav-item">
					<a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">Login</a>
					<div id="w8" class="dropdown-menu">
						<a class="dropdown-item" href="https://www.mtsa.com.my">Dealer Login</a>
						<a class="dropdown-item" href="/site/login">User Login</a>
					</div>
				</li>
			</ul>
		</div>
	</nav>
</header>
<header class="main-header-responsive">
    <a href="/"><img src="/wp-content/themes/mt-wordpress/images/logo.png" alt="Motor Trader logo" id="logo-responsive"></a>
    <button type="button" class="btn btn-default header__nav">
        <span class="header__burger"></span>
        <span class="header__burger header__burger--adjust"></span>
        <span class="header__burger"></span>
    </button>
</header>

