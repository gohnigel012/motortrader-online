function mtalert(_message, _title) {
    
    _title = _title || 'Motor Trader';
    _message = _message || '';
    
    jQuery("#alert-primary .modal-title").html(_title);
    jQuery("#alert-primary .modal-body").html(_message);

    jQuery('#alert-primary').modal();
}

jQuery(".car-list__bookmark").fadeIn(1000);
jQuery(".car-list__bookmark").click(function(e) {
    e.preventDefault(); 

    var _this = jQuery(this);
    var _urn = jQuery(this).attr('data-urn');

    if (jQuery(this).hasClass('car-list__show-bookmark')) {
        jQuery.ajax({
            type: 'post',
            url: DEFINE_AJAX_REMOVE_BOOKMARK_URL,
            dataType: 'json',
            data: {'urn_no': _urn},
            timeout: 120000,
            beforeSend : function(){
                
            },
            success: function(data) {
                if (data.result == '1') {
                    jQuery(".car-list__bookmark[data-urn=" + _urn + "]").removeClass('car-list__show-bookmark');
                } else {
                    //
                }
            }
        });
    } else {
        jQuery.ajax({
            type: 'post',
            url: DEFINE_AJAX_ADD_BOOKMARK_URL,
            dataType: 'json',
            data: {'urn_no': _urn},
            timeout: 120000,
            beforeSend : function(){
                
            },
            success: function(data) {
                if (data.result == '1') {
                    jQuery(".car-list__bookmark[data-urn=" + _urn + "]").addClass('car-list__show-bookmark');
                } else {
                    //
                }
            }
        });
    }
});

jQuery('[data-toggle="tooltip"]').tooltip();

function scrolltoelement(_id) {
    if (jQuery(_id).length > 0) {
        jQuery('html, body').animate({
            scrollTop: jQuery(_id).offset().top - 150
        }, 800);
    }
}

function calculate() {
    var price = jQuery("#calculator-price").val();
    var deposit = jQuery("#calculator-deposit").val();
    var interest = jQuery("#calculator-interest").val();
    var term = jQuery("#calculator-terms").val();

    var priceError = 0;
    var depositError = 0;
    var interestError = 0;
    var termError = 0;  

    var priceData = parseFloat(price);
    var depositData = parseFloat(deposit);
    var interestData = parseFloat(interest);
    var termData = parseFloat(term);
    
    if (isNaN(priceData)) {
        jQuery("#calculator-price").val("0");
        priceError = 1;
    } else {
        jQuery("#calculator-price").val(priceData);
    }
    
    if (isNaN(depositData)) {
        jQuery("#calculator-deposit").val("");
        depositError = 1;
    } else {
        if (depositData > price) {
            depositData = price;
        }
        
        jQuery("#calculator-deposit").val(depositData);
    }
    
    if (interest == "") {
        jQuery("#calculator-interest").val("");
    } else {
        if (interestData!=interest) {
            jQuery("#calculator-interest").val(interestData);
            interestError = 1;
        }
    }
    
    if (isNaN(termData)) {
        jQuery("#calculator-terms").val("0");
        termError = 1;
    } else {
        if (termData > 11) {
            termData = 11;
        }
        
        if (termData == 0) {
            termData = 1;
        }

        jQuery("#calculator-terms").val(termData);
    }
    
    if (priceError == 0 && depositError == 0 && interestError == 0 && termError==0) {
        var depositAmount = depositData;
        var carPaidAmount = priceData - depositAmount;
        var interestAmount = interestData / 100;
        var interestPaidAmount = (termData * interestAmount) * carPaidAmount;
        var finalAmount = (interestPaidAmount + carPaidAmount) / (termData * 12);
        
        finalAmount = Number((finalAmount).toFixed(2)); 
        finalAmount = (finalAmount).formatMoney(2, '.', ',');

        jQuery("#calculator-result").text('RM ' + finalAmount);
    }       
}

Number.prototype.formatMoney = function(c, d, t) {
    var n = this, 
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "." : d, 
    t = t == undefined ? "," : t, 
    s = n < 0 ? "-" : "", 
    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
    j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};