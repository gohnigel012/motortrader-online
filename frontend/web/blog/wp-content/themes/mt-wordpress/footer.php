<footer id="footer-section">
        <div class="row">
            <div class="col-md-9">
                <div class="footer-section__left">
                    <div class="row">
                        <div class="col-md-2">
                            <h3>Buy</h3>
                            <ul class="footer-section__location">
                                <li><a href="/car">Cars</a></li>
                                <li><a href="/autopart">Auto parts</a></li>
                                <li><a href="/numberplate">Number plates</a></li>
                                <li><a href="/car?bodyType=bt9">Commercial cars</a></li>
                                <li><a href="/bike">Motorcycle</a></li>
                                <li>Others</li>
                                <li><a href="/dealer">All dealers</a></li>
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <h3>Sell</h3>
                            <ul class="footer-section__location">
                                <li>Sell your car</li>
                                <li>Dealer platform</li>
                                <li>Build a website</li>
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <h3>Insights</h3>
                            <ul class="footer-section__location">
                                <li>News</li>
                                <li>New car</li>
                                <li>Advice</li>
                                <li>Promotion</li>
                                <li>Review</li>
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <h3>Tools</h3>
                            <ul class="footer-section__location">
                                <li>Favourite</li>
                                <li>Comparison</li>
                                <li>Car loan calculator</li>
                                <li>Road tax calculator</li>
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <h3>Partners</h3>
                            <ul class="footer-section__location">
                                <li>Cloudkia Solutions</li>
                            </ul>
                        </div>
                    </div>
                    <div class="text-center low-footer">
                        <div>
                            <ul class="low-footer__bottom-section">
                                <li class="low-footer__bottom-section-list low-footer__bottom-section-list--no-padding-left"><a href="/site/terms-and-conditions">Terms &amp; Conditions</a></li>
                                <li class="low-footer__bottom-section-list"><a href="/site/contact-us">Contact</a></li>
                                <li class="low-footer__bottom-section-list"><a href="/site/privacy">Privacy Policy</a></li>
                                <li class="low-footer__bottom-section-list"><a href="/site/sitemap">Site Map</a></li>
                            </ul>
                            <p class="text-center low-footer__bottom-copyright">Copyright &copy; MT Digital Sdn Bhd</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="footer-section__right">
                    <div>
                        <p class="footer-section__connect">
                            <a href="https://www.facebook.com/MotorTraderMY" target="_mt_facebook" rel="noopener nofollow"><img class="footer-section__connect_icon" src="/images/facebookfooter.png" alt="Facebook"></a>
                            <a href="https://www.instagram.com/motortradermy/" target="_mt_instagram" rel="noopener nofollow"><img class="footer-section__connect_icon" src="/images/instagramfooter.png" alt="Instagram"></a>
                            <a href="https://twitter.com/#!/MotorTraderMY" target="_mt_twitter" rel="noopener nofollow"><img class="footer-section__connect_icon" src="/images/twitterfooter.png" alt="Twitter"></a>
                            <a href="https://www.youtube.com/user/MotorTraderMalaysia" target="_mt_youtube" rel="noopener nofollow"><img class="footer-section__connect_icon" src="/images/youtubefooter.png" alt="YouTube"></a>
                        </p>
                        <h3>Headquarters MT Digital Sdn Bhd (429449-W)</h3>
                        <br>
                        <p class="footer-section__contact">C-10-6, Zetapark</p>
                        <p class="footer-section__contact">Jln Tmn Ibu Kota</p>
                        <p class="footer-section__contact">55300 Kuala Lumpur</p>     
                        <br>
                        <p class="footer-section__contact">Tel: 012-7089464</p>
                        <p class="footer-section__contact">Email: info@mtdigital.my</p>             
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-section__left-responsive">
            <hr class="footer-section__divider">
            <h4 class="footer-section__column" data-toggle="collapse" data-target="#buy">Buy <span class="footer-section__column-plus">+</span></h4>
            <div id="buy" class="collapse">
                <ul class="footer-section__location">
                    <li><a href="/car">Cars</a></li>
                    <li><a href="/autopart">Auto parts</a></li>
                    <li><a href="/numberplate">Number plates</a></li>
                    <li><a href="/car?bodyType=bt9">Commercial cars</a></li>
                    <li><a href="/bike">Motorcycle</a></li>
                    <li>Others</li>
                    <li><a href="/dealer">All dealers</a></li>
                </ul>
            </div>
            <hr class="footer-section__divider">
            <h4 class="footer-section__column" data-toggle="collapse" data-target="#sell">Sell <span class="footer-section__column-plus">+</span></h4>
            <div id="sell" class="collapse">
                <ul class="footer-section__location">
                    <li>Sell your car</li>
                    <li>Dealer platform</li>
                    <li>Build a website</li>
                </ul>
            </div>
            <hr class="footer-section__divider">
            <h4 class="footer-section__column" data-toggle="collapse" data-target="#insights">Insights <span class="footer-section__column-plus">+</span></h4>
            <div id="insights" class="collapse">
                <ul class="footer-section__location">
                    <li>News</li>
                    <li>New car</li>
                    <li>Advice</li>
                    <li>Promotion</li>
                    <li>Review</li>
                </ul>
            </div>    
            <hr class="footer-section__divider">
            <h4 class="footer-section__column" data-toggle="collapse" data-target="#tools">Tools <span class="footer-section__column-plus">+</span></h4>
            <div id="tools" class="collapse">
                <ul class="footer-section__location">
                    <li>Favourite</li>
                    <li>Comparison</li>
                    <li>Car loan calculator</li>
                    <li>Road tax calculator</li>
                </ul>
            </div>
            <hr class="footer-section__divider">
            <h4 class="footer-section__column" data-toggle="collapse" data-target="#partners">Partners <span class="footer-section__column-plus">+</span></h4>
            <div id="partners" class="collapse">
                <ul class="footer-section__location">
                    <li>Cloudkia Solutions</li>
                </ul>
            </div>
            <hr class="footer-section__divider">
            <div>
                <ul class="low-footer__bottom-section">
                    <li class="low-footer__bottom-section-list low-footer__bottom-section-list--no-padding-left"><a href="site/terms-and-conditions">Terms &amp; Conditions</a></li>
                    <li class="low-footer__bottom-section-list"><a href="site/contact-us">Contact</a></li>
                </ul>
                <ul class="low-footer__bottom-section">
                    <li class="low-footer__bottom-section-list low-footer__bottom-section-list--no-padding-left"><a href="site/privacy">Privacy Policy</a></li>
                    <li class="low-footer__bottom-section-list"><a href="site/sitemap">Site Map</a></li>
                </ul>
                <p class="text-center">Copyright &copy; MT Digital Sdn Bhd</p>
            </div>
        </div>
</footer>
<?php wp_footer(); ?>