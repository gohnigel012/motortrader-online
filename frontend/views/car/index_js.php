<?php
use app\models\MasterGeneral;

$master_general = new MasterGeneral();
?>
<script>
	jQuery("#link-slider").protoSlider({
		'fadeintime': 1000
	});

	window.addEventListener("load", function(event) {
	    lazyload();
	});
	
	jQuery('#car-description-open').click(function () {
		if(jQuery('#car-description-show').hasClass('d-none')){
			jQuery('#car-description-show').removeClass('d-none');
			jQuery('#car-description-open').attr("src", "/images/close.png");
		} else {
			jQuery('#car-description-show').addClass('d-none');
			jQuery('#car-description-open').attr("src", "/images/plus.png");
		}
	});

	jQuery('#car-filter-open').click(function () {
		if(jQuery('#car-filter-show').hasClass('d-none')){
			jQuery('#car-filter-show').removeClass('d-none');
			jQuery('#car-filter-open').attr("src", "/images/close.png");
		} else {
			jQuery('#car-filter-show').addClass('d-none');
			jQuery('#car-filter-open').attr("src", "/images/plus.png");
		}
	});

	jQuery('#car-condition-open').click(function () {
		if(jQuery('#car-condition-show').hasClass('d-none')){
			jQuery('#car-condition-show').removeClass('d-none');
			jQuery('#car-condition-open').attr("src", "/images/close.png");
		} else {
			jQuery('#car-condition-show').addClass('d-none');
			jQuery('#car-condition-open').attr("src", "/images/plus.png");
		}
	});

	jQuery('#car-make-open').click(function () {
		if(jQuery('#car-make-show').hasClass('d-none')){
			jQuery('#car-make-show').removeClass('d-none');
			jQuery('#car-make-open').attr("src", "/images/close.png");
		} else {
			jQuery('#car-make-show').addClass('d-none');
			jQuery('#car-make-open').attr("src", "/images/plus.png");
		}
	});

	jQuery('#car-model-open').click(function () {
		if(jQuery('#car-model-show').hasClass('d-none')){
			jQuery('#car-model-show').removeClass('d-none');
			jQuery('#car-model-open').attr("src", "/images/close.png");
		} else {
			jQuery('#car-model-show').addClass('d-none');
			jQuery('#car-model-open').attr("src", "/images/plus.png");
		}
	});

	jQuery('#car-variant-open').click(function () {
		if(jQuery('#car-variant-show').hasClass('d-none')){
			jQuery('#car-variant-show').removeClass('d-none');
			jQuery('#car-variant-open').attr("src", "/images/close.png");
		} else {
			jQuery('#car-variant-show').addClass('d-none');
			jQuery('#car-variant-open').attr("src", "/images/plus.png");
		}
	});

	jQuery('#car-type-open').click(function () {
		if(jQuery('#car-type-show').hasClass('d-none')){
			jQuery('#car-type-show').removeClass('d-none');
			jQuery('#car-type-open').attr("src", "/images/close.png");
		} else {
			jQuery('#car-type-show').addClass('d-none');
			jQuery('#car-type-open').attr("src", "/images/plus.png");
		}
	});

	jQuery('#car-state-open').click(function () {
		if(jQuery('#car-state-show').hasClass('d-none')){
			jQuery('#car-state-show').removeClass('d-none');
			jQuery('#car-state-open').attr("src", "/images/close.png");
		} else {
			jQuery('#car-state-show').addClass('d-none');
			jQuery('#car-state-open').attr("src", "/images/plus.png");
		}
	});

	jQuery('#car-transmission-open').click(function () {
		if(jQuery('#car-transmission-show').hasClass('d-none')){
			jQuery('#car-transmission-show').removeClass('d-none');
			jQuery('#car-transmission-open').attr("src", "/images/close.png");
		} else {
			jQuery('#car-transmission-show').addClass('d-none');
			jQuery('#car-transmission-open').attr("src", "/images/plus.png");
		}
	});

	jQuery('#car-fuel-open').click(function () {
		if(jQuery('#car-fuel-show').hasClass('d-none')){
			jQuery('#car-fuel-show').removeClass('d-none');
			jQuery('#car-fuel-open').attr("src", "/images/close.png");
		} else {
			jQuery('#car-fuel-show').addClass('d-none');
			jQuery('#car-fuel-open').attr("src", "/images/plus.png");
		}
	});

	jQuery('#car-wheel-open').click(function () {
		if(jQuery('#car-wheel-show').hasClass('d-none')){
			jQuery('#car-wheel-show').removeClass('d-none');
			jQuery('#car-wheel-open').attr("src", "/images/close.png");
		} else {
			jQuery('#car-wheel-show').addClass('d-none');
			jQuery('#car-wheel-open').attr("src", "/images/plus.png");
		}
	});

	

    $( "#slider-range" ).slider({
      range: true,
      min: 100,
      max: 9999999,
      values: [ <?php echo isset($_REQUEST['minPrice']) && $_REQUEST['minPrice'] != null ? $_REQUEST['minPrice'] : 50000; ?>, <?php echo isset($_REQUEST['maxPrice']) && $_REQUEST['maxPrice'] != null ? $_REQUEST['maxPrice'] : 1251000; ?> ],
      slide: function( event, ui ) {
        $( "#minPrice" ).text( ui.values[ 0 ] );
        $( "#maxPrice" ).text( ui.values[ 1 ] );
      }
    });

    $( "#minPrice" ).text( $( "#slider-range" ).slider( "values", 0 ))  ;
    $( "#maxPrice" ).text( $( "#slider-range" ).slider( "values", 1 ))  ;

    jQuery('#custom-handle').mouseup(function() {
		window.location = "car?minPrice=" + $( "#slider-range" ).slider( "values", 0 ) + "&maxPrice=" + $( "#slider-range" ).slider( "values", 1 );
	});

	jQuery('#custom-handle-2').mouseup(function() {
		window.location = "car?minPrice=" + $( "#slider-range" ).slider( "values", 0 ) + "&maxPrice=" + $( "#slider-range" ).slider( "values", 1 );
	});

    $( "#slider-range-2" ).slider({
      range: true,
      min: new Date().getFullYear() - 20,
      max: new Date().getFullYear(),
      values: [ <?php echo isset($_REQUEST['minYear']) && $_REQUEST['minYear'] != null ? $_REQUEST['minYear'] : date('Y', strtotime("-12 year")); ?>, <?php echo isset($_REQUEST['maxYear']) && $_REQUEST['maxYear'] != null ? $_REQUEST['maxYear'] : date('Y', strtotime("-2 year")); ?> ],
      slide: function( event, ui ) {
        $( "#minYear" ).text( ui.values[ 0 ] );
        $( "#maxYear" ).text( ui.values[ 1 ] );
      }
    });

	$( "#minYear" ).text( $( "#slider-range-2" ).slider( "values", 0 ));
    $( "#maxYear" ).text( $( "#slider-range-2" ).slider( "values", 1 ));

     jQuery('#custom-handle-3').mouseup(function() {
		window.location = "car?minYear=" + $( "#slider-range-2" ).slider( "values", 0 ) + "&maxYear=" + $( "#slider-range-2" ).slider( "values", 1 );
	});

	jQuery('#custom-handle-4').mouseup(function() {
		window.location = "car?minYear=" + $( "#slider-range-2" ).slider( "values", 0 ) + "&maxYear=" + $( "#slider-range-2" ).slider( "values", 1 );
	});    

    $( "#slider-range-3" ).slider({
      range: true,
      min: 0,
      max: 100000,
      values: [ <?php echo isset($_REQUEST['min_mileage']) && $_REQUEST['min_mileage'] != null ? $_REQUEST['min_mileage'] : 10; ?>, <?php echo isset($_REQUEST['max_mileage']) && $_REQUEST['max_mileage'] != null ? $_REQUEST['max_mileage'] : 6000; ?> ],
      slide: function( event, ui ) {
        $( "#minMileage" ).text( ui.values[ 0 ] );
        $( "#maxMileage" ).text( ui.values[ 1 ] );
      }
    });

    getMileageCode($( "#slider-range-3" ).slider( "values", 0 ), $( "#slider-range-3" ).slider( "values", 1 ));

    $( "#minMileage" ).text( $( "#slider-range-3" ).slider( "values", 0 ));
    $( "#maxMileage" ).text( $( "#slider-range-3" ).slider( "values", 1 ));

    jQuery('#custom-handle-5').mouseup(function() {
		window.location = "car?mileage=" + mileage_code + "&min_mileage=" + $( "#slider-range-3" ).slider( "values", 0 ) + "&max_mileage=" + $( "#slider-range-3" ).slider( "values", 1 );
	});

	jQuery('#custom-handle-6').mouseup(function() {
		window.location = "car?mileage=" + mileage_code + "&min_mileage=" + $( "#slider-range-3" ).slider( "values", 0 ) + "&max_mileage=" + $( "#slider-range-3" ).slider( "values", 1 );
	});


	function getMileageCode($minMileage, $maxMileage) {
		mileage_code = '';
<?php
	$mileage_array = $master_general->getMileage();

	foreach($mileage_array as $mileage_array_id_loop => $mileage_array_data_loop) {
		preg_match_all('!\d+(?:,\d+)*!', $mileage_array_data_loop, $matches);
		if($mileage_array_data_loop != 'More than 500,000 km') {		
?>
		if ($minMileage >= parseInt('<?php echo str_replace(',', '', $matches[0][0]); ?>') && $maxMileage < parseInt('<?php echo str_replace(',', '', $matches[0][1]); ?>') ) {
	    	mileage_code = '<?php echo $mileage_array_id_loop; ?>';
	    }
<?php	
		} else {
?>
		if ($minMileage >= parseInt('<?php echo str_replace(',', '', $matches[0][0]); ?>') && $maxMileage < parseInt('<?php echo str_replace(',', '', $matches[0][0]); ?>') ) {
	    	mileage_code = '<?php echo $mileage_array_id_loop; ?>';
	    }
<?php

		}
	}
?>
	}

	function popupvideo(_url, _title, _urn) {
		var _urn = _urn || '';

		// ga('send', 'event', 'Auto Parts', 'video', _urn, {
		// 	nonInteraction: true
		// });

		var _title = _title || 'Video';

		jQuery("#videoForm #video-title").text(_title);

		if (_url.indexOf('?autoplay=1&mute=1') > 0 ) {
			jQuery("#popup-video-object").css("display","block");
			jQuery("#popup-video-object").attr('data', _url);
			jQuery("#popup-video-video").css("display","none");	
		} else {
			jQuery("#popup-video-object").css("display","none");
			jQuery("#popup-video-video").css("display","block");
			jQuery("#popup-video-video").attr('src', _url);	
		}

		jQuery("#popupFrame").css("display","block");

		// var video = jQuery("#popup-video")[0];
		// if (video.paused) {
		//     video.play();
		// }

		jQuery("#popup-video").prop('muted', true)
	}

	function closeVideo(){
		jQuery("#popupFrame").css("display","none");
		jQuery("#popup-video-object").attr('data', '');
		jQuery("#popup-video-video").attr('src', '');	
	}
	
</script>