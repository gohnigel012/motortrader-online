<script>
    jQuery("#home-popup-make").change(function() {
        var current_make = jQuery(this).val();
        loadcarmodel(current_make, "#home-popup-model");
    });

    function loadcarmodel(current_make, selector, is_osc) {

        var is_osc = is_osc || 0;

        jQuery.ajax({
            type: 'post',
            url: '<?php echo Yii::$app->urlManager->createAbsoluteUrl('ajax/model'); ?>',
            dataType: 'json',
            data: {'make': current_make, 'is_osc': is_osc},
            timeout: 120000,
            beforeSend : function(){
                jQuery(selector).attr('disabled', true);
                jQuery(selector).html('<option value="">All Models</option>');
            },
            success: function(data) {
                jQuery(selector).attr('disabled', false);
                
                var display_html = '';
                display_html += '<option value="">All Models</option>';
                if (data.result == '1') {
                    if (data.car_model_group.length === 0) {
                                    
                    } else {
                        display_html += '<optgroup label="------ Group ------">';
                        display_html += '</optgroup>';
                        jQuery.each(data.car_model_group, function(i, item) {
                            display_html += '<option value="' + item.id + '">' + item.name + '</option>';
                        });
                        display_html += '<optgroup label="-------------------">';
                        display_html += '</optgroup>';
                    }
                    jQuery.each(data.model, function(i, item) {
                        display_html += '<option value="' + item.id + '">' + item.name + '</option>';
                    });
                }

                jQuery(selector).html(display_html);
            }
        });
    }

    function headerUpdateCarType(_type, _value) {
        if (_type == 'sel') {
            var _index = jQuery("#home-popup-car-type option:selected").attr("data-index");
            jQuery("input[data-name=header-car-type][data-index=" + _index + "]").click();
        } else {
            var _index = jQuery("input[data-name=header-car-type]:checked").attr("data-index");
            jQuery("#home-popup-car-type").prop('selectedIndex', (_index - 1));
            //.attr('selected', true);
        }
    }
</script>