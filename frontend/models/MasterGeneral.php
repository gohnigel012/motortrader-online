<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_general".
 *
 * @property string $code_type type of codes
 * @property string|null $code_name types naming
 * @property string $cmn_code
 * @property string|null $cmn_name
 * @property string|null $cmn_value1
 * @property string|null $cmn_value2
 * @property int|null $order_seq order sequence 1～
 * @property string|null $del_flg 1: deleted
 */
class MasterGeneral extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_general';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code_type', 'cmn_code'], 'required'],
            [['order_seq'], 'default', 'value' => null],
            [['order_seq'], 'integer'],
            [['code_type'], 'string', 'max' => 2],
            [['code_name', 'cmn_name', 'cmn_value1', 'cmn_value2'], 'string', 'max' => 255],
            [['cmn_code'], 'string', 'max' => 20],
            [['del_flg'], 'string', 'max' => 1],
            [['code_type', 'cmn_code'], 'unique', 'targetAttribute' => ['code_type', 'cmn_code']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'code_type' => 'Code Type',
            'code_name' => 'Code Name',
            'cmn_code' => 'Cmn Code',
            'cmn_name' => 'Cmn Name',
            'cmn_value1' => 'Cmn Value1',
            'cmn_value2' => 'Cmn Value2',
            'order_seq' => 'Order Seq',
            'del_flg' => 'Del Flg',
        ];
    }

    public function getMileage() {
        return [
                'm1' => '0 - 4,999km', 
                'm2' => '5,000 - 9,999km', 
                'm3' => '10,000 - 15,999km', 
                'm4' => '15,000 - 19,999km', 
                'm5' => '20,000 - 24,999km', 
                'm6' => '25,000 - 29,999km', 
                'm7' => '30,000 - 34,999km', 
                'm8' => '35,000 - 39,999km', 
                'm9' => '40,000 - 44,999km', 
                'm10' => '45,000 - 49,999km', 
                'm11' => '50,000 - 54,999km', 
                'm12' => '55,000 - 59,999km', 
                'm13' => '60,000 - 64,999km', 
                'm14' => '65,000 - 69,999km', 
                'm15' => '70,000 - 74,999km', 
                'm16' => '75,000 - 79,999km', 
                'm17' => '80,000 - 84,999km', 
                'm18' => '85,000 - 89,999km', 
                'm19' => '90,000 - 94,999km', 
                'm20' => '95,000 - 99,999km', 
                'm21' => '100,000 - 104,999km', 
                'm22' => '105,000 - 109,999km', 
                'm23' => '110,000 - 114,999km', 
                'm24' => '115,000 - 119,999km', 
                'm25' => '120,000 - 124,999km', 
                'm26' => '125,000 - 129,999km', 
                'm27' => '130,000 - 134,999km', 
                'm28' => '135,000 - 139,999km', 
                'm29' => '140,000 - 144,999km', 
                'm30' => '145,000 - 149,999km', 
                'm31' => '150,000 - 154,999km', 
                'm32' => '155,000 - 159,999km', 
                'm33' => '160,000 - 164,999km', 
                'm34' => '165,000 - 169,999km', 
                'm35' => '170,000 - 174,999km', 
                'm36' => '175,000 - 179,999km', 
                'm37' => '180,000 - 184,999km', 
                'm38' => '185,000 - 189,999km', 
                'm39' => '190,000 - 194,999km', 
                'm40' => '195,000 - 199,999km', 
                'm41' => '200,000 - 204,999km', 
                'm42' => '205,000 - 209,999km', 
                'm43' => '210,000 - 214,999km', 
                'm44' => '215,000 - 219,999km', 
                'm45' => '220,000 - 224,999km', 
                'm46' => '225,000 - 229,999km', 
                'm47' => '230,000 - 234,999km', 
                'm48' => '235,000 - 239,999km', 
                'm49' => '240,000 - 244,999km', 
                'm50' => '245,000 - 249,999km', 
                'm51' => '250,000 - 254,999km', 
                'm52' => '255,000 - 259,999km', 
                'm53' => '260,000 - 264,999km', 
                'm54' => '265,000 - 269,999km', 
                'm55' => '270,000 - 274,999km', 
                'm56' => '275,000 - 279,999km', 
                'm57' => '280,000 - 284,999km', 
                'm58' => '285,000 - 289,999km', 
                'm59' => '290,000 - 294,999km', 
                'm60' => '295,000 - 299,999km', 
                'm61' => '300,000 - 304,999km', 
                'm62' => '305,000 - 309,999km', 
                'm63' => '310,000 - 314,999km', 
                'm64' => '315,000 - 319,999km', 
                'm65' => '320,000 - 324,999km', 
                'm66' => '325,000 - 329,999km', 
                'm67' => '330,000 - 334,999km', 
                'm68' => '335,000 - 339,999km', 
                'm69' => '340,000 - 344,999km', 
                'm70' => '345,000 - 349,999km', 
                'm71' => '350,000 - 354,999km', 
                'm72' => '355,000 - 359,999km', 
                'm73' => '360,000 - 364,999km', 
                'm74' => '365,000 - 369,999km', 
                'm75' => '370,000 - 374,999km', 
                'm76' => '375,000 - 379,999km', 
                'm77' => '380,000 - 384,999km', 
                'm78' => '385,000 - 389,999km', 
                'm79' => '390,000 - 394,999km', 
                'm80' => '395,000 - 399,999km', 
                'm81' => '400,000 - 404,999km', 
                'm82' => '405,000 - 409,999km', 
                'm83' => '410,000 - 414,999km', 
                'm84' => '415,000 - 419,999km', 
                'm85' => '420,000 - 424,999km', 
                'm86' => '425,000 - 429,999km', 
                'm87' => '430,000 - 434,999km', 
                'm88' => '435,000 - 439,999km', 
                'm89' => '440,000 - 444,999km', 
                'm90' => '445,000 - 449,999km', 
                'm91' => '450,000 - 454,999km', 
                'm92' => '455,000 - 459,999km', 
                'm93' => '460,000 - 464,999km', 
                'm94' => '465,000 - 469,999km', 
                'm95' => '470,000 - 474,999km', 
                'm96' => '475,000 - 479,999km', 
                'm97' => '480,000 - 484,999km', 
                'm98' => '485,000 - 489,999km', 
                'm99' => '490,000 - 499,999km', 
                'm100' => 'More than 500,000 km', 
            ];
    }

    public function getColor() {
        $return_array = [];

        $master_general_memcache_key = "/master_general/color";
        $master_general_memcache = Yii::$app->cache->get($master_general_memcache_key);
        if ($master_general_memcache !== false) {
            $return_array = $master_general_memcache;
        } else {
            $clasification_data = $this->find()->where(['code_type' => '05'])->orderBy('order_seq')->one();
            if ($clasification_data) {
                foreach ($clasification_data as $data) {
                    $return_array['cmn_code'] = 'cmn_name';
                }
            }
            Yii::$app->cache->set($master_general_memcache_key, $return_array, 3600);
        }

        return $return_array;
    }

    public function getLocation() {
        $return_array = [];
        
        $master_general_memcache_key = "/master_general/location";
        $master_general_memcache = Yii::$app->cache->get($master_general_memcache_key);
        if ($master_general_memcache !== false) {
            $return_array = $master_general_memcache;
        } else {

            $clasification_data = $this->find()->where(['code_type' => '06'])->orderBy('order_seq')->all();
                                                        
            if ($clasification_data) {
                foreach ($clasification_data as $data) {
                    $return_array[$data['cmn_code']] = $data['cmn_name'];
                }
            }
            Yii::$app->cache->set($master_general_memcache_key, $return_array, 3600);
        }

        return $return_array;
    }

    public function getBodyType() {
        return [    'bt1' => 'Sedan',
                    'bt2' => 'Wagon',
                    'bt3' => 'Coupe',
                    'bt4' => 'Convertible',
                    'bt5' => 'Hatchback',
                    'bt6' => 'MPV',
                    'bt7' => 'SUV',
                    'bt8' => 'Pickup',
                    'bt9' => 'Commercial',
                    'bt10' => 'Others'
                    ];
    }

    public function getTransmission() {
        return ['at' => 'Automatic',
                'mt' => 'Manual'
                ];
    }
}
