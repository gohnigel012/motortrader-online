<script>

    jQuery('#icon-box-car').click(function() {
        jQuery("#car-search").removeClass("d-none");
        jQuery("#auto-parts-search").addClass("d-none");
        jQuery("#number-plates-search").addClass("d-none");
        jQuery("#motorcycle-search").addClass("d-none");
        jQuery("#icon-box-car").addClass("active");
        jQuery("#icon-box-auto-parts").removeClass("active");
        jQuery("#icon-box-number-plates").removeClass("active");
        jQuery("#icon-box-motorcycle").removeClass("active");
    });

    jQuery('#icon-box-auto-parts').click(function() {
        jQuery("#auto-parts-search").removeClass("d-none");
        jQuery("#car-search").addClass("d-none");
        jQuery("#number-plates-search").addClass("d-none");
        jQuery("#motorcycle-search").addClass("d-none");
        jQuery("#icon-box-auto-parts").addClass("active");
        jQuery("#icon-box-car").removeClass("active");
        jQuery("#icon-box-number-plates").removeClass("active");
        jQuery("#icon-box-motorcycle").removeClass("active");
        
    });

    jQuery('#icon-box-number-plates').click(function() {
        jQuery("#number-plates-search").removeClass("d-none");
        jQuery("#auto-parts-search").addClass("d-none");
        jQuery("#car-search").addClass("d-none");
        jQuery("#motorcycle-search").addClass("d-none");
        jQuery("#icon-box-number-plates").addClass("active");
        jQuery("#icon-box-car").removeClass("active");
        jQuery("#icon-box-auto-parts").removeClass("active");
        jQuery("#icon-box-motorcycle").removeClass("active");
    });

    jQuery('#icon-box-motorcycle').click(function() {
        jQuery("#motorcycle-search").removeClass("d-none");
        jQuery("#auto-parts-search").addClass("d-none");
        jQuery("#car-search").addClass("d-none");
        jQuery("#number-plates-search").addClass("d-none");
        jQuery("#icon-box-motorcycle").addClass("active");
        jQuery("#icon-box-car").removeClass("active");
        jQuery("#icon-box-auto-parts").removeClass("active");
        jQuery("#icon-box-number-plates").removeClass("active");
    });

    jQuery("#make-home-popup").change(function() {
        loadModel();
    });

    jQuery("#min-price").change(function() {
        for(var i = 0; i < jQuery("#max-price option").length; ++i) {
            if(parseInt(jQuery("#min-price").val()) > parseInt(jQuery("#max-price option")[i].value)) {
                jQuery("#max-price option")[i].setAttribute('disabled', 'disabled');
            } else {
                jQuery("#max-price option")[i].removeAttribute('disabled');
            };
        };
    });

    jQuery("#max-price").change(function() {
        for(var i = 0; i < jQuery("#min-price option").length; ++i) {
            if(parseInt(jQuery("#max-price").val()) < parseInt(jQuery("#min-price option")[i].value)) {
                jQuery("#min-price option")[i].setAttribute('disabled', 'disabled');
            } else {
                jQuery("#min-price option")[i].removeAttribute('disabled');
            };
        };
    });

    jQuery('#icon-box-brands').click(function() {
        jQuery('#list-brands').removeClass("d-none");
        jQuery('#list-type').addClass("d-none");
        jQuery('#icon-box-brands').addClass('active');
         jQuery('#icon-box-type').removeClass('active');
    });

    jQuery('#icon-box-type').click(function() {
        jQuery('#list-type').removeClass("d-none");
        jQuery('#list-brands').addClass("d-none");
        jQuery('#icon-box-brands').removeClass('active');
         jQuery('#icon-box-type').addClass('active');
    });

    jQuery('#show-more-cars').click(function() {
        if(jQuery('#all-shown-cars').hasClass('d-none')) {
            jQuery('#all-shown-cars').removeClass('d-none');
        } else {
            jQuery('#all-shown-cars').addClass('d-none');
        }
    });

    function loadModel(){
        var make = jQuery("#make-home-popup").val();
        if (make != '') {
            jQuery.ajax({
                type: 'POST',
                dataType: 'json',
                url: '<?php echo Yii::$app->urlManager->createAbsoluteUrl('ajax/model'); ?>',
                data: {
                    'make': make
                },
                success: function(data){
                    if (data.result == 1) {
                        var display_html = '';
                        display_html += '<option value="">All Models</option>';
                        if (data.result == '1') {
                            jQuery.each(data.model, function(i, item) {
                                display_html += '<option value="' + item.id + '">' + item.name + '</option>';
                            });
                        }
                        jQuery("#model-home-popup").html(display_html).attr('disabled', false);
                    } else {
                        alert(data.message);
                    }
                }     
            });
        } else {
            jQuery("#model-home-popup").html('<option value="">All Models</option>').attr('disabled', true);
        }

        jQuery("#model-home-popup").html('<option value="">All Models</option>').attr('disabled', true);
    }

    window.addEventListener("load", function(event) {
        lazyload();
    }); 
</script>