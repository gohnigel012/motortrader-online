<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_dealer_review".
 *
 * @property int $master_dealer_review_id
 * @property string|null $dealer_id
 * @property string|null $dealer_name
 * @property string|null $master_dealer_review_status
 */
class MasterDealerReview extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_dealer_review';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dealer_id'], 'string', 'max' => 20],
            [['dealer_name'], 'string', 'max' => 80],
            [['master_dealer_review_status'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'master_dealer_review_id' => 'Master Dealer Review ID',
            'dealer_id' => 'Dealer ID',
            'dealer_name' => 'Dealer Name',
            'master_dealer_review_status' => 'Master Dealer Review Status',
        ];
    }

    public static function getdealerreviewinfo($id)
    {
        $master_dealer_review_model = MasterDealerReview::find()->where(['dealer_id' => $id])->one();
        if ($master_dealer_review_model) {
            return $master_dealer_review_model;
        }
        return false;                                                                   
    }
}
