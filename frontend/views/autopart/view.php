<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$temp_category = explode(">", $trans_advert_model->model_name);

$filter_category = '';
	
switch(trim($temp_category[0])){
	case 'Rims & Tyres':
		$filter_category = 1;
		break;
	case 'Rims & Tires':
		$filter_category = 1;
		break;	
	case 'Car security & Navigation system':
		$filter_category = 2;
		break;
	case 'In car entertainment & Car navigation system':
		$filter_category = 2;
		break;	
	case 'Performance Part':
		$filter_category = 3;
		break;
	case 'Exterior & Body Parts':
		$filter_category = 4;
		break;
	case 'Engine & Transmission':
		$filter_category = 5;
		break;
	case 'Car Care':
		$filter_category = 7;
		break;
	case 'Interior Accesories':
		$filter_category = 8;
		break;
	case 'Int. Accessories':
		$filter_category = 8;
		break;	
	case 'Oils, Coolants & Fluids':
		$filter_category = 9;
		break;
	case 'Clothing':
		$filter_category = 10;
		break;
	case 'Other Accesories':
		$filter_category = 11;
		break;
	case 'Half-cut':
		$filter_category = 12;
		break;	
}

$sub_category_array = $master_car_sub_part->getSubCategory($filter_category);

$this->title = $trans_advert_model->make_name;
$this->params['breadcrumbs'][] = ['label' => 'Auto parts', 'url' => ['/autopart']];
$this->params['breadcrumbs'][] = ['label' => trim($temp_category[0]), 'url' => ['/autopart?category=' . $filter_category]];

if(count($sub_category_array)) {
	foreach($sub_category_array as $model_id_loop => $model_data_loop) {
		$this->params['breadcrumbs'][] = ['label' => $model_data_loop, 'url' => ['/autopart?subcategory=' . $model_id_loop]];
	}
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-home">
	<div class="car-view">
		<div class="row">
			<div class="col-lg-6">
<?php
	if (isset($pdata['picture_array']) && count($pdata['picture_array'])) {	
		$count = 0;
?>				
				<div id="demo" class="carousel slide" data-ride="carousel">

					<!-- Indicators -->
					<ul class="carousel-indicators">
<?php
		if ($pdata['autopart_video'] !== false)	{
?>
						<li data-target="#demo" data-slide-to="0" class="active carousel-indicators-items"></li>
<?php
			foreach ($pdata['picture_array'] as $index_loop => $data_loop) {
?>
						<li data-target="#demo" data-slide-to="<?php echo $index_loop; ?>" class="<?php echo $pdata['autopart_video'] === false && $index_loop == 1 ? 'active' : ''; ?> carousel-indicators-items"></li>
<?php				
			}
		} else {
			foreach ($pdata['picture_array'] as $index_loop => $data_loop) {
?>
					    <li data-target="#demo" data-slide-to="<?php echo $index_loop - 1; ?>" class="<?php echo $pdata['autopart_video'] === false && $index_loop == 1 ? 'active' : ''; ?> carousel-indicators-items"></li>
<?php
			}
		}
?>
					</ul>
					  
					<!-- The slideshow -->
					<div class="carousel-inner">

<?php
		$trans_advert_super_deals_model = $super_deals->getsuperdeals($trans_advert_model->urn_no);
			
		$has_super_deals = $trans_advert_super_deals_model['got_super_deals'];

		if ($pdata['autopart_video'] !== false)	{
?>
					    <div class="carousel-item active <?php echo $has_super_deals === '1' ? 'superdeals-featured-cnabadv__featured-ads-section-listing--border' : ''; ?>">
<?php
			if (trim($pdata['autopart_video']['youtube_id'])) {
?>			
							<object id="advert-video" controls autoplay muted width="100%" class="video img img-fluid video-vertical-frame" data="<?php echo $pdata['autopart_video']['video_url'] ?>"></object>
<?php
			} else {
?>
							<video id="advert-video" controls autoplay muted width="100%" class="video img img-fluid video-vertical-frame" src="<?php echo $pdata['autopart_video']['video_url'] ?>"></video>
<?php
			}
?>
					    </div>
<?php
	}
?>

<?php
		foreach ($pdata['picture_array'] as $index_loop => $data_loop) {
?>
					    <div class="carousel-item <?php echo $pdata['autopart_video'] === false && $index_loop == 1 ? 'active' : ''; ?>" <?php echo $has_super_deals === '1' ? 'superdeals-featured-cnabadv__featured-ads-section-listing--border' : ''; ?>>
					      <img src="/images/ajax-loader.gif" data-src="<?php echo (@getimagesize($pdata['picture_array'][$index_loop]['url'])) ? $pdata['picture_array'][$index_loop]['url'] : '/images/autopart.png'; ?>" alt="Autopart" class="car-view__image lazyload">
					    </div>
<?php
		}
?>
					</div>

<?php 		
		if($has_super_deals === '1') { 
?>
						<button class="btn superdeals-featured-cnabadv__insights-btn superdeals-featured-cnabadv__insights-btn--position">Super Deals</button>
<?php 
		} 
?>
					  
					<!-- Left and right controls -->
					<a class="carousel-control-prev" href="#demo" data-slide="prev">
					    <span class="carousel-control-prev-icon"></span>
					</a>
					<a class="carousel-control-next" href="#demo" data-slide="next">
					    <span class="carousel-control-next-icon"></span>
					</a>

					<p class="carousel-text"></p>
				</div>
<?php
	}
?>
			</div>
			<div class="col-lg-6">
				<p class="car-view__flex">Last updated on <?php echo date("j F Y", strtotime($trans_advert_model->upd_data_date)) ?> <a href="">Report this ad</a></p>
				<h1 class="banner-search__title banner-search__title--gap"><?php echo $trans_advert_model->make_name; ?></h1>
				<div class="row">
					<div class="col-lg-3">
						<div class="row no-gutters">
							<div class="col-4">
								<p><img class="featured-ads-section__icon" src="/images/caricon.png" alt="condition" /></p>
							</div>
							<div class="col-8">
								<span class="featured-ads-section__text">Condition</span>
								<p><?php echo $pdata['condition']; ?></p>
							</div>
						</div>
					</div>
					<div class="col-lg-3">
	                    <div class="row no-gutters">
	                        <div class="col-4">
	                            <p><img class="featured-ads-section__icon" src="/images/location.png" alt="location" /></p>
	                        </div>
	                        <div class="col-8">
	                            <span class="featured-ads-section__text">Location</span>
<?php	
	if (trim($trans_advert_model->location_name) !== false) {		
?>                            
	                            <p><a href="/autopart?location=<?php echo $trans_advert_model->location_cd; ?>" class="car-view__link"><?php echo $trans_advert_model->location_name; ?></a></p>
<?php
	}
?>
	                        </div>
	                    </div>
	               </div>
				</div>
<?php
	$trans_advert_super_deals_model = $super_deals->getsuperdeals($trans_advert_model->urn_no);	
	$has_super_deals = $trans_advert_super_deals_model['got_super_deals'];

	$display_price = 'POA';

	if (isset($trans_advert_super_deals_model) && date("Y-m-d H:i:s") >= $trans_advert_super_deals_model['super_deals_schedule_start_date'] && date("Y-m-d H:i:s") <= $trans_advert_super_deals_model['super_deals_schedule_end_date']) {
		if ($trans_advert_model->price == 0) {
?>
			<p class="car-view__price"><span class="car-view__price-figure"><?php echo $display_price; ?></span></p>
<?php
		} else {
			if ($trans_advert_super_deals_model['sale_price'] == '0') {
				echo $display_price;
			} else {
				$display_price = number_format($trans_advert_super_deals_model['sale_price']); 
?>				
			<p class="car-view__price"><small>RM</small> <span class="car-view__price-figure"><?php echo $display_price; ?></span></p>
<?php
				if ($trans_advert_super_deals_model['sale_price'] != $trans_advert_super_deals_model['regular_price']) { 			
?>
					<p class="car-view__price"><small>RM</small> <span class="car-view__price-figure"><?php echo number_format($trans_advert_super_deals_model['regular_price']); ?></span></p>				
<?php
				}
			}
		}
	} else {
		if ($trans_advert_model->price == 0) {
			//
		} else {
			$display_price = number_format($trans_advert_model->price); 
		}
?>
			<p class="car-view__price"><small class="car-view__price-currency">RM</small> <span class="car-view__price-figure"><?php echo $display_price; ?></span></p>
<?php
	}
?>					
				<p class="text-right"><img src="/images/previous.png"> <img src="/images/next.png"></p>	
				<div class="row">
<?php
		$count = 0;
		if ($pdata['autopart_video'] !== false)	{
?>
					<div class="col-3">
						<p><img src="/images/vid_ico.png" alt="Video" class="car-view__video" onmouseover="showCarousel('0');"></img></p>
					</div>
<?php
			foreach ($pdata['picture_array'] as $index_loop => $data_loop) {
?>					
					<div class="col-3">
						<p><img src="/images/ajax-loader.gif" data-src="<?php echo (@getimagesize($pdata['picture_array'][$index_loop]['url'])) ? $pdata['picture_array'][$index_loop]['url'] : '/images/autopart.png'; ?>" class="car-view__image car-view__image--margin lazyload" onmouseover="showCarousel('<?php echo $index_loop; ?>');"></p>
					</div>
<?php
			}

		} else {
			foreach ($pdata['picture_array'] as $index_loop => $data_loop) {
?>					
					<div class="col-3">
						<p><img src="/images/ajax-loader.gif" data-src="<?php echo (@getimagesize($pdata['picture_array'][$index_loop]['url'])) ? $pdata['picture_array'][$index_loop]['url'] : '/images/autopart.png'; ?>" class="car-view__image car-view__image--margin lazyload" onmouseover="showCarousel('<?php echo $index_loop - 1; ?>');"></p>
					</div>
<?php
			}
		}
?>
				</div>
			</div>
		</div>
	</div>
	<div class="car-view__fixed">
		<div class="row no-gutters">
	    	<div class="col-lg-7">
				<p>
					<a class="btn car-view__fixed-button">
						<img src="/images/bookmark.png">
					</a> 
					<a class="btn car-view__fixed-button">
						<img src="/images/share.png">
					</a> 
					<a class="btn car-view__fixed-button">
						<img src="/images/dualcar.png">
					</a> 
					<span class="car-view__fixed-text car-view__fixed-text--stylise">
						<?php echo $trans_advert_model->make_name; ?>
					</span>
				</p>
	    	</div>
	    	<div class="col-lg-5">
				<div class="row no-gutters">
	    			<div class="col-lg-4">
	    				<div class="car-view__fixed-price">
		    				<p class="car-view__fixed-text text-right"><small class="car-view__fixed-text--position">RM</small> <span class="car-view__fixed-text--enlarge"><?php echo $display_price; ?></span></p>
	    				</div>
	    			</div>
	    			<div class="col-lg-8">
	    				<p>
		    				<button class="btn car-view__search-btn"><span class="car-view__search-btn-text">Contact</span></button>
							<button class="btn car-view__search-btn"><span class="car-view__search-btn-text">WhatsApp</span></button>
						</p>
	    			</div>
	    		</div>
	    	</div>
	    </div>
    </div>
	<div class="superdeals-featured-cnabadv superdeals-featured-cnabadv--background-color">		
		<div class="superdeals-featured-cnabadv-container">
			<h1 class="banner-search__title banner-search__title--position"><a href="<?php echo $pdata['dealer_seourl']; ?>"><?php echo $pdata['master_dealer_model'] !== false ? $pdata['master_dealer_model']->dealer_name : 'Private Dealer'; ?></a></h1>
			<p class="car-view__rating-list">
<?php 
	$master_dealer_review_model = $master_dealer_review->getdealerreviewinfo($trans_advert_model->dealer_id);
	if ($master_dealer_review_model) {
		if ($master_dealer_review_model->master_dealer_review_status == '1') {
			
			$count_master_dealer_review_info = $master_dealer_review_info->countdealerreviewinfo($master_dealer_review_model->dealer_id);

			$average_total_overall = $master_dealer_review_info->getaveragerating($master_dealer_review_model->dealer_id);

			switch ($average_total_overall) {
				case '1':
					echo '	<img src="/images/star.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 1.0 </span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '?type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>
						';
				break;
				case '1.5':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 1.5 </span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '?type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '2':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 2.0 </span>
								<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '?type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '2.5':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 2.5 </span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '?type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '3':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 3.0 </span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '?type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '3.5':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 3.5</span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '?type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '4':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 4.0</span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '?type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '4.5':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<span class="car-view__rating"> 4.5</span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '?type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>	
						';
				break;
				case '5':
					echo '	<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<img src="/images/star.png">
							<span class="car-view__rating"> 5.0</span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '?type=nav-dealer-review">
								<span class="rml-5">' . $count_master_dealer_review_info . ' reviews</span>
							</a>
						';
				break;
				case '0':
					echo '	<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<img src="/images/star-empty.png">
							<span class="car-view__rating"> 0.0</span>
							<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '?type=nav-dealer-review">
								<span>No review yet</span>
							</a>';
				break;
			}
		}
	} else {
		echo '	<img src="/images/star-empty.png">
				<img src="/images/star-empty.png">
				<img src="/images/star-empty.png">
				<img src="/images/star-empty.png">
				<img src="/images/star-empty.png">
				<span class="car-view__rating"> 0.0</span>
				<a class="car-view__link car-view__link--padding" href="' . $pdata['dealer_seourl'] . '?type=nav-dealer-review">
					<span>No review yet</span>
				</a>';
	}
?>			
			</p>
			<div class="row">
				<div class="col-lg-7">
					<div class="car-view__dealer">
						<div class="row no-gutters">
							<div class="col-1">
								<p><img class="featured-ads-section__icon" src="/images/dealer.png"></p>
							</div>
							<div class="col-11">
								<p class="car-view__dealer-type"><?php echo $pdata['master_dealer_model'] !== false ? 'Dealer' : 'Private'; ?></p>
							</div>
						</div>
					</div>
<?php
	if ($pdata['master_dealer_model'] !== false) {
?>				
					<div class="car-view__dealer">
						<div class="row">
							<div class="col-1">
								<p><img class="featured-ads-section__icon" src="/images/location.png"></p>
							</div>
							<div class="col-11">
								<p class="car-view__dealer-type"><?php echo $pdata['master_dealer_model']->location_name; ?></p>
							</div>
						</div>
					</div>				
					<div class="car-view__dealer">
						<div class="row">
							<div class="col-1">
								<p><img class="featured-ads-section__icon" src="/images/caricon.png"></p>
							</div>
							<div class="col-11">
								<p class="car-view__dealer-type"><a class="car-view__link" href="<?php echo $pdata['dealer_seourl']; ?>">View all <?php echo $pdata['master_dealer_model']->total_stock; ?> cars / items</a></p>
							</div>
						</div>
					</div>
<?php
	}
?>
				</div>
				<div class="col-lg-5">
					<p class="text-center"><button class="btn car-view__search-btn car-view__search-btn--align"><span class="car-view__search-btn-text">Drive there</span></button> <button class="btn car-view__search-btn car-view__search-btn--red car-view__search-btn--align"><span class="car-view__search-btn-text">Contact</span></button></p>
				</div>
			</div>
		</div>
	</div>
	<div class="superdeals-featured-cnabadv">
		<p class="banner-search__title banner-search__title--margin text-center">Description</p>
        <div class="container">
        	<div class="car-view__tab-show">
        		<p>
<?php
	$description = '';

	if ($pdata['master_dealer_model'] !== false) {
		if (trim($trans_advert_model->qe_add_desc) != '') {
			$temp = Yii::$app->general->avanserreplace($pdata['master_dealer_model']->dealer_id, $trans_advert_model->qe_add_desc);
			$description .= nl2br(Yii::$app->general->censoredfilter((is_string($temp) ? $temp : $trans_advert_model->qe_add_desc)));
		}
		
		if (trim($trans_advert_model->additional_desc) != '' ) {
			$temp = Yii::$app->general->avanserreplace($pdata['master_dealer_model']->dealer_id, $trans_advert_model->additional_desc);
			$description .= nl2br(Yii::$app->general->censoredfilter(($temp != 0 ? $temp : $trans_advert_model->additional_desc)));
		}
		
		if (trim($trans_advert_model->feature_desc) != '') {
			$temp = Yii::$app->general->avanserreplace($pdata['master_dealer_model']->dealer_id, $trans_advert_model->feature_desc);
			$description .= nl2br(Yii::$app->general->censoredfilter(($temp != 0 ? $temp : $trans_advert_model->feature_desc)));
		}
	} else {
		if (trim($trans_advert_model->qe_add_desc) != '') {
			$description .= nl2br(Yii::$app->general->censoredfilter($trans_advert_model->qe_add_desc));
		}
		
		if (trim($trans_advert_model->additional_desc) != '' ) {
			$description .= nl2br(Yii::$app->general->censoredfilter($trans_advert_model->additional_desc));
		}
		
		if (trim($trans_advert_model->feature_desc) != '') {
			$description .= nl2br(Yii::$app->general->censoredfilter($trans_advert_model->feature_desc));
		}
	}

	$censored_string = Yii::$app->general->censoredfilter($description, '-', $matched_phone);

	if ($matched_phone !== false) {
		$matched_phone = Yii::$app->general->filterphonex($matched_phone);
	}

	$filterphone_string = Yii::$app->general->filterphone($censored_string, '<a href="javascript:void(0)" onclick="showphoneindetails(this)" class="showphoneindetails-pre" data-phone="__PHONE__">' . $matched_phone . '</a>');
	echo $filterphone_string;

?>        		
        		</p>
        	</div>
        </div>
    </div>
    <div class="superdeals-featured-cnabadv superdeals-featured-cnabadv--background-color">
        <div class="superdeals-featured-cnabadv__box">
        	<h2 class="text-center banner-search__title banner-search__title--margin">Accessories for <?php echo $trans_advert_model->make_name; ?></h2>
        	<div class="row no-gutters">
<?php
	$count = 0;
	foreach ($pdata['similar'] as $featured_data_loop) {
		$count++;

		$trans_advert_super_deals_model = $super_deals->getsuperdeals($featured_data_loop['urn_no']);
			
		$has_super_deals = $trans_advert_super_deals_model['got_super_deals'];
		
		if ($featured_data_loop['poa'] == 1) {
			$price = 'POA';
		} else {
			$price = '<small class="featured-ads-section__price-currency">RM </small>' . number_format($featured_data_loop['price']);
		}		
?>       		
        		<div class="col-lg-4">
	        		<div class="superdeals-featured-cnabadv__featured-ads-section-listing superdeals-featured-cnabadv__featured-ads-section-listing--center margin-bottom-10">
	        			<p class="text-center car-view__position">
	        				<a href="<?php echo $featured_data_loop['seourl']; ?>?utm_medium=FeaturedAds_Detail"><img class="featured-ads-section__img" src="<?php echo (@getimagesize($featured_data_loop['image'])) ? $featured_data_loop['image'] : '/images/autopart.png'; ?>" alt="<?php addslashes($featured_data_loop['make_name']); ?>">
	        				</a>
	        				<span class="car-list__bookmark <?php echo (in_array($featured_data_loop['urn_no'], $pdata['bookmark_array']) ? 'car-list__show-bookmark' : '') ?>" data-urn="<?php echo $featured_data_loop['urn_no']; ?>"><img class="car-list__none" src="/images/bookmark.png"><img class="car-list__all" src="/images/bookmarked.png"></span>
	        			</p>
	        			 <p class="text-center featured-ads-section__title"><a href="<?php echo $featured_data_loop['seourl']; ?>?utm_medium=FeaturedAds_Detail"><?php echo $featured_data_loop['make_name']; ?></a></p>
	                    <p class="featured-ads-section__price text-right"><?php echo $price; ?></p>
	                    <hr/>
	                    <div class="row no-gutters">
	                        <div class="col-3">
	                            <p><img class="featured-ads-section__icon" src="/images/location.png" alt="location" /></p>
	                        </div>
	                        <div class="col-7">
	                            <span class="featured-ads-section__text">Location</span>
	                            <p><?php echo $featured_data_loop['location_name']; ?></p>
	                        </div>
	                    </div>
	                    <div class="row no-gutters">
	                        <div class="col-3">
	                            <p><img class="featured-ads-section__icon" src="/images/autoparticon.png" alt="category" /></p>
	                        </div>
	                        <div class="col-7">
	                            <span class="featured-ads-section__text">Category</span>
									<p><?php echo $temp_category[0]; ?></p>
	                        </div>
	                    </div>
		                </div>
	                </div>
<?php 
	} 
?>
    		</div>
    		<p class="text-center car-view__link-outer"><a href="<?php echo Yii::$app->general->generatecarsseo('autopart', 0, 0, '', 0, '', '', '', 0, ['make' => $trans_advert_model->make_cd, 'model' => $trans_advert_model->model_cd]); ?>" class="car-view__link">View all</a></p>
    	</div>
    </div>
</div>