<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use app\models\MasterDealer;
use app\models\MasterDealerReview;
use app\models\MasterDealerReviewInfo;
use app\models\MasterGeneral;
use app\models\SuperDeals;
use app\models\TransOrder;

/**
 * Specialnumber controller
 */
class SpecialnumberController extends Controller
{
	/**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
    	$master_dealer = new MasterDealer();
        $master_general = new MasterGeneral();
        $super_deals = new SuperDeals();
        $trans_order = new TransOrder();

        $location_array = $master_general->getLocation();

        $url_filter_params = [];

        $pdata = [];

        $pdata['page_location'] = 'Malaysia';
        $pdata['page'] = 1;
        $pdata['limit'] = 9;
        $pdata['offset'] = 0;
        
        $pdata['autoscroll'] = false;
        
        if (isset($_REQUEST['limit']) && $_REQUEST['limit']) {
            $limit = $_REQUEST['limit'];
            $pdata['offset'] = 0;
        }

        $pdata['page'] = 1;
        if (isset($_REQUEST['page']) && (int)$_REQUEST['page'] == $_REQUEST['page']) {
            
            $pdata['autoscroll'] = 'result';
            
            $pdata['page'] = (int)$_REQUEST['page'];

            if ($pdata['page'] > 0) {
                $pdata['offset'] = $pdata['limit'] * ($pdata['page'] - 1);
            } else {
                $pdata['offset'] = 0;
            }
        }

        if ($pdata['page'] > 1) {
            $url_filter_params['page'] = $pdata['page'];
        }

        $trans_advert_criteria = $trans_order->find()->where("t.urn_no is not null AND t.classification_cd ='2493' AND t.special_no <> '' AND t.status = '40'");
        $trans_advert_params_array = [];

        $pdata['filter']['location'] = '';
        $pdata['filter']['locationname'] = '';
        
        $pdata['filter']['superdeals'] = '0';
        if (isset($_REQUEST['superdeals'])) {
            $pdata['filter']['superdeals'] = $_REQUEST['superdeals'];
        }
        
        if (isset($_REQUEST['location']) && isset($location_array[$_REQUEST['location']])) {
            $pdata['filter']['location'] = $_REQUEST['location'];
            $pdata['filter']['locationname'] = strtolower(preg_replace('~[^\\pL\d]+~u', '_', $location_array[$pdata['filter']['location']]));

        } else if (isset($_REQUEST['locationname']) && trim($_REQUEST['locationname'])) {
            $pdata['filter']['locationname'] = ltrim($_REQUEST['locationname'], "/");
        }

        if (trim($pdata['filter']['location'])){
            $pdata['filter']['location'] = $pdata['filter']['location'];

            $trans_advert_criteria->andWhere(" location_cd = :location ");
            $trans_advert_params_array[':location'] = $pdata['filter']['location'];

            $tmp_location_array = Yii::$app->general->location();
            if (isset($tmp_location_array[$pdata['filter']['location']])) {
                $pdata['page_location'] = $tmp_location_array[$pdata['filter']['location']] . ', Malaysia';
            }
        }
        
        $pdata['filter']['dealers_type'] = 'all';
        if (isset($_REQUEST['dealers_type']) && in_array($_REQUEST['dealers_type'], ['1', '2'])) {
            $pdata['filter']['dealers_type'] = $_REQUEST['dealers_type'];
        }

        $pdata['sort'] = 'updatehigh';
        if (isset($_REQUEST['sort']) && in_array($_REQUEST['sort'], ['ahigh', 'alow', 'pricehigh', 'pricelow', 'updatelow', 'updatehigh'])) {
            $pdata['autoscroll'] = 'result';
            $pdata['sort'] = $_REQUEST['sort'];
        }

        $trans_advert_select_array = [];
        $trans_advert_select_array[] = '*';

        if (in_array($pdata['sort'], ['pricelow', 'pricehigh'])) {
            $trans_advert_select_array[] = "(CASE WHEN price = '0' OR poa_flg = '1' THEN 9999999999 ELSE price::numeric END) as plprice";
            $trans_advert_select_array[] = "(CASE WHEN price = '0' OR poa_flg = '1' THEN 0 ELSE price::numeric END) as phprice";
        } else if (in_array($pdata['sort'], ['alow', 'ahigh'])) {
            // $trans_advert_select_array[] = "regexp_replace(trim(from special_no), '[^0-9]+', '', 'g') as order_special_no";
            $trans_advert_select_array[] = "trim(from special_no) as order_special_no";
        }

        $trans_advert_criteria->select($trans_advert_select_array)->from("trans_order t");

        if (isset($_REQUEST['minPrice']) && trim($_REQUEST['minPrice'])) {
            $min_price = str_replace(",", "", $_REQUEST['minPrice']);
            if (is_numeric($min_price)) {
                $pdata['filter']['minPrice'] = $min_price;
                // $trans_advert_criteria->addCondition(" CAST(t.price AS DECIMAL) >= :minPrice");
                $trans_advert_criteria->andWhere(" (CASE WHEN price = 'POA' OR poa_flg = '1' THEN -1 ELSE CAST(t.price AS DECIMAL) END) >= :minPrice");
                $trans_advert_params_array[':minPrice'] = $pdata['filter']['minPrice'];
            }
        }

        if (isset($_REQUEST['maxPrice']) && trim($_REQUEST['maxPrice'])) {
            $max_price = str_replace(",", "", $_REQUEST['maxPrice']);
            if (is_numeric($max_price)) {
                $pdata['filter']['maxPrice'] = $max_price;
                // $trans_advert_criteria->addCondition(" CAST(t.price AS DECIMAL) <= :maxPrice ");
                $trans_advert_criteria->andWhere(" (CASE WHEN price = 'POA' OR poa_flg = '1' THEN -1 ELSE CAST(t.price AS DECIMAL) END) <= :maxPrice");
                $trans_advert_params_array[':maxPrice'] = $pdata['filter']['maxPrice'];
            }
        }
        
        switch ($pdata['filter']['dealers_type']) {
            case '1':
                $trans_advert_criteria->innerJoin("master_dealer md", "md.dealer_id = t.dealer_id");
                $trans_advert_criteria->andWhere("md.dealer_type = '1'");
                break;
            case '2':
                $trans_advert_criteria->andWhere("md.dealer_type = '2'");
                break;
        }

        $pdata['filter']['from_date'] = false;
        if (isset($_REQUEST['days']) && (int)$_REQUEST['days']) {
            if (in_array($_REQUEST['days'], [1, 7, 30, 90])) {
                $pdata['filter']['from_date'] = date('Y-m-d H:i:s', strtotime('- ' . $_REQUEST['days'] . ' days'));
            }
            
            $trans_advert_criteria->where("upd_data_date >= :from ");
            $trans_advert_params_array[':from'] = $pdata['filter']['from_date'];
        }

        $pdata['filter']['video_file'] = false;
        if (isset($_REQUEST['video']) && (int)$_REQUEST['video']) {
            $pdata['filter']['video_file'] = $_REQUEST['video'];
            
            $trans_advert_criteria->innerJoin('trans_order_youtube tay', 'tay.urn_no = t.urn_no');
        }

        if ($pdata['filter']['superdeals'] == '1') {
            $has_join_table = true;
            if ($has_join_table) {
                $trans_advert_criteria->innerJoin('super_deals sd', 'sd.urn_no = t.urn_no');
                $trans_advert_criteria->andWhere(" sd.super_deals_schedule_start_date <= :date ");
                $trans_advert_criteria->andWhere(" sd.super_deals_schedule_end_date >= :date ");
                $trans_advert_params_array[':date'] = date("Y-m-d H:i:s");
            } else {
                $trans_advert_criteria->innerJoin('super_deals', 'sd.urn_no = t.urn_no');
                $trans_advert_criteria->andWhere(" sd.super_deals_schedule_start_date <= :date ");
                $trans_advert_criteria->andWhere(" sd.super_deals_schedule_end_date >= :date ");
                $trans_advert_params_array[':date'] = date("Y-m-d H:i:s");
            }
        }

        $pdata['filter']['keyword'] = '';
        if (isset($_REQUEST['keyword'])) {
            $pdata['filter']['keyword'] = trim($_REQUEST['keyword']);

            if (!isset($_REQUEST['sort']) || $_REQUEST['sort'] == '') {
                $pdata['sort'] = 'alow';
            }
        }
        
        if (trim($pdata['filter']['keyword']) && trim($pdata['filter']['keyword'])) {
            $trans_advert_criteria->andWhere(" ( REPLACE(REPLACE(special_no, '\n', ''), ' ', '') ilike :keyword OR REPLACE(special_no, '\n', ' ') ilike :keyword OR special_no ilike :keyword )");
            $trans_advert_params_array[':keyword'] = "%" . trim($pdata['filter']['keyword']) . "%";
        }

        $pdata['filter']['hpno'] = '';
        if (isset($_REQUEST['hpno'])) {
            $pdata['autoscroll'] = 'result';
            $pdata['filter']['hpno'] = $_REQUEST['hpno'];
            $trans_advert_criteria->andWhere('special_no ilike :hpno');
            $trans_advert_params_array[':hpno'] = $pdata['filter']['hpno'] . "%";
        }
        
        if (count($trans_advert_params_array)) $trans_advert_criteria->params = $trans_advert_params_array;

        $pdata['page_label'] = 'phone numbers';

        $pdata['total'] = $trans_advert_criteria->count();
        
        switch($pdata['sort']) {
            case 'ahigh':
                // $trans_advert_criteria->orderBy("order_special_no DESC, upddate DESC";
                $trans_advert_criteria->orderBy([
                    "CAST(substring(nullif(trim(from regexp_replace(special_no, '[^0-9]+', '', 'g')), '') from 1 for 14) AS BIGINT)" => SORT_DESC, 
                    "trim(from regexp_replace(special_no, '[0-9]+', '', 'g'))" => SORT_DESC, 
                    "upd_data_date" => SORT_DESC
                ]);
            
            break;
            case 'alow':
                $trans_advert_criteria->orderBy([
                    "CAST(substring(nullif(trim(from regexp_replace(special_no, '[^0-9]+', '', 'g')), '') from 1 for 14) AS BIGINT)" => SORT_DESC, 
                    "trim(from regexp_replace(special_no, '[0-9]+', '', 'g'))" => SORT_ASC, 
                    "upd_data_date" => SORT_DESC
                ]);
                // $trans_advert_criteria->orderBy("order_special_no, upddate DESC";
            break;
            case 'pricehigh':
                $trans_advert_criteria->orderBy("phprice DESC, upd_data_date DESC");
            break;
            case 'pricelow':
                $trans_advert_criteria->orderBy("plprice, upd_data_date DESC");
            break;
            case 'updatelow':
                $trans_advert_criteria->orderBy("upd_data_date ");
            break;
            default:
                $trans_advert_criteria->orderBy("upd_data_date DESC ");
            break;
        }

        $trans_advert_criteria->limit = $pdata['limit'];
        $trans_advert_criteria->offset = $pdata['offset'];

        $trans_advert_model = $trans_advert_criteria->all();
        
        $recent_view_array = Yii::$app->general->advfromrecent('specialnumber');
        $pdata['recentview'] = [];
        if (count($recent_view_array)) {
            foreach ($recent_view_array as $recent_urn_loop) {
                $recent_view_info = $trans_order->loadinfo($recent_urn_loop);
                if ($recent_view_info !== false) {
                    $pdata['recentview'][] = $recent_view_info;
                }
            }
        }

        $pdata['bookmark_array'] = Yii::$app->general->advfrombookmark();

        /*
            Define URL
            Remember to put default at the last
        */

        $url_parse = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
        parse_str($url_parse, $url_array);
        unset($url_array['page']);
        unset($url_array['sort']);

        // $url_array['autoscroll'] = 'result';
        
        $pdata['url_superdeals_on'] = Yii::$app->general->generatecarsseo('specialnumber', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', 1, $url_array);
        $pdata['url_superdeals_off'] = Yii::$app->general->generatecarsseo('specialnumber', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', 0, $url_array);
        
        $pdata['url_sort_a_high'] = Yii::$app->general->generatecarsseo('specialnumber', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', 0, $url_array + ['sort' => 'ahigh']);
        $pdata['url_sort_a_low'] = Yii::$app->general->generatecarsseo('specialnumber', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', 0, $url_array + ['sort' => 'alow']);
        $pdata['url_sort_price_high'] = Yii::$app->general->generatecarsseo('specialnumber', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', 0, $url_array + ['sort' => 'pricehigh']);
        $pdata['url_sort_price_low'] = Yii::$app->general->generatecarsseo('specialnumber', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', 0, $url_array + ['sort' => 'pricelow']);
        $pdata['url_sort_update_low'] = Yii::$app->general->generatecarsseo('specialnumber', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', 0, $url_array + ['sort' => 'updatehigh']);
        $pdata['url_sort_update_high'] = Yii::$app->general->generatecarsseo('specialnumber', 0, 0, $pdata['filter']['locationname'], 0, '', '', '', 0, $url_array + ['sort' => 'updatelow']);

        $url_array['sort'] = $pdata['sort'];

        if (count($url_array)) {
            unset($url_array['sort']);
            $url_array['sort'] = $pdata['sort'];
        }

        $pdata['url_pagination'] = Yii::$app->general->generatecarsseo('special-phone-number', 0, 0, $pdata['filter']['locationname'], '__PAGE__', '', '', '', 0, $url_array);

    	return $this->render('index', ['master_dealer' => $master_dealer, 'master_general' => $master_general, 'super_deals' => $super_deals, 'trans_advert_model' => $trans_advert_model, 'pdata' => $pdata]);
    }

    public function actionView()
    {
        $master_dealer = new MasterDealer();
        $master_dealer_review = new MasterDealerReview();
        $master_dealer_review_info = new MasterDealerReviewInfo();
        $super_deals = new SuperDeals();
        $trans_order = new TransOrder();

        $pdata = [];

        if (isset($_REQUEST['urn_no'])) {
            $trans_advert_model = $trans_order->find()->where(['urn_no' => $_REQUEST['urn_no'], 'classification_cd' => '2493'])->one();

            if ($trans_advert_model) {
                $pdata['dealer_type'] =  $master_dealer->findOne($trans_advert_model->dealer_id);

                if($pdata['dealer_type']) {
                    $pdata['dealer_seourl'] = Yii::$app->general->generateseo('dealer', $trans_advert_model->dealer_id, $pdata['dealer_type']->dealer_id, $pdata['dealer_type']->dealer_name);
                } else {
                    $pdata['dealer_type'] = false;
                    $pdata['dealer_seourl'] = Yii::$app->general->generateseo('dealer', $trans_advert_model->dealer_id, 'private');
                }

                $matched_keyword = '';
                if (preg_match('/[\d]+/i', $trans_advert_model->special_no, $matched_keyword_array) !== false) {
                    if (count($matched_keyword_array) && isset($matched_keyword_array[0])) {
                        $matched_keyword = $matched_keyword_array[0];
                    }
                }

                $pdata['similar'] = $trans_order->similar('2493', 6, $trans_advert_model->make_cd, $trans_advert_model->model_cd, $matched_keyword, '');

                $pdata['bookmark_array'] = Yii::$app->general->advfrombookmark();

                return $this->render('view', ['master_dealer_review' => $master_dealer_review, 'master_dealer_review_info' => $master_dealer_review_info, 'master_dealer' => $master_dealer, 'super_deals' => $super_deals, 'trans_advert_model' => $trans_advert_model, 'pdata' => $pdata]);
            } else {
                return $this->redirect(Yii::$app->createAbsoluteUrl('specialnumber'));
            }
        } else {
            return $this->redirect(Yii::$app->createAbsoluteUrl('specialnumber'));
        }
    }
}