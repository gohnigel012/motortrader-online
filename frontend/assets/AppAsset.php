<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/style.css',
        'js/jquery-ui/jquery-ui.css',
    ];
    public $js = [
        'js/jquery-ui/jquery-ui.js',
        'js/lazyload/lazyload.js',
        'js/protoSlider/protoSlider.js',
        'js/script.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}
