<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);

?>
<?php echo $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language; ?>">
    <head>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-TMZ6DRS');</script>
        <!-- End Google Tag Manager -->

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-184034168-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-184034168-1');
        </script>
        <meta charset="<?php echo Yii::$app->charset; ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="This website based on the existing MotorTrader website that is currently being maintained by Cloudkia Solutions Sdn Bhd. This website sells advertisements which shows sales of different cars from different vendors.">
        <?php echo $this->registerCsrfMetaTags(); ?>
        <title><?php echo Html::encode($this->title); ?></title>
        <?php echo $this->head(); ?>
        <link rel="canonical" href="http://motortrader-project.localhost">
        <link rel="stylesheet" type="text/css" href="/assets/owl.carousel/owl.carousel.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <script>
            var DEFINE_AJAX_MODEL_URL = "<?php echo Yii::$app->urlManager->createAbsoluteUrl('ajax/model')?>";
            var DEFINE_AJAX_VARIANT_URL = "<?php echo Yii::$app->urlManager->createAbsoluteUrl('ajax/variant')?>";
            var DEFINE_AJAX_ADD_BOOKMARK_URL = "<?php echo Yii::$app->urlManager->createAbsoluteUrl('bookmark/addbookmark')?>";
            var DEFINE_AJAX_REMOVE_BOOKMARK_URL = "<?php echo Yii::$app->urlManager->createAbsoluteUrl('bookmark/removebookmark')?>";
            var DEFINE_AJAX_REPORT_ADVERT_URL = "<?php echo Yii::$app->urlManager->createAbsoluteUrl('ajax/reportadv')?>";
            var DEFINE_AJAX_REMOVE_ADVERT_URL = "<?php echo Yii::$app->urlManager->createAbsoluteUrl('ajax/removeadv')?>";
            var DEFINE_CALCULATIOR_CALCULATE_ADVERT_URL = "<?php echo Yii::$app->urlManager->createAbsoluteUrl('calculator/calculate')?>";
            var DEFINE_ROADTAX_CALCULATIOR_CALCULATE_ADVERT_URL = "<?php echo Yii::$app->urlManager->createAbsoluteUrl('calculator/roadtaxcalculator')?>";
        </script>
    </head>
<?php echo $this->beginBody(); ?>
    <body>
        
        <?php echo $this->render('header'); ?>

        <?php echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]); ?>
        <?php echo Alert::widget(); ?>
        <?php echo $content; ?>

        <?php echo $this->render('footer'); ?>

<?php echo $this->endBody(); ?>
    </body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TMZ6DRS"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?php
        // echo $this->render('advancedSearch_js');

        if (Url::current() == "/") {
            echo $this->render('/site/index_js');
        } else if (strpos(Url::current(), "/car/index") !== false) {
            echo $this->render('/car/index_js');
        } else if(strpos(Url::current(), "/car/view") !== false) {
            echo $this->render('/car/view_js');
        } else if (strpos(Url::current(), "/dealer/index") !== false) {
            echo $this->render('/dealer/index_js');
        } else if (strpos(Url::current(), "/dealer/view") !== false) {
            echo $this->render('/dealer/view_js');
        } else if (strpos(Url::current(), "/autopart/index") !== false) {
            echo $this->render('/autopart/index_js');
        } else if (strpos(Url::current(), "/autopart/view") !== false) {
            echo $this->render('/autopart/view_js');
        } else if (strpos(Url::current(), "/numberplate/index") !== false) {
            echo $this->render('/numberplate/index_js');
        } else if (strpos(Url::current(), "/numberplate/view") !== false) {
            echo $this->render('/numberplate/view_js');
        } else if (strpos(Url::current(), "/bike/index") !== false) {
            echo $this->render('/bike/index_js');
        } else if (strpos(Url::current(), "/bike/view") !== false) {
            echo $this->render('/bike/view_js');
        } else if (strpos(Url::current(), "/specialnumber/index") !== false) {
            echo $this->render('/specialnumber/index_js');
        } else if (strpos(Url::current(), "/specialnumber/view") !== false) {
            echo $this->render('/specialnumber/view_js');
        } else if (Url::current() == "/site/login") {
            echo $this->render('/site/login_js');
        } else if (Url::current() == "/site/faq") {
            echo $this->render('/site/faq_js');
        } else if (Url::current() == "/bookmark/index") {
            echo $this->render('/bookmark/index_js');
        }
     ?>
</html>
<?php echo $this->endPage(); ?>
