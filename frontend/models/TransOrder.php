<?php

namespace app\models;

use Yii;
use yii\db\Connection;
use app\models\TransOrderExtraInfo;

/**
 * This is the model class for table "trans_order".
 *
 * @property string $urn_no URN Number
 * @property string $urn_no_seq
 * @property string|null $status
 * @property string|null $dealer_id
 * @property string|null $nvic
 * @property string|null $order_number
 * @property string|null $reg_data_date
 * @property string|null $upd_data_date
 * @property string|null $special_no
 * @property string|null $package_cd
 * @property string|null $classification_cd
 * @property string|null $send_mail_flg 1:Send mail required
 * @property string|null $make_nil_flg 1:If make not in list
 * @property string|null $make_cd
 * @property string|null $make_name from text when CarNotInListFlg = 1
 * @property string|null $model_nil_flg 1:If model not in list
 * @property string|null $model_cd
 * @property string|null $model_name from text when CarNotInListFlg = 1
 * @property string|null $variant_nil_flg 1:If variant not in list
 * @property string|null $variant_cd
 * @property string|null $variant_name from text when CarNotInListFlg = 1
 * @property string|null $year_make
 * @property string|null $color_code
 * @property string|null $color_name
 * @property string|null $price
 * @property string|null $phone_no1
 * @property string|null $phone_no2
 * @property string|null $phone_no3
 * @property string|null $location_cd
 * @property string|null $feature_desc
 * @property string|null $additional_desc
 * @property string|null $ad_text
 * @property string|null $title_content For Max Power Use
 * @property string|null $upd_user
 * @property string|null $upd_date
 * @property string|null $issue_date1
 * @property string|null $issue_date2
 * @property float|null $gross_price
 * @property string|null $poa_flg 1:On
 * @property string|null $latest_flg
 * @property string|null $sales_status
 * @property string|null $location_name
 * @property string|null $fp_flg
 * @property string|null $qe_add_desc
 * @property string|null $plan_cd
 * @property string|null $payment_id
 * @property string|null $issue_no
 * @property string|null $year_reg
 * @property string|null $city_name
 * @property string|null $ebay_flg
 * @property string|null $ebay_itemid
 * @property string|null $ebay_start_date
 * @property string|null $ebay_expiry_date
 * @property string|null $magazine_title
 * @property string|null $is_used_flg
 * @property string|null $transmission_code
 * @property string|null $transmission_name
 * @property string|null $mileage_code
 * @property string|null $mileage_name
 * @property string|null $engine_code
 * @property string|null $engine_name
 * @property string|null $bodytype_code
 * @property string|null $bodytype_name
 * @property string|null $coupon
 * @property string|null $coupon_redeemed
 * @property int|null $uploaded_to_aws
 * @property string|null $premium_flg
 */
class TransOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trans_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['urn_no', 'urn_no_seq'], 'required'],
            [['reg_data_date', 'upd_data_date', 'upd_date', 'ebay_start_date', 'ebay_expiry_date'], 'safe'],
            [['gross_price'], 'number'],
            [['qe_add_desc'], 'string'],
            [['uploaded_to_aws'], 'default', 'value' => null],
            [['uploaded_to_aws'], 'integer'],
            [['urn_no', 'dealer_id', 'make_cd', 'model_cd', 'variant_cd', 'year_make', 'upd_user', 'plan_cd', 'year_reg', 'ebay_itemid', 'transmission_code', 'mileage_code', 'engine_code', 'bodytype_code'], 'string', 'max' => 20],
            [['urn_no_seq', 'issue_no'], 'string', 'max' => 5],
            [['status', 'sales_status'], 'string', 'max' => 2],
            [['nvic', 'order_number', 'package_cd', 'classification_cd', 'color_code', 'location_cd', 'issue_date1', 'issue_date2'], 'string', 'max' => 10],
            [['special_no', 'color_name', 'price', 'phone_no1', 'phone_no2', 'phone_no3', 'payment_id'], 'string', 'max' => 50],
            [['send_mail_flg', 'make_nil_flg', 'model_nil_flg', 'variant_nil_flg', 'poa_flg', 'latest_flg', 'fp_flg', 'ebay_flg', 'is_used_flg', 'coupon_redeemed', 'premium_flg'], 'string', 'max' => 1],
            [['make_name', 'model_name', 'variant_name', 'feature_desc', 'title_content', 'magazine_title'], 'string', 'max' => 255],
            [['additional_desc'], 'string', 'max' => 1024],
            [['ad_text'], 'string', 'max' => 500],
            [['location_name', 'city_name'], 'string', 'max' => 100],
            [['transmission_name', 'mileage_name', 'engine_name', 'bodytype_name'], 'string', 'max' => 40],
            [['coupon'], 'string', 'max' => 32],
            [['urn_no', 'urn_no_seq'], 'unique', 'targetAttribute' => ['urn_no', 'urn_no_seq']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'urn_no' => 'Urn No',
            'urn_no_seq' => 'Urn No Seq',
            'status' => 'Status',
            'dealer_id' => 'Dealer ID',
            'nvic' => 'Nvic',
            'order_number' => 'Order Number',
            'reg_data_date' => 'Reg Data Date',
            'upd_data_date' => 'Upd Data Date',
            'special_no' => 'Special No',
            'package_cd' => 'Package Cd',
            'classification_cd' => 'Classification Cd',
            'send_mail_flg' => 'Send Mail Flg',
            'make_nil_flg' => 'Make Nil Flg',
            'make_cd' => 'Make Cd',
            'make_name' => 'Make Name',
            'model_nil_flg' => 'Model Nil Flg',
            'model_cd' => 'Model Cd',
            'model_name' => 'Model Name',
            'variant_nil_flg' => 'Variant Nil Flg',
            'variant_cd' => 'Variant Cd',
            'variant_name' => 'Variant Name',
            'year_make' => 'Year Make',
            'color_code' => 'Color Code',
            'color_name' => 'Color Name',
            'price' => 'Price',
            'phone_no1' => 'Phone No1',
            'phone_no2' => 'Phone No2',
            'phone_no3' => 'Phone No3',
            'location_cd' => 'Location Cd',
            'feature_desc' => 'Feature Desc',
            'additional_desc' => 'Additional Desc',
            'ad_text' => 'Ad Text',
            'title_content' => 'Title Content',
            'upd_user' => 'Upd User',
            'upd_date' => 'Upd Date',
            'issue_date1' => 'Issue Date1',
            'issue_date2' => 'Issue Date2',
            'gross_price' => 'Gross Price',
            'poa_flg' => 'Poa Flg',
            'latest_flg' => 'Latest Flg',
            'sales_status' => 'Sales Status',
            'location_name' => 'Location Name',
            'fp_flg' => 'Fp Flg',
            'qe_add_desc' => 'Qe Add Desc',
            'plan_cd' => 'Plan Cd',
            'payment_id' => 'Payment ID',
            'issue_no' => 'Issue No',
            'year_reg' => 'Year Reg',
            'city_name' => 'City Name',
            'ebay_flg' => 'Ebay Flg',
            'ebay_itemid' => 'Ebay Itemid',
            'ebay_start_date' => 'Ebay Start Date',
            'ebay_expiry_date' => 'Ebay Expiry Date',
            'magazine_title' => 'Magazine Title',
            'is_used_flg' => 'Is Used Flg',
            'transmission_code' => 'Transmission Code',
            'transmission_name' => 'Transmission Name',
            'mileage_code' => 'Mileage Code',
            'mileage_name' => 'Mileage Name',
            'engine_code' => 'Engine Code',
            'engine_name' => 'Engine Name',
            'bodytype_code' => 'Bodytype Code',
            'bodytype_name' => 'Bodytype Name',
            'coupon' => 'Coupon',
            'coupon_redeemed' => 'Coupon Redeemed',
            'uploaded_to_aws' => 'Uploaded To Aws',
            'premium_flg' => 'Premium Flg',
        ];
    }

    public function getTransOrderPictures()
    {
      return $this->hasMany(TransOrderPicture::className(), ['urn_no' => 'urn_no', 'urn_no_seq' => 'urn_no_seq']);
    }

    public function featured($classification_cd, $limit=12, $make='', $model='', $keyword='', $used_flag='', $interactive = 0) {

        $featured_array = [];

        $trans_order_memcache_key = "/trans_advert/featured/make/" . $make . '/model/' . $model . '/keyword/' . $keyword . '/used/' . $used_flag . '/limit/' . $limit . '/interactive/' . $interactive;
        
        $connection = new Connection([
            'dsn' => 'pgsql:host=localhost;port=5432;dbname=mtsa',
            'username' => 'postgres',
            'password' => 'postgres',
        ]);
        $connection->open();

        $trans_order_memcache = Yii::$app->cache->get($trans_order_memcache_key);
        if ($trans_order_memcache !== false) {
            $featured_array = $trans_order_memcache;
        } else {
            $union_sql_array = [];

            $bind_keyword_array = [];

            // prioritized used flag and I assume all type of used flag has lot of adverts
            if($classification_cd == '1167') { //bike
                switch ($used_flag) {
                    case 'used':
                                if (trim($keyword)) {
                                    $bind_keyword_array[':keyword'] = "%" . trim($keyword) . "%";

                                    $keyword_temp_array = array();
                                    // $keyword_temp_array[] = " dealer_name ILIKE :keyword" ;
                                    $keyword_temp_array[] = " car_info ILIKE :keyword" ;
                                    $keyword_temp_array[] = " make_name ILIKE :keyword" ;
                                    $keyword_temp_array[] = " model_name ILIKE :keyword" ;
                                    $keyword_temp_array[] = " variant_name ILIKE :keyword" ;
                                    // $keyword_temp_array[] = " qe_add_desc ILIKE :keyword" ;

                                    $union_sql_array[] = "( SELECT 1 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND is_used_flg = '1' AND classification_cd = '1167' AND (" . implode(" OR ", $keyword_temp_array) . ") 
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                }

                                if ($make !== false && trim($make) && $model !== false && trim($model)) {
                                    
                                    $union_sql_array[] = "( SELECT 2 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND is_used_flg = '1' AND classification_cd = '1167' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' 
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                
                                } else if ($make !== false && trim($make) && $model == false) {
                                    
                                    $union_sql_array[] = "( SELECT 3 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND is_used_flg = '1' AND classification_cd = '1167' AND make_cd = '" . trim($make) . "' 
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                
                                } else {

                                    $union_sql_array[] = "( SELECT 5 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND is_used_flg = '1' AND classification_cd = '1167' 
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                
                                }       
                        
                        break;
                    case 'new':
                                if (trim($keyword)) {
                                    $bind_keyword_array[':keyword'] = "%" . trim($keyword) . "%";

                                    $keyword_temp_array = array();
                                    $keyword_temp_array[] = " make_name ILIKE :keyword" ;
                                    $keyword_temp_array[] = " model_name ILIKE :keyword" ;
                                    $keyword_temp_array[] = " variant_name ILIKE :keyword" ;
                                    // $keyword_temp_array[] = " dealer_name ILIKE :keyword" ;
                                    $keyword_temp_array[] = " car_info ILIKE :keyword" ;
                                    // $keyword_temp_array[] = " qe_add_desc ILIKE :keyword" ;

                                    $union_sql_array[] = "( SELECT 1 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND is_used_flg = '2' AND classification_cd = '1167' AND (" . implode(" OR ", $keyword_temp_array) . ") 
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                }

                                if ($make !== false && trim($make) && $model !== false && trim($model)) {
                                    
                                    $union_sql_array[] = "( SELECT 2 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND is_used_flg = '2' AND classification_cd = '1167' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' 
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                
                                } else if ($make !== false && trim($make) && $model == false) {
                                    
                                    $union_sql_array[] = "( SELECT 3 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND is_used_flg = '2' AND classification_cd = '1167' AND make_cd = '" . trim($make) . "' 
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                
                                } else {

                                    $union_sql_array[] = "( SELECT 5 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND is_used_flg = '2' AND classification_cd = '1167' 
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                            
                                }
                                
                        break;
                    case 'recond':
                                if (trim($keyword)) {
                                    $bind_keyword_array[':keyword'] = "%" . trim($keyword) . "%";

                                    $keyword_temp_array = array();
                                    // $keyword_temp_array[] = " dealer_name ILIKE :keyword" ;
                                    $keyword_temp_array[] = " car_info ILIKE :keyword" ;
                                    $keyword_temp_array[] = " make_name ILIKE :keyword" ;
                                    $keyword_temp_array[] = " model_name ILIKE :keyword" ;
                                    $keyword_temp_array[] = " variant_name ILIKE :keyword" ;
                                    // $keyword_temp_array[] = " qe_add_desc ILIKE :keyword" ;

                                    $union_sql_array[] = "( SELECT 1 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND is_used_flg = '3' AND classification_cd = '1167' AND (" . implode(" OR ", $keyword_temp_array) . ") 
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                }

                                if ($make !== false && trim($make) && $model !== false && trim($model)) {
                                    
                                    $union_sql_array[] = "( SELECT 2 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND is_used_flg = '3' AND classification_cd = '1167' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' 
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                
                                } else if ($make !== false && trim($make) && $model == false) {
                                    
                                    $union_sql_array[] = "( SELECT 3 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND is_used_flg = '3' AND classification_cd = '1167' AND make_cd = '" . trim($make) . "' 
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                
                                } else {

                                    $union_sql_array[] = "( SELECT 5 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND is_used_flg = '3' AND classification_cd = '1167' 
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                                }   
                                
                        break;
                    default:
                                if (trim($keyword)) {
                                    $bind_keyword_array[':keyword'] = "%" . trim($keyword) . "%";

                                    $keyword_temp_array = array();
                                    // $keyword_temp_array[] = " dealer_name ILIKE :keyword" ;
                                    $keyword_temp_array[] = " car_info ILIKE :keyword" ;
                                    $keyword_temp_array[] = " make_name ILIKE :keyword" ;
                                    $keyword_temp_array[] = " model_name ILIKE :keyword" ;
                                    $keyword_temp_array[] = " variant_name ILIKE :keyword" ;
                                    // $keyword_temp_array[] = " qe_add_desc ILIKE :keyword" ;

                                    $union_sql_array[] = "( SELECT 1 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND classification_cd = '1167' AND (" . implode(" OR ", $keyword_temp_array) . ")
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                }

                                if ($make !== false && trim($make) && $model !== false && trim($model)) {
                                    
                                    $union_sql_array[] = "( SELECT 2 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND classification_cd = '1167' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' 
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                                            
                                } else if ($make !== false && trim($make) && $model == false) {
                                    
                                    $union_sql_array[] = "( SELECT 3 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND classification_cd = '1167' AND make_cd = '" . trim($make) . "' 
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                
                                } else {

                                    $union_sql_array[] = "( SELECT 5 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND classification_cd = '1167' 
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                                        
                                }   
                                
                        break;
                }
            } else {
                switch ($used_flag) {
                case 'used':
                    if (trim($keyword)) {
                        $bind_keyword_array[':keyword'] = "%" . trim($keyword) . "%";

                        $keyword_temp_array = [];
                        // $keyword_temp_array[] = " dealer_name ILIKE :keyword" ;
                        $keyword_temp_array[] = " make_name ILIKE :keyword" ;
                        $keyword_temp_array[] = " model_name ILIKE :keyword" ;
                        $keyword_temp_array[] = " variant_name ILIKE :keyword" ;
                        // $keyword_temp_array[] = " qe_add_desc ILIKE :keyword" ;

                        $union_sql_array[] = "( SELECT 1 as priority, * FROM trans_order WHERE premium_flg = '1' AND is_used_flg = '1' AND classification_cd = '1141' AND (" . implode(" OR ", $keyword_temp_array) . ") ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                    }

                    if ($make !== false && trim($make) && $model !== false && trim($model)) {
                        
                        $union_sql_array[] = "( SELECT 2 as priority, * FROM trans_order WHERE premium_flg = '1' AND is_used_flg = '1' AND classification_cd = '1141' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                    
                    } else if ($make !== false && trim($make) && $model == false) {
                        
                        $union_sql_array[] = "( SELECT 3 as priority, * FROM trans_order WHERE premium_flg = '1' AND is_used_flg = '1' AND classification_cd = '1141' AND make_cd = '" . trim($make) . "' ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                    
                    } else {

                        $union_sql_array[] = "( SELECT 5 as priority, * FROM trans_order WHERE premium_flg = '1' AND is_used_flg = '1' AND classification_cd = '1141' ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                    
                    }       
            
                    break;
                case 'new':
                    if (trim($keyword)) {
                        $bind_keyword_array[':keyword'] = "%" . trim($keyword) . "%";

                        $keyword_temp_array = [];
                        $keyword_temp_array[] = " make_name ILIKE :keyword" ;
                        $keyword_temp_array[] = " model_name ILIKE :keyword" ;
                        $keyword_temp_array[] = " variant_name ILIKE :keyword" ;
                        // $keyword_temp_array[] = " dealer_name ILIKE :keyword" ;
                        // $keyword_temp_array[] = " qe_add_desc ILIKE :keyword" ;

                        $union_sql_array[] = "( SELECT 1 as priority, * 
                                                FROM trans_order
                                                WHERE premium_flg = '1' AND is_used_flg = '2' AND classification_cd = '1141' AND (" . implode(" OR ", $keyword_temp_array) . ") 
                                                ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                    }

                    if ($make !== false && trim($make) && $model !== false && trim($model)) {
                        
                        $union_sql_array[] = "( SELECT 2 as priority, * 
                                                FROM trans_order
                                                WHERE premium_flg = '1' AND is_used_flg = '2' AND classification_cd = '1141' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "'  
                                                ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                    
                    } else if ($make !== false && trim($make) && $model == false) {
                        
                        $union_sql_array[] = "( SELECT 3 as priority, * 
                                                FROM trans_order
                                                WHERE premium_flg = '1' AND is_used_flg = '2' AND classification_cd = '1141' AND make_cd = '" . trim($make) . "' 
                                                ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                    
                    } else {

                        $union_sql_array[] = "( SELECT 5 as priority, * 
                                                FROM trans_order
                                                WHERE premium_flg = '1' AND is_used_flg = '2' AND classification_cd = '1141' 
                                                ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                
                    }
                    
                    break;
                case 'recond':
                    if (trim($keyword)) {
                        $bind_keyword_array[':keyword'] = "%" . trim($keyword) . "%";

                        $keyword_temp_array = [];
                        // $keyword_temp_array[] = " dealer_name ILIKE :keyword" ;
                        $keyword_temp_array[] = " make_name ILIKE :keyword" ;
                        $keyword_temp_array[] = " model_name ILIKE :keyword" ;
                        $keyword_temp_array[] = " variant_name ILIKE :keyword" ;
                        // $keyword_temp_array[] = " qe_add_desc ILIKE :keyword" ;

                        $union_sql_array[] = "( SELECT 1 as priority, * 
                                                FROM trans_order
                                                WHERE premium_flg = '1' AND is_used_flg = '3' AND classification_cd = '1141' AND (" . implode(" OR ", $keyword_temp_array) . ") 
                                                ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                    }

                    if ($make !== false && trim($make) && $model !== false && trim($model)) {
                        
                        $union_sql_array[] = "( SELECT 2 as priority, * 
                                                FROM trans_order
                                                WHERE premium_flg = '1' AND is_used_flg = '3' AND classification_cd = '1141' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' 
                                                ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                    
                    } else if ($make !== false && trim($make) && $model == false) {
                        
                        $union_sql_array[] = "( SELECT 3 as priority, * 
                                                FROM trans_order
                                                WHERE premium_flg = '1' AND is_used_flg = '3' AND classification_cd = '1141' AND make_cd = '" . trim($make) . "' 
                                                ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                    
                    } else {

                        $union_sql_array[] = "( SELECT 5 as priority, * 
                                                FROM trans_order
                                                WHERE premium_flg = '1' AND is_used_flg = '3' AND classification_cd = '1141' 
                                                ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                    }   
                            
                    break;
                case 'commercial':
                    if (trim($keyword)) {
                        $bind_keyword_array[':keyword'] = "%" . trim($keyword) . "%";

                        $keyword_temp_array = [];
                        // $keyword_temp_array[] = " dealer_name ILIKE :keyword" ;
                        $keyword_temp_array[] = " make_name ILIKE :keyword" ;
                        $keyword_temp_array[] = " model_name ILIKE :keyword" ;
                        $keyword_temp_array[] = " variant_name ILIKE :keyword" ;
                        // $keyword_temp_array[] = " qe_add_desc ILIKE :keyword" ;

                        $union_sql_array[] = "( SELECT 1 as priority, * 
                                                FROM trans_order
                                                WHERE premium_flg = '1' AND bodytype_code = 'bt9' AND classification_cd = '1141' AND (" . implode(" OR ", $keyword_temp_array) . ") 
                                                ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                    }

                    if ($make !== false && trim($make) && $model !== false && trim($model)) {
                        
                        $union_sql_array[] = "( SELECT 2 as priority, * 
                                                FROM trans_order
                                                WHERE premium_flg = '1' AND bodytype_code = 'bt9' AND classification_cd = '1141' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' 
                                                ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                    
                    } else if ($make !== false && trim($make) && $model == false) {
                        
                        $union_sql_array[] = "( SELECT 3 as priority, * 
                                                FROM trans_order
                                                WHERE premium_flg = '1' AND bodytype_code = 'bt9' AND classification_cd = '1141' AND make_cd = '" . trim($make) . "' 
                                                ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                    
                    } else {

                        $union_sql_array[] = "( SELECT 5 as priority, * 
                                                FROM trans_order
                                                WHERE premium_flg = '1' AND bodytype_code = 'bt9' AND classification_cd = '1141' 
                                                ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                    
                    }   
                            
                    break;
                default:
                    if (trim($keyword)) {
                        $bind_keyword_array[':keyword'] = "%" . trim($keyword) . "%";

                        $keyword_temp_array = [];
                        // $keyword_temp_array[] = " dealer_name ILIKE :keyword" ;
                        $keyword_temp_array[] = " make_name ILIKE :keyword" ;
                        $keyword_temp_array[] = " model_name ILIKE :keyword" ;
                        $keyword_temp_array[] = " variant_name ILIKE :keyword" ;
                        // $keyword_temp_array[] = " qe_add_desc ILIKE :keyword" ;

                        $union_sql_array[] = "( SELECT 1 as priority, * 
                                                FROM trans_order
                                                WHERE premium_flg = '1' AND classification_cd = '1141' AND (" . implode(" OR ", $keyword_temp_array) . ")
                                                ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                    }

                    if ($make !== false && trim($make) && $model !== false && trim($model)) {
                        
                        $union_sql_array[] = "( SELECT 2 as priority, * 
                                                FROM trans_order
                                                WHERE premium_flg = '1' AND classification_cd = '1141' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' 
                                                ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                                
                    } else if ($make !== false && trim($make) && $model == false) {
                        
                        $union_sql_array[] = "( SELECT 3 as priority, * 
                                                FROM trans_order
                                                WHERE premium_flg = '1' AND classification_cd = '1141' AND make_cd = '" . trim($make) . "' 
                                                ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                    
                    } else {

                        $union_sql_array[] = "( SELECT 5 as priority, * 
                                                FROM trans_order
                                                WHERE premium_flg = '1' AND classification_cd = '1141'
                                                ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                            
                    }   
                            
                    break;
            }
            }
            

            $trans_order_model_command = $connection->createCommand("SELECT * FROM (" . implode(" UNION ", $union_sql_array) . ") as class ORDER BY priority FETCH FIRST 12 ROWS ONLY");

            if (count($bind_keyword_array)) { // with keyword
                foreach ($bind_keyword_array as $bind_keyword_key_loop => $bind_keyword_value_loop) {
                    $trans_order_model_command->bindParam($bind_keyword_key_loop, $bind_keyword_value_loop);
                }
            }

            $trans_order_model = $trans_order_model_command->queryAll();
            
            foreach($trans_order_model as $trans_order_data_loop){

                 if ($classification_cd == '1167') { 
                    $seourl = Yii::$app->general->generateseo('bike', $trans_order_data_loop['urn_no'], $trans_order_data_loop['make_name'] . '-' . $trans_order_data_loop['model_name']);
                } else {
                   $seourl = Yii::$app->general->generateseo('car', $trans_order_data_loop['urn_no'], $trans_order_data_loop['make_name'] . '-' . $trans_order_data_loop['model_name']); 
               }
                // if ($classification_cd == '1166') { 
                //     $seourl = Yii::$app->general->generateseo('autopart', $trans_order_data_loop['urn_no'], $trans_order_data_loop['make_name'] . '-' . $trans_order_data_loop['model_name']));
                // } else if ($advert_category == '3') { 
                //     // $seourl = Yii::$app->general->generateseo('numberplate', $trans_order_data_loop['urn_no'], $trans_order_data_loop['car_info']);   
                //     $seourl = "bbbb";                       
                // } else if ($advert_category == '5') { 
                //     // $seourl = Yii::$app->general->generateseo('special-phone-number', $trans_order_data_loop['urn_no'], $trans_order_data_loop['car_info']);           
                //     $seourl = "cccc";              
                // }
                
                $advert_image = Yii::$app->general->advertimageurl($trans_order_data_loop['urn_no'] . '.jpg', 'webad');
                 // $advert_image = "images/vios.png";
                
                //process each item here
                $featured_array[] = [
                                        'seourl' => $seourl,
                                        'urn_no' => $trans_order_data_loop['urn_no'],
                                        'year_make' => $trans_order_data_loop['year_make'],
                                        'make_name' => $trans_order_data_loop['make_name'],
                                        'model_name' => $trans_order_data_loop['model_name'],
                                        'location_name' => $trans_order_data_loop['location_name'],
                                        'bodytype_code' => $trans_order_data_loop['bodytype_code'],
                                        'image' => $advert_image,
                                        'price' => $trans_order_data_loop['price'],
                                        'poa' => ($trans_order_data_loop['price'] == 0 ? 1 : $trans_order_data_loop['poa_flg']),
                                        'mileage_name' => $trans_order_data_loop['mileage_name'],
                                        'transmission_name' => $trans_order_data_loop['transmission_name'],
                                        'is_used_flg' => $trans_order_data_loop['is_used_flg'],
                                        'dealer_id' => $trans_order_data_loop['dealer_id']
                                    ];

            }

            Yii::$app->cache->set($trans_order_memcache_key, $featured_array, 240);
        }
        

        return $featured_array;
    }

    public function similar($classification_cd, $limit=12, $make='', $model='', $keyword='', $used_flag='', $interactive = 0) {

        $featured_array = [];

        $trans_order_memcache_key = "/trans_order/similar//make/" . $make . '/model/' . $model . '/keyword/' . $keyword . '/used/' . $used_flag . '/limit/' . $limit . '/interactive/' . $interactive;

        $connection = new Connection([
            'dsn' => 'pgsql:host=localhost;port=5432;dbname=mtsa',
            'username' => 'postgres',
            'password' => 'postgres',
        ]);
        $connection->open();
        
        $trans_order_memcache = Yii::$app->cache->get($trans_order_memcache_key);
        if ($trans_order_memcache !== false) {
            $featured_array = $trans_order_memcache;
        } else {
            $union_sql_array = [];

            $bind_keyword_array = [];

            // prioritized used flag and I assume all type of used flag has lot of adverts
            if ($classification_cd == '1166') {  //autoparts
                if (trim($keyword)) { // with keyword
                    
                    $keyword_temp_array = [];
                    $count_keyword = 0;
                    foreach (explode(" ", $keyword) as $keyword_loop) {
                        $count_keyword++;
                        
                        $keyword_temp_array[] = " make_name ILIKE :keyword_" . $count_keyword ;
                        $bind_keyword_array[':keyword_' . $count_keyword] = "%" . trim($keyword_loop) . "%";
                    }
                    
                    $union_sql_array[] = "( SELECT 1 as priority, * FROM trans_order WHERE classification_cd = '1166' AND premium_flg = '1' AND " . implode(" OR ", $keyword_temp_array) . " AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                    $union_sql_array[] = "( SELECT 2 as priority, * 
                                            FROM trans_order WHERE classification_cd = '1166' AND " . implode(" OR ", $keyword_temp_array) . " AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                }

                $union_sql_array[] = "( SELECT 3 as priority, * 
                                        FROM trans_order WHERE classification_cd = '1166' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                $union_sql_array[] = "( SELECT 4 as priority, * 
                                        FROM trans_order WHERE classification_cd = '1166' AND premium_flg = '1' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";


            } else if ($classification_cd == '1165') {  //numberplate

                if (trim($keyword)) { // with keyword
                    $bind_keyword_array[':keyword'] = "%" . trim($keyword) . "%";
                    
                    $bind_keyword_array[':keyword2'] = "%" . trim($keyword) . "%";
                    if (strlen($keyword) > 1) {
                        $bind_keyword_array[':keyword2'] = "%" . trim(substr($keyword, 0, 2)) . "%";
                    }

                    $union_sql_array[] = "( SELECT 1 as priority, * 
                                            FROM trans_order
                                            WHERE classification_cd = '1165' AND status = '40' AND premium_flg = '1' AND date_part('days', now() - upd_data_date) <= '30' AND ( special_no ilike :keyword OR special_no ilike :keyword2 ) AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                    $union_sql_array[] = "( SELECT 2 as priority, * 
                                            FROM trans_order
                                            WHERE classification_cd = '1165' AND status = '40' AND date_part('days', now() - upd_data_date) <= '30' AND ( special_no ilike :keyword OR special_no ilike :keyword2 ) AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                }

                $union_sql_array[] = "( SELECT 3 as priority, * 
                                        FROM trans_order
                                        WHERE classification_cd = '1165' AND status = '40' AND premium_flg = '1' AND date_part('days', now() - upd_data_date) <= '30' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                        ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                $union_sql_array[] = "( SELECT 4 as priority, * 
                                        FROM trans_order
                                        WHERE classification_cd = '1165' AND status = '40' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                        ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
            
            } else if ($classification_cd == '2493') {  //phone number

                if (trim($keyword)) { // with keyword
                    $bind_keyword_array[':keyword'] = "%" . trim($keyword) . "%";
                    
                    $bind_keyword_array[':keyword2'] = "%" . trim($keyword) . "%";
                    if (strlen($keyword) > 1) {
                        $bind_keyword_array[':keyword2'] = "%" . trim(substr($keyword, 0, 2)) . "%";
                    }

                    $union_sql_array[] = "( SELECT 1 as priority, * 
                                            FROM trans_order
                                            WHERE classification_cd = '2493' AND status = '40' AND premium_flg = '1' AND date_part('days', now() - upd_data_date) <= '30' AND ( special_no ilike :keyword OR special_no ilike :keyword2 ) AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                    $union_sql_array[] = "( SELECT 2 as priority, * 
                                            FROM trans_order
                                            WHERE classification_cd = '2493' AND status = '40' AND date_part('days', now() - upd_data_date) <= '30' AND ( special_no ilike :keyword OR special_no ilike :keyword2 ) AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                }

                $union_sql_array[] = "( SELECT 3 as priority, * 
                                        FROM trans_order
                                        WHERE classification_cd = '2493' AND status = '40' AND premium_flg = '1' AND date_part('days', now() - upd_data_date) <= '30' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                        ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                $union_sql_array[] = "( SELECT 4 as priority, * 
                                        FROM trans_order
                                        WHERE classification_cd = '2493' AND status = '40' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                        ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
            
            } else if ($classification_cd == '1167') {  //bike

                // prioritized used flag and I assume all type of used flag has lot of adverts
                switch ($used_flag) {
                    case 'used':
                                if (trim($keyword)) {
                                    $bind_keyword_array[':keyword'] = "%" . trim($keyword) . "%";

                                    $keyword_temp_array = array();
                                    // $keyword_temp_array[] = " dealer_name ILIKE :keyword" ;
                                    $keyword_temp_array[] = " make_name ILIKE :keyword" ;
                                    $keyword_temp_array[] = " model_name ILIKE :keyword" ;
                                    $keyword_temp_array[] = " variant_name ILIKE :keyword" ;
                                    // $keyword_temp_array[] = " qe_add_desc ILIKE :keyword" ;

                                    $union_sql_array[] = "( SELECT 1 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND status = '40' AND is_used_flg = '1' AND classification_cd = '1167' AND (" . implode(" OR ", $keyword_temp_array) . ") AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                                    $union_sql_array[] = "( SELECT 2 as priority, * 
                                                            FROM trans_order
                                                            WHERE is_used_flg = '1' AND status = '40' AND classification_cd = '1167' AND (" . implode(" OR ", $keyword_temp_array) . ") AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                }

                                if ($make !== false && trim($make) && $model !== false && trim($model)) {
                                    $union_sql_array[] = "( SELECT 3 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND status = '40' AND is_used_flg = '1' AND classification_cd = '1167' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                                    $union_sql_array[] = "( SELECT 4 as priority, * 
                                                            FROM trans_order
                                                            WHERE is_used_flg = '1' AND status = '40' AND classification_cd = '1167' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                }

                                if ($make !== false && trim($make)) {
                                    $union_sql_array[] = "( SELECT 5 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND status = '40' AND classification_cd = '1167' AND is_used_flg = '1' AND make_cd = '" . trim($make) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                                    $union_sql_array[] = "( SELECT 6 as priority, * 
                                                            FROM trans_order
                                                            WHERE is_used_flg = '1' AND status = '40' AND classification_cd = '1167' AND make_cd = '" . trim($make) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') 
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                }

                                $union_sql_array[] = "( SELECT 7 as priority, * 
                                                        FROM trans_order
                                                        WHERE premium_flg = '1' AND status = '40' AND classification_cd = '1167' AND is_used_flg = '1' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                        ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                                $union_sql_array[] = "( SELECT 8 as priority, * 
                                                        FROM trans_order
                                                        WHERE is_used_flg = '1' AND status = '40' AND classification_cd = '1167' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                        ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                        break;
                    case 'new':
                                if (trim($keyword)) {
                                    $bind_keyword_array[':keyword'] = "%" . trim($keyword) . "%";

                                    $keyword_temp_array = array();
                                    $keyword_temp_array[] = " make_name ILIKE :keyword" ;
                                    $keyword_temp_array[] = " model_name ILIKE :keyword" ;
                                    $keyword_temp_array[] = " variant_name ILIKE :keyword" ;
                                    // $keyword_temp_array[] = " dealer_name ILIKE :keyword" ;
                                    // $keyword_temp_array[] = " qe_add_desc ILIKE :keyword" ;

                                    $union_sql_array[] = "( SELECT 1 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND status = '40' AND is_used_flg = '2' AND classification_cd = '1167' AND (" . implode(" OR ", $keyword_temp_array) . ") AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                                    $union_sql_array[] = "( SELECT 2 as priority, * 
                                                            FROM trans_order
                                                            WHERE is_used_flg = '2' AND status = '40' AND classification_cd = '1167' AND (" . implode(" OR ", $keyword_temp_array) . ") AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                }

                                if ($make !== false && trim($make) && $model !== false && trim($model)) {
                                    $union_sql_array[] = "( SELECT 3 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND status = '40' AND is_used_flg = '2' AND classification_cd = '1167' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                                    $union_sql_array[] = "( SELECT 4 as priority, * 
                                                            FROM trans_order
                                                            WHERE is_used_flg = '2' AND status = '40' AND classification_cd = '1167' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                }

                                if ($make !== false && trim($make)) {
                                    $union_sql_array[] = "( SELECT 5 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND status = '40' AND classification_cd = '1167' AND is_used_flg = '2' AND make_cd = '" . trim($make) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                                    $union_sql_array[] = "( SELECT 6 as priority, * 
                                                            FROM trans_order
                                                            WHERE is_used_flg = '2' AND status = '40' AND classification_cd = '1167' AND make_cd = '" . trim($make) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                }

                                $union_sql_array[] = "( SELECT 7 as priority, * 
                                                        FROM trans_order
                                                        WHERE premium_flg = '1' AND status = '40' AND classification_cd = '1167' AND is_used_flg = '2' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                        ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                                $union_sql_array[] = "( SELECT 8 as priority, * 
                                                        FROM trans_order
                                                        WHERE is_used_flg = '2' AND status = '40' AND classification_cd = '1167' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                        ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                        break;
                    case 'recond':
                                if (trim($keyword)) {
                                    $bind_keyword_array[':keyword'] = "%" . trim($keyword) . "%";

                                    $keyword_temp_array = array();
                                    // $keyword_temp_array[] = " dealer_name ILIKE :keyword" ;
                                    $keyword_temp_array[] = " make_name ILIKE :keyword" ;
                                    $keyword_temp_array[] = " model_name ILIKE :keyword" ;
                                    $keyword_temp_array[] = " variant_name ILIKE :keyword" ;
                                    // $keyword_temp_array[] = " qe_add_desc ILIKE :keyword" ;

                                    $union_sql_array[] = "( SELECT 1 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND status = '40' AND classification_cd = '1167' AND is_used_flg = '3' AND (" . implode(" OR ", $keyword_temp_array) . ") AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') 
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                                    $union_sql_array[] = "( SELECT 2 as priority, * 
                                                            FROM trans_order
                                                            WHERE is_used_flg = '3' AND status = '40' AND classification_cd = '1167' AND (" . implode(" OR ", $keyword_temp_array) . ") AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                }

                                if ($make !== false && trim($make) && $model !== false && trim($model)) {
                                    $union_sql_array[] = "( SELECT 3 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND status = '40' AND classification_cd = '1167' AND is_used_flg = '3' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                                    $union_sql_array[] = "( SELECT 4 as priority, * 
                                                            FROM trans_order
                                                            WHERE is_used_flg = '3' AND status = '40' AND classification_cd = '1167' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                }

                                if ($make !== false && trim($make)) {
                                    $union_sql_array[] = "( SELECT 5 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND status = '40' AND classification_cd = '1167' AND is_used_flg = '3' AND make_cd = '" . trim($make) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                                    $union_sql_array[] = "( SELECT 6 as priority, * 
                                                            FROM trans_order
                                                            WHERE is_used_flg = '3' AND status = '40' AND classification_cd = '1167' AND make_cd = '" . trim($make) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                }

                                $union_sql_array[] = "( SELECT 7 as priority, * 
                                                        FROM trans_order
                                                        WHERE premium_flg = '1' AND status = '40' AND classification_cd = '1167' AND is_used_flg = '3' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                        ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                                $union_sql_array[] = "( SELECT 8 as priority, * 
                                                        FROM trans_order
                                                        WHERE is_used_flg = '3' AND status = '40' AND classification_cd = '1167' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                        ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                        break;
                    default:
                                if (trim($keyword)) {
                                    $bind_keyword_array[':keyword'] = "%" . trim($keyword) . "%";

                                    $keyword_temp_array = array();
                                    // $keyword_temp_array[] = " dealer_name ILIKE :keyword" ;
                                    $keyword_temp_array[] = " car_info ILIKE :keyword" ;
                                    $keyword_temp_array[] = " make_name ILIKE :keyword" ;
                                    $keyword_temp_array[] = " model_name ILIKE :keyword" ;
                                    $keyword_temp_array[] = " variant_name ILIKE :keyword" ;
                                    // $keyword_temp_array[] = " qe_add_desc ILIKE :keyword" ;

                                    $union_sql_array[] = "( SELECT 1 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND status = '40' AND classification_cd = '1167' AND (" . implode(" OR ", $keyword_temp_array) . ") AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                                    $union_sql_array[] = "( SELECT 2 as priority, * 
                                                            FROM trans_order
                                                            WHERE status = '40' AND classification_cd = '1167' AND (" . implode(" OR ", $keyword_temp_array) . ") AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                }

                                if ($make !== false && trim($make) && $model !== false && trim($model)) {
                                    $union_sql_array[] = "( SELECT 3 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND status = '40' AND classification_cd = '1167' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') 
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                                    $union_sql_array[] = "( SELECT 4 as priority, * 
                                                            FROM trans_order
                                                            WHERE make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' AND status = '40' AND classification_cd = '1167' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                                }

                                if ($make !== false && trim($make)) {
                                    $union_sql_array[] = "( SELECT 5 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND status = '40' AND classification_cd = '1167' AND make_cd = '" . trim($make) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                                    $union_sql_array[] = "( SELECT 6 as priority, * 
                                                            FROM trans_order
                                                            WHERE make_cd = '" . trim($make) . "' AND status = '40' AND classification_cd = '1167' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                                }

                                $union_sql_array[] = "( SELECT 7 as priority, * 
                                                        FROM trans_order
                                                        WHERE premium_flg = '1' AND status = '40' AND classification_cd = '1167' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                        ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                                $union_sql_array[] = "( SELECT 8 as priority, * 
                                                        FROM trans_order
                                                        WHERE  status = '40' AND classification_cd = '1167' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss')
                                                        ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                        break;
                }
            } else {
                switch ($used_flag) {
                    case 'used':
                        if (trim($keyword)) {
                            $bind_keyword_array[':keyword'] = "%" . trim($keyword) . "%";

                            $keyword_temp_array = [];
                            // $keyword_temp_array[] = " dealer_name ILIKE :keyword" ;
                            $keyword_temp_array[] = " make_name ILIKE :keyword" ;
                            $keyword_temp_array[] = " model_name ILIKE :keyword" ;
                            $keyword_temp_array[] = " variant_name ILIKE :keyword" ;
                            // $keyword_temp_array[] = " qe_add_desc ILIKE :keyword" ;

                            $union_sql_array[] = "( SELECT 1 as priority, * 
                                                    FROM trans_order
                                                    WHERE premium_flg = '1' AND status = '40' AND is_used_flg = '1' AND classification_cd = '1141' AND (" . implode(" OR ", $keyword_temp_array) . ") AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141' 
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                            $union_sql_array[] = "( SELECT 2 as priority, * 
                                                    FROM trans_order
                                                    WHERE is_used_flg = '1' AND status = '40' AND classification_cd = '1141' AND (" . implode(" OR ", $keyword_temp_array) . ") AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141' 
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                        }

                        if ($make !== false && trim($make) && $model !== false && trim($model)) {
                            $union_sql_array[] = "( SELECT 3 as priority, * 
                                                    FROM trans_order
                                                    WHERE premium_flg = '1' AND status = '40' AND is_used_flg = '1' AND classification_cd = '1141' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "'AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141' 
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                            $union_sql_array[] = "( SELECT 4 as priority, * 
                                                    FROM trans_order
                                                    WHERE is_used_flg = '1' AND status = '40 AND classification_cd = '1141'' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141' 
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                        }

                        if ($make !== false && trim($make)) {
                            $union_sql_array[] = "( SELECT 5 as priority, * 
                                                    FROM trans_order
                                                    WHERE premium_flg = '1' AND status = '40' AND is_used_flg = '1' AND classification_cd = '1141' AND make_cd = '" . trim($make) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141' 
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                            $union_sql_array[] = "( SELECT 6 as priority, * 
                                                    FROM trans_order
                                                    WHERE is_used_flg = '1' AND status = '40' AND classification_cd = '1141' AND make_cd = '" . trim($make) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141' 
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                        }

                        $union_sql_array[] = "( SELECT 7 as priority, * 
                                                        FROM trans_order
                                                        WHERE premium_flg = '1' AND status = '40' AND is_used_flg = '1' AND classification_cd = '1141' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141' 
                                                        ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                        $union_sql_array[] = "( SELECT 8 as priority, * 
                                                        FROM trans_order
                                                        WHERE is_used_flg = '1' AND status = '40' AND classification_cd = '1141' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141' 
                                                        ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                        break;
                    case 'new':
                        if (trim($keyword)) {
                            $bind_keyword_array[':keyword'] = "%" . trim($keyword) . "%";

                            $keyword_temp_array = [];
                            $keyword_temp_array[] = " make_name ILIKE :keyword" ;
                            $keyword_temp_array[] = " model_name ILIKE :keyword" ;
                            $keyword_temp_array[] = " variant_name ILIKE :keyword" ;
                            // $keyword_temp_array[] = " dealer_name ILIKE :keyword" ;
                            // $keyword_temp_array[] = " qe_add_desc ILIKE :keyword" ;

                            $union_sql_array[] = "( SELECT 1 as priority, * 
                                                    FROM trans_order
                                                    WHERE premium_flg = '1' AND status = '40' AND is_used_flg = '2' AND (" . implode(" OR ", $keyword_temp_array) . ") AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                            $union_sql_array[] = "( SELECT 2 as priority, * 
                                                    FROM trans_order
                                                    WHERE is_used_flg = '2' AND status = '40' AND (" . implode(" OR ", $keyword_temp_array) . ") AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                        }

                        if ($make !== false && trim($make) && $model !== false && trim($model)) {
                            $union_sql_array[] = "( SELECT 3 as priority, * 
                                                    FROM trans_order
                                                    WHERE premium_flg = '1' AND status = '40' AND is_used_flg = '2' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                            $union_sql_array[] = "( SELECT 4 as priority, * 
                                                    FROM trans_order
                                                    WHERE is_used_flg = '2' AND status = '40' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                        }

                        if ($make !== false && trim($make)) {
                            $union_sql_array[] = "( SELECT 5 as priority, * 
                                                    FROM trans_order
                                                    WHERE premium_flg = '1' AND status = '40' AND is_used_flg = '2' AND make_cd = '" . trim($make) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                            $union_sql_array[] = "( SELECT 6 as priority, * 
                                                    FROM trans_order
                                                    WHERE is_used_flg = '2' AND status = '40' AND make_cd = '" . trim($make) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                        }

                        $union_sql_array[] = "( SELECT 7 as priority, * 
                                                        FROM trans_order
                                                        WHERE premium_flg = '1' AND status = '40' AND is_used_flg = '2' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                        ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                        $union_sql_array[] = "( SELECT 8 as priority, * 
                                                        FROM trans_order
                                                        WHERE is_used_flg = '2' AND status = '40' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                        ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                        break;
                    case 'recond':
                        if (trim($keyword)) {
                            $bind_keyword_array[':keyword'] = "%" . trim($keyword) . "%";

                            $keyword_temp_array = [];
                            // $keyword_temp_array[] = " dealer_name ILIKE :keyword" ;
                            $keyword_temp_array[] = " make_name ILIKE :keyword" ;
                            $keyword_temp_array[] = " model_name ILIKE :keyword" ;
                            $keyword_temp_array[] = " variant_name ILIKE :keyword" ;
                            // $keyword_temp_array[] = " qe_add_desc ILIKE :keyword" ;

                            $union_sql_array[] = "( SELECT 1 as priority, * 
                                                    FROM trans_order
                                                    WHERE premium_flg = '1' AND status = '40' AND is_used_flg = '3' AND (" . implode(" OR ", $keyword_temp_array) . ") AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                            $union_sql_array[] = "( SELECT 2 as priority, * 
                                                    FROM trans_order
                                                    WHERE is_used_flg = '3' AND status = '40' AND (" . implode(" OR ", $keyword_temp_array) . ") AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                        }

                        if ($make !== false && trim($make) && $model !== false && trim($model)) {
                            $union_sql_array[] = "( SELECT 3 as priority, * 
                                                    FROM trans_order
                                                    WHERE premium_flg = '1' AND status = '40' AND is_used_flg = '3' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                            $union_sql_array[] = "( SELECT 4 as priority, * 
                                                    FROM trans_order
                                                    WHERE is_used_flg = '3' AND status = '40' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                        }

                        if ($make !== false && trim($make)) {
                            $union_sql_array[] = "( SELECT 5 as priority, * 
                                                    FROM trans_order
                                                    WHERE premium_flg = '1' AND status = '40' AND is_used_flg = '3' AND make_cd = '" . trim($make) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                            $union_sql_array[] = "( SELECT 6 as priority, * 
                                                    FROM trans_order
                                                    WHERE is_used_flg = '3' AND status = '40' AND make_cd = '" . trim($make) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                        }

                        $union_sql_array[] = "( SELECT 7 as priority, * 
                                                        FROM trans_order
                                                        WHERE premium_flg = '1' AND status = '40' AND is_used_flg = '3' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                        ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                        $union_sql_array[] = "( SELECT 8 as priority, * 
                                                        FROM trans_order
                                                        WHERE is_used_flg = '3' AND status = '40' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                        ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                        break;
                    case 'commercial':
                        if (trim($keyword)) {
                            $bind_keyword_array[':keyword'] = "%" . trim($keyword) . "%";

                            $keyword_temp_array = [];
                            // $keyword_temp_array[] = " dealer_name ILIKE :keyword" ;
                            $keyword_temp_array[] = " make_name ILIKE :keyword" ;
                            $keyword_temp_array[] = " model_name ILIKE :keyword" ;
                            $keyword_temp_array[] = " variant_name ILIKE :keyword" ;
                            // $keyword_temp_array[] = " qe_add_desc ILIKE :keyword" ;

                            $union_sql_array[] = "( SELECT 1 as priority, * 
                                                    FROM trans_order
                                                    WHERE premium_flg = '1' AND status = '40' AND bodytype_code = 'bt9' AND (" . implode(" OR ", $keyword_temp_array) . ") AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";


                            $union_sql_array[] = "( SELECT 2 as priority, * 
                                                    FROM trans_order
                                                    WHERE bodytype_code = 'bt9' AND status = '40' AND (" . implode(" OR ", $keyword_temp_array) . ") AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                        }

                        if ($make !== false && trim($make) && $model !== false && trim($model)) {
                                    $union_sql_array[] = "( SELECT 3 as priority, * 
                                                            FROM trans_order
                                                            WHERE premium_flg = '1' AND status = '40' AND bodytype_code = 'bt9' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                                    $union_sql_array[] = "( SELECT 4 as priority, * 
                                                            FROM trans_order
                                                            WHERE bodytype_code = 'bt9' AND status = '40' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                            ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                        }

                        if ($make !== false && trim($make)) {
                            $union_sql_array[] = "( SELECT 5 as priority, * 
                                                    FROM trans_order
                                                    WHERE premium_flg = '1' AND status = '40' AND bodytype_code = 'bt9' AND make_cd = '" . trim($make) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                            $union_sql_array[] = "( SELECT 6 as priority, * 
                                                    FROM trans_order
                                                    WHERE bodytype_code = 'bt9' AND status = '40' AND make_cd = '" . trim($make) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                        }

                        $union_sql_array[] = "( SELECT 7 as priority, * 
                                                        FROM trans_order
                                                        WHERE premium_flg = '1' AND status = '40' AND bodytype_code = 'bt9' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                        ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                        $union_sql_array[] = "( SELECT 8 as priority, * 
                                                        FROM trans_order
                                                        WHERE bodytype_code = 'bt9' AND status = '40' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141'
                                                        ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                        break;
                    default:
                        if (trim($keyword)) {
                            $bind_keyword_array[':keyword'] = "%" . trim($keyword) . "%";

                            $keyword_temp_array = [];
                            // $keyword_temp_array[] = " dealer_name ILIKE :keyword" ;
                            $keyword_temp_array[] = " make_name ILIKE :keyword" ;
                            $keyword_temp_array[] = " model_name ILIKE :keyword" ;
                            $keyword_temp_array[] = " variant_name ILIKE :keyword" ;
                            // $keyword_temp_array[] = " qe_add_desc ILIKE :keyword" ;

                            $union_sql_array[] = "( SELECT 1 as priority, * 
                                                    FROM trans_order
                                                    WHERE premium_flg = '1' AND status = '40' AND (" . implode(" OR ", $keyword_temp_array) . ") AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141' 
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                            $union_sql_array[] = "( SELECT 2 as priority, * 
                                                    FROM trans_order
                                                    WHERE status = '40' AND (" . implode(" OR ", $keyword_temp_array) . ") AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141' 
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                        }

                        if ($make !== false && trim($make) && $model !== false && trim($model)) {
                            $union_sql_array[] = "( SELECT 3 as priority, * 
                                                    FROM trans_order
                                                    WHERE premium_flg = '1' AND status = '40' AND make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141' 
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                            $union_sql_array[] = "( SELECT 4 as priority, * 
                                                    FROM trans_order
                                                    WHERE make_cd = '" . trim($make) . "' AND model_cd = '" . trim($model) . "' AND status = '40' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141' 
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                        }

                        if ($make !== false && trim($make)) {
                            $union_sql_array[] = "( SELECT 5 as priority, * 
                                                    FROM trans_order
                                                    WHERE premium_flg = '1' AND make_cd = '" . trim($make) . "' AND status = '40' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141' 
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                            $union_sql_array[] = "( SELECT 6 as priority, * 
                                                    FROM trans_order
                                                    WHERE make_cd = '" . trim($make) . "'AND status = '40' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141' 
                                                    ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                        }

                        $union_sql_array[] = "( SELECT 7 as priority, * 
                                                FROM trans_order
                                                WHERE premium_flg = '1' AND status = '40' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141' 
                                                ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";

                        $union_sql_array[] = "( SELECT 8 as priority, * 
                                                        FROM trans_order
                                                        WHERE status = '40' AND to_char(upd_data_date,'yyyymmddhh24miss') >= to_char(now() + interval '-1 year','yyyymmddhh24miss') AND classification_cd = '1141' 
                                                        ORDER BY RANDOM() FETCH FIRST " . $limit . " ROWS ONLY )";
                        break;
                }
            }
            
            $trans_order_model_command = $connection->createCommand("SELECT * FROM (" . implode(" UNION ", $union_sql_array) . ") as class ORDER BY priority FETCH FIRST " . $limit . " ROWS ONLY");

            if (count($bind_keyword_array)) { // with keyword
                foreach ($bind_keyword_array as $bind_keyword_key_loop => $bind_keyword_value_loop) {
                    $trans_order_model_command->bindParam($bind_keyword_key_loop, $bind_keyword_value_loop);
                }
            }

            $trans_order_model = $trans_order_model_command->queryAll();
            
            foreach ($trans_order_model as $trans_order_data_loop) {
                if ($classification_cd == '1167') {
                    $seourl = Yii::$app->general->generateseo('bike', $trans_order_data_loop['urn_no']);
                    
                } else if($classification_cd == '1166') {
                    if ($interactive) {
                        $seourl = Yii::$app->general->generateseo('dealerautopart', $trans_order_data_loop['urn_no'], $trans_order_data_loop['make_name'] . $trans_order_data_loop['model_name']);
                    } else {
                        $seourl = Yii::$app->general->generateseo('autopart', $trans_order_data_loop['urn_no'], $trans_order_data_loop['make_name'] . $trans_order_data_loop['model_name']);
                    }
                } else if($classification_cd == '1165') {
                    if ($interactive) {
                        $seourl = Yii::$app->general->generateseo('dealernumberplate', $trans_order_data_loop['urn_no'], $trans_order_data_loop['make_name'] . $trans_order_data_loop['special_no']);
                    } else {
                        $seourl = Yii::$app->general->generateseo('numberplate', $trans_order_data_loop['urn_no'], $trans_order_data_loop['special_no']);
                    }
                } else if($classification_cd == '2493') {
                    if ($interactive) {
                        $seourl = Yii::$app->general->generateseo('dealerspecialnumber', $trans_order_data_loop['urn_no'], $trans_order_data_loop['make_name'] . $trans_order_data_loop['special_no']);
                    } else {
                        $seourl = Yii::$app->general->generateseo('specialnumber', $trans_order_data_loop['urn_no'], $trans_order_data_loop['special_no']);
                    }
                } else {
                    if ($interactive) {
                        $seourl = Yii::$app->general->generateseo('dealercar', $trans_order_data_loop['urn_no'], $trans_order_data_loop['make_name'] . $trans_order_data_loop['model_name']);
                    } else {
                        $seourl = Yii::$app->general->generateseo('car', $trans_order_data_loop['urn_no'], $trans_order_data_loop['make_name'] . ' ' . $trans_order_data_loop['model_name']);
                    }
                }
                //process each item here
                $featured_array[] = [   'seourl' => $seourl,
                                        'urn_no' => $trans_order_data_loop['urn_no'],
                                        'make_name' => $trans_order_data_loop['make_name'],
                                        'model_name' => $trans_order_data_loop['model_name'],
                                        'year_make' => $trans_order_data_loop['year_make'],
                                        'location_name' => $trans_order_data_loop['location_name'],
                                        'bodytype_code' => $trans_order_data_loop['bodytype_code'],
                                        'special_no' => $trans_order_data_loop['special_no'],
                                        'image' => Yii::$app->general->advertimageurl($trans_order_data_loop['urn_no'] . '.jpg', 'wmfull', strtotime($trans_order_data_loop['upd_data_date'])),
                                        'price' => $trans_order_data_loop['price'],
                                        'poa' => ($trans_order_data_loop['price'] == 0 ? 1 : $trans_order_data_loop['poa_flg']),
                                        'mileage_name' => $trans_order_data_loop['mileage_name'],
                                        'transmission_name' => $trans_order_data_loop['transmission_name'],
                                        'is_used_flg' => $trans_order_data_loop['is_used_flg'],
                                        'dealer_id' => $trans_order_data_loop['dealer_id']
                                    ];

            }

            Yii::$app->cache->set($trans_order_memcache_key, $featured_array, 180);
        }

        return $featured_array;
    }

    public function displaysuitableapcmm($make_cd, $model_cd) {
        $displaysuitableap = [];
        $displaysuitableap2 = [];
        $response_array = [];
        $advert_image = '';

        $trans_order_extra_info = new TransOrderExtraInfo();

        $trans_order_autopart_suitable_memcache_key = "/trans_order/autopartsuitable/" . $make_cd . "/" . $model_cd;
        $trans_order_autopart_suitable_memcache = Yii::$app->cache->get($trans_order_autopart_suitable_memcache_key);
        if ($trans_order_autopart_suitable_memcache !== false) {
            $response_array = $trans_order_autopart_suitable_memcache;
        } else {
            
            $only_selected_car_make_model = $trans_order_extra_info->find()->where(['trans_order_extra_key' => 'only_selected_car_make_model'])->all();
            if (isset($only_selected_car_make_model)) {
                foreach ($only_selected_car_make_model as $only_selected_car_make_model_loop) {
                    $only_selected_car_make_model_list = [];
                    $only_selected_car_make_model_list = json_decode($only_selected_car_make_model_loop->trans_order_extra_value);
                    foreach ($only_selected_car_make_model_list as $only_selected_car_make_model_key_loop => $only_selected_car_make_model_data_loop) {
                        if (($only_selected_car_make_model_key_loop == $make_cd) && ($only_selected_car_make_model_data_loop == $model_cd)) {
                            $displaysuitableap[] = $only_selected_car_make_model_loop->urn_no;
                        }
                    }
                }
            }

            $only_selected_car_make = $trans_order_extra_info->find()->where(['trans_order_extra_key' => 'only_selected_car_make'])->all();
            if (isset($only_selected_car_make)) {
                foreach ($only_selected_car_make as $only_selected_car_make_loop) {
                    $only_selected_car_make_list = [];
                    $only_selected_car_make_list = json_decode($only_selected_car_make_loop->trans_order_extra_value);
                    foreach ($only_selected_car_make_list as $only_selected_car_make_key_loop => $only_selected_car_make_data_loop) {
                        if ($only_selected_car_make_data_loop == $make_cd) {
                            $displaysuitableap[] = $only_selected_car_make_loop->urn_no;
                        }
                    }
                }
            }
            
            $any_car_make = $trans_order_extra_info->find()->where(['trans_order_extra_key' =>  'any_car_make'])->all();
            if (isset($any_car_make)) {
                foreach ($any_car_make as $any_car_make_loop) {
                    $displaysuitableap[] = $any_car_make_loop->urn_no;
                }
            }
            
            $displaysuitableap2 = array_slice($displaysuitableap, 0, 6);

            if (isset($displaysuitableap2)) {
                foreach ($displaysuitableap2 as $displaysuitableap2_loop) {
                    $trans_order_model = $this->find()->where(['urn_no' =>  $displaysuitableap2_loop])->one();
                    if (isset($trans_order_model)) {
                        $advert_image = Yii::$app->general->advertimageurl($trans_order_model->urn_no . '.jpg', 'wmfull', strtotime($trans_order_model->upd_data_date));
                        
                        $seo_url = Yii::$app->general->generateseo('autopart', $trans_order_model->urn_no, $trans_order_model->make_name);
                    
                        $response_array[] = [  'urn_no' => $trans_order_model->urn_no,
                                                'image' => $advert_image,
                                                'seourl' => $seo_url,
                                                'year_make' => $trans_order_model->year_make,
                                                'make_name' => $trans_order_model->make_name,
                                                'model_name' => $trans_order_model->model_name,
                                                'price' => $trans_order_model->price,
                                                'poa' => ($trans_order_model->price == 0 ? 1 : $trans_order_model->poa_flg),
                                                'location_name' => $trans_order_model->location_name,
                                            ];
                    }
                }
                Yii::$app->cache->set($trans_order_autopart_suitable_memcache_key, $response_array, 600);
            }
        }
        return $response_array;
    }

    public function advcompare($compare_array) {
        $trans_order_criteria = ['and', 'urn_no', $compare_array];
        
        $compare_total = $this->find()->where($trans_order_criteria)->count();

        // $trans_order_criteria->order = "upd_data_date ASC";
        // $trans_order_criteria->limit = 4;   
        
        $trans_order_model = $this->find()->where($trans_order_criteria)->orderBy("upd_data_date ASC")->limit(4)->all();
        
        if ($trans_order_model) {
            return $trans_order_model;
        }
        return false;
    }

    public function loadinfo($urn) {
        $response_array = false;

        $trans_order_memcache_key = "/trans_order/info/urn/" . $urn;
        $trans_order_memcache = Yii::$app->cache->get($trans_order_memcache_key);
        if ($trans_order_memcache !== false) {
            $response_array = $trans_order_memcache;
        } else {
            $trans_order_model = $this->find->where(['urn_no' => $urn])->one();
            if ($trans_order_model) {
                $advert_image = Yii::$app->general->advertimageurl($trans_order_model->urn_no . '.jpg', 'wmfull', strtotime($trans_order_model->upd_data_date));
                
                if ($trans_order_model->classification_cd == '1167') {
                    $seourl = Yii::$app->general->generateseo('bike', $trans_order_model->urn_no, $trans_order_model->make_name . ' ' . $trans_order_model->model_name);
                } else {
                    $seourl = Yii::$app->general->generateseo('car', $trans_order_model->urn_no, $trans_order_model->make_name . ' ' . $trans_order_model->model_name);
                    if ($trans_order_model->advert_category == '2') { 
                        $seourl = Yii::$app->general->generateseo('autopart', $trans_order_model->urn_no, $trans_order_model->make_name . ' ' . $trans_order_model->model_name);
                    } else if ($trans_order_model->advert_category == '3') { 
                        $seourl = Yii::$app->general->generateseo('numberplate', $trans_order_model->urn_no, $trans_order_model->make_name . ' ' . $trans_order_model->model_name);
                    }
                }
                
                if ($trans_order_model->poa_flg == 1 || $trans_order_model->price == 0) {
                    $tmp_price = 'POA';
                } else {
                    $tmp_price = 'RM '. number_format($trans_order_model->price);
                }

                $response_array = ['urn_no' => $trans_order_model->urn_no,
                                    'title' => $trans_order_model->make_name . ' ' . $trans_order_model->model_name,
                                    'image' => $advert_image,
                                    'seourl' => $seourl,
                                    'price' => $tmp_price,
                                    'special_no' => $trans_order_model->special_no,
                                    'classification_cd' => $trans_order_model->classification_cd
                                    ];
            }
            
            Yii::$app->cache->set($trans_order_memcache_key, $response_array, 300);
        }

        return $response_array;
    }

    public function getpopularmodel($make_cd, $limit=30) {
        $popular_car_model_advert_array = [];
        
        $trans_order_memcache_key = "/trans_order/popular/make_cd/" . $make_cd;
        $trans_order_memcache = Yii::$app->cache->get($trans_order_memcache_key);
        if ($trans_order_memcache !== false) {
            $popular_car_model_advert_array = $trans_order_memcache;
        } else {
            $trans_order_criteria = $this->find()->select("model_cd, COUNT(urn_no) AS total");
            $trans_order_criteria->where("make_cd = :make_cd AND model_cd IS NOT NULL AND status = '40' AND upd_data_date >= :from_date");
            $trans_order_criteria->params([':make_cd' => (string)$make_cd, ':from_date' => date("Y-m-d", strtotime("- 1 year"))]);
            $trans_order_criteria->orderBy("total DESC ");
            $trans_order_criteria->groupBy("model_cd");
            $trans_order_criteria->limit($limit);
            
            $trans_order_model = $trans_order_criteria->all();
            
            
            if (count($trans_order_model)) {
                foreach ($trans_order_model as $trans_order_data_loop) {
                    $popular_car_model_advert_array[$trans_order_data_loop->model_cd] = $trans_order_data_loop->urn_no;
                }
            }

            Yii::$app->cache->set($trans_order_memcache_key, $popular_car_model_advert_array, 300);
        }

        return $popular_car_model_advert_array;
    }
}
