<script>
jQuery(document).ready(function() {
    jQuery( "#frm-login" ).submit(function( event ) {
      event.preventDefault();

      jQuery.ajax({
          type: 'post',
          url: '<?php echo Yii::$app->urlManager->createAbsoluteUrl('ajax/login')?>',
          dataType: 'json',
          data: jQuery('#frm-login').serialize(),
          timeout: 120000,
          beforeSend : function(){
            //
          },
          success: function(data) {
            if (data.result == '1') {
              window.location.href = '<?php echo Yii::$app->urlManager->createAbsoluteUrl('user') ?>';
            } else {
              mtalert(data.message);
            }
          }
      });
  });

  jQuery( "#frm-register" ).submit(function( event ) {
        event.preventDefault();

        if (jQuery("#register-agree:checked").length) {
          jQuery.ajax({
              type: 'post',
              url: '<?php echo Yii::$app->urlManager->createAbsoluteUrl('ajax/register')?>',
              dataType: 'json',
              data: jQuery('#frm-register').serialize(),
              timeout: 120000,
              beforeSend : function(){
                //
              },
              success: function(data) {
                mtalert(data.message);
                jQuery("#alert-primary .btn-primary").click(function() {
                  if (data.result == '1') {
                    window.location.href = '<?php echo Yii::$app->urlManager->createAbsoluteUrl('user') ?>';
                  }
                });
              }
          });
        } else {
          mtalert("Please read and agree our terms and conditions");
        }
    });
});
</script>