<?php
	add_theme_support( 'menus' );
	add_theme_support( 'nav-menus' );

	register_nav_menus( array(
	    'primary' => __( 'Primary menu', 'mt-wordpress' ),
	) );

	function custom_theme_assets() {
	wp_enqueue_style( 'style', get_stylesheet_uri() );
}
 
	add_action( 'wp_enqueue_scripts', 'custom_theme_assets' );
?>