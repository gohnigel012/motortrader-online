<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trans_order_extra_info".
 *
 * @property string $urn_no
 * @property string $trans_order_extra_key
 * @property string|null $trans_order_extra_value
 */
class TransOrderExtraInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trans_order_extra_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['urn_no', 'trans_order_extra_key'], 'required'],
            [['trans_order_extra_value'], 'string'],
            [['urn_no'], 'string', 'max' => 20],
            [['trans_order_extra_key'], 'string', 'max' => 64],
            [['urn_no', 'trans_order_extra_key'], 'unique', 'targetAttribute' => ['urn_no', 'trans_order_extra_key']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'urn_no' => 'Urn No',
            'trans_order_extra_key' => 'Trans Order Extra Key',
            'trans_order_extra_value' => 'Trans Order Extra Value',
        ];
    }

    public function getvalue($urn, $key) {
        $value = false;

        $trans_advert_extra_info_memcache_key = "/trans_advert_extra_info/getvalue/urn/" . $urn . '/key/' . $key;
        $trans_advert_extra_info_memcache = Yii::$app->cache->get($trans_advert_extra_info_memcache_key);
        if ($trans_advert_extra_info_memcache !== false) {
            $value = $trans_advert_extra_info_memcache;
        } else {
            $trans_advert_extra_info_model = $this->find()->where(['trans_order_extra_key' => $key, 'urn_no' => $urn])->one();
            if ($trans_advert_extra_info_model && trim($trans_advert_extra_info_model->trans_order_extra_value)) {
                $value = $trans_advert_extra_info_model->trans_order_extra_value;
                Yii::$app->cache->set($trans_advert_extra_info_memcache_key, $value, 240);
            }
        }

        return $value;
    }

    public function getadvertextrainfo($urn) {
        $extra_info_array = [];

        $trans_order_extra_info_get_memcache_key = "/trans_order_extra_info/getadvertextrainfo/urn/" . $urn;
        $trans_order_extra_info_get_memcache = Yii::$app->cache->get($trans_order_extra_info_get_memcache_key);
        if ($trans_order_extra_info_get_memcache !== false) {
            $value = $trans_order_extra_info_get_memcache;
        } else {
            
            $trans_order_extra_info_model = $this->find()->where(['urn_no' => $urn])->all();
            if (count($trans_order_extra_info_model)) {
                foreach ($trans_order_extra_info_model as $trans_order_extra_info_data_loop) {
                    $extra_info_array[$trans_order_extra_info_data_loop->trans_order_extra_key] = $trans_order_extra_info_data_loop->trans_order_extra_value;
                }
            }
            Yii::$app->cache->set($trans_order_extra_info_get_memcache_key, $extra_info_array, 60);
        }

        return $extra_info_array;
    }

    public function getanycarmake($urn) {
        $value = false;

        $trans_order_extra_info_get_memcache_key = "/trans_order_extra_info/getanycarmake/urn/" . $urn;
        $trans_order_extra_info_get_memcache = Yii::$app->cache->get($trans_order_extra_info_get_memcache_key);
        if ($trans_order_extra_info_get_memcache !== false) {
            $value = $trans_order_extra_info_get_memcache;
        } else {
            
            $trans_order_extra_info_model = $this->find()->where(['urn_no' => $urn, 'trans_order_extra_key' => 'any_car_make'])->one();
            $value = $trans_order_extra_info_model;

            Yii::$app->cache->set($trans_order_extra_info_get_memcache_key, $value, 240);
        }

        return $value;
    }

    public function getonlyselectedcarmakemodel($urn) {
        $value = false;

        $trans_order_extra_info_get_memcache_key = "/trans_order_extra_info/getonlyselectedcarmakemodel/urn/" . $urn;
        $trans_order_extra_info_get_memcache = Yii::$app->cache->get($trans_order_extra_info_get_memcache_key);
        if ($trans_order_extra_info_get_memcache !== false) {
            $value = $trans_order_extra_info_get_memcache;
        } else {
            
            $trans_order_extra_info_model = $this->find()->where(['urn_no' => $urn, 'trans_order_extra_key' => 'only_selected_car_make_model'])->one();
            $value = $trans_order_extra_info_model;

            Yii::$app->cache->set($trans_order_extra_info_get_memcache_key, $value, 240);
        }

        return $value;
    }

    public function getonlyselectedcarmake($urn) {
        $value = false;

        $trans_order_extra_info_get_memcache_key = "/trans_order_extra_info/getonlyselectedcarmake/urn/" . $urn;
        $trans_order_extra_info_get_memcache = Yii::$app->cache->get($trans_order_extra_info_get_memcache_key);
        if ($trans_order_extra_info_get_memcache !== false) {
            $value = $trans_order_extra_info_get_memcache;
        } else {
            
            $trans_order_extra_info_model = $this->find()->where(['urn_no' => $urn, 'trans_order_extra_key' => 'only_selected_car_make'])->one();
            $value = $trans_order_extra_info_model;
            
            Yii::$app->cache->set($trans_order_extra_info_get_memcache_key, $value, 240);
        }

        return $value;
    }


}
