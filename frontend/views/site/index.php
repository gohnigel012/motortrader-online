<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = 'Motor Trader Homepage';
?>
<div class="page-home">
    <div id="home-banner">
        <div class="banner__show">
            <p><a href="/site/faq"><img src="/images/almera.png" id="banner"/></a></p>
            <p><a href="" class="banner__link">Discover the new Almera 2020</a></p>
        </div>
        <div id="banner-search">
            <h1 class="text-center banner-search__title banner-search__title--search-position">Find your future car</h1>
            <div class="tab banner-search__icon text-center">
                <button class="tablinks banner-search__icon-box active" id="icon-box-car">
                    <span>Cars</span>
                </button>
                <button class="tablinks banner-search__icon-box" id="icon-box-auto-parts">
                    <span>Auto Parts</span>
                </button>
                <button class="tablinks banner-search__icon-box" id="icon-box-number-plates">
                    <span>Number Plates</span>
                </button>
                <button class="tablinks banner-search__icon-box" id="icon-box-motorcycle">
                    <span>Motorcycle</span>
                </button>
            </div>
            <form name="form-home-banner" id="form-search-car" action="/car" method="get">
                <div id="car-search"> 
                    <div class="banner-search__selection banner-search__selection--style">
                        <select name="make" class="form-control" id="make-home-popup">
                            <option value="">All brands</option>
<?php
    $make_array = $master_car_make->getmake();
    $popular_array = [];
    $popular_array['1010'] = 'TOYOTA';
    $popular_array['1020'] = 'HONDA';
    $popular_array['9009'] = 'PERODUA';
    $popular_array['2100'] = 'PROTON';
    $popular_array['1015'] = 'NISSAN';
    $popular_array['2025'] = 'MERCEDES-BENZ';
    $popular_array['2015'] = 'BMW';
    if (count($popular_array)) {
?>
                            <optgroup label="------ Popular Make ------">
<?php
        foreach ($popular_array as $popular_make_id_loop => $popular_make_data_loop) {
            $display_number = $trans_statistic_count->displaycount('car_make', $popular_make_id_loop);
            if ($display_number !== false) {
?>
                                <option value="<?php echo $popular_make_id_loop; ?>"><?php echo $popular_make_data_loop; ?> <?php echo $display_number; ?></option>
<?php
            }
        }
?>
                            </optgroup>
<?php
    }
    
?>
                            <optgroup label="--------------------------">
<?php
    foreach ($make_array as $car_make_value_loop => $car_make_loop) {
        $display_number = $trans_statistic_count->displaycount('car_make', $car_make_value_loop);
        if ($display_number !== false) {
?>
                                <option value="<?php echo $car_make_value_loop ?>"><?php echo $car_make_loop ?> <?php echo $display_number ?></option>
<?php
        }
    }       
?>
                           </optgroup>                         
                        </select>
                    </div>
                    <div class="banner-search__selection banner-search__selection--style">
                        <select name="model" class="form-control" id="model-home-popup">
                            <option value="">All models</option>
<?php
    if (isset($_REQUEST['make']) && trim($_REQUEST['make'])) {
        $model_array = $master_car_make->getmodelbymake($_REQUEST['make']);
        $model_group_array = $master_car_make_group->getmodelgroup($_REQUEST['make']);
        $car_model_array = [];
        $car_model_group_array = [];
        $display_number_model_group = 0;

        if (count($model_array)) {
            foreach ($model_array as $car_model_value_loop => $car_model_data_loop){
                $display_number = $trans_statistic_count->displaycount('car_model', $car_model_value_loop);
                $is_model_group = false;
                if ($display_number !== false) {
                    if (count($model_group_array)) {
                        foreach ($model_group_array as $model_group_array_loop => $model_group_array_data_loop) {
                            foreach ($model_group_array_data_loop['model_group'] as $model_group_key_loop => $model_group_data_loop) {
                                if ($car_model_value_loop == $model_group_data_loop) {
                                    $is_model_group = true;
                                }
                            }
                        }
                        
                        if ($is_model_group == false) {
                            $car_model_array[$car_model_value_loop] = $car_model_data_loop;
                        }
                    } else {
                        $car_model_array[$car_model_value_loop] = $car_model_data_loop;
                    }
                }
            }
        }

        if (count($model_group_array)) {
            $count = 0;
            foreach ($model_group_array as $model_group_array_loop => $model_group_array_data_loop) {
                $count++;
                $group_name = $model_group_array_data_loop['group_name'];
                $model_group_list = $model_group_array_data_loop['model_group_list'];
                $display_number_model_group = 0;
                foreach ($model_group_array_data_loop['model_group'] as $model_group_key_loop => $model_group_data_loop) {
                    if (count($model_array)) {
                        foreach ($model_array as $car_model_value_loop => $car_model_data_loop) {
                            $display_number = $trans_statistic_count->displaycount('car_model', $car_model_value_loop);
                            if ($display_number !== false) {
                                if ($car_model_value_loop == $model_group_data_loop) {
                                    $display_number = str_replace(array('(', ')'),"",$display_number);
                                    $display_number_model_group += $display_number;
                                    $is_model_group = true;
                                }
                            }
                        }
                    }
                }
                
                if ($is_model_group == true && $display_number_model_group != 0) {
                    $car_model_group_array[$model_group_list] = $group_name . ' ' . '(' . $display_number_model_group . ')';
                }
            }
        }
        if (count($car_model_group_array) === 0) {
                
        } else {
?>
                            <optgroup label="------ Model Group ------">
                            </optgroup>
<?php               
        foreach ($car_model_group_array as $car_model_group_key_loop => $car_model_group_data_loop) {           
?>
                                <option value="<?php echo $car_model_group_key_loop ?>" <?php echo (isset($_REQUEST['model']) && $car_model_group_key_loop == $_REQUEST['model'] ? 'selected' : '') ?>><?php echo $car_model_group_data_loop ?> (<?php echo $display_number_model_group ?>)</option>                                            
<?php                                               
            }
?>
                            <optgroup label="--------------------------">
                            </optgroup>
<?php                   
        }                   
        
        foreach ($car_model_array as $car_model_array_value_loop => $car_model_array_data_loop){
            $display_number = $trans_statistic_count->displaycount('car_model', $car_model_array_value_loop);
            if ($display_number !== false) {
?>  
                                <option value="<?php echo $car_model_array_value_loop ?>" <?php echo (isset($_REQUEST['model']) && $car_model_array_value_loop == $_REQUEST['model'] ? 'selected' : '') ?>><?php echo $car_model_array_data_loop ?> <?php echo $display_number ?></option>
<?php                       
            }
        }                   
    }       
?>                                           
                        </select>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <div class="banner-search__selection">
                                <select name="minPrice" class="form-control" id="min-price">
                                    <option value="">Min. price</option>
<?php
    $price_range_array = Yii::$app->general->filterPriceRange();
    $desc_price_range_array = Yii::$app->general->filterPriceRange(false);
    $default_min_price = (isset($_REQUEST['minPrice']) && is_numeric($_REQUEST['minPrice']) ? $_REQUEST['minPrice'] : '' );
    $default_max_price = (isset($_REQUEST['maxPrice']) && is_numeric($_REQUEST['maxPrice']) ? $_REQUEST['maxPrice'] : '' );
    foreach ($price_range_array as $price_range_value_loop => $price_range_label_loop) {
?>
                                    <option value="<?php echo $price_range_value_loop; ?>" <?php echo ($default_min_price == $price_range_value_loop ? 'selected': ''); ?>><?php echo $price_range_label_loop; ?></option>
<?php
    }
?>
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="banner-search__selection">
                                <select name="maxPrice" class="form-control make-home-popup" id="max-price">
                                    <option value="">Max. price</option>
<?php
    foreach ($desc_price_range_array as $price_range_value_loop => $price_range_label_loop) {
?>
                                    <option value="<?php echo $price_range_value_loop; ?>" <?php echo ($default_max_price == $price_range_value_loop ? 'selected': ''); ?>><?php echo $price_range_label_loop; ?></option>
<?php
    }
?>                                
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="banner-search__selection">
                        <p class="text-center"><button class="btn btn-default banner-search__search-btn "type="submit"><span class="banner-search__search-btn-text">Search</span></button></p>
                        <p class="text-center"><a class="banner-search__icon-text" href="">Advanced Search</a></p>
                    </div>
                </div>
            </form>
            <div id="auto-parts-search" class="d-none banner-search__selection">
                <form name="frm-home-banner" id="frm-search-autoparts" action="/autopart" method="get">
                    <p><input type="text" name="keyword" id="home-keyword-num" class="form-control banner-search__text" placeholder="eg. Auto Gear Box" value="<?php echo (isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '') ?>" autocomplete="off"></p>
                    <p class="text-center banner-search__selection"><button class="btn btn-default banner-search__search-btn" type="submit"><span class="banner-search__search-btn-text">Search</span></button></p>
                </form>
            </div>
            <div id="number-plates-search" class="d-none banner-search__selection">
                <form name="frm-home-banner" id="frm-search-numberplates" action="/numberplate" method="get">
                    <p><input type="text" name="keyword" id="
                        " class="form-control banner-search__text" placeholder="eg. Wa 123" value="<?php echo (isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '') ?>" autocomplete="off" spellcheck="false" dir="auto"></p>
                    <p class="text-center banner-search__selection"><button class="btn btn-default banner-search__search-btn" type="submit"><span class="banner-search__search-btn-text">Search</span></button></p>
                </form>
            </div>
            <div id="motorcycle-search" class="d-none banner-search__selection">
                 <form name="frm-home-banner" id="frm-search-motorcycle" action="/bike" method="get">
                    <p><input type="text" name="keyword" id="home-keyword-bike" class="form-control banner-search__text" placeholder="eg. Yamaha" value="<?php echo (isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '') ?>" autocomplete="off"></p>
                    <p class="text-center banner-search__selection"><button class="btn btn-default banner-search__search-btn" type="submit"><span class="banner-search__search-btn-text">Search</span></button></p>
                </form>
            </div>
        </div>
    </div>
    <div id="browser-by-car-make__section">
        <h1 class="text-center banner-search__title banner-search__title--browse-position">Pick your preferred</h1>
        <div class="tab browser-by-car-make__icon">
            <button class="tablinks browser-by-car-make__icon-box active" id="icon-box-brands">
                <span>Brands</span>
            </button>
            <button class="tablinks browser-by-car-make__icon-box" id="icon-box-type">
                <span>Type</span>
            </button>
        </div>
        <div class="browser-by-car-make__list" id="list-brands">
            <div class="browser-by-car-make__row">
                <div class="row">
                    <div class="col-4 col-lg-2">
                        <div class="browser-by-car-make__col browser-by-car-make__logo browser-by-car-make__logo--margin">
                            <a href="car?make=1010">
                                <img src="images/toyotalogo.png" class="img-car-make__logo" alt="Toyota">
                            </a>
                        </div>
                    </div>
                    <div class="col-4 col-lg-2">
                        <div class="browser-by-car-make__col browser-by-car-make__logo browser-by-car-make__logo--margin">
                            <a href="car?make=1020">
                                <img src="images/hondalogo.png" class="img-car-make__logo" alt="Honda">
                            </a>
                        </div>
                    </div>
                    <div class="col-4 col-lg-2">
                        <div class="browser-by-car-make__col browser-by-car-make__logo browser-by-car-make__logo--margin">
                            <a href="car?make=1020">
                                <img src="images/perodualogo.png" class="img-car-make__logo" alt="Perodua">
                            </a>
                        </div>
                    </div>
                    <div class="col-4 col-lg-2">
                        <div class="browser-by-car-make__col browser-by-car-make__logo browser-by-car-make__logo--margin">
                            <a href="car?make=9009">
                                <img src="images/protonlogo.png" class="img-car-make__logo" alt="Category-lorry-icon">
                            </a>
                        </div>
                    </div>
                    <div class="col-4 col-lg-2">
                        <div class="browser-by-car-make__col browser-by-car-make__logo browser-by-car-make__logo--margin">
                            <a href="car?make=2025">
                                <img src="images/mercedeslogo.png" class="img-car-make__logo" alt="Category-car-icon">
                            </a>
                        </div>
                    </div>
                    <div class="col-4 col-lg-2">
                        <div class=" browser-by-car-make__col browser-by-car-make__logo browser-by-car-make__logo--margin">
                            <a href="car?make=2015">
                                <img src="images/bmwlogo.png" class="img-car-make__logo" alt="Category-car-icon">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="browser-by-car-make__row">
                <div class="row">
                    <div class="col-4 col-lg-2">
                        <div class="browser-by-car-make__col browser-by-car-make__logo browser-by-car-make__logo--margin">
                            <a href="car?make=1015">
                                <img src="images/nissanlogo.png" class="img-car-make__logo" alt="Nissan">
                            </a>
                        </div>
                    </div>
                    <div class="col-4 col-lg-2">
                        <div class="browser-by-car-make__col browser-by-car-make__logo browser-by-car-make__logo--margin">
                            <a href="car?make=2035">
                            <img src="/images/volkswagenlogo.png" class="img-car-make__logo" alt="Volkswagen">
                            </a>
                        </div>
                    </div>
                    <div class="col-4 col-lg-2">
                        <div class="browser-by-car-make__col browser-by-car-make__logo browser-by-car-make__logo--margin">
                            <a href="car?make=1025">
                            <img src="images/mazdalogo.png" class="img-car-make__logo" alt="Mazda">
                            </a>
                        </div>
                    </div>
                    <div class="col-4 col-lg-2">
                        <div class="browser-by-car-make__col browser-by-car-make__logo browser-by-car-make__logo--margin">
                            <a href="car?make=2010">
                                <img src="images/audilogo.png" class="img-car-make__logo" alt="Audi">
                            </a>
                        </div>
                    </div>
                    <div class="col-4 col-lg-2">
                        <div class="browser-by-car-make__col browser-by-car-make__logo browser-by-car-make__logo--margin">
                            <a href="car?make=6020">
                            <img src="images/hyundailogo.png" class="img-car-make__logo" alt="Hyundai">
                            </a>
                        </div>
                    </div>
                    <div class="col-4 col-lg-2">
                        <div class="browser-by-car-make__col browser-by-car-make__logo browser-by-car-make__logo--margin">
                            <a href="car?make=6010">
                                <img src="images/kialogo.png" class="img-car-make__logo" alt="Kia">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-center">
                <a class="browser-by-car-make__more superdeals-featured-cnabadv__link" id="show-more-cars">
                View all 71 brands
                </a>
            </p>
            <div class="browser-by-car-make__all d-none" id="all-shown-cars">
<?php
    $make_array = $master_car_make->getmake();
    if (count($make_array)) {
?>
                <ul class="browser-by-car-make__array-row-list">
<?php
        foreach ($make_array as $make_array_id_loop => $make_array_data_loop) {
            $display_number = $trans_statistic_count->displaycount('car_make', $make_array_id_loop);
            if ($display_number !== false) {
?>
                    <li><a href="<?php echo Yii::$app->urlManager->createAbsoluteUrl(['car', 'make' => $make_array_id_loop]) ?>"><?php echo $make_array_data_loop; ?> <?php echo $display_number; ?></a></li>
<?php
            }
        }
?>
                </ul>
<?php
    }
?>
            </div>
        </div>
        <div class="browser-by-car-make__list d-none" id="list-type">
            <p class="text-center">
                <a class="browser-by-car-make__more superdeals-featured-cnabadv__link" href="">
                    View all 71 types
                </a>
            </p>
        </div>
    </div>
<?php
    if (isset($pdata['superdeals_featured_cnabadv_list']) && count($pdata['superdeals_featured_cnabadv_list'])) {   
        foreach ($pdata['superdeals_featured_cnabadv_list'] as $superdeals_featured_cnabadv_list_key_loop => $superdeals_featured_cnabadv_list_data_loop) {
        $count = count($superdeals_featured_cnabadv_list_data_loop);
            if (isset($superdeals_featured_cnabadv_list_data_loop) && count($superdeals_featured_cnabadv_list_data_loop) && $superdeals_featured_cnabadv_list_data_loop != '') {    
?>              
    <div class="superdeals-featured-cnabadv superdeals-featured-cnabadv--background-color">
        <div class="superdeals-featured-cnabadv__box">
            <h2 class="text-center banner-search__title banner-search__title--superdeals-position"><?php echo $superdeals_featured_cnabadv_list_key_loop ?></h2>
            <div class="row">
<?php
            $count_advert = 0;
                foreach ($superdeals_featured_cnabadv_list_data_loop as $sd_feat_cnabadv_list_data_loop) {
                    $count_advert++;
                    $trans_advert_super_deals_model = $super_deals->getsuperdeals($sd_feat_cnabadv_list_data_loop['urn_no']);
                    $has_super_deals = $trans_advert_super_deals_model['got_super_deals'];
                    if (isset($sd_feat_cnabadv_list_data_loop['is_superdeals']) && trim($sd_feat_cnabadv_list_data_loop['is_superdeals']) && $sd_feat_cnabadv_list_data_loop['is_superdeals'] == '1') {
                        if ($sd_feat_cnabadv_list_data_loop['sale_price'] == '0') {
                            $price =  'POA';
                        } else {
                            $price = '<span class="price-super-deals">RM ' . number_format($sd_feat_cnabadv_list_data_loop['sale_price']) . '</span>';

                            $deposit = 10;
                            $interest = 3.69;
                            if ($trans_advert_data_loop->is_used_flg == '2') {      
                                $term = 9;
                            } else {
                                $term = 7;
                            }

                            $interestRate = $interest / 100;    
                            $oriPrice = str_replace(",","", $sd_feat_cnabadv_list_data_loop['sale_price']);
                            $depositAmount = $oriPrice * ($deposit / 100);
                            $loanPaidAmount = $oriPrice - $depositAmount; 
                            $interestPaidAmount = ($term * $interestRate) * $loanPaidAmount;                            
                            $month = ($interestPaidAmount + $loanPaidAmount) / ($term * 12);
                            $month = number_format(round($month));
                            if ($sd_feat_cnabadv_list_data_loop['sale_price'] != $sd_feat_cnabadv_list_data_loop['regular_price']) {
                                $price .= '<br><span class="price-before-super-deals">RM ' .number_format($sd_feat_cnabadv_list_data_loop['regular_price']) . '</span>';
                            }
                        }
                    } else {
                        if ($sd_feat_cnabadv_list_data_loop['poa'] == 1) {
                            $price = 'POA';
                        } else {
                            $price = '<small class="featured-ads-section__price-currency">RM</small> ' . number_format($sd_feat_cnabadv_list_data_loop['price']);

                            $deposit = 10;
                            $interest = 3.69;
                            if ($sd_feat_cnabadv_list_data_loop['is_used_flg'] == '2') {      
                                $term = 9;
                            } else {
                                $term = 7;
                            }
                            $interestRate = $interest / 100;    
                            $oriPrice = str_replace(",","", number_format($sd_feat_cnabadv_list_data_loop['price']));
                            $depositAmount = $oriPrice * ($deposit / 100);
                            $loanPaidAmount = $oriPrice - $depositAmount; 
                            $interestPaidAmount = ($term * $interestRate) * $loanPaidAmount;                            
                            $month = ($interestPaidAmount + $loanPaidAmount) / ($term * 12);
                            $month = number_format(round($month));
                        }
                    }
?>                        
                <div class="col-lg-4">
                    <div class="superdeals-featured-cnabadv__featured-ads-section-listing">
                        <p class="text-center"><a href="car/view?urn_no=<?php echo $sd_feat_cnabadv_list_data_loop['urn_no']; ?>&car_name=<?php echo $sd_feat_cnabadv_list_data_loop['make_name'] . '-' . $sd_feat_cnabadv_list_data_loop['model_name']; ?>"><img class="featured-ads-section__img lazyload" src="/images/ajax-loader.gif" data-src="<?php echo (@getimagesize($sd_feat_cnabadv_list_data_loop['image'])) ? $sd_feat_cnabadv_list_data_loop['image'] : '/images/mercedes.png'; ?>" alt="<?php echo $sd_feat_cnabadv_list_data_loop['make_name'] . ' ' . $sd_feat_cnabadv_list_data_loop['model_name']; ?>"></a></p>
                        <p class="featured-ads-section__title"><a href="car/view?urn_no=<?php echo $sd_feat_cnabadv_list_data_loop['urn_no']; ?>&car_name=<?php echo $sd_feat_cnabadv_list_data_loop['make_name'] . '-' . $sd_feat_cnabadv_list_data_loop['model_name']; ?>"><?php echo $sd_feat_cnabadv_list_data_loop['make_name'] . ' ' . $sd_feat_cnabadv_list_data_loop['model_name']; ?></a></p>
                        <div class="row">
                            <div class="col-4">
                                <p class="featured-ads-section__month">RM <?php echo $month; ?>/month</p>
                            </div>
                            <div class="col-8 text-right">
                                <p class="featured-ads-section__price"><?php echo $price; ?></p>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-6">
                                <div class="row no-gutters">
                                    <div class="col-6">
                                        <p><img class="featured-ads-section__icon" src="images/calendar.png" alt="calendar" /></p>
                                    </div>
                                    <div class="col-6">
                                        <span class="featured-ads-section__text">Year</span>
                                        <p class="featured-ads-section__desc"><?php echo $sd_feat_cnabadv_list_data_loop['year_make']; ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="row no-gutters">
                                    <div class="col-6">
                                        <p><img class="featured-ads-section__icon" src="images/location.png" alt="location" /></p>
                                    </div>
                                    <div class="col-6">
                                        <span class="featured-ads-section__text">Location</span>
                                        <p class="featured-ads-section__desc"><?php echo $sd_feat_cnabadv_list_data_loop['location_name']; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="row no-gutters">
                                    <div class="col-6">
                                        <p><img class="featured-ads-section__icon" src="images/mileage.png" alt="mileage" /></p>
                                    </div>
                                    <div class="col-6">
                                        <span class="featured-ads-section__text">Mileage</span>
                                        <p class="featured-ads-section__desc"><?php echo $sd_feat_cnabadv_list_data_loop['mileage_name']; ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="row no-gutters">
                                    <div class="col-6">
                                        <p><img class="featured-ads-section__icon" src="images/transmission.png" alt="location" /></p>
                                    </div>
                                    <div class="col-6">
                                        <span class="featured-ads-section__text">Transmission</span>
                                        <p class="featured-ads-section__desc"><?php echo $sd_feat_cnabadv_list_data_loop['transmission_name']; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<?php
                }
?>                          
            </div>
 <?php
                if (count($pdata['superdeals_featured_cnabadv_list']) > 3) {
 ?>           
            <p class="text-center featured-ads-section__page"><img src="/images/previous.png" alt="Previous"> <span>1/2</span> <img src="/images/next.png" alt="Next"></p>
        
<?php
                }
?>
        </div>
    </div>
<?php
            }
        }
    }
?>
    <div class="superdeals-featured-cnabadv">
        <h2 class="text-center banner-search__title banner-search__title--insights-position">Motor Trader Insights</h2>
        <div class="superdeals-featured-cnabadv__insight">
            <div class="row">
                <div class="col-lg-3">
                    <div class="superdeals-featured-cnabadv__insights">
<?php
    if (isset($news_array)) {
        foreach ($news_array['local'] as $local_loop) {
?>
                        <p><a href="<?php echo $local_loop['link']; ?>" title=""><img src="<?php echo $local_loop['thumbnail']; ?>" alt="" class="img-fluid superdeals-featured-cnabadv__image"></a></p>
                        <p><button class="btn btn-default superdeals-featured-cnabadv__insights-btn">Local News</button></p>
                        <p><a href="<?php echo $local_loop['link']; ?>" title="<?php echo $local_loop['title']; ?>"><?php echo $local_loop['title']; ?></a></p>
                        <p class="superdeals-featured-cnabadv__insights-desc"><?php echo $local_loop['desc']; ?></p>
                        <p class="superdeals-featured-cnabadv__insights-date"><?php echo $local_loop['date']; ?></p>
<?php 
        }
    }
?>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="superdeals-featured-cnabadv__insights">
<?php
    if (isset($news_array)) {
        foreach ($news_array['highlights'] as $highlights_loop) {
?>
                        <p><a href="<?php echo $highlights_loop['link']; ?>" title="<?php echo $highlights_loop['title']; ?>"><img src="<?php echo $highlights_loop['thumbnail']; ?>" alt="<?php echo $highlights_loop['title']; ?>" class="img-fluid superdeals-featured-cnabadv__image"></a></p>
                        <p><button class="btn btn-default superdeals-featured-cnabadv__insights-btn">Highlights</button></p>
                        <p><a href="<?php echo $highlights_loop['link']; ?>" title="<?php echo $highlights_loop['title']; ?>"><?php echo $highlights_loop['title']; ?></a></p>
                        <p class="superdeals-featured-cnabadv__insights-date"><?php echo $highlights_loop['date']; ?></p>
<?php 
        }
    }
?>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="superdeals-featured-cnabadv__insights">
<?php
    if (isset($news_array)) {
        foreach($news_array['motor_trader_promotion'] as $promotions_loop) {
?>
                        <p><a href="<?php echo $promotions_loop['link']; ?>" title="<?php echo $promotions_loop['title']; ?>"><img src="<?php echo $promotions_loop['thumbnail']; ?>" alt="<?php echo $promotions_loop['title']; ?>" class="img-fluid superdeals-featured-cnabadv__image"></a></p>
                        <p><button class="btn btn-default superdeals-featured-cnabadv__insights-btn">Promotion</button></p>
                        <p><a href="<?php echo $promotions_loop['link']; ?>" title="<?php echo $promotions_loop['title']; ?>"><?php echo $promotions_loop['title']; ?></a></p>
                        <p class="superdeals-featured-cnabadv__insights-desc"><?php echo $promotions_loop['desc']; ?></p>
                        <p class="superdeals-featured-cnabadv__insights-date"><?php echo $promotions_loop['date']; ?></p>
<?php 
        }
    }
?>                        
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="superdeals-featured-cnabadv__insights">
<?php
    if (isset($news_array)) {
        foreach ($news_array['jobs_opening'] as $jobs_opening_loop){
?>
                        <p><a href="<?php echo $jobs_opening_loop['link']; ?>" title="<?php echo $jobs_opening_loop['title']; ?>"><img src="<?php echo $jobs_opening_loop['thumbnail']; ?>" alt="<?php echo $jobs_opening_loop['title']; ?>" class="img-fluid superdeals-featured-cnabadv__image"></a></p>
                        <p><button class="btn btn-default superdeals-featured-cnabadv__insights-btn">Job Opening</button></p>
                        <p><a href="<?php echo $jobs_opening_loop['link']; ?>" title="<?php echo $jobs_opening_loop['title']; ?>"><?php echo $jobs_opening_loop['title']; ?></a></p>
                        <p class="superdeals-featured-cnabadv__insights-desc"><?php echo $jobs_opening_loop['desc']; ?></p>
                        <p class="superdeals-featured-cnabadv__insights-date"><?php echo $jobs_opening_loop['date']; ?></p>
<?php 
        }
    }
?>
                    </div>
                </div>
            </div>
        </div>
        <p class="text-center superdeals-featured-cnabadv__paragraph"><a href="https://news.motortrader.com.my/" class="superdeals-featured-cnabadv__link">View all insights</a></p>
    </div>
</div>
